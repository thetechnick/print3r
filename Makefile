export CGO_ENABLED:=0

VERSION=$(shell ./scripts/git-version)
BRANCH=$(shell git rev-parse --abbrev-ref HEAD)
BUILD_DATE=$(shell date +%s)
REPO=github.com/thetechnick/print3r
LD_FLAGS="-w -X $(REPO)/pkg/version.Version=$(VERSION) -X $(REPO)/pkg/version.Branch=$(BRANCH) -X $(REPO)/pkg/version.BuildDate=$(BUILD_DATE)"

all: build

build: bin/print3r-server bin/dummy-printer bin/print3rctl

bin/%: ui/assets_vfsdata.go
	@go build -o bin/$* -v -ldflags $(LD_FLAGS) $(REPO)/cmd/$*

test:
	@./scripts/test

codegen: tools
	@./scripts/codegen

tools: bin/protoc bin/protoc-gen-go bin/protoc-gen-grpc-gateway

ui: ui/assets_vfsdata.go

ui/assets_vfsdata.go: ui/build
	go generate ./ui

ui/build:
	cd ui && webdev build

bin/protoc:
	@./scripts/get-protoc

bin/protoc-gen-go:
	@go build -o bin/protoc-gen-go $(REPO)/vendor/github.com/golang/protobuf/protoc-gen-go

bin/protoc-gen-grpc-gateway:
	@go build -o bin/protoc-gen-grpc-gateway $(REPO)/vendor/github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway

cleanui:
	@rm -rf ui/build
	@rm -rf ui/assets_vfsdata.go

cleango:
	@rm -rf bin/*/
	@rm -rf bin/dummy-printer bin/print3r-server bin/print3rctl

clean: cleanui cleango

clean-release:
	@rm -rf _output

release: \
	clean \
	cleanui \
	clean-release \
	_output/print3r-linux-amd64.tar.gz \
	_output/print3r-linux-arm.tar.gz \
	_output/print3r-linux-arm64.tar.gz \
	_output/print3r-darwin-amd64.tar.gz

bin/linux-amd64/print3r-server: GOARGS = GOOS=linux GOARCH=amd64
bin/linux-arm6/print3r-server: GOARGS = GOOS=linux GOARCH=arm GOARM=6
bin/linux-arm7/print3r-server: GOARGS = GOOS=linux GOARCH=arm GOARM=7
bin/darwin-amd64/print3r-server: GOARGS = GOOS=darwin GOARCH=amd64

bin/linux-amd64/dummy-printer: GOARGS = GOOS=linux GOARCH=amd64
bin/linux-arm6/dummy-printer: GOARGS = GOOS=linux GOARCH=arm GOARM=6
bin/linux-arm7/dummy-printer: GOARGS = GOOS=linux GOARCH=arm GOARM=7
bin/darwin-amd64/dummy-printer: GOARGS = GOOS=darwin GOARCH=amd64

bin/linux-amd64/print3rctl: GOARGS = GOOS=linux GOARCH=amd64
bin/linux-arm6/print3rctl: GOARGS = GOOS=linux GOARCH=arm GOARM=6
bin/linux-arm7/print3rctl: GOARGS = GOOS=linux GOARCH=arm GOARM=7
bin/darwin-amd64/print3rctl: GOARGS = GOOS=darwin GOARCH=amd64

bin/%/print3r-server: ui/assets_vfsdata.go
	$(GOARGS) go build -o $@ -ldflags $(LD_FLAGS) -a $(REPO)/cmd/print3r-server

bin/%/dummy-printer:
	$(GOARGS) go build -o $@ -ldflags $(LD_FLAGS) -a $(REPO)/cmd/dummy-printer

bin/%/print3rctl:
	$(GOARGS) go build -o $@ -ldflags $(LD_FLAGS) -a $(REPO)/cmd/print3rctl

_output/print3r-%.tar.gz: NAME=print3r-$(VERSION)-$*
_output/print3r-%.tar.gz: DEST=_output/$(NAME)
_output/print3r-%.tar.gz: bin/%/print3r
	mkdir -p $(DEST)
	cp bin/$*/print3r $(DEST)
	./scripts/release-files $(DEST)
	tar zcvf $(DEST).tar.gz -C _output $(NAME)

.PHONY: all build test release codegen tools ui clean cleanui cleango
.SECONDARY: _output/print3r-linux-amd64
