package main

import (
	"bufio"
	"fmt"
	"io"
	"math/rand"
	"os"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/pflag"
	"github.com/tarm/serial"
)

var flags = pflag.NewFlagSet("weblicator", pflag.ContinueOnError)

func init() {
	flags.String("device", "", "serial device")
	flags.String("wait", "0s", "wait between commands")
}

func main() {
	rand.Seed(time.Now().UnixNano())
	if err := flags.Parse(os.Args[1:]); err != nil {
		log.WithError(err).Fatal("parsing flags")
	}
	device, err := flags.GetString("device")
	if device == "" || err != nil {
		log.WithError(err).Fatal("missing device")
	}
	waitString, err := flags.GetString("wait")
	if err != nil {
		log.WithError(err).Fatal("error parsing wait")
	}
	wait, err := time.ParseDuration(waitString)
	if err != nil {
		log.WithError(err).Fatal("error parsing wait")
	}

	port, err := serial.OpenPort(&serial.Config{
		Name: device,
		Baud: 115200,
	})
	if err != nil {
		log.WithError(err).Fatal("open port")
		return
	}
	defer port.Close()

	read := make(chan string)
	write := make(chan string)
	closeCh := make(chan interface{})
	go func() {
		r := bufio.NewReader(port)
		for {
			line, _, err := r.ReadLine()
			if err == io.EOF {
				close(read)
				close(write)
				close(closeCh)
				return
			}
			read <- string(line)
			time.Sleep(wait)

			l := string(line)

			if strings.Contains(l, "M190") || strings.Contains(l, "M109") {
				time.Sleep(10 * time.Second)
				write <- "ok\n"
				continue
			}
			if strings.Contains(l, "M105") {
				write <- fmt.Sprintf("ok T:%.1f /0.0 B:%.1f /0.0 T0:%.1f /0.0 @:0 B@:0\n", randTemp(), randTemp(), randTemp())
				continue
			}

			write <- "ok\n"
		}
	}()

	for {
		select {
		case line := <-read:
			fmt.Println("Rec: ", line)
		case line := <-write:
			fmt.Println("Sen: ", line)
			port.Write([]byte(line))
		case <-closeCh:
			return
		}
	}
}

func randTemp() float64 {
	min := 20.0
	max := 30.0
	return min + rand.Float64()*(max-min)
}
