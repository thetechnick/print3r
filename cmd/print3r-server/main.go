package main

import (
	"io/ioutil"
	"os"
	"os/signal"
	"strings"
	"sync"

	"github.com/boltdb/bolt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	v1services "github.com/thetechnick/print3r/pkg/api/v1/services"
	"github.com/thetechnick/print3r/pkg/print"
	"github.com/thetechnick/print3r/pkg/printer"
	"github.com/thetechnick/print3r/pkg/server"
	"github.com/thetechnick/print3r/pkg/storage"
	boltdb "github.com/thetechnick/print3r/pkg/storage/bolt"
	"github.com/thetechnick/print3r/pkg/storage/inmem"
	"github.com/thetechnick/print3r/pkg/version"
	"github.com/thetechnick/print3r/pkg/video"
	"github.com/thetechnick/print3r/pkg/volume"
)

var flags = pflag.NewFlagSet("print3r", pflag.ContinueOnError)

func init() {
	flags.String("log-level", "info", "log level")
	flags.String("address", "0.0.0.0:8080", "server secure address")
	flags.String("insecure-http-address", "0.0.0.0:8081", "server insecure http address")
	flags.String("insecure-grpc-address", "0.0.0.0:8082", "server insecure grpc address")
	flags.String("cert", "server.crt", "server certificate")
	flags.String("key", "server.key", "server key")
	flags.String("db", "print3r.db", "database file")
}

func main() {
	log.SetOutput(os.Stderr)
	if err := flags.Parse(os.Args[1:]); err != nil {
		log.WithError(err).Fatal("parsing flags")
	}

	viper.BindPFlags(flags)
	viper.SetEnvPrefix("print3r")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))

	log.SetLevel(log.DebugLevel)

	log.Infof("version: %s", version.Version)
	certPEM, err := ioutil.ReadFile(viper.GetString("cert"))
	if err != nil {
		log.WithError(err).Fatal("loading certificate")
	}
	keyPEM, err := ioutil.ReadFile(viper.GetString("key"))
	if err != nil {
		log.WithError(err).Fatal("loading key")
	}

	boltDB, err := bolt.Open(viper.GetString("db"), 0600, nil)
	if err != nil {
		log.WithError(err).Fatal("open database")
	}
	defer boltDB.Close()

	eventHub := storage.NewEventHub()
	go eventHub.Run()

	volumeStorage, err := boltdb.NewVolumeStorage(boltDB, eventHub)
	if err != nil {
		log.WithError(err).Fatal("create volume storage")
	}
	printerStorage, err := boltdb.NewPrinterStorage(boltDB, eventHub)
	if err != nil {
		log.WithError(err).Fatal("create printer storage")
	}
	printStorage, err := boltdb.NewPrintStorage(boltDB, eventHub)
	if err != nil {
		log.WithError(err).Fatal("create print storage")
	}
	videoStreamStorage, err := boltdb.NewVideoStreamStorage(boltDB, eventHub)
	if err != nil {
		log.WithError(err).Fatal("create video stream storage")
	}

	volumeService := volume.NewService(
		volumeStorage,
	)

	volumeController := volume.NewController(
		volumeStorage, volumeService, eventHub,
	)
	go func() {
		log.WithError(volumeController.Run()).Fatal("error starting volume controller")
	}()

	connectionManager := printer.NewConnectionManager(printerStorage)
	printerController := printer.NewController(
		printerStorage, printStorage, connectionManager, eventHub)
	go func() {
		log.WithError(printerController.Run()).Fatal("error starting printer controller")
	}()

	printService := print.NewService(
		printerStorage, printStorage, volumeService,
		volumeStorage, connectionManager)

	printController := print.NewController(
		printService, printStorage, eventHub,
	)
	go func() {
		fatal(printController.Run(), "error starting print controller")
	}()

	videoStreamController := video.NewController(
		videoStreamStorage, eventHub,
	)
	go func() {
		fatal(videoStreamController.Run(), "error starting video stream controller")
	}()

	videoStreamEndpoint := v1services.NewVideoStreamStreamEndpoint(videoStreamController, videoStreamStorage)

	s, err := server.NewServer(
		&server.Config{
			Address:             viper.GetString("address"),
			InsecureHTTPAddress: viper.GetString("insecure-http-address"),
			InsecureGRPCAddress: viper.GetString("insecure-grpc-address"),
			CertPEM:             certPEM,
			KeyPEM:              keyPEM,
		},
		volumeService,
		v1services.NewVersionServiceServer(),
		v1services.NewVolumeServiceServer(
			volumeStorage,
			inmem.NewVolumeLimiter(),
			inmem.NewVolumeFileLimiter(),
			volumeService),
		v1services.NewResourceEventServiceServer(eventHub),
		v1services.NewPrinterServiceServer(
			printerStorage,
			inmem.NewPrinterLimiter(),
			connectionManager,
		),
		v1services.NewPrintServiceServer(printStorage, inmem.NewPrintLimiter()),
		v1services.NewVideoStreamServiceServer(videoStreamStorage, inmem.NewVideoStreamLimiter()),
		videoStreamEndpoint,
	)
	if err != nil {
		log.WithError(err).Fatal("creating server")
	}
	go func() {
		fatal(s.Run(), "starting server")
	}()

	var w sync.WaitGroup
	signalCh := make(chan os.Signal, 1)
	signal.Notify(signalCh, os.Interrupt)

	w.Add(1)
	go func() {
		<-signalCh
		log.Info("shutting down video")
		videoStreamController.Close()
		w.Done()
	}()

	w.Wait()
}

func fatal(err error, msg string) {
	if err != nil {
		log.WithError(err).Fatal(msg)
	}
}
