package main

import (
	"github.com/thetechnick/print3r/pkg/print3rctl"
	"github.com/thetechnick/print3r/pkg/print3rctl/cmd"
)

func main() {
	f := print3rctl.NewFactory()
	ctl := cmd.NewPrint3rCtlCommand(f)
	ctl.Execute()
}
