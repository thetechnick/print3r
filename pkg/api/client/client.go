package client

import (
	"github.com/thetechnick/print3r/pkg/api/v1"
	"google.golang.org/grpc"
)

type Client interface {
	Print() Print
	Printer() Printer
	Version() Version
	ResourceEvent() ResourceEvent
}

type Opt func(c *client)

type client struct {
	conn          *grpc.ClientConn
	print         Print
	printer       Printer
	version       Version
	resourceEvent ResourceEvent
}

func NewClient(conn *grpc.ClientConn, opts ...Opt) Client {
	c := &client{conn: conn}
	for _, opt := range opts {
		opt(c)
	}
	c.print = &print{v1.NewPrintServiceClient(c.conn)}
	c.printer = &printer{v1.NewPrinterServiceClient(c.conn)}
	c.version = &version{v1.NewVersionServiceClient(c.conn)}
	c.resourceEvent = &resourceEvent{v1.NewResourceEventServiceClient(c.conn)}
	return c
}

func (c *client) ResourceEvent() ResourceEvent {
	return c.resourceEvent
}

func (c *client) Printer() Printer {
	return c.printer
}

func (c *client) Print() Print {
	return c.print
}

func (c *client) Version() Version {
	return c.version
}
