package client

import (
	"context"
	"fmt"
	"time"

	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/v1"
)

type Print interface {
	List(ctx context.Context, opts api.PrintListRequest) (list *api.PrintList, raw *v1.PrintList, err error)
	Cancel(ctx context.Context, print *api.Print) (updated *api.Print, raw *v1.Print, err error)
	Create(ctx context.Context, print *api.Print) (out *api.Print, raw *v1.Print, err error)
	Delete(ctx context.Context, print *api.Print) (deleted *api.Print, raw *v1.Print, err error)
	Update(ctx context.Context, print *api.Print) (updated *api.Print, raw *v1.Print, err error)
	UpdateStatus(ctx context.Context, print *api.Print) (updated *api.Print, raw *v1.Print, err error)
}

type print struct {
	c v1.PrintServiceClient
}

func (p *print) List(ctx context.Context, req api.PrintListRequest) (list *api.PrintList, raw *v1.PrintList, err error) {
	v1Req := &v1.PrintListRequest{}
	if err = v1.Scheme.Convert(&req, v1Req); err != nil {
		return
	}
	if raw, err = p.c.List(ctx, v1Req); err != nil {
		return
	}

	list = &api.PrintList{}
	if err = v1.Scheme.Convert(raw, list); err != nil {
		return
	}
	return
}

func (p *print) Show(ctx context.Context, name string) (print *api.Print, raw *v1.Print, err error) {
	if raw, err = p.c.Show(ctx, &v1.Print{
		Metadata: &v1.ObjectMeta{
			Name: name,
		},
	}); err != nil {
		return
	}
	print = &api.Print{}
	if err = v1.Scheme.Convert(raw, print); err != nil {
		return
	}
	return
}

func (p *print) Update(ctx context.Context, print *api.Print) (updated *api.Print, raw *v1.Print, err error) {
	in := &v1.Print{}
	if err = v1.Scheme.Convert(print, in); err != nil {
		return
	}

	if raw, err = p.c.Update(ctx, in); err != nil {
		return
	}

	updated = &api.Print{}
	if err = v1.Scheme.Convert(raw, updated); err != nil {
		return
	}
	return
}

func (p *print) UpdateStatus(ctx context.Context, print *api.Print) (updated *api.Print, raw *v1.Print, err error) {
	in := &v1.Print{}
	if err = v1.Scheme.Convert(print, in); err != nil {
		return
	}

	if raw, err = p.c.UpdateStatus(ctx, in); err != nil {
		return
	}

	updated = &api.Print{}
	if err = v1.Scheme.Convert(raw, updated); err != nil {
		return
	}
	return
}

func (p *print) Cancel(ctx context.Context, print *api.Print) (updated *api.Print, raw *v1.Print, err error) {
	now := time.Now()
	print.Spec.State = api.PrintSpecStateCanceled
	if updated, _, err = p.Update(ctx, print); err != nil {
		return
	}

	if c, ok := updated.Status.Conditions.Get(api.PrintConditionTypeCanceled); ok && c.Status == api.ConditionStatusTrue {
		// already canceled
		return
	}

	t := time.NewTicker(500 * time.Millisecond)
	defer t.Stop()
	for range t.C {
		if updated, raw, err = p.Show(ctx, updated.Name); err != nil {
			return
		}
		if c, ok := updated.Status.Conditions.Get(api.PrintConditionTypeCanceled); ok && c.LastTransitionTime.After(now) {
			if c.Status == api.ConditionStatusTrue {
				// print is now canceled
				return
			}
			if c.Status == api.ConditionStatusFalse {
				// print failed to cancel
				err = fmt.Errorf("print failed to cancel: [%s] %s", c.Reason, c.Message)
				return
			}
		}
	}
	return
}

func (p *print) Create(ctx context.Context, print *api.Print) (out *api.Print, raw *v1.Print, err error) {
	in := &v1.Print{}
	if err = v1.Scheme.Convert(print, in); err != nil {
		return
	}
	if raw, err = p.c.Create(ctx, in); err != nil {
		return
	}
	out = &api.Print{}
	if err = v1.Scheme.Convert(raw, out); err != nil {
		return
	}
	return
}

func (p *print) Delete(ctx context.Context, print *api.Print) (deleted *api.Print, raw *v1.Print, err error) {
	in := &v1.Print{}
	if err = v1.Scheme.Convert(print, in); err != nil {
		return
	}
	if raw, err = p.c.Delete(ctx, in); err != nil {
		return
	}
	deleted = &api.Print{}
	if err = v1.Scheme.Convert(raw, deleted); err != nil {
		return
	}
	return
}
