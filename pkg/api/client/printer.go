package client

import (
	"context"
	"fmt"
	"time"

	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/v1"
)

// Printer client
type Printer interface {
	Create(ctx context.Context, printer *api.Printer) (out *api.Printer, raw *v1.Printer, err error)
	List(ctx context.Context, opts api.PrinterListRequest) (list *api.PrinterList, raw *v1.PrinterList, err error)
	Show(ctx context.Context, name string) (printer *api.Printer, raw *v1.Printer, err error)
	Delete(ctx context.Context, printer *api.Printer) (deleted *api.Printer, raw *v1.Printer, err error)
	Update(ctx context.Context, printer *api.Printer) (updated *api.Printer, raw *v1.Printer, err error)
	UpdateStatus(ctx context.Context, printer *api.Printer) (updated *api.Printer, raw *v1.Printer, err error)

	Connect(ctx context.Context, printer *api.Printer) (updated *api.Printer, raw *v1.Printer, err error)
	Disconnect(ctx context.Context, printer *api.Printer) (updated *api.Printer, raw *v1.Printer, err error)
}

type printer struct {
	c v1.PrinterServiceClient
}

func (p *printer) Create(ctx context.Context, printer *api.Printer) (out *api.Printer, raw *v1.Printer, err error) {
	in := &v1.Printer{}
	if err = v1.Scheme.Convert(printer, in); err != nil {
		return
	}
	if raw, err = p.c.Create(ctx, in); err != nil {
		return
	}
	out = &api.Printer{}
	if err = v1.Scheme.Convert(raw, out); err != nil {
		return
	}
	return
}

func (p *printer) Delete(ctx context.Context, printer *api.Printer) (deleted *api.Printer, raw *v1.Printer, err error) {
	in := &v1.Printer{}
	if err = v1.Scheme.Convert(printer, in); err != nil {
		return
	}
	if raw, err = p.c.Delete(ctx, in); err != nil {
		return
	}
	deleted = &api.Printer{}
	if err = v1.Scheme.Convert(raw, deleted); err != nil {
		return
	}
	return
}

func (p *printer) List(ctx context.Context, req api.PrinterListRequest) (list *api.PrinterList, raw *v1.PrinterList, err error) {
	v1Req := &v1.PrinterListRequest{}
	if err = v1.Scheme.Convert(&req, v1Req); err != nil {
		return
	}
	if raw, err = p.c.List(ctx, v1Req); err != nil {
		return
	}

	list = &api.PrinterList{}
	if err = v1.Scheme.Convert(raw, list); err != nil {
		return
	}
	return
}

func (p *printer) Show(ctx context.Context, name string) (printer *api.Printer, raw *v1.Printer, err error) {
	if raw, err = p.c.Show(ctx, &v1.Printer{
		Metadata: &v1.ObjectMeta{
			Name: name,
		},
	}); err != nil {
		return
	}
	printer = &api.Printer{}
	if err = v1.Scheme.Convert(raw, printer); err != nil {
		return
	}
	return
}

func (p *printer) Update(ctx context.Context, printer *api.Printer) (updated *api.Printer, raw *v1.Printer, err error) {
	in := &v1.Printer{}
	if err = v1.Scheme.Convert(printer, in); err != nil {
		return
	}

	if raw, err = p.c.Update(ctx, in); err != nil {
		return
	}

	updated = &api.Printer{}
	if err = v1.Scheme.Convert(raw, updated); err != nil {
		return
	}
	return
}

func (p *printer) UpdateStatus(ctx context.Context, printer *api.Printer) (updated *api.Printer, raw *v1.Printer, err error) {
	in := &v1.Printer{}
	if err = v1.Scheme.Convert(printer, in); err != nil {
		return
	}

	if raw, err = p.c.UpdateStatus(ctx, in); err != nil {
		return
	}

	updated = &api.Printer{}
	if err = v1.Scheme.Convert(raw, updated); err != nil {
		return
	}
	return
}

func (p *printer) Connect(ctx context.Context, printer *api.Printer) (updated *api.Printer, raw *v1.Printer, err error) {
	now := time.Now()
	printer.Spec.State = api.PrinterSpecStateConnected
	if updated, _, err = p.Update(ctx, printer); err != nil {
		return
	}
	if c, ok := updated.Status.Conditions.Get(api.PrinterConditionTypeConnected); ok && c.Status == api.ConditionStatusTrue {
		// already connected
		return
	}

	t := time.NewTicker(500 * time.Millisecond)
	defer t.Stop()
	for range t.C {
		if updated, raw, err = p.Show(ctx, updated.Name); err != nil {
			return
		}
		if c, ok := updated.Status.Conditions.Get(api.PrinterConditionTypeConnected); ok && c.LastTransitionTime.After(now) {
			if c.Status == api.ConditionStatusTrue {
				// printer is now connected!
				return
			}
			if c.Status == api.ConditionStatusFalse {
				// printer failed to connect
				err = fmt.Errorf("printer failed to connect: [%s] %s", c.Reason, c.Message)
				return
			}
		}
	}
	return
}

func (p *printer) Disconnect(ctx context.Context, printer *api.Printer) (updated *api.Printer, raw *v1.Printer, err error) {
	now := time.Now()
	printer.Spec.State = api.PrinterSpecStateDisconnected
	if updated, raw, err = p.Update(ctx, printer); err != nil {
		return
	}
	if c, ok := updated.Status.Conditions.Get(api.PrinterConditionTypeConnected); ok && c.Status == api.ConditionStatusFalse {
		// already connected
		return
	}

	t := time.NewTicker(500 * time.Millisecond)
	defer t.Stop()
	for range t.C {
		if updated, raw, err = p.Show(ctx, updated.Name); err != nil {
			return
		}
		if c, ok := updated.Status.Conditions.Get(api.PrinterConditionTypeConnected); ok && c.LastTransitionTime.After(now) {
			if c.Status == api.ConditionStatusTrue {
				// printer failed to connect
				err = fmt.Errorf("printer failed to disconnect: [%s] %s", c.Reason, c.Message)
				return
			}
			if c.Status == api.ConditionStatusFalse {
				// printer is now disconnected!
				return
			}
		}
	}
	return
}
