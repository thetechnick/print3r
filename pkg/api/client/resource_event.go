package client

import (
	"context"

	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/v1"
)

type ResourceEvent interface {
	Stream(ctx context.Context, opts ResourceEventStreamOpts) (ResourceEventStreamClient, error)
}

type resourceEvent struct {
	c v1.ResourceEventServiceClient
}

type ResourceEventStreamOpts struct {
	Type string
}

type ResourceEventStreamClient interface {
	Recv() (*api.ResourceEvent, *v1.ResourceEvent, error)
}

type resourceEventStreamClient struct {
	c v1.ResourceEventService_StreamClient
}

func (c *resourceEventStreamClient) Recv() (event *api.ResourceEvent, raw *v1.ResourceEvent, err error) {
	raw, err = c.c.Recv()
	if err != nil {
		return
	}
	event = &api.ResourceEvent{}
	if err = v1.Scheme.Convert(raw, event); err != nil {
		return
	}
	return
}

func (r *resourceEvent) Stream(
	ctx context.Context,
	opts ResourceEventStreamOpts,
) (stream ResourceEventStreamClient, err error) {
	s := &resourceEventStreamClient{}
	stream = s

	s.c, err = r.c.Stream(ctx, &v1.ResourceEventStreamRequest{
		Type: opts.Type,
	})
	if err != nil {
		return
	}

	return
}
