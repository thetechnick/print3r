package client

import (
	"context"

	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/v1"
)

type Version interface {
	Version(ctx context.Context) (version *api.Version, raw *v1.APIVersion, err error)
}

type version struct {
	c v1.VersionServiceClient
}

func (v *version) Version(ctx context.Context) (version *api.Version, raw *v1.APIVersion, err error) {
	raw, err = v.c.Version(ctx, &v1.VersionRequest{})
	if err != nil {
		return
	}

	version = &api.Version{}
	if err = v1.Scheme.Convert(raw, version); err != nil {
		return
	}

	return
}
