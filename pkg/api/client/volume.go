package client

import (
	"context"

	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/v1"
)

type Volume interface {
	List(ctx context.Context, opts api.VolumeListRequest) (list *api.VolumeList, raw *v1.VolumeList, err error)
	Cancel(ctx context.Context, volume *api.Volume) (updated *api.Volume, raw *v1.Volume, err error)
	Create(ctx context.Context, volume *api.Volume) (out *api.Volume, raw *v1.Volume, err error)
	Delete(ctx context.Context, volume *api.Volume) (deleted *api.Volume, raw *v1.Volume, err error)
	Update(ctx context.Context, volume *api.Volume) (updated *api.Volume, raw *v1.Volume, err error)
	UpdateStatus(ctx context.Context, volume *api.Volume) (updated *api.Volume, raw *v1.Volume, err error)
}

type volume struct {
	c v1.VolumeServiceClient
}

func (p *volume) List(ctx context.Context, req api.VolumeListRequest) (list *api.VolumeList, raw *v1.VolumeList, err error) {
	v1Req := &v1.VolumeListRequest{}
	if err = v1.Scheme.Convert(&req, v1Req); err != nil {
		return
	}
	if raw, err = p.c.List(ctx, v1Req); err != nil {
		return
	}

	list = &api.VolumeList{}
	if err = v1.Scheme.Convert(raw, list); err != nil {
		return
	}
	return
}

func (p *volume) Show(ctx context.Context, name string) (volume *api.Volume, raw *v1.Volume, err error) {
	if raw, err = p.c.Show(ctx, &v1.Volume{
		Metadata: &v1.ObjectMeta{
			Name: name,
		},
	}); err != nil {
		return
	}
	volume = &api.Volume{}
	if err = v1.Scheme.Convert(raw, volume); err != nil {
		return
	}
	return
}

func (p *volume) Update(ctx context.Context, volume *api.Volume) (updated *api.Volume, raw *v1.Volume, err error) {
	in := &v1.Volume{}
	if err = v1.Scheme.Convert(volume, in); err != nil {
		return
	}

	if raw, err = p.c.Update(ctx, in); err != nil {
		return
	}

	updated = &api.Volume{}
	if err = v1.Scheme.Convert(raw, updated); err != nil {
		return
	}
	return
}

func (p *volume) UpdateStatus(ctx context.Context, volume *api.Volume) (updated *api.Volume, raw *v1.Volume, err error) {
	in := &v1.Volume{}
	if err = v1.Scheme.Convert(volume, in); err != nil {
		return
	}

	if raw, err = p.c.UpdateStatus(ctx, in); err != nil {
		return
	}

	updated = &api.Volume{}
	if err = v1.Scheme.Convert(raw, updated); err != nil {
		return
	}
	return
}

func (p *volume) Create(ctx context.Context, volume *api.Volume) (out *api.Volume, raw *v1.Volume, err error) {
	in := &v1.Volume{}
	if err = v1.Scheme.Convert(volume, in); err != nil {
		return
	}
	if raw, err = p.c.Create(ctx, in); err != nil {
		return
	}
	out = &api.Volume{}
	if err = v1.Scheme.Convert(raw, out); err != nil {
		return
	}
	return
}

func (p *volume) Delete(ctx context.Context, volume *api.Volume) (deleted *api.Volume, raw *v1.Volume, err error) {
	in := &v1.Volume{}
	if err = v1.Scheme.Convert(volume, in); err != nil {
		return
	}
	if raw, err = p.c.Delete(ctx, in); err != nil {
		return
	}
	deleted = &api.Volume{}
	if err = v1.Scheme.Convert(raw, deleted); err != nil {
		return
	}
	return
}
