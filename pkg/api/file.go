package api

// FileAnalysis represents the analysis result of a file
type FileAnalysis struct {
	CommandCount, ExtruderTemperature,
	BedTemperature, LayerCount int
	Commands           map[string]int
	AverageLayerHeight float32
	Max                FileAnalysisCoordinates
	Min                FileAnalysisCoordinates
	FilamentExtrusion  map[string]float64
}

// FileAnalysisCoordinates represents a coordinate set
type FileAnalysisCoordinates struct {
	X, Y, Z float64
}
