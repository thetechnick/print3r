package api

import (
	"time"
)

// TimeSeries represents a series of values over time
type TimeSeries []*TimeSeriesPoint

// TimeSeriesPoint represents a value at a point in time
type TimeSeriesPoint struct {
	Time  time.Time
	Value float64
}

// Get returns a slice of TimeSeriesPoints
func (g *TimeSeries) Get() []*TimeSeriesPoint {
	if g == nil {
		return nil
	}
	return *g
}
