package api

import (
	"fmt"
	"strings"
)

// LabelSelector represents a parsed lable selector
type LabelSelector struct {
	match   map[string]string
	noMatch map[string]string
}

// Match checks if the labels match this selector
func (s *LabelSelector) Match(labels map[string]string) bool {
	if labels == nil {
		labels = map[string]string{}
	}
	for k, v := range s.match {
		if lv, ok := labels[k]; !ok || v != lv {
			return false
		}
	}
	for k, v := range s.noMatch {
		if lv, ok := labels[k]; ok && v == lv || ok && v == "" {
			return false
		}
	}
	return true
}

// ParseSelector parses selector queries
func ParseSelector(labelSelector string) (*LabelSelector, error) {
	if labelSelector == "" {
		return nil, nil
	}

	match := map[string]string{}
	noMatch := map[string]string{}
	selectors := strings.Split(labelSelector, ",")

	for _, selector := range selectors {
		var parts []string
		switch {
		case strings.Contains(selector, "!="):
			parts = strings.SplitN(selector, "!=", 2)
			if len(parts) < 2 {
				return nil, fmt.Errorf("invalid selector")
			}
			noMatch[parts[0]] = parts[1]

		case strings.Contains(selector, "=="):
			parts = strings.SplitN(selector, "==", 2)
			if len(parts) < 2 {
				return nil, fmt.Errorf("invalid selector")
			}
			match[parts[0]] = parts[1]

		case strings.Contains(selector, "="):
			parts = strings.SplitN(selector, "=", 2)
			if len(parts) < 2 {
				return nil, fmt.Errorf("invalid selector")
			}
			match[parts[0]] = parts[1]

		default:
			if strings.HasPrefix(selector, "!") {
				noMatch[selector[1:]] = ""
			} else {
				match[selector] = ""
			}
		}
	}

	return &LabelSelector{
		match:   match,
		noMatch: noMatch,
	}, nil
}
