package api

import "time"

// ObjectMeta represents object metadata
type ObjectMeta struct {
	UID, Name, ResourceVersion string
	Created                    time.Time
	Updated                    time.Time
	Labels                     map[string]string
	Owner                      *ObjectReference
	Type                       string
}

// GetType gets the type of the object
func (m *ObjectMeta) GetType() string {
	return m.Type
}

// SetType sets the type of the object
func (m *ObjectMeta) SetType(t string) {
	m.Type = t
}

// GetName gets the name of the object
func (m *ObjectMeta) GetName() string {
	return m.Name
}

// SetName sets the name of the object
func (m *ObjectMeta) SetName(name string) {
	m.Name = name
}

// GetUID gets the uid of the object
func (m *ObjectMeta) GetUID() string {
	return m.UID
}

// SetUID sets the uid of the object
func (m *ObjectMeta) SetUID(uid string) {
	m.UID = uid
}

// GetResourceVersion gets the resource version of the object
func (m *ObjectMeta) GetResourceVersion() string {
	return m.ResourceVersion
}

// SetResourceVersion sets the resource version of the object
func (m *ObjectMeta) SetResourceVersion(v string) {
	m.ResourceVersion = v
}

// GetLabels gets the labels of the object
func (m *ObjectMeta) GetLabels() map[string]string {
	return m.Labels
}

// SetLabels sets the labels of the object
func (m *ObjectMeta) SetLabels(l map[string]string) {
	m.Labels = l
}

// GetOwner gets the owner of the object
func (m *ObjectMeta) GetOwner() *ObjectReference {
	return m.Owner
}

// SetOwner sets the owner of the object
func (m *ObjectMeta) SetOwner(o *ObjectReference) {
	m.Owner = o
}

// GetCreatedTime gets the created time of the object
func (m *ObjectMeta) GetCreatedTime() time.Time {
	return m.Created
}

// SetCreatedTime sets the created time of the object
func (m *ObjectMeta) SetCreatedTime(c time.Time) {
	m.Created = c
}

// GetUpdatedTime gets the updated time of the object
func (m *ObjectMeta) GetUpdatedTime() time.Time {
	return m.Updated
}

// SetUpdatedTime sets the updated time of the object
func (m *ObjectMeta) SetUpdatedTime(u time.Time) {
	m.Updated = u
}

// Ref returns a Object reference to the object the metadata belongs to
func (m *ObjectMeta) Ref() *ObjectReference {
	return &ObjectReference{UID: m.UID, Name: m.Name, Type: m.Type}
}

// ListMeta represents list metadata
type ListMeta struct {
	ResourceVersion, Continue string
	Owner                     *ObjectReference
}

// ObjectReference represents the reference to another object
type ObjectReference struct {
	Type, UID, Name string
}

// Object is the generic object access interface
type Object interface {
	GetType() string
	SetType(string)
	GetName() string
	SetName(string)
	GetUID() string
	SetUID(string)
	GetResourceVersion() string
	SetResourceVersion(string)
	GetLabels() map[string]string
	SetLabels(map[string]string)
	GetOwner() *ObjectReference
	SetOwner(*ObjectReference)
	GetCreatedTime() time.Time
	SetCreatedTime(time.Time)
	GetUpdatedTime() time.Time
	SetUpdatedTime(time.Time)
}
