package api

import (
	"time"
)

// PrintList represents a list of prints
type PrintList struct {
	ListMeta
	Items []*Print
}

// Print represents a print
type Print struct {
	ObjectMeta
	Spec   PrintSpec
	Status PrintStatus
}

// NewPrint creates a new print object
func NewPrint() *Print {
	now := time.Now()
	return &Print{
		Spec: PrintSpec{
			State: PrintSpecStatePrinted,
		},
		Status: PrintStatus{
			Conditions: NewConditionMapCollection([]Condition{
				Condition{
					Type:               PrintConditionTypeStarted,
					Status:             ConditionStatusFalse,
					LastTransitionTime: now,
				},
				Condition{
					Type:               PrintConditionTypePaused,
					Status:             ConditionStatusFalse,
					LastTransitionTime: now,
				},
				Condition{
					Type:               PrintConditionTypeCanceled,
					Status:             ConditionStatusFalse,
					LastTransitionTime: now,
				},
				Condition{
					Type:               PrintConditionTypeFinished,
					Status:             ConditionStatusFalse,
					LastTransitionTime: now,
				},
			}),
		},
	}
}

// PrintSpecState represents the desired state of a print
type PrintSpecState string

// PrintSpecState values
const (
	PrintSpecStateUnknown  PrintSpecState = "Unknown"
	PrintSpecStatePrinted  PrintSpecState = "Printed"
	PrintSpecStatePaused   PrintSpecState = "Paused"
	PrintSpecStateCanceled PrintSpecState = "Canceled"
)

// PrintSpec represents the spec of the print
type PrintSpec struct {
	Printer string
	FileRef FileRef
	State   PrintSpecState
}

// FileRef represents a file reference
type FileRef struct {
	Volume, File string
}

// FileName returns the filename of the file reference
func (s PrintSpec) FileName() string {
	return s.FileRef.File
}

// VolumeName returns the volume name
func (s PrintSpec) VolumeName() string {
	return s.FileRef.Volume
}

// PrintStatus represents a prints status
type PrintStatus struct {
	Progress   float64
	Conditions ConditionCollection
}

// PrintPhase represents a prints's current state
type PrintPhase string

// PrintStatusState's values
const (
	PrintPhaseUnknown  PrintPhase = "Unknown"
	PrintPhaseFinished PrintPhase = "Finished"
	PrintPhaseCanceled PrintPhase = "Canceled"
	PrintPhasePending  PrintPhase = "Pending"
	PrintPhaseHeating  PrintPhase = "Heating"
	PrintPhasePrinting PrintPhase = "Printing"
	PrintPhaseError    PrintPhase = "Error"
	PrintPhasePaused   PrintPhase = "Paused"
)

// PrintConditionTypes
const (
	PrintConditionTypeStarted  = "Started"
	PrintConditionTypePaused   = "Paused"
	PrintConditionTypeCanceled = "Canceled"
	PrintConditionTypeFinished = "Finished"
)

// PrintListRequest represents a list request
type PrintListRequest struct {
	Continue, Selector string
	Limit              int
}
