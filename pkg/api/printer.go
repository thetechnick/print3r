package api

import (
	"time"
)

// PrinterList represents a list of printers
type PrinterList struct {
	ListMeta
	Items []*Printer
}

// Printer represents a printer
type Printer struct {
	ObjectMeta
	Spec   PrinterSpec
	Status PrinterStatus
}

// NewPrinter creates a new printer object
func NewPrinter() *Printer {
	now := time.Now()
	return &Printer{
		Status: PrinterStatus{
			Conditions: NewConditionMapCollection([]Condition{
				{
					Type:               PrinterConditionTypeConnected,
					Status:             ConditionStatusFalse,
					LastTransitionTime: now,
				},
				{
					Type:               PrinterConditionTypePrinting,
					Status:             ConditionStatusFalse,
					LastTransitionTime: now,
				},
				{
					Type:               PrinterConditionTypeBedClean,
					Status:             ConditionStatusTrue,
					LastTransitionTime: now,
				},
			}),
		},
	}
}

// PrinterSpec represents the printers specifications
type PrinterSpec struct {
	Model, Device, VideoStream string
	BaudRate                   int64
	State                      PrinterSpecState
}

// PrinterSpecState represents the desired state of the printer
type PrinterSpecState string

// PrinterSpecState values
const (
	PrinterSpecStateUnknown      PrinterSpecState = "Unknown"
	PrinterSpecStateConnected    PrinterSpecState = "Connected"
	PrinterSpecStateDisconnected PrinterSpecState = "Disconnected"
)

// PrinterStatus represents the current state of the printer
type PrinterStatus struct {
	Conditions  ConditionCollection
	Temperature *PrinterTemperature
	Print       *PrinterPrintStatus
}

// PrinterPrintStatus holds informations about the current active print
type PrinterPrintStatus struct {
	Name string
}

// PrinterTemperature represents a printers temperature
type PrinterTemperature struct {
	ExtruderTarget,
	ExtruderActual,
	BedTarget,
	BedActual float32
}

// PrinterConditionTypes
const (
	PrinterConditionTypeConnected = "Connected"
	PrinterConditionTypePrinting  = "Printing"
	PrinterConditionTypeBedClean  = "BedClean"
)

// PrinterTemperatureHistory represents a printers temperature over time
type PrinterTemperatureHistory struct {
	ExtruderTarget,
	ExtruderActual,
	BedTarget,
	BedActual *TimeSeries
}

// PrinterTemperatureHistoryRequest hold options when requesting the printers temperature graph
type PrinterTemperatureHistoryRequest struct {
	Name       string
	Start, End time.Time
	Size       int
}

// PrinterListRequest holds options for listing printers
type PrinterListRequest struct {
	Continue, Selector string
	Limit              int
}
