package api

import "time"

// ResourceEventOperation type of a resource event
type ResourceEventOperation string

// ResourceEventOperation values
var (
	ResourceEventOperationUnknown ResourceEventOperation = "Unknown"
	ResourceEventOperationCreated ResourceEventOperation = "Created"
	ResourceEventOperationDeleted ResourceEventOperation = "Deleted"
	ResourceEventOperationUpdated ResourceEventOperation = "Updated"
)

// ResourceEvent represents a event of a resource
type ResourceEvent struct {
	ID          string
	Operation   ResourceEventOperation
	EmittedTime time.Time
	Old, New    interface{}
}
