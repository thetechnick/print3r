package conversion

import (
	"errors"
	"fmt"
	"reflect"
)

// Converter converts api object to other versions
type Converter interface {
	RegisterConversionFunc(conversionFunc interface{}) error
	Convert(src, dest interface{}) error
}

// NewConverter creates a new Converter
func NewConverter() Converter {
	c := &converter{
		conversionFuncs: newConversionFuncs(),
	}
	for _, dc := range DefaultConversions {
		if err := c.RegisterConversionFunc(dc); err != nil {
			panic(err)
		}
	}
	return c
}

type converter struct {
	conversionFuncs conversionFuncs
}

func (c *converter) RegisterConversionFunc(conversionFunc interface{}) error {
	return c.conversionFuncs.Add(conversionFunc)
}

func (c *converter) Convert(src, dest interface{}) error {
	return c.doConvert(src, dest, c.convert)
}

func (c *converter) convert(sv, dv reflect.Value, scope *scope) error {
	// Convert sv to dv.
	dt, st := dv.Type(), sv.Type()
	pair := typePair{st, dt}
	if fv, ok := c.conversionFuncs.fns[pair]; ok {
		return c.callConverter(sv, dv, fv, scope)
	}

	return fmt.Errorf("no converter registered for conversion of %v to %v", dt, st)
}

type conversionFunc func(sv, dv reflect.Value, scope *scope) error

func (c *converter) doConvert(src, dest interface{}, f conversionFunc) error {
	dv, err := enforcePtr(dest)
	if err != nil {
		return fmt.Errorf("%v: for (src: %v) (dest: %v)", err, src, dest)
	}
	if !dv.CanAddr() && !dv.CanSet() {
		return fmt.Errorf("can't write to dest")
	}
	sv, err := enforcePtr(src)
	if err != nil {
		if err == ErrIsNil {
			return nil
		}
		return err
	}
	s := &scope{
		converter: c,
	}
	s.srcStack.push(scopeStackElem{})
	s.destStack.push(scopeStackElem{})
	return f(sv, dv, s)
}

func (c *converter) callConverter(sv, dv, custom reflect.Value, scope *scope) error {
	if !sv.CanAddr() {
		sv2 := reflect.New(sv.Type())
		sv2.Elem().Set(sv)
		sv = sv2
	} else {
		sv = sv.Addr()
	}
	if !dv.CanAddr() {
		if !dv.CanSet() {
			return scope.errorf("can't addr or set dest")
		}
		dvOrig := dv
		dvv := reflect.New(dvOrig.Type())
		defer func() { dvOrig.Set(dvv) }()
	} else {
		dv = dv.Addr()
	}
	args := []reflect.Value{sv, dv, reflect.ValueOf(scope)}
	ret := custom.Call(args)[0].Interface()
	// This convolution is necessary because nil interfaces won't convert
	// to errors.
	if ret == nil {
		return nil
	}
	return ret.(error)
}

type typePair struct {
	source reflect.Type
	dest   reflect.Type
}

// newConversionFuncs creates a new ConversionFuncs mapping
func newConversionFuncs() conversionFuncs {
	return conversionFuncs{fns: make(map[typePair]reflect.Value)}
}

// ConversionFuncs holds the typePair to conversionFunc mapping
type conversionFuncs struct {
	fns map[typePair]reflect.Value
}

// Add adds the provided conversion functions to the lookup table - they must have the signature
// `func(type1, type2, Scope) error`. Functions are added in the order passed and will override
// previously registered pairs.
func (c conversionFuncs) Add(fns ...interface{}) error {
	for _, fn := range fns {
		fv := reflect.ValueOf(fn)
		ft := fv.Type()
		if err := verifyConversionFunctionSignature(ft); err != nil {
			return err
		}
		c.fns[typePair{ft.In(0).Elem(), ft.In(1).Elem()}] = fv
	}
	return nil
}

// ErrIsNil is returned if one of the inputs is nil
var ErrIsNil = errors.New("expected pointer, but got nil")

// enforcePtr ensures that obj is a pointer of some sort. Returns a reflect.Value
// of the dereferenced pointer, ensuring that it is settable/addressable.
// Returns an error if this is not possible.
func enforcePtr(obj interface{}) (reflect.Value, error) {
	v := reflect.ValueOf(obj)
	if v.Kind() != reflect.Ptr {
		if v.Kind() == reflect.Invalid {
			return reflect.Value{}, fmt.Errorf("expected pointer, but got invalid kind")
		}
		return reflect.Value{}, fmt.Errorf("expected pointer, but got %v type", v.Type())
	}
	if v.IsNil() {
		return reflect.Value{}, ErrIsNil
	}
	return v.Elem(), nil
}

// Scope is passed to conversion funcs to allow them to continue an ongoing conversion.
type Scope interface {
	// Call Convert to convert sub-objects.
	Convert(src, dest interface{}) error
}

// scope contains information about an ongoing conversion.
type scope struct {
	converter Converter
	srcStack  scopeStack
	destStack scopeStack
}

func (s *scope) Convert(src, dest interface{}) error {
	return s.converter.Convert(src, dest)
}

// describe prints the path to get to the current (source, dest) values.
func (s *scope) describe() (src, dest string) {
	return s.srcStack.describe(), s.destStack.describe()
}

// error makes an error that includes information about where we were in the objects
// we were asked to convert.
func (s *scope) errorf(message string, args ...interface{}) error {
	srcPath, destPath := s.describe()
	where := fmt.Sprintf("converting %v to %v: ", srcPath, destPath)
	return fmt.Errorf(where+message, args...)
}

type scopeStackElem struct {
	tag   reflect.StructTag
	value reflect.Value
	key   string
}

type scopeStack []scopeStackElem

func (s *scopeStack) pop() {
	n := len(*s)
	*s = (*s)[:n-1]
}

func (s *scopeStack) push(e scopeStackElem) {
	*s = append(*s, e)
}

func (s *scopeStack) top() *scopeStackElem {
	return &(*s)[len(*s)-1]
}

func (s scopeStack) describe() string {
	desc := ""
	if len(s) > 1 {
		desc = "(" + s[1].value.Type().String() + ")"
	}
	for i, v := range s {
		if i < 2 {
			// First layer on stack is not real; second is handled specially above.
			continue
		}
		if v.key == "" {
			desc += fmt.Sprintf(".%v", v.value.Type())
		} else {
			desc += fmt.Sprintf(".%v", v.key)
		}
	}
	return desc
}
