package v1

import (
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/runtime/conversion"
	"github.com/thetechnick/print3r/pkg/util"
)

// conditionConverters contains convert functions for the Condition object
var conditionConverters = []interface{}{
	// ConditionCollection
	func(in *[]*Condition, out *api.ConditionCollection, s conversion.Scope) (err error) {
		m := api.ConditionMapCollection{}
		*out = m
		if in == nil {
			return
		}
		for _, f := range *in {
			outCondition := api.Condition{}
			if err = s.Convert(f, &outCondition); err != nil {
				return
			}
			m[outCondition.Type] = outCondition
		}
		return
	},
	func(in *api.ConditionCollection, out *[]*Condition, s conversion.Scope) (err error) {
		if in == nil || *in == nil {
			return
		}
		for _, f := range (*in).List() {
			outCondition := &Condition{}
			if err = s.Convert(&f, outCondition); err != nil {
				return
			}
			*out = append(*out, outCondition)
		}
		return
	},

	// []Condition
	func(in *[]*Condition, out *[]api.Condition, s conversion.Scope) (err error) {
		if in == nil {
			return
		}
		for _, f := range *in {
			outCondition := api.Condition{}
			if err = s.Convert(f, &outCondition); err != nil {
				return
			}
			*out = append(*out, outCondition)
		}
		return
	},
	func(in *[]api.Condition, out *[]*Condition, s conversion.Scope) (err error) {
		if in == nil {
			return
		}
		for _, f := range *in {
			outCondition := &Condition{}
			if err = s.Convert(&f, outCondition); err != nil {
				return
			}
			*out = append(*out, outCondition)
		}
		return
	},

	// Condition
	func(in *Condition, out *api.Condition, s conversion.Scope) (err error) {
		out.Type = in.Type
		out.Reason = in.Reason
		out.Message = in.Message
		if out.LastTransitionTime, err = util.Timestamp(in.LastTransitionTime); err != nil {
			return
		}
		if err = s.Convert(&in.Status, &out.Status); err != nil {
			return
		}
		return
	},
	func(in *api.Condition, out *Condition, s conversion.Scope) (err error) {
		out.Type = in.Type
		out.Reason = in.Reason
		out.Message = in.Message
		if out.LastTransitionTime, err = util.TimestampProto(in.LastTransitionTime); err != nil {
			return
		}
		if err = s.Convert(&in.Status, &out.Status); err != nil {
			return
		}
		return
	},

	// ConditionStatus
	func(in *Condition_Status, out *api.ConditionStatus, s conversion.Scope) (err error) {
		n, ok := Condition_Status_name[int32(*in)]
		if !ok {
			*out = api.ConditionStatusUnknown
			return
		}
		*out = api.ConditionStatus(n)
		return
	},
	func(in *api.ConditionStatus, out *Condition_Status, s conversion.Scope) (err error) {
		v, ok := Condition_Status_value[string(*in)]
		if !ok {
			*out = Condition_Unknown
			return
		}
		*out = Condition_Status(v)
		return
	},
}
