package v1

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/thetechnick/print3r/pkg/api"
)

func TestCondition(t *testing.T) {
	v1o := &Condition{}
	apio := &api.Condition{
		Type:               "Something",
		Reason:             "Testing",
		Status:             api.ConditionStatusTrue,
		LastTransitionTime: time.Now(),
	}

	t.Run("from api.Condition to v1.Condition", func(t *testing.T) {
		err := Scheme.Convert(apio, v1o)
		if !assert.NoError(t, err) {
			return
		}

		assert.Equal(t, apio.Type, v1o.Type)
		assert.Equal(t, apio.Reason, v1o.Reason)
		assert.Equal(t, Condition_True, v1o.Status)
	})

	t.Run("From v1.Condition to api.Condition", func(t *testing.T) {
		apio2 := &api.Condition{}

		err := Scheme.Convert(v1o, apio2)
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, apio.Type, apio2.Type)
		assert.Equal(t, apio.Reason, apio2.Reason)
		assert.Equal(t, api.ConditionStatusTrue, apio2.Status)
	})
}
