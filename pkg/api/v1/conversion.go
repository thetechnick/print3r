package v1

import (
	fmt "fmt"

	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/runtime"
	"github.com/thetechnick/print3r/pkg/api/runtime/conversion"
	"github.com/thetechnick/print3r/pkg/util"
)

func addConversionFuncs(scheme *runtime.Scheme) error {
	scheme.AddConversionFuncs(conditionConverters...)
	scheme.AddConversionFuncs(printerConverters...)
	scheme.AddConversionFuncs(printConverters...)
	scheme.AddConversionFuncs(volumeConverters...)
	scheme.AddConversionFuncs(videoStreamConverters...)
	return scheme.AddConversionFuncs(
		// Version
		func(in *APIVersion, out *api.Version, s conversion.Scope) (err error) {
			out.Branch = in.Branch
			out.GoVersion = in.GoVersion
			out.Platform = in.Platform
			out.Version = in.Version
			if out.BuildDate, err = util.Timestamp(in.BuildDate); err != nil {
				return
			}
			return
		},
		func(in *api.Version, out *APIVersion, s conversion.Scope) (err error) {
			out.Branch = in.Branch
			out.GoVersion = in.GoVersion
			out.Platform = in.Platform
			out.Version = in.Version
			if out.BuildDate, err = util.TimestampProto(in.BuildDate); err != nil {
				return
			}
			return
		},

		// ListMeta
		func(in *ListMeta, out *api.ListMeta, s conversion.Scope) (err error) {
			out.Continue = in.Continue
			out.ResourceVersion = in.ResourceVersion
			if in.Owner != nil {
				out.Owner = &api.ObjectReference{}
				if err = s.Convert(in.Owner, out.Owner); err != nil {
					return
				}
			}
			return
		},
		func(in *api.ListMeta, out *ListMeta, s conversion.Scope) (err error) {
			out.Continue = in.Continue
			out.ResourceVersion = in.ResourceVersion
			if in.Owner != nil {
				out.Owner = &ObjectReference{}
				if err = s.Convert(in.Owner, out.Owner); err != nil {
					return
				}
			}
			return
		},

		// ObjectMeta
		func(in *ObjectMeta, out *api.ObjectMeta, s conversion.Scope) (err error) {
			out.Name = in.Name
			out.Labels = in.Labels
			out.UID = in.Uid
			out.ResourceVersion = in.ResourceVersion
			out.Type = in.Type

			out.UID = in.Uid
			if out.Created, err = util.Timestamp(in.Created); err != nil {
				return
			}
			if out.Updated, err = util.Timestamp(in.Updated); err != nil {
				return
			}
			if in.Owner != nil {
				out.Owner = &api.ObjectReference{}
				if err = s.Convert(in.Owner, out.Owner); err != nil {
					return
				}
			}
			return
		},
		func(in *api.ObjectMeta, out *ObjectMeta, s conversion.Scope) (err error) {
			out.Name = in.Name
			out.Labels = in.Labels
			out.ResourceVersion = in.ResourceVersion
			out.Uid = in.UID
			out.Type = in.Type

			if out.Created, err = util.TimestampProto(in.Created); err != nil {
				return
			}
			if out.Updated, err = util.TimestampProto(in.Updated); err != nil {
				return
			}
			if in.Owner != nil {
				out.Owner = &ObjectReference{}
				if err = s.Convert(in.Owner, out.Owner); err != nil {
					return
				}
			}
			return
		},

		// ObjectReference
		func(in *ObjectReference, out *api.ObjectReference, s conversion.Scope) (err error) {
			out.Name = in.Name
			out.UID = in.Uid
			out.Type = in.Type
			return
		},
		func(in *api.ObjectReference, out *ObjectReference, s conversion.Scope) (err error) {
			out.Name = in.Name
			out.Uid = in.UID
			out.Type = in.Type
			return
		},

		// ResourceEventOperation
		func(in *ResourceEvent_Operation, out *api.ResourceEventOperation, s conversion.Scope) (err error) {
			*out = api.ResourceEventOperation(ResourceEvent_Operation_name[int32(*in)])
			return
		},
		func(in *api.ResourceEventOperation, out *ResourceEvent_Operation, s conversion.Scope) (err error) {
			*out = ResourceEvent_Operation(ResourceEvent_Operation_value[string(*in)])
			return
		},

		// ResourceEvent
		func(in *ResourceEvent, out *api.ResourceEvent, s conversion.Scope) (err error) {
			out.ID = in.Id
			if out.EmittedTime, err = util.Timestamp(in.EmittedTime); err != nil {
				return
			}
			if err = s.Convert(&in.Operation, &out.Operation); err != nil {
				return
			}
			if out.New, err = fromAny(in.New, s); err != nil {
				return
			}
			if out.Old, err = fromAny(in.Old, s); err != nil {
				return
			}

			return
		},
		func(in *api.ResourceEvent, out *ResourceEvent, s conversion.Scope) (err error) {
			out.Id = in.ID
			if out.EmittedTime, err = util.TimestampProto(in.EmittedTime); err != nil {
				return
			}
			if err = s.Convert(&in.Operation, &out.Operation); err != nil {
				return
			}
			if out.New, err = toAny(in.New, s); err != nil {
				return
			}
			if out.Old, err = toAny(in.Old, s); err != nil {
				return
			}
			return
		},
	)
}

func toAny(in interface{}, s conversion.Scope) (out *any.Any, err error) {
	if in == nil {
		return
	}
	var pb proto.Message
	switch in.(type) {
	case *api.Printer:
		printer := &Printer{}
		if err = s.Convert(in.(*api.Printer), printer); err != nil {
			return
		}
		pb = printer

	case *api.Print:
		print := &Print{}
		if err = s.Convert(in.(*api.Print), print); err != nil {
			return
		}
		pb = print

	case *api.Volume:
		volume := &Volume{}
		if err = s.Convert(in.(*api.Volume), volume); err != nil {
			return
		}
		pb = volume

	default:
		return nil, fmt.Errorf("unknown resource event data: %#v", in)
	}

	if out, err = util.MarshalAny(pb); err != nil {
		return
	}
	return
}

func fromAny(any *any.Any, s conversion.Scope) (out interface{}, err error) {
	if any == nil {
		return
	}
	var data ptypes.DynamicAny
	if err = util.UnmarshalAny(any, &data); err != nil {
		return
	}
	m := data.Message
	switch m.(type) {
	case *Printer:
		printer := &api.Printer{}
		if err = s.Convert(m.(*Printer), printer); err != nil {
			return
		}
		out = printer

	case *Print:
		print := &api.Print{}
		if err = s.Convert(m.(*Print), print); err != nil {
			return
		}
		out = print

	case *Volume:
		volume := &api.Volume{}
		if err = s.Convert(m.(*Volume), volume); err != nil {
			return
		}
		out = volume

	default:
		return nil, fmt.Errorf("unknown resource event data: %#v", m)
	}
	return
}
