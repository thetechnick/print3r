package v1

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"github.com/thetechnick/print3r/pkg/api"
)

func TestVersion(t *testing.T) {
	v1v := &APIVersion{}
	iv := &api.Version{
		Version:   "1.2.3",
		Branch:    "test",
		GoVersion: "1.2.3",
		Platform:  "linux/amd64",
		BuildDate: time.Now(),
	}

	t.Run("from api.Version to v1.Version", func(t *testing.T) {
		assert := assert.New(t)

		err := Scheme.Convert(iv, v1v)
		if !assert.NoError(err) {
			return
		}

		assert.Equal(iv.Version, v1v.Version)
		assert.Equal(iv.Branch, v1v.Branch)
		assert.Equal(iv.GoVersion, v1v.GoVersion)
		assert.Equal(iv.Platform, v1v.Platform)
		assert.NotNil(v1v.BuildDate)
	})

	t.Run("from v1.Version to api.Version", func(t *testing.T) {
		assert := assert.New(t)
		av := &api.Version{}
		err := Scheme.Convert(v1v, av)

		if !assert.NoError(err) {
			return
		}

		assert.Equal(v1v.Version, av.Version)
		assert.Equal(v1v.Branch, av.Branch)
		assert.Equal(v1v.GoVersion, av.GoVersion)
		assert.Equal(v1v.Platform, av.Platform)
		assert.Equal(iv.BuildDate.Unix(), av.BuildDate.Unix())
	})
}

func TestObjectMeta(t *testing.T) {
	v1v := &ObjectMeta{}
	iv := &api.ObjectMeta{
		UID:             "1",
		Name:            "test",
		ResourceVersion: "1",
		Created:         time.Now(),
		Updated:         time.Now(),
		Labels: map[string]string{
			"label": "value",
		},
	}

	t.Run("from api.ObjectMeta to v1.ObjectMeta", func(t *testing.T) {
		assert := assert.New(t)

		err := Scheme.Convert(iv, v1v)
		if !assert.NoError(err) {
			return
		}

		assert.Equal(iv.UID, v1v.Uid)
		assert.Equal(iv.Name, v1v.Name)
		assert.Equal(iv.ResourceVersion, v1v.ResourceVersion)
		assert.NotNil(v1v.Created)
		assert.NotNil(v1v.Updated)
		if assert.NotNil(v1v.Labels) {
			l, ok := v1v.Labels["label"]
			assert.True(ok, "labels key should be present in metadata.labels")
			assert.Equal("value", l)
		}
	})

	t.Run("from v1.ObjectMeta to api.ObjectMeta", func(t *testing.T) {
		assert := assert.New(t)
		av := &api.ObjectMeta{}
		err := Scheme.Convert(v1v, av)

		if !assert.NoError(err) {
			return
		}

		assert.Equal(v1v.Uid, av.UID)
		assert.Equal(v1v.Name, av.Name)
		assert.Equal(v1v.ResourceVersion, av.ResourceVersion)
		assert.Equal(iv.Created.Unix(), av.Created.Unix())
		assert.Equal(iv.Updated.Unix(), av.Updated.Unix())
		if assert.NotNil(av.Labels) {
			l, ok := av.Labels["label"]
			assert.True(ok, "labels key should be present in metadata.labels")
			assert.Equal("value", l)
		}
	})
}

func TestObjectReference(t *testing.T) {
	v1v := &ObjectReference{}
	iv := &api.ObjectReference{
		UID:  "1",
		Name: "test",
	}

	t.Run("from api.ObjectReference to v1.ObjectReference", func(t *testing.T) {
		assert := assert.New(t)

		err := Scheme.Convert(iv, v1v)
		if !assert.NoError(err) {
			return
		}

		assert.Equal(iv.UID, v1v.Uid)
		assert.Equal(iv.Name, v1v.Name)
	})

	t.Run("from v1.ObjectReference to api.ObjectReference", func(t *testing.T) {
		assert := assert.New(t)
		av := &api.ObjectReference{}
		err := Scheme.Convert(v1v, av)

		if !assert.NoError(err) {
			return
		}

		assert.Equal(v1v.Uid, av.UID)
		assert.Equal(v1v.Name, av.Name)
	})
}

func TestVolume(t *testing.T) {
	oapi1 := &api.Volume{
		ObjectMeta: api.ObjectMeta{
			Name: "test",
		},
		Spec: api.VolumeSpec{
			Local: &api.LocalVolumeSource{Path: "/path"},
		},
		Status: api.VolumeStatus{},
	}
	opb := &Volume{}

	t.Run("from api.Volume to v1.Volume", func(t *testing.T) {
		assert := assert.New(t)

		err := Scheme.Convert(oapi1, opb)
		if !assert.NoError(err) {
			return
		}

		assert.Equal(oapi1.Spec.Local.Path, opb.Spec.Local.Path)
		assert.Equal(oapi1.Name, opb.Metadata.Name)
		if assert.NotNil(opb.Status) {
		}
	})

	t.Run("from v1.Volume to api.Volume", func(t *testing.T) {
		assert := assert.New(t)
		oapi2 := &api.Volume{}

		err := Scheme.Convert(opb, oapi2)
		if !assert.NoError(err) {
			return
		}

		assert.Equal(oapi1.Spec.Local.Path, oapi2.Spec.Local.Path)
		assert.Equal(oapi1.Name, oapi2.Name)
	})
}

func TestResourceEvent(t *testing.T) {
	oapi1 := &api.ResourceEvent{
		ID:          "1234",
		EmittedTime: time.Now(),
		Operation:   api.ResourceEventOperationDeleted,
		Old: &api.Printer{
			Spec: api.PrinterSpec{
				State: api.PrinterSpecStateConnected,
			},
			Status: api.PrinterStatus{
				Conditions: api.ConditionMapCollection{},
			},
		},
		New: nil,
	}
	opb := &ResourceEvent{}

	t.Run("from api.ResourceEvent to v1.ResourceEvent", func(t *testing.T) {
		err := Scheme.Convert(oapi1, opb)
		if !assert.NoError(t, err) {
			return
		}

		assert.Equal(t, oapi1.ID, opb.Id)
		assert.NotNil(t, opb.EmittedTime)
		assert.Equal(t, ResourceEvent_Deleted, opb.Operation)
		if assert.NotNil(t, opb.Old) {
			assert.Equal(t, "type.print3r.xyz/print3r.api.v1.Printer", opb.Old.GetTypeUrl())
		}
		assert.Nil(t, opb.New)
	})

	t.Run("from v1.ResourceEvent to api.ResourceEvent", func(t *testing.T) {
		oapi2 := &api.ResourceEvent{}

		err := Scheme.Convert(opb, oapi2)
		if !assert.NoError(t, err) {
			return
		}

		assert.Equal(t, "1234", oapi2.ID)
		assert.Equal(t, oapi1.EmittedTime.UTC(), oapi2.EmittedTime.UTC())
		assert.Equal(t, oapi1.Operation, oapi2.Operation)
		assert.Equal(t, oapi1.Old, oapi2.Old)
		assert.Equal(t, oapi1.New, oapi2.New)
	})
}
