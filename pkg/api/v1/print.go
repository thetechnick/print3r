package v1

import (
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/runtime/conversion"
)

var printConverters = []interface{}{
	// PrintListRequest
	func(in *PrintListRequest, out *api.PrintListRequest, s conversion.Scope) (err error) {
		out.Continue = in.Continue
		out.Selector = in.Selector
		out.Limit = int(in.Limit)
		return
	},
	func(in *api.PrintListRequest, out *PrintListRequest, s conversion.Scope) (err error) {
		out.Continue = in.Continue
		out.Selector = in.Selector
		out.Limit = int64(in.Limit)
		return
	},

	// FileRef
	func(in *FileRef, out *api.FileRef, s conversion.Scope) (err error) {
		out.File = in.File
		out.Volume = in.Volume
		return
	},
	func(in *api.FileRef, out *FileRef, s conversion.Scope) (err error) {
		out.File = in.File
		out.Volume = in.Volume
		return
	},

	// PrintList
	func(in *PrintList, out *api.PrintList, s conversion.Scope) (err error) {
		if err = s.Convert(in.Metadata, &out.ListMeta); err != nil {
			return
		}
		if err = s.Convert(&in.Items, &out.Items); err != nil {
			return
		}
		return
	},
	func(in *api.PrintList, out *PrintList, s conversion.Scope) (err error) {
		out.Metadata = &ListMeta{}
		if err = s.Convert(&in.ListMeta, out.Metadata); err != nil {
			return
		}
		if err = s.Convert(&in.Items, &out.Items); err != nil {
			return
		}
		return
	},

	// []Print
	func(in *[]*Print, out *[]*api.Print, s conversion.Scope) (err error) {
		if in == nil {
			return
		}
		for _, f := range *in {
			outPrint := &api.Print{}
			if err = s.Convert(f, outPrint); err != nil {
				return
			}
			*out = append(*out, outPrint)
		}
		return
	},
	func(in *[]*api.Print, out *[]*Print, s conversion.Scope) (err error) {
		if in == nil {
			return
		}
		for _, f := range *in {
			outPrint := &Print{}
			if err = s.Convert(f, outPrint); err != nil {
				return
			}
			*out = append(*out, outPrint)
		}
		return
	},

	// Print
	func(in *Print, out *api.Print, s conversion.Scope) (err error) {
		if err = s.Convert(in.Metadata, &out.ObjectMeta); err != nil {
			return
		}
		if in.Spec != nil {
			if err = s.Convert(in.Spec, &out.Spec); err != nil {
				return
			}
		}
		if in.Status != nil {
			if err = s.Convert(in.Status, &out.Status); err != nil {
				return
			}
		}
		return
	},
	func(in *api.Print, out *Print, s conversion.Scope) (err error) {
		out.Metadata = &ObjectMeta{}
		if err = s.Convert(&in.ObjectMeta, out.Metadata); err != nil {
			return
		}
		out.Spec = &PrintSpec{}
		if err = s.Convert(&in.Spec, out.Spec); err != nil {
			return
		}
		out.Status = &PrintStatus{}
		if err = s.Convert(&in.Status, out.Status); err != nil {
			return
		}
		return
	},

	// PrintStatus
	func(in *PrintStatus, out *api.PrintStatus, s conversion.Scope) (err error) {
		out.Progress = in.Progress
		if err = s.Convert(&in.Conditions, &out.Conditions); err != nil {
			return
		}
		return
	},
	func(in *api.PrintStatus, out *PrintStatus, s conversion.Scope) (err error) {
		out.Progress = in.Progress
		if err = s.Convert(&in.Conditions, &out.Conditions); err != nil {
			return
		}
		return
	},

	// PrintSpec
	func(in *PrintSpec, out *api.PrintSpec, s conversion.Scope) (err error) {
		out.Printer = in.Printer
		if err = s.Convert(in.FileRef, &out.FileRef); err != nil {
			return
		}
		if err = s.Convert(&in.State, &out.State); err != nil {
			return
		}
		return
	},
	func(in *api.PrintSpec, out *PrintSpec, s conversion.Scope) (err error) {
		out.Printer = in.Printer
		out.FileRef = &FileRef{}
		if err = s.Convert(&in.FileRef, out.FileRef); err != nil {
			return
		}
		if err = s.Convert(&in.State, &out.State); err != nil {
			return
		}
		return
	},

	// PrintSpecState
	func(in *PrintSpec_State, out *api.PrintSpecState, s conversion.Scope) (err error) {
		n, ok := PrintSpec_State_name[int32(*in)]
		if !ok {
			*out = api.PrintSpecStateUnknown
			return
		}
		*out = api.PrintSpecState(n)
		return
	},
	func(in *api.PrintSpecState, out *PrintSpec_State, s conversion.Scope) (err error) {
		v, ok := PrintSpec_State_value[string(*in)]
		if !ok {
			*out = PrintSpec_Unknown
			return
		}
		*out = PrintSpec_State(v)
		return
	},
}
