package v1

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/thetechnick/print3r/pkg/api"
)

func TestPrint(t *testing.T) {
	v1v := &Print{}
	iv := &api.Print{
		Spec: api.PrintSpec{
			Printer: "test",
			FileRef: api.FileRef{
				File:   "test",
				Volume: "test",
			},
			State: api.PrintSpecStatePrinted,
		},
		Status: api.PrintStatus{
			Progress:   78,
			Conditions: api.ConditionMapCollection{},
		},
	}
	iv.Status.Conditions.SetMultiple(
		api.Condition{
			Type:   "Started",
			Status: api.ConditionStatusTrue,
		},
		api.Condition{
			Type:   "Heated",
			Status: api.ConditionStatusTrue,
		},
		api.Condition{
			Type:   "Paused",
			Status: api.ConditionStatusFalse,
		},
		api.Condition{
			Type:   "Finished",
			Status: api.ConditionStatusFalse,
		})
	t.Run("from api.Print to v1.Print", func(t *testing.T) {
		err := Scheme.Convert(iv, v1v)
		if !assert.NoError(t, err) {
			return
		}

		assert.NotNil(t, v1v.Spec.FileRef)
		assert.NotNil(t, v1v.Spec.Printer)
		assert.Equal(t, iv.Status.Progress, v1v.Status.Progress)
		assert.Equal(t, PrintSpec_Printed, v1v.Spec.State)
	})

	t.Run("from v1.Print to api.Print", func(t *testing.T) {
		av := &api.Print{}
		err := Scheme.Convert(v1v, av)

		if !assert.NoError(t, err) {
			return
		}

		assert.Equal(t, iv.Spec.FileRef, av.Spec.FileRef)
		assert.Equal(t, iv.Spec.Printer, av.Spec.Printer)
		assert.Equal(t, iv.Status.Progress, av.Status.Progress)
		assert.Equal(t, iv.Spec.State, av.Spec.State)
	})
}
