package v1

import (
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/runtime/conversion"
	"github.com/thetechnick/print3r/pkg/util"
)

var printerConverters = []interface{}{
	func(in *api.PrinterListRequest, out *PrinterListRequest, s conversion.Scope) (err error) {
		out.Continue = in.Continue
		out.Selector = in.Selector
		out.Limit = int64(in.Limit)
		return
	},
	func(in *PrinterListRequest, out *api.PrinterListRequest, s conversion.Scope) (err error) {
		out.Continue = in.Continue
		out.Selector = in.Selector
		out.Limit = int(in.Limit)
		return
	},
	func(in *api.PrinterTemperatureHistoryRequest, out *PrinterTemperatureHistoryRequest, s conversion.Scope) (err error) {
		if out.Start, err = util.TimestampProto(in.Start); err != nil {
			return
		}
		if out.End, err = util.TimestampProto(in.End); err != nil {
			return
		}
		out.Size = int64(in.Size)
		out.Name = in.Name
		return
	},
	func(in *PrinterTemperatureHistoryRequest, out *api.PrinterTemperatureHistoryRequest, s conversion.Scope) (err error) {
		if out.Start, err = util.Timestamp(in.Start); err != nil {
			return
		}
		if out.End, err = util.Timestamp(in.End); err != nil {
			return
		}
		out.Size = int(in.Size)
		out.Name = in.Name
		return
	},

	func(in *api.PrinterTemperatureHistory, out *PrinterTemperatureHistory, s conversion.Scope) (err error) {
		for _, p := range in.BedActual.Get() {
			op := &PrinterTemperatureHistoryPoint{}
			if err = s.Convert(p, op); err != nil {
				return
			}
			out.BedActual = append(out.BedActual, op)
		}
		for _, p := range in.BedTarget.Get() {
			op := &PrinterTemperatureHistoryPoint{}
			if err = s.Convert(p, op); err != nil {
				return
			}
			out.BedTarget = append(out.BedTarget, op)
		}

		for _, p := range in.ExtruderActual.Get() {
			op := &PrinterTemperatureHistoryPoint{}
			if err = s.Convert(p, op); err != nil {
				return
			}
			out.ExtruderActual = append(out.ExtruderActual, op)
		}

		for _, p := range in.ExtruderTarget.Get() {
			op := &PrinterTemperatureHistoryPoint{}
			if err = s.Convert(p, op); err != nil {
				return
			}
			out.ExtruderTarget = append(out.ExtruderTarget, op)
		}
		return
	},
	func(in *api.TimeSeriesPoint, out *PrinterTemperatureHistoryPoint, s conversion.Scope) (err error) {
		if out.Time, err = util.TimestampProto(in.Time); err != nil {
			return
		}
		out.Value = float32(in.Value)
		return
	},

	func(in *PrinterList, out *api.PrinterList, s conversion.Scope) (err error) {
		if err = s.Convert(in.Metadata, &out.ListMeta); err != nil {
			return
		}
		if err = s.Convert(&in.Items, &out.Items); err != nil {
			return
		}
		return
	},
	func(in *api.PrinterList, out *PrinterList, s conversion.Scope) (err error) {
		out.Metadata = &ListMeta{}
		if err = s.Convert(&in.ListMeta, out.Metadata); err != nil {
			return
		}
		if err = s.Convert(&in.Items, &out.Items); err != nil {
			return
		}
		return
	},

	// []Printer
	func(in *[]*Printer, out *[]*api.Printer, s conversion.Scope) (err error) {
		if in == nil {
			return
		}
		for _, f := range *in {
			outPrinter := &api.Printer{}
			if err = s.Convert(f, outPrinter); err != nil {
				return
			}
			*out = append(*out, outPrinter)
		}
		return
	},
	func(in *[]*api.Printer, out *[]*Printer, s conversion.Scope) (err error) {
		if in == nil {
			return
		}
		for _, f := range *in {
			outPrinter := &Printer{}
			if err = s.Convert(f, outPrinter); err != nil {
				return
			}
			*out = append(*out, outPrinter)
		}
		return
	},

	// Printer
	func(in *Printer, out *api.Printer, s conversion.Scope) (err error) {
		if err = s.Convert(in.Metadata, &out.ObjectMeta); err != nil {
			return
		}
		if in.Spec != nil {
			if err = s.Convert(in.Spec, &out.Spec); err != nil {
				return
			}
		}
		if in.Status != nil {
			if err = s.Convert(in.Status, &out.Status); err != nil {
				return
			}
		}
		return
	},
	func(in *api.Printer, out *Printer, s conversion.Scope) (err error) {
		out.Metadata = &ObjectMeta{}
		if err = s.Convert(&in.ObjectMeta, out.Metadata); err != nil {
			return
		}
		out.Spec = &PrinterSpec{}
		if err = s.Convert(&in.Spec, out.Spec); err != nil {
			return
		}
		out.Status = &PrinterStatus{}
		if err = s.Convert(&in.Status, out.Status); err != nil {
			return
		}
		return
	},

	// PrinterSpec
	func(in *PrinterSpec, out *api.PrinterSpec, s conversion.Scope) (err error) {
		out.Model = in.Model
		out.Device = in.Device
		out.BaudRate = in.BaudRate
		out.VideoStream = in.VideoStream
		if err = s.Convert(&in.State, &out.State); err != nil {
			return
		}
		return
	},
	func(in *api.PrinterSpec, out *PrinterSpec, s conversion.Scope) (err error) {
		out.Model = in.Model
		out.Device = in.Device
		out.BaudRate = in.BaudRate
		out.VideoStream = in.VideoStream
		if err = s.Convert(&in.State, &out.State); err != nil {
			return
		}
		return
	},

	// PrinterSpecState
	func(in *PrinterSpec_State, out *api.PrinterSpecState, s conversion.Scope) (err error) {
		n, ok := PrinterSpec_State_name[int32(*in)]
		if !ok {
			*out = api.PrinterSpecStateUnknown
			return
		}
		*out = api.PrinterSpecState(n)
		return
	},
	func(in *api.PrinterSpecState, out *PrinterSpec_State, s conversion.Scope) (err error) {
		v, ok := PrinterSpec_State_value[string(*in)]
		if !ok {
			*out = PrinterSpec_Unknown
			return
		}
		*out = PrinterSpec_State(v)
		return
	},

	// PrinterTemperature
	func(in *PrinterTemperature, out *api.PrinterTemperature, s conversion.Scope) (err error) {
		out.ExtruderTarget = in.ExtruderTarget
		out.ExtruderActual = in.ExtruderActual
		out.BedTarget = in.BedTarget
		out.BedActual = in.BedActual
		return
	},
	func(in *api.PrinterTemperature, out *PrinterTemperature, s conversion.Scope) (err error) {
		out.ExtruderTarget = in.ExtruderTarget
		out.ExtruderActual = in.ExtruderActual
		out.BedTarget = in.BedTarget
		out.BedActual = in.BedActual
		return
	},

	// PrinterStatus
	func(in *PrinterStatus, out *api.PrinterStatus, s conversion.Scope) (err error) {
		if err = s.Convert(&in.Conditions, &out.Conditions); err != nil {
			return
		}
		if in.Print != nil {
			out.Print = &api.PrinterPrintStatus{}
			if err = s.Convert(in.Print, out.Print); err != nil {
				return
			}
		}
		if in.Temperature != nil {
			out.Temperature = &api.PrinterTemperature{}
			if err = s.Convert(in.Temperature, out.Temperature); err != nil {
				return
			}
		}
		return
	},
	func(in *api.PrinterStatus, out *PrinterStatus, s conversion.Scope) (err error) {
		if err = s.Convert(&in.Conditions, &out.Conditions); err != nil {
			return
		}
		if in.Print != nil {
			out.Print = &PrinterPrintStatus{}
			if err = s.Convert(in.Print, out.Print); err != nil {
				return
			}
		}
		if in.Temperature != nil {
			out.Temperature = &PrinterTemperature{}
			if err = s.Convert(in.Temperature, out.Temperature); err != nil {
				return
			}
		}
		return
	},

	// PrinterPrintStatus
	func(in *PrinterPrintStatus, out *api.PrinterPrintStatus, s conversion.Scope) (err error) {
		out.Name = in.Name
		return
	},
	func(in *api.PrinterPrintStatus, out *PrinterPrintStatus, s conversion.Scope) (err error) {
		out.Name = in.Name
		return
	},
}
