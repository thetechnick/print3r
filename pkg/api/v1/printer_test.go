package v1

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/thetechnick/print3r/pkg/api"
)

func TestPrinter(t *testing.T) {
	oapi1 := &api.Printer{
		Spec: api.PrinterSpec{
			Model:       "test-model",
			Device:      "test-device",
			VideoStream: "test-video",
			BaudRate:    123,
			State:       api.PrinterSpecStateConnected,
		},
		Status: api.PrinterStatus{
			Conditions:  api.ConditionMapCollection{"test": {}},
			Temperature: &api.PrinterTemperature{},
		},
	}
	opb := &Printer{}

	t.Run("from api.Printer to v1.Printer", func(t *testing.T) {
		err := Scheme.Convert(oapi1, opb)
		if !assert.NoError(t, err) {
			return
		}

		assert.Equal(t, oapi1.Spec.Model, opb.Spec.Model)
		assert.Equal(t, oapi1.Spec.VideoStream, opb.Spec.VideoStream)
		assert.Equal(t, oapi1.Spec.Device, opb.Spec.Device)
		assert.Equal(t, oapi1.Spec.BaudRate, opb.Spec.BaudRate)
		assert.Equal(t, PrinterSpec_Connected, opb.Spec.State)
		if assert.NotNil(t, opb.Status) {
			assert.NotNil(t, opb.Status.Temperature)
			assert.Len(t, opb.Status.Conditions, 1)
		}
	})

	t.Run("from v1.Printer to api.Printer", func(t *testing.T) {
		oapi2 := &api.Printer{}

		err := Scheme.Convert(opb, oapi2)
		if !assert.NoError(t, err) {
			return
		}

		assert.Equal(t, oapi1.Spec.Model, oapi2.Spec.Model)
		assert.Equal(t, oapi1.Spec.Device, oapi2.Spec.Device)
		assert.Equal(t, oapi1.Spec.VideoStream, oapi2.Spec.VideoStream)
		assert.Equal(t, oapi1.Spec.BaudRate, oapi2.Spec.BaudRate)
		assert.Equal(t, api.PrinterSpecStateConnected, oapi2.Spec.State)
		if assert.NotNil(t, oapi2.Status) {
			assert.NotNil(t, oapi2.Status.Temperature)
			assert.Len(t, oapi2.Status.Conditions, 1)
		}
	})
}

func TestPrinterTemperature(t *testing.T) {
	oapi1 := &api.PrinterTemperature{
		ExtruderTarget: 0.1,
		ExtruderActual: 0.2,
		BedTarget:      0.3,
		BedActual:      0.4,
	}
	opb := &PrinterTemperature{}

	t.Run("from api.PrinterTemperature to v1.PrinterTemperature", func(t *testing.T) {
		err := Scheme.Convert(oapi1, opb)
		if !assert.NoError(t, err) {
			return
		}

		assert.Equal(t, oapi1.ExtruderTarget, opb.ExtruderTarget)
		assert.Equal(t, oapi1.ExtruderActual, opb.ExtruderActual)
		assert.Equal(t, oapi1.BedTarget, opb.BedTarget)
		assert.Equal(t, oapi1.BedActual, opb.BedActual)
	})

	t.Run("from v1.PrinterTemperature to api.PrinterTemperature", func(t *testing.T) {
		oapi2 := &api.PrinterTemperature{}

		err := Scheme.Convert(opb, oapi2)
		if !assert.NoError(t, err) {
			return
		}

		assert.Equal(t, oapi1.ExtruderTarget, oapi2.ExtruderTarget)
		assert.Equal(t, oapi1.ExtruderActual, oapi2.ExtruderActual)
		assert.Equal(t, oapi1.BedTarget, oapi2.BedTarget)
		assert.Equal(t, oapi1.BedActual, oapi2.BedActual)
	})
}
