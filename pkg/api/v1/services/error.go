package services

import (
	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/printer"
	db "github.com/thetechnick/print3r/pkg/storage"
	"github.com/thetechnick/print3r/pkg/util"
	spb "google.golang.org/genproto/googleapis/rpc/status"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// api.ErrorFieldViolation descriptions
const (
	// NotEmpty is returned when the field cannot be empty
	NotEmpty = "Cannot be empty"

	// MutuallyExclusive is returned when the field cannot be specified with another
	MutuallyExclusive = "Mutually exclusive"
)

func validationError(details ...proto.Message) error {
	s, err := statusWithDetails(codes.InvalidArgument, "invalid request", details...)
	if err != nil {
		log.WithError(err).Error("creating validation error")
		return status.Error(codes.Internal, "error creating reporting status")
	}
	return s.Err()
}

func checkError(err error, log *log.Entry) error {
	if _, ok := status.FromError(err); ok {
		return err
	}
	switch err {
	case db.ErrNotFound:
		return status.New(codes.NotFound, "not found").Err()
	case db.ErrNameConflict:
		return status.New(codes.AlreadyExists, err.Error()).Err()
	case db.ErrIdentifierMissing:
		return status.New(codes.InvalidArgument, err.Error()).Err()
	case db.ErrResourceVersionChanged:
		return status.New(codes.Aborted, err.Error()).Err()
	case printer.ErrPrinterNotConnected:
		return status.New(codes.FailedPrecondition, err.Error()).Err()
	}
	log.WithError(err).Warn("internal error")
	return status.New(codes.Internal, "internal error").Err()
}

func statusWithDetails(c codes.Code, msg string, details ...proto.Message) (*status.Status, error) {
	p := &spb.Status{Code: int32(c), Message: msg}
	if c == codes.OK {
		return status.FromProto(p), nil
	}
	for _, detail := range details {
		any, err := util.MarshalAny(detail)
		if err != nil {
			return nil, err
		}
		p.Details = append(p.Details, any)
	}
	return status.FromProto(p), nil
}
