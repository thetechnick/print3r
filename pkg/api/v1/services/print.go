package services

import (
	"context"
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/v1"
	db "github.com/thetechnick/print3r/pkg/storage"
	"github.com/thetechnick/print3r/pkg/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type printService struct {
	storage db.PrintStorage
	limiter db.PrintLimiter
	log     *log.Entry
}

// NewPrintServiceServer returns a new PrintServiceServer
func NewPrintServiceServer(
	storage db.PrintStorage,
	limiter db.PrintLimiter,
) v1.PrintServiceServer {
	return &printService{
		storage: storage,
		limiter: limiter,
		log:     log.WithField("module", "v1.services.PrintServiceServer"),
	}
}

func (s *printService) checkError(err error) error {
	return checkError(err, s.log)
}

func (s *printService) Create(ctx context.Context, req *v1.Print) (res *v1.Print, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.Print{}
	if err = validatePrint(req); err != nil {
		return
	}
	if req.Spec.State.String() == "" {
		req.Spec.State = v1.PrintSpec_Printed
	}

	obj := api.NewPrint()
	if err = v1.Scheme.Convert(req, obj); err != nil {
		return
	}
	if obj.Name != "" {
		if err = s.storage.Create(ctx, obj); err != nil {
			return
		}
	} else {
		for {
			// retry generating name on conflict
			fileName := strings.Replace(obj.Spec.FileName(), ".gcode", "", 1)
			obj.Name = fmt.Sprintf("%s-%s", fileName, util.RandNameSuffix(8))
			if err = s.storage.Create(ctx, obj); err != nil {
				if err != db.ErrNameConflict {
					return
				}
				continue
			}
			break
		}
	}
	if err = v1.Scheme.Convert(obj, res); err != nil {
		return
	}
	return
}

func (s *printService) Delete(ctx context.Context, req *v1.Print) (res *v1.Print, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.Print{}

	if err = validatePrintMetadata(req); err != nil {
		return
	}

	var p *api.Print
	p, err = s.storage.FindOneByName(ctx, req.Metadata.Name)
	if err != nil {
		return
	}
	if p == nil {
		err = status.Error(codes.NotFound, "print not found")
		return
	}

	if p, err = s.storage.Delete(ctx, p); err != nil {
		return
	}

	if err = v1.Scheme.Convert(p, res); err != nil {
		return
	}
	return
}

func (s *printService) List(ctx context.Context, req *v1.PrintListRequest) (res *v1.PrintList, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.PrintList{}

	var (
		selector     *api.LabelSelector
		all          []*api.Print
		responseList []*api.Print
		cursor       string
	)

	if req.Continue != "" {
		responseList, err = s.limiter.Continue(ctx, req.Continue, int(req.Limit))
		if err != nil {
			return
		}
	} else {
		selector, err = api.ParseSelector(req.Selector)
		if err != nil {
			err = status.Error(codes.InvalidArgument, err.Error())
			return
		}

		all, err = s.storage.All(ctx)
		if err != nil {
			return
		}
		if selector != nil {
			for _, i := range all {
				if selector.Match(i.Labels) {
					responseList = append(responseList, i)
				}
			}
		} else {
			responseList = all
		}
		responseList, cursor = s.limiter.Limit(ctx, responseList, int(req.Limit))
	}

	res = &v1.PrintList{
		Metadata: &v1.ListMeta{
			Continue: cursor,
		},
	}
	printerListItems := []*v1.Print{}
	if err = v1.Scheme.Convert(&responseList, &printerListItems); err != nil {
		return
	}
	res.Items = printerListItems
	return
}

func (s *printService) Show(ctx context.Context, req *v1.Print) (res *v1.Print, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.Print{}

	if err = validatePrintMetadata(req); err != nil {
		return
	}

	obj, err := s.storage.FindOneByName(ctx, req.Metadata.Name)
	if err != nil {
		return
	}
	if obj == nil {
		err = status.Error(codes.NotFound, "print not found")
		return
	}
	if err = v1.Scheme.Convert(obj, res); err != nil {
		return
	}
	return
}

func (s *printService) Update(ctx context.Context, req *v1.Print) (res *v1.Print, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.Print{}
	if err = validatePrint(req); err != nil {
		return
	}

	obj := api.NewPrint()
	if err = v1.Scheme.Convert(req, obj); err != nil {
		return
	}
	if err = s.storage.Update(ctx, obj); err != nil {
		return
	}
	if err = v1.Scheme.Convert(obj, res); err != nil {
		return
	}
	return
}

func (s *printService) UpdateStatus(ctx context.Context, req *v1.Print) (res *v1.Print, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.Print{}
	if err = validatePrint(req); err != nil {
		return
	}

	obj := api.NewPrint()
	if err = v1.Scheme.Convert(req, obj); err != nil {
		return
	}
	if err = s.storage.UpdateStatus(ctx, obj); err != nil {
		return
	}
	if err = v1.Scheme.Convert(obj, res); err != nil {
		return
	}
	return
}
