package services

import (
	"context"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/v1"
	"github.com/thetechnick/print3r/pkg/printer"
	db "github.com/thetechnick/print3r/pkg/storage"
	"github.com/thetechnick/print3r/pkg/timeseries"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type printerServiceServer struct {
	storage db.PrinterStorage
	limiter db.PrinterLimiter
	cm      printer.ConnectionManager
	log     *log.Entry
}

// NewPrinterServiceServer returns a new PrinterServiceServer
func NewPrinterServiceServer(
	storage db.PrinterStorage,
	limiter db.PrinterLimiter,
	cm printer.ConnectionManager,
) v1.PrinterServiceServer {
	return &printerServiceServer{
		storage: storage,
		limiter: limiter,
		cm:      cm,
		log:     log.WithField("module", "v1.services.PrinterServiceServer"),
	}
}

func (s *printerServiceServer) checkError(err error) error {
	return checkError(err, s.log)
}

func (s *printerServiceServer) TemperatureHistory(ctx context.Context, req *v1.PrinterTemperatureHistoryRequest) (res *v1.PrinterTemperatureHistory, err error) {
	defer func() {
		err = s.checkError(err)
	}()

	res = &v1.PrinterTemperatureHistory{}
	var (
		p          *api.Printer
		history    *printer.TemperatureHistory
		apiHistory = &api.PrinterTemperatureHistory{}
		request    = &api.PrinterTemperatureHistoryRequest{}
	)
	if err = v1.Scheme.Convert(req, request); err != nil {
		return
	}
	if request.Start.IsZero() {
		err = status.Error(codes.InvalidArgument, "start must be specified")
		return
	}
	if request.End.IsZero() {
		err = status.Error(codes.InvalidArgument, "end must be specified")
		return
	}
	if request.Size < 1 {
		err = status.Error(codes.InvalidArgument, "size must be positive")
		return
	}

	p, err = s.storage.FindOneByName(ctx, request.Name)
	if err != nil {
		return
	}
	if p == nil {
		err = status.Error(codes.NotFound, "printer not found")
		return
	}

	history, err = s.cm.TemperatureHistory(ctx, p)
	if err != nil {
		return
	}

	history.RLock()
	defer history.RUnlock()
	if history.BedActual != nil {
		apiHistory.BedActual = toTimeSeries(history.BedActual, request.Start, request.End, request.Size)
	}
	if history.BedTarget != nil {
		apiHistory.BedTarget = toTimeSeries(history.BedTarget, request.Start, request.End, request.Size)
	}
	if history.ExtruderActual != nil {
		apiHistory.ExtruderActual = toTimeSeries(history.ExtruderActual, request.Start, request.End, request.Size)
	}
	if history.ExtruderTarget != nil {
		apiHistory.ExtruderTarget = toTimeSeries(history.ExtruderTarget, request.Start, request.End, request.Size)
	}

	if err = v1.Scheme.Convert(apiHistory, res); err != nil {
		return
	}
	return
}

func toTimeSeries(in *timeseries.TimeSeries, start, end time.Time, num int) *api.TimeSeries {
	o := in.Range(start, end, num, timeseries.Avg)
	ts := api.TimeSeries{}

	for _, v := range o.Points() {
		ts = append(ts, &api.TimeSeriesPoint{
			Time:  v.Time(),
			Value: v.Value(),
		})
	}
	if len(ts) == 0 {
		ts = append(ts, &api.TimeSeriesPoint{
			Time:  start,
			Value: 0,
		}, &api.TimeSeriesPoint{
			Time:  end,
			Value: 0,
		})
	} else if start.Before(ts[0].Time) {
		ts = append(api.TimeSeries{{Time: start, Value: 0}}, ts...)
	}

	return &ts
}

func (s *printerServiceServer) Create(ctx context.Context, req *v1.Printer) (res *v1.Printer, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.Printer{}

	if err = validatePrinter(req); err != nil {
		return
	}

	obj := api.NewPrinter()
	if err = v1.Scheme.Convert(req, obj); err != nil {
		return
	}
	if err = s.storage.Create(ctx, obj); err != nil {
		return
	}
	if err = v1.Scheme.Convert(obj, res); err != nil {
		return
	}
	return
}

func (s *printerServiceServer) Delete(ctx context.Context, req *v1.Printer) (res *v1.Printer, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.Printer{}

	if err = validatePrinterMetadata(req); err != nil {
		return
	}

	var p *api.Printer
	p, err = s.storage.FindOneByName(ctx, req.Metadata.Name)
	if err != nil {
		return
	}
	if p == nil {
		err = status.Error(codes.NotFound, "printer not found")
		return
	}

	if p, err = s.storage.Delete(ctx, p); err != nil {
		return
	}

	if err = v1.Scheme.Convert(p, res); err != nil {
		return
	}
	return
}

func (s *printerServiceServer) List(ctx context.Context, req *v1.PrinterListRequest) (res *v1.PrinterList, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.PrinterList{}

	var (
		selector     *api.LabelSelector
		all          []*api.Printer
		responseList []*api.Printer
		cursor       string
	)

	if req.Continue != "" {
		responseList, err = s.limiter.Continue(ctx, req.Continue, int(req.Limit))
		if err != nil {
			return
		}
	} else {
		selector, err = api.ParseSelector(req.Selector)
		if err != nil {
			err = status.Error(codes.InvalidArgument, err.Error())
			return
		}

		all, err = s.storage.All(ctx)
		if err != nil {
			return
		}
		if selector != nil {
			for _, i := range all {
				if selector.Match(i.Labels) {
					responseList = append(responseList, i)
				}
			}
		} else {
			responseList = all
		}
		responseList, cursor = s.limiter.Limit(ctx, responseList, int(req.Limit))
	}

	res = &v1.PrinterList{
		Metadata: &v1.ListMeta{
			Continue: cursor,
		},
	}
	printerListItems := []*v1.Printer{}
	if err = v1.Scheme.Convert(&responseList, &printerListItems); err != nil {
		return
	}
	res.Items = printerListItems
	return
}

func (s *printerServiceServer) Show(ctx context.Context, req *v1.Printer) (res *v1.Printer, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.Printer{}

	if err = validatePrinterMetadata(req); err != nil {
		return
	}

	obj, err := s.storage.FindOneByName(ctx, req.Metadata.Name)
	if err != nil {
		return
	}
	if obj == nil {
		err = status.Error(codes.NotFound, "printer not found")
		return
	}
	if err = v1.Scheme.Convert(obj, res); err != nil {
		return
	}
	return
}

func (s *printerServiceServer) Update(ctx context.Context, req *v1.Printer) (res *v1.Printer, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.Printer{}
	if err = validatePrinter(req); err != nil {
		return
	}

	obj := api.NewPrinter()
	if err = v1.Scheme.Convert(req, obj); err != nil {
		return
	}
	if err = s.storage.Update(ctx, obj); err != nil {
		return
	}
	if err = v1.Scheme.Convert(obj, res); err != nil {
		return
	}
	return
}

func (s *printerServiceServer) UpdateStatus(ctx context.Context, req *v1.Printer) (res *v1.Printer, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.Printer{}
	if err = validatePrinter(req); err != nil {
		return
	}

	obj := api.NewPrinter()
	if err = v1.Scheme.Convert(req, obj); err != nil {
		return
	}
	if err = s.storage.UpdateStatus(ctx, obj); err != nil {
		return
	}
	if err = v1.Scheme.Convert(obj, res); err != nil {
		return
	}
	return
}
