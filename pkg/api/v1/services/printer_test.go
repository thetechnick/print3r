package services

import (
	"context"
	"testing"

	"github.com/golang/protobuf/ptypes"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/v1"
	"github.com/thetechnick/print3r/pkg/printer"
	printertest "github.com/thetechnick/print3r/pkg/printer/test"
	dbtest "github.com/thetechnick/print3r/pkg/storage/test"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func TestPrinterService(t *testing.T) {
	t.Run("TemperatureHistory", func(t *testing.T) {
		t.Run("not found", func(t *testing.T) {
			var (
				storage dbtest.PrinterStorageMock
				limiter dbtest.PrinterLimiterMock
				cm      printertest.ConnectionManagerMock
				p       *api.Printer
			)
			service := NewPrinterServiceServer(&storage, &limiter, &cm)

			storage.On("FindOneByName", mock.Anything, "").Return(p, nil)

			ctx := context.Background()
			req := &v1.PrinterTemperatureHistoryRequest{
				Start: ptypes.TimestampNow(),
				End:   ptypes.TimestampNow(),
				Size:  10,
			}
			_, err := service.TemperatureHistory(ctx, req)
			if assert.Error(t, err) {
				status, ok := status.FromError(err)
				if assert.True(t, ok) {
					assert.Equal(t, codes.NotFound, status.Code())
				}
			}
		})

		var (
			storage dbtest.PrinterStorageMock
			limiter dbtest.PrinterLimiterMock
			cm      printertest.ConnectionManagerMock
			p       = &api.Printer{}
			history = &printer.TemperatureHistory{}
		)
		service := NewPrinterServiceServer(&storage, &limiter, &cm)

		storage.On("FindOneByName", mock.Anything, mock.Anything).Return(p, nil)
		cm.On("TemperatureHistory", mock.Anything, mock.Anything).Return(history, nil)

		ctx := context.Background()
		req := &v1.PrinterTemperatureHistoryRequest{
			Name:  "test",
			Start: ptypes.TimestampNow(),
			End:   ptypes.TimestampNow(),
			Size:  10,
		}
		resp, err := service.TemperatureHistory(ctx, req)
		if assert.NoError(t, err) {
			assert.NotNil(t, resp)
		}
		storage.AssertCalled(t, "FindOneByName", mock.Anything, "test")
		cm.AssertCalled(t, "TemperatureHistory", mock.Anything, p)
	})

	t.Run("Create", func(t *testing.T) {
		var (
			storage dbtest.PrinterStorageMock
			limiter dbtest.PrinterLimiterMock
			cm      printertest.ConnectionManagerMock
		)
		service := NewPrinterServiceServer(&storage, &limiter, &cm)

		storage.On("Create", mock.Anything, mock.Anything).Return(nil)

		ctx := context.Background()
		req := &v1.Printer{
			Metadata: &v1.ObjectMeta{
				Name: "test",
			},
			Spec: &v1.PrinterSpec{
				BaudRate: 123,
				Device:   "123",
			},
		}
		resp, err := service.Create(ctx, req)
		if assert.NoError(t, err) {
			assert.NotNil(t, resp)
		}
		storage.AssertCalled(t, "Create", mock.Anything, mock.Anything)
	})

	t.Run("Delete", func(t *testing.T) {
		var (
			storage dbtest.PrinterStorageMock
			limiter dbtest.PrinterLimiterMock
			cm      printertest.ConnectionManagerMock
			p       = &api.Printer{}
		)
		service := NewPrinterServiceServer(&storage, &limiter, &cm)

		storage.On("FindOneByName", mock.Anything, mock.Anything).Return(p, nil)
		storage.On("Delete", mock.Anything, mock.Anything).Return(p, nil)

		ctx := context.Background()
		req := &v1.Printer{
			Metadata: &v1.ObjectMeta{
				Name: "test",
			},
		}
		resp, err := service.Delete(ctx, req)
		if assert.NoError(t, err) {
			assert.NotNil(t, resp)
		}
		storage.AssertCalled(t, "FindOneByName", mock.Anything, "test")
		storage.AssertCalled(t, "Delete", mock.Anything, p)
	})

	t.Run("UpdateStatus", func(t *testing.T) {
		var (
			storage dbtest.PrinterStorageMock
			limiter dbtest.PrinterLimiterMock
			cm      printertest.ConnectionManagerMock
		)
		service := NewPrinterServiceServer(&storage, &limiter, &cm)

		storage.On("UpdateStatus", mock.Anything, mock.Anything).Return(nil)

		ctx := context.Background()
		req := &v1.Printer{
			Metadata: &v1.ObjectMeta{
				Name: "test",
			},
			Spec: &v1.PrinterSpec{
				BaudRate: 123,
				Device:   "123",
			},
		}
		resp, err := service.UpdateStatus(ctx, req)
		if assert.NoError(t, err) {
			assert.NotNil(t, resp)
		}
		storage.AssertCalled(t, "UpdateStatus", mock.Anything, mock.Anything)
	})

	t.Run("Update", func(t *testing.T) {
		var (
			storage dbtest.PrinterStorageMock
			limiter dbtest.PrinterLimiterMock
			cm      printertest.ConnectionManagerMock
		)
		service := NewPrinterServiceServer(&storage, &limiter, &cm)

		storage.On("Update", mock.Anything, mock.Anything).Return(nil)

		ctx := context.Background()
		req := &v1.Printer{
			Metadata: &v1.ObjectMeta{
				Name: "test",
			},
			Spec: &v1.PrinterSpec{
				BaudRate: 123,
				Device:   "123",
			},
		}
		resp, err := service.Update(ctx, req)
		if assert.NoError(t, err) {
			assert.NotNil(t, resp)
		}
		storage.AssertCalled(t, "Update", mock.Anything, mock.Anything)
	})

	t.Run("Show", func(t *testing.T) {
		var (
			storage dbtest.PrinterStorageMock
			limiter dbtest.PrinterLimiterMock
			cm      printertest.ConnectionManagerMock
			p       = &api.Printer{}
		)
		service := NewPrinterServiceServer(&storage, &limiter, &cm)

		storage.On("FindOneByName", mock.Anything, mock.Anything).Return(p, nil)

		ctx := context.Background()
		req := &v1.Printer{
			Metadata: &v1.ObjectMeta{
				Name: "test",
			},
		}
		resp, err := service.Show(ctx, req)
		if assert.NoError(t, err) {
			assert.NotNil(t, resp)
		}
		storage.AssertCalled(t, "FindOneByName", mock.Anything, "test")
	})

	t.Run("List", func(t *testing.T) {
		var (
			storage dbtest.PrinterStorageMock
			limiter dbtest.PrinterLimiterMock
			cm      printertest.ConnectionManagerMock
			p       = []*api.Printer{{}}
		)
		service := NewPrinterServiceServer(&storage, &limiter, &cm)

		limiter.On("Limit", mock.Anything, mock.Anything, mock.Anything).Return(p, "cursor")
		storage.On("All", mock.Anything).Return(p, nil)

		ctx := context.Background()
		req := &v1.PrinterListRequest{
			Limit:    10,
			Selector: "!hans",
		}
		resp, err := service.List(ctx, req)
		if assert.NoError(t, err) && assert.NotNil(t, resp) {
			assert.Len(t, resp.Items, 1)
			assert.Equal(t, "cursor", resp.Metadata.Continue)
		}
		storage.AssertCalled(t, "All", mock.Anything)
		limiter.AssertCalled(t, "Limit", mock.Anything, mock.Anything, 10)

		t.Run("continue", func(t *testing.T) {
			var (
				storage dbtest.PrinterStorageMock
				limiter dbtest.PrinterLimiterMock
				cm      printertest.ConnectionManagerMock
				p       = []*api.Printer{{}}
			)
			service := NewPrinterServiceServer(&storage, &limiter, &cm)

			limiter.On("Continue", mock.Anything, mock.Anything, mock.Anything).Return(p, nil)

			ctx := context.Background()
			req := &v1.PrinterListRequest{
				Continue: "cursor",
				Limit:    20,
			}
			resp, err := service.List(ctx, req)
			if assert.NoError(t, err) && assert.NotNil(t, resp) {
				assert.Len(t, resp.Items, 1)
			}
			storage.AssertNotCalled(t, "All", mock.Anything)
			limiter.AssertCalled(t, "Continue", mock.Anything, "cursor", 20)
		})
	})
}
