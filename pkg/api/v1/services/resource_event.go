package services

import (
	"fmt"

	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api/v1"
	db "github.com/thetechnick/print3r/pkg/storage"
	"github.com/thetechnick/print3r/pkg/util"
)

type resourceEventService struct {
	hub db.EventHub
	log *log.Entry
}

// NewResourceEventServiceServer creates a new event service instance
func NewResourceEventServiceServer(hub db.EventHub) v1.ResourceEventServiceServer {
	return &resourceEventService{
		hub: hub,
		log: log.WithField("module", "v1.services.EventService"),
	}
}

func (s *resourceEventService) checkError(err error) error {
	return checkError(err, s.log)
}

func (s *resourceEventService) Stream(
	req *v1.ResourceEventStreamRequest,
	stream v1.ResourceEventService_StreamServer,
) (err error) {
	defer func() {
		err = s.checkError(err)
	}()

	var (
		t         string
		operation v1.ResourceEvent_Operation
	)
	if req.Type != "" &&
		proto.MessageType(req.Type) == nil {
		err = fmt.Errorf("unknown message type %q", req.Type)
		return
	} else if req.Type != "" {
		t = util.TypeURL + req.Type
	}
	if req.Operation != 0 &&
		req.Operation != v1.ResourceEvent_Created &&
		req.Operation != v1.ResourceEvent_Updated &&
		req.Operation != v1.ResourceEvent_Deleted {
		// TODO throw validation error
	} else if req.Operation != 0 {
		operation = req.Operation
	}

	client := s.hub.Register()
	defer s.hub.Unregister(client)

	for {
		select {
		case <-stream.Context().Done():
			s.log.Debug("Client gone")
			return stream.Context().Err()
		case event, closed := <-client.Events():
			if !closed {
				s.log.Debug("send closed gone")
				return nil
			}
			if event == nil {
				continue
			}

			v1Event := &v1.ResourceEvent{}
			if err = v1.Scheme.Convert(event, v1Event); err != nil {
				return
			}

			// filters
			switch v1Event.Operation {
			case v1.ResourceEvent_Created, v1.ResourceEvent_Updated:
				if t != "" && v1Event.New.TypeUrl != t {
					continue
				}
			case v1.ResourceEvent_Deleted:
				if t != "" && v1Event.Old.TypeUrl != t {
					continue
				}
			}

			if operation != 0 && v1Event.Operation != operation {
				continue
			}

			if err = stream.Send(v1Event); err != nil {
				return err
			}
		}
	}
}
