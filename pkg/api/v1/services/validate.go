package services

import (
	"path"

	"github.com/golang/protobuf/proto"
	"github.com/thetechnick/print3r/pkg/api/v1"
)

func validateMetadata(obj *v1.ObjectMeta) []proto.Message {
	var details []proto.Message

	if obj == nil {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       ".metadata",
			Description: NotEmpty,
		})
	} else {
		if obj.Name == "" {
			details = append(details, &v1.ErrorFieldViolation{
				Field:       ".metadata.name",
				Description: NotEmpty,
			})
		}
	}

	return details
}

func validatePrinterMetadata(obj *v1.Printer) error {
	var details []proto.Message

	if obj == nil {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       ".",
			Description: NotEmpty,
		})
		return validationError(details...)
	}
	for _, d := range validateMetadata(obj.Metadata) {
		details = append(details, d)
	}
	if len(details) > 0 {
		return validationError(details...)
	}
	return nil
}

func validateVideoStreamMetadata(obj *v1.VideoStream) error {
	var details []proto.Message

	if obj == nil {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       ".",
			Description: NotEmpty,
		})
		return validationError(details...)
	}
	for _, d := range validateMetadata(obj.Metadata) {
		details = append(details, d)
	}
	if len(details) > 0 {
		return validationError(details...)
	}
	return nil
}

func validatePrinter(obj *v1.Printer) error {
	var details []proto.Message

	if obj == nil {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       ".",
			Description: NotEmpty,
		})
		return validationError(details...)
	}
	if obj.Metadata == nil {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       ".metadata",
			Description: NotEmpty,
		})
	} else {
		if obj.Metadata.Name == "" {
			details = append(details, &v1.ErrorFieldViolation{
				Field:       ".metadata.name",
				Description: NotEmpty,
			})
		}
	}

	if obj.Spec == nil {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       ".spec",
			Description: NotEmpty,
		})
	} else {
		if obj.Spec.BaudRate == 0 {
			details = append(details, &v1.ErrorFieldViolation{
				Field:       ".spec.baudRate",
				Description: NotEmpty,
			})
		}
		if obj.Spec.Device == "" {
			details = append(details, &v1.ErrorFieldViolation{
				Field:       ".spec.device",
				Description: NotEmpty,
			})
		}
	}

	if len(details) > 0 {
		return validationError(details...)
	}
	return nil
}

func validateVolumeMetadata(obj *v1.Volume) error {
	var details []proto.Message

	if obj == nil {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       ".",
			Description: NotEmpty,
		})
		return validationError(details...)
	}
	for _, d := range validateMetadata(obj.Metadata) {
		details = append(details, d)
	}
	if len(details) > 0 {
		return validationError(details...)
	}
	return nil
}

func validateVolume(v *v1.Volume) error {
	var details []proto.Message
	if v == nil {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       ".",
			Description: NotEmpty,
		})
		return validationError(details...)
	}
	if v.Metadata == nil {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       ".metadata",
			Description: NotEmpty,
		})
	}
	if v.Metadata != nil && v.Metadata.Name == "" {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       ".metadata.name",
			Description: NotEmpty,
		})
	}
	if v.Spec == nil {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       ".spec",
			Description: NotEmpty,
		})
	} else {
		if v.Spec.Local == nil {
			details = append(details, &v1.ErrorFieldViolation{
				Field:       ".spec.local",
				Description: NotEmpty,
			})
		}

		if v.Spec.Local != nil {
			if v.Spec.Local.Path == "" {
				details = append(details, &v1.ErrorFieldViolation{
					Field:       ".spec.local.path",
					Description: NotEmpty,
				})
			} else if !path.IsAbs(v.Spec.Local.Path) {
				details = append(details, &v1.ErrorFieldViolation{
					Field:       ".spec.local.path",
					Description: "Must be absolute",
				})
			}
		}
	}

	if len(details) > 0 {
		return validationError(details...)
	}
	return nil
}

func validatePrintMetadata(obj *v1.Print) error {
	var details []proto.Message
	if obj == nil {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       "print",
			Description: NotEmpty,
		})
		return validationError(details...)
	}
	for _, d := range validateMetadata(obj.Metadata) {
		details = append(details, d)
	}
	if len(details) > 0 {
		return validationError(details...)
	}
	return nil
}

func validatePrint(v *v1.Print) error {
	var details []proto.Message
	if v == nil {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       "print",
			Description: NotEmpty,
		})
	}
	if v.Spec == nil {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       "print.spec",
			Description: NotEmpty,
		})
	} else {
		if v.Spec.Printer == "" {
			details = append(details, &v1.ErrorFieldViolation{
				Field:       "print.spec.printer",
				Description: NotEmpty,
			})
		}
		if v.Spec.FileRef == nil {
			details = append(details, &v1.ErrorFieldViolation{
				Field:       "print.spec.fileRef",
				Description: NotEmpty,
			})
		} else {
			if v.Spec.FileRef.File == "" {
				details = append(details, &v1.ErrorFieldViolation{
					Field:       "print.spec.fileRef.file",
					Description: NotEmpty,
				})
			}
			if v.Spec.FileRef.Volume == "" {
				details = append(details, &v1.ErrorFieldViolation{
					Field:       "print.spec.fileRef.volume",
					Description: NotEmpty,
				})
			}
		}
	}
	if len(details) > 0 {
		return validationError(details...)
	}
	return nil
}

func validateVideoStream(obj *v1.VideoStream) error {
	var details []proto.Message

	if obj == nil {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       ".",
			Description: NotEmpty,
		})
		return validationError(details...)
	}
	if obj.Metadata == nil {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       ".metadata",
			Description: NotEmpty,
		})
	} else {
		if obj.Metadata.Name == "" {
			details = append(details, &v1.ErrorFieldViolation{
				Field:       ".metadata.name",
				Description: NotEmpty,
			})
		}
	}

	if obj.Spec == nil {
		details = append(details, &v1.ErrorFieldViolation{
			Field:       ".spec",
			Description: NotEmpty,
		})
	} else {
		if obj.Spec.Motion == nil {
			details = append(details, &v1.ErrorFieldViolation{
				Field:       ".spec.motion",
				Description: NotEmpty,
			})
		} else {
			if obj.Spec.Motion.Device == "" {
				details = append(details, &v1.ErrorFieldViolation{
					Field:       ".spec.motion.device",
					Description: NotEmpty,
				})
			}
			if obj.Spec.Motion.Height == 0 {
				details = append(details, &v1.ErrorFieldViolation{
					Field:       ".spec.motion.height",
					Description: NotEmpty,
				})
			}
			if obj.Spec.Motion.Width == 0 {
				details = append(details, &v1.ErrorFieldViolation{
					Field:       ".spec.motion.width",
					Description: NotEmpty,
				})
			}
		}
	}

	if len(details) > 0 {
		return validationError(details...)
	}
	return nil
}
