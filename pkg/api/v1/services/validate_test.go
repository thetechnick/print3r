package services

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/thetechnick/print3r/pkg/api/v1"
)

func TestValidateMetadata(t *testing.T) {
	t.Run("null", func(t *testing.T) {
		details := validateMetadata(nil)
		assert.Len(t, details, 1)
	})

	t.Run("name empty", func(t *testing.T) {
		details := validateMetadata(&v1.ObjectMeta{})
		assert.Len(t, details, 1)
	})
}

func TestValidatePrinterMetadata(t *testing.T) {
	t.Run("null", func(t *testing.T) {
		err := validatePrinterMetadata(nil)
		assert.Error(t, err)
	})
}

func TestValidateVolumeMetadata(t *testing.T) {
	t.Run("null", func(t *testing.T) {
		err := validateVolumeMetadata(nil)
		assert.Error(t, err)
	})
}

func TestValidatePrintMetadata(t *testing.T) {
	t.Run("null", func(t *testing.T) {
		err := validatePrintMetadata(nil)
		assert.Error(t, err)
	})
}
