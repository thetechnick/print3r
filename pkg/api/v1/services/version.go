package services

import (
	"context"

	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api/v1"
	"github.com/thetechnick/print3r/pkg/version"
)

// NewVersionServiceServer returns a new VersionServiceServer
func NewVersionServiceServer() v1.VersionServiceServer {
	return &vss{
		log: log.WithField("module", "v1.services.VersionService"),
	}
}

type vss struct {
	log *log.Entry
}

func (s *vss) checkError(err error) error {
	return checkError(err, s.log)
}

func (s *vss) Version(ctx context.Context, req *v1.VersionRequest) (res *v1.APIVersion, err error) {
	defer func() {
		err = s.checkError(err)
	}()

	v := version.Get()
	res = &v1.APIVersion{}
	if err = v1.Scheme.Convert(v, res); err != nil {
		return
	}
	return
}
