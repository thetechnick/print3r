package services

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/thetechnick/print3r/pkg/api/v1"
)

func TestVersionService(t *testing.T) {
	t.Run("Version", func(t *testing.T) {
		s := NewVersionServiceServer()

		ctx := context.Background()
		req := &v1.VersionRequest{}
		res, err := s.Version(ctx, req)
		if assert.NoError(t, err) &&
			assert.NotNil(t, res) &&
			assert.NotNil(t, res.Version) {
			assert.Equal(t, "was not build properly", res.Branch)
			assert.Equal(t, "was not build properly", res.Version)
		}
	})
}
