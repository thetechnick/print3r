package services

import (
	"context"

	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/v1"
	"github.com/thetechnick/print3r/pkg/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type videoStreamService struct {
	storage storage.VideoStreamStorage
	limiter storage.VideoStreamLimiter
	log     *log.Entry
}

// NewVideoStreamServiceServer returns a new VideoStreamServiceServer
func NewVideoStreamServiceServer(
	storage storage.VideoStreamStorage,
	limiter storage.VideoStreamLimiter,
) v1.VideoStreamServiceServer {
	return &videoStreamService{
		storage: storage,
		limiter: limiter,
		log:     log.WithField("module", "v1.services.VideoStreamServiceServer"),
	}
}

func (s *videoStreamService) checkError(err error) error {
	return checkError(err, s.log)
}

func (s *videoStreamService) Create(ctx context.Context, req *v1.VideoStream) (res *v1.VideoStream, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.VideoStream{}

	if err = validateVideoStream(req); err != nil {
		return
	}

	obj := api.NewVideoStream()
	if err = v1.Scheme.Convert(req, obj); err != nil {
		return
	}
	if err = s.storage.Create(ctx, obj); err != nil {
		return
	}
	if err = v1.Scheme.Convert(obj, res); err != nil {
		return
	}
	return
}

func (s *videoStreamService) Delete(ctx context.Context, req *v1.VideoStream) (res *v1.VideoStream, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.VideoStream{}

	if err = validateVideoStreamMetadata(req); err != nil {
		return
	}

	var p *api.VideoStream
	p, err = s.storage.FindOneByName(ctx, req.Metadata.Name)
	if err != nil {
		return
	}
	if p == nil {
		err = status.Error(codes.NotFound, "volume not found")
		return
	}

	if p, err = s.storage.Delete(ctx, p); err != nil {
		return
	}

	if err = v1.Scheme.Convert(p, res); err != nil {
		return
	}
	return
}

func (s *videoStreamService) Update(ctx context.Context, req *v1.VideoStream) (res *v1.VideoStream, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.VideoStream{}
	if err = validateVideoStream(req); err != nil {
		return
	}

	obj := &api.VideoStream{}
	if err = v1.Scheme.Convert(req, obj); err != nil {
		return
	}
	if err = s.storage.Update(ctx, obj); err != nil {
		return
	}
	if err = v1.Scheme.Convert(obj, res); err != nil {
		return
	}
	return
}

func (s *videoStreamService) UpdateStatus(ctx context.Context, req *v1.VideoStream) (res *v1.VideoStream, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.VideoStream{}
	if err = validateVideoStream(req); err != nil {
		return
	}

	obj := &api.VideoStream{}
	if err = v1.Scheme.Convert(req, obj); err != nil {
		return
	}
	if err = s.storage.UpdateStatus(ctx, obj); err != nil {
		return
	}
	if err = v1.Scheme.Convert(obj, res); err != nil {
		return
	}
	return
}

func (s *videoStreamService) List(ctx context.Context, req *v1.VideoStreamListRequest) (res *v1.VideoStreamList, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.VideoStreamList{}

	var (
		selector     *api.LabelSelector
		all          []*api.VideoStream
		responseList []*api.VideoStream
		cursor       string
	)

	if req.Continue != "" {
		responseList, err = s.limiter.Continue(ctx, req.Continue, int(req.Limit))
		if err != nil {
			return
		}
	} else {
		selector, err = api.ParseSelector(req.Selector)
		if err != nil {
			err = status.Error(codes.InvalidArgument, err.Error())
			return
		}

		all, err = s.storage.All(ctx)
		if err != nil {
			return
		}
		if selector != nil {
			for _, i := range all {
				if selector.Match(i.Labels) {
					responseList = append(responseList, i)
				}
			}
		} else {
			responseList = all
		}
		responseList, cursor = s.limiter.Limit(ctx, responseList, int(req.Limit))
	}

	res = &v1.VideoStreamList{
		Metadata: &v1.ListMeta{
			Continue: cursor,
		},
	}
	listItems := []*v1.VideoStream{}
	if err = v1.Scheme.Convert(&responseList, &listItems); err != nil {
		return
	}
	res.Items = listItems
	return
}

func (s *videoStreamService) Show(ctx context.Context, req *v1.VideoStream) (res *v1.VideoStream, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.VideoStream{}

	if err = validateVideoStreamMetadata(req); err != nil {
		return
	}

	obj, err := s.storage.FindOneByName(ctx, req.Metadata.Name)
	if err != nil {
		return
	}
	if obj == nil {
		err = status.Error(codes.NotFound, "volume not found")
		return
	}
	if err = v1.Scheme.Convert(obj, res); err != nil {
		return
	}
	return
}
