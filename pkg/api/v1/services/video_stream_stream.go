package services

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"

	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/storage"
	"github.com/thetechnick/print3r/pkg/video"
)

type VideoStreamStreamEndpoint struct {
	controller video.Controller
	storage    storage.VideoStreamStorage
	log        *log.Entry
}

func NewVideoStreamStreamEndpoint(controller video.Controller, storage storage.VideoStreamStorage) *VideoStreamStreamEndpoint {
	return &VideoStreamStreamEndpoint{
		controller: controller,
		storage:    storage,
		log:        log.WithField("module", "v1.services.VideoStreamStreamEndpoint"),
	}
}

func (e *VideoStreamStreamEndpoint) Handle(w http.ResponseWriter, req *http.Request, pathParams map[string]string) {
	var (
		videoStream *api.VideoStream
		err         error
		ctx         = context.Background()
	)
	if name, ok := pathParams["metadata.name"]; ok {
		if videoStream, err = e.storage.FindOneByName(ctx, name); err != nil {
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintln(w, `{"code": 5, "message": "internal error"}`)
			return
		}
	}
	if videoStream == nil {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintln(w, `{"code": 2, "message": "video stream not found"}`)
		return
	}
	if port := e.controller.Port(ctx, videoStream); port != 0 {
		url, err := url.Parse(fmt.Sprintf("http://localhost:%d", port))
		if err != nil {
			panic(err)
		}
		httputil.NewSingleHostReverseProxy(url).ServeHTTP(w, req)
		return
	}
	w.WriteHeader(http.StatusServiceUnavailable)
	fmt.Fprintln(w, `{"code": 14, "message": "video stream not ready"}`)
	return
}
