package services

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/v1"
	dbtest "github.com/thetechnick/print3r/pkg/storage/test"
)

func TestVideoStreamService(t *testing.T) {
	t.Run("Create", func(t *testing.T) {
		var (
			storage dbtest.VideoStreamStorageMock
			limiter dbtest.VideoStreamLimiterMock
		)
		service := NewVideoStreamServiceServer(&storage, &limiter)

		storage.On("Create", mock.Anything, mock.Anything).Return(nil)

		ctx := context.Background()
		req := &v1.VideoStream{
			Metadata: &v1.ObjectMeta{
				Name: "test",
			},
			Spec: &v1.VideoStreamSpec{
				Motion: &v1.MotionConfig{
					Device: "/test",
					Height: 100,
					Width:  100,
				},
			},
		}
		resp, err := service.Create(ctx, req)
		if assert.NoError(t, err) {
			assert.NotNil(t, resp)
		}
		storage.AssertCalled(t, "Create", mock.Anything, mock.Anything)
	})

	t.Run("Delete", func(t *testing.T) {
		var (
			storage dbtest.VideoStreamStorageMock
			limiter dbtest.VideoStreamLimiterMock
			p       = &api.VideoStream{}
		)
		service := NewVideoStreamServiceServer(&storage, &limiter)

		storage.On("FindOneByName", mock.Anything, mock.Anything).Return(p, nil)
		storage.On("Delete", mock.Anything, mock.Anything).Return(p, nil)

		ctx := context.Background()
		req := &v1.VideoStream{
			Metadata: &v1.ObjectMeta{
				Name: "test",
			},
		}
		resp, err := service.Delete(ctx, req)
		if assert.NoError(t, err) {
			assert.NotNil(t, resp)
		}
		storage.AssertCalled(t, "FindOneByName", mock.Anything, "test")
		storage.AssertCalled(t, "Delete", mock.Anything, p)
	})

	t.Run("UpdateStatus", func(t *testing.T) {
		var (
			storage dbtest.VideoStreamStorageMock
			limiter dbtest.VideoStreamLimiterMock
		)
		service := NewVideoStreamServiceServer(&storage, &limiter)

		storage.On("UpdateStatus", mock.Anything, mock.Anything).Return(nil)

		ctx := context.Background()
		req := &v1.VideoStream{
			Metadata: &v1.ObjectMeta{
				Name: "test",
			},
			Spec: &v1.VideoStreamSpec{
				Motion: &v1.MotionConfig{
					Device: "/test",
					Height: 100,
					Width:  100,
				},
			},
		}
		resp, err := service.UpdateStatus(ctx, req)
		if assert.NoError(t, err) {
			assert.NotNil(t, resp)
		}
		storage.AssertCalled(t, "UpdateStatus", mock.Anything, mock.Anything)
	})

	t.Run("Update", func(t *testing.T) {
		var (
			storage dbtest.VideoStreamStorageMock
			limiter dbtest.VideoStreamLimiterMock
		)
		service := NewVideoStreamServiceServer(&storage, &limiter)

		storage.On("Update", mock.Anything, mock.Anything).Return(nil)

		ctx := context.Background()
		req := &v1.VideoStream{
			Metadata: &v1.ObjectMeta{
				Name: "test",
			},
			Spec: &v1.VideoStreamSpec{
				Motion: &v1.MotionConfig{
					Device: "/test",
					Height: 100,
					Width:  100,
				},
			},
		}
		resp, err := service.Update(ctx, req)
		if assert.NoError(t, err) {
			assert.NotNil(t, resp)
		}
		storage.AssertCalled(t, "Update", mock.Anything, mock.Anything)
	})

	t.Run("Show", func(t *testing.T) {
		var (
			storage dbtest.VideoStreamStorageMock
			limiter dbtest.VideoStreamLimiterMock
			p       = &api.VideoStream{}
		)
		service := NewVideoStreamServiceServer(&storage, &limiter)

		storage.On("FindOneByName", mock.Anything, mock.Anything).Return(p, nil)

		ctx := context.Background()
		req := &v1.VideoStream{
			Metadata: &v1.ObjectMeta{
				Name: "test",
			},
		}
		resp, err := service.Show(ctx, req)
		if assert.NoError(t, err) {
			assert.NotNil(t, resp)
		}
		storage.AssertCalled(t, "FindOneByName", mock.Anything, "test")
	})

	t.Run("List", func(t *testing.T) {
		var (
			storage dbtest.VideoStreamStorageMock
			limiter dbtest.VideoStreamLimiterMock
			p       = []*api.VideoStream{{}}
		)
		service := NewVideoStreamServiceServer(&storage, &limiter)

		limiter.On("Limit", mock.Anything, mock.Anything, mock.Anything).Return(p, "cursor")
		storage.On("All", mock.Anything).Return(p, nil)

		ctx := context.Background()
		req := &v1.VideoStreamListRequest{
			Limit:    10,
			Selector: "!hans",
		}
		resp, err := service.List(ctx, req)
		if assert.NoError(t, err) && assert.NotNil(t, resp) {
			assert.Len(t, resp.Items, 1)
			assert.Equal(t, "cursor", resp.Metadata.Continue)
		}
		storage.AssertCalled(t, "All", mock.Anything)
		limiter.AssertCalled(t, "Limit", mock.Anything, mock.Anything, 10)

		t.Run("continue", func(t *testing.T) {
			var (
				storage dbtest.VideoStreamStorageMock
				limiter dbtest.VideoStreamLimiterMock
				p       = []*api.VideoStream{{}}
			)
			service := NewVideoStreamServiceServer(&storage, &limiter)

			limiter.On("Continue", mock.Anything, mock.Anything, mock.Anything).Return(p, nil)

			ctx := context.Background()
			req := &v1.VideoStreamListRequest{
				Continue: "cursor",
				Limit:    20,
			}
			resp, err := service.List(ctx, req)
			if assert.NoError(t, err) && assert.NotNil(t, resp) {
				assert.Len(t, resp.Items, 1)
			}
			storage.AssertNotCalled(t, "All", mock.Anything)
			limiter.AssertCalled(t, "Continue", mock.Anything, "cursor", 20)
		})
	})
}
