package services

import (
	"context"

	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/v1"
	db "github.com/thetechnick/print3r/pkg/storage"
	"github.com/thetechnick/print3r/pkg/volume"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type volumeService struct {
	storage     db.VolumeStorage
	limiter     db.VolumeLimiter
	fileLimiter db.VolumeFileLimiter
	service     volume.Service
	log         *log.Entry
}

// NewVolumeServiceServer returns a new VolumeServiceServer
func NewVolumeServiceServer(
	storage db.VolumeStorage,
	limiter db.VolumeLimiter,
	fileLimiter db.VolumeFileLimiter,
	service volume.Service,
) v1.VolumeServiceServer {
	return &volumeService{
		storage:     storage,
		limiter:     limiter,
		fileLimiter: fileLimiter,
		service:     service,
		log:         log.WithField("module", "v1.services.VolumeServiceServer"),
	}
}

func (s *volumeService) checkError(err error) error {
	return checkError(err, s.log)
}

func (s *volumeService) ListFiles(ctx context.Context, req *v1.VolumeFileListRequest) (res *v1.VolumeFileList, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.VolumeFileList{}
	var (
		v       *api.Volume
		vfl     = &api.VolumeFileList{}
		request = &api.VolumeFileListRequest{}
	)
	if err = v1.Scheme.Convert(req, request); err != nil {
		return
	}

	if v, err = s.storage.FindOneByName(ctx, request.Name); err != nil {
		return
	}
	if v == nil {
		err = status.Error(codes.NotFound, "volume not found")
		return
	}
	if req.Continue != "" {
		vfl.Items, err = s.fileLimiter.Continue(ctx, req.Continue, int(req.Limit))
		if err != nil {
			return
		}
	} else {
		source := s.service.Source(ctx, v)
		if source == nil {
			err = status.Error(codes.NotFound, "volume not found")
			return
		}
		if vfl.Items, err = source.List(ctx); err != nil {
			return
		}
		vfl.Items, vfl.Continue = s.fileLimiter.Limit(ctx, vfl.Items, int(req.Limit))
	}
	vfl.Owner = v.Ref()
	if err = v1.Scheme.Convert(vfl, res); err != nil {
		return
	}
	return
}

func (s *volumeService) Create(ctx context.Context, req *v1.Volume) (res *v1.Volume, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.Volume{}

	if err = validateVolume(req); err != nil {
		return
	}

	obj := &api.Volume{}
	if err = v1.Scheme.Convert(req, obj); err != nil {
		return
	}
	if err = s.storage.Create(ctx, obj); err != nil {
		return
	}
	if err = v1.Scheme.Convert(obj, res); err != nil {
		return
	}
	return
}

func (s *volumeService) Delete(ctx context.Context, req *v1.Volume) (res *v1.Volume, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.Volume{}

	if err = validateVolumeMetadata(req); err != nil {
		return
	}

	var p *api.Volume
	p, err = s.storage.FindOneByName(ctx, req.Metadata.Name)
	if err != nil {
		return
	}
	if p == nil {
		err = status.Error(codes.NotFound, "volume not found")
		return
	}

	if p, err = s.storage.Delete(ctx, p); err != nil {
		return
	}

	if err = v1.Scheme.Convert(p, res); err != nil {
		return
	}
	return
}

func (s *volumeService) Update(ctx context.Context, req *v1.Volume) (res *v1.Volume, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.Volume{}
	if err = validateVolume(req); err != nil {
		return
	}

	obj := &api.Volume{}
	if err = v1.Scheme.Convert(req, obj); err != nil {
		return
	}
	if err = s.storage.Update(ctx, obj); err != nil {
		return
	}
	if err = v1.Scheme.Convert(obj, res); err != nil {
		return
	}
	return
}

func (s *volumeService) UpdateStatus(ctx context.Context, req *v1.Volume) (res *v1.Volume, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.Volume{}
	if err = validateVolume(req); err != nil {
		return
	}

	obj := &api.Volume{}
	if err = v1.Scheme.Convert(req, obj); err != nil {
		return
	}
	if err = s.storage.UpdateStatus(ctx, obj); err != nil {
		return
	}
	if err = v1.Scheme.Convert(obj, res); err != nil {
		return
	}
	return
}

func (s *volumeService) List(ctx context.Context, req *v1.VolumeListRequest) (res *v1.VolumeList, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.VolumeList{}

	var (
		selector     *api.LabelSelector
		all          []*api.Volume
		responseList []*api.Volume
		cursor       string
	)

	if req.Continue != "" {
		responseList, err = s.limiter.Continue(ctx, req.Continue, int(req.Limit))
		if err != nil {
			return
		}
	} else {
		selector, err = api.ParseSelector(req.Selector)
		if err != nil {
			err = status.Error(codes.InvalidArgument, err.Error())
			return
		}

		all, err = s.storage.All(ctx)
		if err != nil {
			return
		}
		if selector != nil {
			for _, i := range all {
				if selector.Match(i.Labels) {
					responseList = append(responseList, i)
				}
			}
		} else {
			responseList = all
		}
		responseList, cursor = s.limiter.Limit(ctx, responseList, int(req.Limit))
	}

	res = &v1.VolumeList{
		Metadata: &v1.ListMeta{
			Continue: cursor,
		},
	}
	listItems := []*v1.Volume{}
	if err = v1.Scheme.Convert(&responseList, &listItems); err != nil {
		return
	}
	res.Items = listItems
	return
}

func (s *volumeService) Show(ctx context.Context, req *v1.Volume) (res *v1.Volume, err error) {
	defer func() {
		err = s.checkError(err)
	}()
	res = &v1.Volume{}

	if err = validateVolumeMetadata(req); err != nil {
		return
	}

	obj, err := s.storage.FindOneByName(ctx, req.Metadata.Name)
	if err != nil {
		return
	}
	if obj == nil {
		err = status.Error(codes.NotFound, "volume not found")
		return
	}
	if err = v1.Scheme.Convert(obj, res); err != nil {
		return
	}
	return
}
