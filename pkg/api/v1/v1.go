package v1

import "github.com/thetechnick/print3r/pkg/api/runtime"

// Scheme register variables
var (
	Scheme *runtime.Scheme
)

func init() {
	Scheme = runtime.NewScheme()
	if err := addConversionFuncs(Scheme); err != nil {
		panic(err)
	}
}
