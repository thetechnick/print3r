package v1

import (
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/runtime/conversion"
)

// VideoStreamServiceStreamPattern is the pattern used by grpc-gateway/runtime.Mux to match a request to its handler
var VideoStreamServiceStreamPattern = runtime.MustPattern(runtime.NewPattern(1, []int{2, 0, 1, 0, 4, 1, 5, 1, 2, 2}, []string{"video_streams", "metadata.name", "stream"}, ""))

var videoStreamConverters = []interface{}{
	func(in *api.VideoStreamListRequest, out *VideoStreamListRequest, s conversion.Scope) (err error) {
		out.Continue = in.Continue
		out.Selector = in.Selector
		out.Limit = int64(in.Limit)
		return
	},
	func(in *VideoStreamListRequest, out *api.VideoStreamListRequest, s conversion.Scope) (err error) {
		out.Continue = in.Continue
		out.Selector = in.Selector
		out.Limit = int(in.Limit)
		return
	},

	func(in *VideoStreamList, out *api.VideoStreamList, s conversion.Scope) (err error) {
		if err = s.Convert(in.Metadata, &out.ListMeta); err != nil {
			return
		}
		if err = s.Convert(&in.Items, &out.Items); err != nil {
			return
		}
		return
	},
	func(in *api.VideoStreamList, out *VideoStreamList, s conversion.Scope) (err error) {
		out.Metadata = &ListMeta{}
		if err = s.Convert(&in.ListMeta, out.Metadata); err != nil {
			return
		}
		if err = s.Convert(&in.Items, &out.Items); err != nil {
			return
		}
		return
	},

	// []VideoStream
	func(in *[]*VideoStream, out *[]*api.VideoStream, s conversion.Scope) (err error) {
		if in == nil {
			return
		}
		for _, f := range *in {
			outVideoStream := &api.VideoStream{}
			if err = s.Convert(f, outVideoStream); err != nil {
				return
			}
			*out = append(*out, outVideoStream)
		}
		return
	},
	func(in *[]*api.VideoStream, out *[]*VideoStream, s conversion.Scope) (err error) {
		if in == nil {
			return
		}
		for _, f := range *in {
			outVideoStream := &VideoStream{}
			if err = s.Convert(f, outVideoStream); err != nil {
				return
			}
			*out = append(*out, outVideoStream)
		}
		return
	},

	// VideoStream
	func(in *VideoStream, out *api.VideoStream, s conversion.Scope) (err error) {
		if err = s.Convert(in.Metadata, &out.ObjectMeta); err != nil {
			return
		}
		if in.Spec != nil {
			if err = s.Convert(in.Spec, &out.Spec); err != nil {
				return
			}
		}
		if in.Status != nil {
			if err = s.Convert(in.Status, &out.Status); err != nil {
				return
			}
		}
		return
	},
	func(in *api.VideoStream, out *VideoStream, s conversion.Scope) (err error) {
		out.Metadata = &ObjectMeta{}
		if err = s.Convert(&in.ObjectMeta, out.Metadata); err != nil {
			return
		}
		out.Spec = &VideoStreamSpec{}
		if err = s.Convert(&in.Spec, out.Spec); err != nil {
			return
		}
		out.Status = &VideoStreamStatus{}
		if err = s.Convert(&in.Status, out.Status); err != nil {
			return
		}
		return
	},

	// VideoStreamSpec
	func(in *VideoStreamSpec, out *api.VideoStreamSpec, s conversion.Scope) (err error) {
		if in.Motion != nil {
			out.Motion = &api.MotionConfig{}
			if err = s.Convert(in.Motion, out.Motion); err != nil {
				return
			}
		}
		return
	},
	func(in *api.VideoStreamSpec, out *VideoStreamSpec, s conversion.Scope) (err error) {
		if in.Motion != nil {
			out.Motion = &MotionConfig{}
			if err = s.Convert(in.Motion, out.Motion); err != nil {
				return
			}
		}
		return
	},

	// VideoStreamStatus
	func(in *VideoStreamStatus, out *api.VideoStreamStatus, s conversion.Scope) (err error) {
		if err = s.Convert(&in.Conditions, &out.Conditions); err != nil {
			return
		}
		return
	},
	func(in *api.VideoStreamStatus, out *VideoStreamStatus, s conversion.Scope) (err error) {
		if err = s.Convert(&in.Conditions, &out.Conditions); err != nil {
			return
		}
		return
	},

	func(in *api.MotionConfig, out *MotionConfig, s conversion.Scope) (err error) {
		out.Device = in.Device
		out.Height = int32(in.Height)
		out.Width = int32(in.Width)
		return
	},
	func(in *MotionConfig, out *api.MotionConfig, s conversion.Scope) (err error) {
		out.Device = in.Device
		out.Height = int(in.Height)
		out.Width = int(in.Width)
		return
	},
}
