package v1

import (
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/runtime/conversion"
)

var volumeConverters = []interface{}{
	// VolumeFileListRequest
	func(in *VolumeFileListRequest, out *api.VolumeFileListRequest, s conversion.Scope) (err error) {
		out.Continue = in.Continue
		out.Limit = int(in.Limit)
		out.Name = in.Name
		return
	},
	func(in *api.VolumeFileListRequest, out *VolumeFileListRequest, s conversion.Scope) (err error) {
		out.Continue = in.Continue
		out.Limit = int64(in.Limit)
		out.Name = in.Name
		return
	},

	// VolumeFileList
	func(in *VolumeFileList, out *api.VolumeFileList, s conversion.Scope) (err error) {
		if err = s.Convert(in.Metadata, &out.ListMeta); err != nil {
			return
		}
		if err = s.Convert(&in.Items, &out.Items); err != nil {
			return
		}
		return
	},
	func(in *api.VolumeFileList, out *VolumeFileList, s conversion.Scope) (err error) {
		out.Metadata = &ListMeta{}
		if err = s.Convert(&in.ListMeta, out.Metadata); err != nil {
			return
		}
		if err = s.Convert(&in.Items, &out.Items); err != nil {
			return
		}
		return
	},

	// []VolumeFile
	func(in *[]*VolumeFile, out *[]*api.VolumeFile, s conversion.Scope) (err error) {
		if in == nil {
			return
		}
		for _, f := range *in {
			outVolumeFile := &api.VolumeFile{}
			if err = s.Convert(f, outVolumeFile); err != nil {
				return
			}
			*out = append(*out, outVolumeFile)
		}
		return
	},
	func(in *[]*api.VolumeFile, out *[]*VolumeFile, s conversion.Scope) (err error) {
		if in == nil {
			return
		}
		for _, f := range *in {
			outVolumeFile := &VolumeFile{}
			if err = s.Convert(f, outVolumeFile); err != nil {
				return
			}
			*out = append(*out, outVolumeFile)
		}
		return
	},

	// VolumeFile
	func(in *VolumeFile, out *api.VolumeFile, s conversion.Scope) (err error) {
		out.Path = in.Path
		out.Size = in.Size
		return
	},
	func(in *api.VolumeFile, out *VolumeFile, s conversion.Scope) (err error) {
		out.Path = in.Path
		out.Size = in.Size
		return
	},

	// VolumeListRequest
	func(in *VolumeListRequest, out *api.VolumeListRequest, s conversion.Scope) (err error) {
		out.Continue = in.Continue
		out.Selector = in.Selector
		out.Limit = int(in.Limit)
		return
	},
	func(in *api.VolumeListRequest, out *VolumeListRequest, s conversion.Scope) (err error) {
		out.Continue = in.Continue
		out.Selector = in.Selector
		out.Limit = int64(in.Limit)
		return
	},

	// VolumeList
	func(in *VolumeList, out *api.VolumeList, s conversion.Scope) (err error) {
		if err = s.Convert(in.Metadata, &out.ListMeta); err != nil {
			return
		}
		if err = s.Convert(&in.Items, &out.Items); err != nil {
			return
		}
		return
	},
	func(in *api.VolumeList, out *VolumeList, s conversion.Scope) (err error) {
		out.Metadata = &ListMeta{}
		if err = s.Convert(&in.ListMeta, out.Metadata); err != nil {
			return
		}
		if err = s.Convert(&in.Items, &out.Items); err != nil {
			return
		}
		return
	},

	// Volume
	func(in *Volume, out *api.Volume, s conversion.Scope) (err error) {
		if err = s.Convert(in.Spec, &out.Spec); err != nil {
			return
		}
		if err = s.Convert(in.Metadata, &out.ObjectMeta); err != nil {
			return
		}
		if err = s.Convert(in.Status, &out.Status); err != nil {
			return
		}
		return
	},
	func(in *api.Volume, out *Volume, s conversion.Scope) (err error) {
		out.Spec = &VolumeSpec{}
		if err = s.Convert(&in.Spec, out.Spec); err != nil {
			return
		}
		out.Metadata = &ObjectMeta{}
		if err = s.Convert(&in.ObjectMeta, out.Metadata); err != nil {
			return
		}
		out.Status = &VolumeStatus{}
		if err = s.Convert(&in.Status, out.Status); err != nil {
			return
		}
		return
	},

	// VolumeStatus
	func(in *VolumeStatus, out *api.VolumeStatus, s conversion.Scope) (err error) {
		return
	},
	func(in *api.VolumeStatus, out *VolumeStatus, s conversion.Scope) (err error) {
		return
	},

	// VolumeSpec
	func(in *VolumeSpec, out *api.VolumeSpec, s conversion.Scope) (err error) {
		if in.Local != nil {
			out.Local = &api.LocalVolumeSource{}
			if err = s.Convert(in.Local, out.Local); err != nil {
				return
			}
		}
		return
	},
	func(in *api.VolumeSpec, out *VolumeSpec, s conversion.Scope) (err error) {
		if in.Local != nil {
			out.Local = &LocalVolumeSource{}
			if err = s.Convert(in.Local, out.Local); err != nil {
				return
			}
		}
		return
	},

	func(in *LocalVolumeSource, out *api.LocalVolumeSource, s conversion.Scope) (err error) {
		if in == nil {
			return
		}
		out.Path = in.Path
		return
	},
	func(in *api.LocalVolumeSource, out *LocalVolumeSource, s conversion.Scope) (err error) {
		if in == nil {
			return
		}
		out.Path = in.Path
		return
	},

	// []Volume
	func(in *[]*Volume, out *[]*api.Volume, s conversion.Scope) (err error) {
		if in == nil {
			return
		}
		for _, f := range *in {
			outVolume := &api.Volume{}
			if err = s.Convert(f, outVolume); err != nil {
				return
			}
			*out = append(*out, outVolume)
		}
		return
	},
	func(in *[]*api.Volume, out *[]*Volume, s conversion.Scope) (err error) {
		if in == nil {
			return
		}
		for _, f := range *in {
			outVolume := &Volume{}
			if err = s.Convert(f, outVolume); err != nil {
				return
			}
			*out = append(*out, outVolume)
		}
		return
	},
}
