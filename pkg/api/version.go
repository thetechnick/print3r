package api

import "time"

// Version represents a components version
type Version struct {
	Branch    string    `json:"branch"`
	Version   string    `json:"version"`
	GoVersion string    `json:"goVersion"`
	Platform  string    `json:"platform"`
	BuildDate time.Time `json:"buildTime"`
}
