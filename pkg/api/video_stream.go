package api

import "time"

// VideoStreamList represents a list of video streams
type VideoStreamList struct {
	ListMeta
	Items []*VideoStream
}

// VideoStream represents a video stream
type VideoStream struct {
	ObjectMeta
	Spec   VideoStreamSpec
	Status VideoStreamStatus
}

// NewVideoStream creates a new video stream object
func NewVideoStream() *VideoStream {
	now := time.Now()
	return &VideoStream{
		Status: VideoStreamStatus{
			Conditions: NewConditionMapCollection([]Condition{
				{
					Type:               VideoStreamConditionTypeReady,
					Status:             ConditionStatusFalse,
					LastTransitionTime: now,
				},
			}),
		},
	}
}

// VideoStreamSpec represents video stream settings
type VideoStreamSpec struct {
	Motion *MotionConfig
}

// MotionConfig holds motion (https://github.com/Motion-Project/motion) config
type MotionConfig struct {
	Device        string
	Height, Width int
}

// VideoStreamStatus holds status information about the video stream
type VideoStreamStatus struct {
	Conditions ConditionCollection
}

// VideoStreamConditionTypes
const (
	VideoStreamConditionTypeReady = "Ready"
)

// VideoStreamListRequest holds options for listing video streams
type VideoStreamListRequest struct {
	Continue, Selector string
	Limit              int
}
