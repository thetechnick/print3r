package api

// VolumeList represents a list of volumes
type VolumeList struct {
	ListMeta
	Items []*Volume
}

// Volume represents a volume
type Volume struct {
	ObjectMeta
	Spec   VolumeSpec
	Status VolumeStatus
}

// VolumeSpec represents a volumes specifications
type VolumeSpec struct {
	Local *LocalVolumeSource
}

// VolumeStatus represents the volume status
type VolumeStatus struct {
}

// LocalVolumeSource represents settings of a path volume source
type LocalVolumeSource struct {
	Path string
}

// VolumeFile represents a file in a volume
type VolumeFile struct {
	Path string
	Size int64
}

// VolumeFileList represents a list of volume files
type VolumeFileList struct {
	ListMeta
	Items []*VolumeFile
}

// VolumeListRequest represents a list request
type VolumeListRequest struct {
	Continue, Selector string
	Limit              int
}

// VolumeFileListRequest represents a volume file list request
type VolumeFileListRequest struct {
	Name, Continue string
	Limit          int
}
