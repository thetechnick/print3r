package file

import (
	"bufio"
	"io"
	"strconv"
	"strings"

	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/util"
)

// Analyser is the type/interface of file analyser funcs
type Analyser func(src io.Reader) (stats *api.FileAnalysis, err error)

// Analyse creates a overview of the files gcode instructions
func Analyse(src io.Reader) (stats *api.FileAnalysis, err error) {
	stats = &api.FileAnalysis{
		Commands: map[string]int{},
	}
	r := bufio.NewReader(src)
	var extruderRelative bool
	var (
		prevX, prevY, prevZ float64
	)
	extruderSum := map[string]float64{} // extruder movement
	prevRetract := map[string]int{}     // extruder retraction
	layers := map[float64]bool{}        // zheight to extruding

	for {
		var l string
		l, err = r.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				err = nil
				break
			}
			return
		}

		line := util.RemoveComments(l)
		if len(line) == 0 || strings.HasPrefix(line, ";") {
			continue
		}

		split := strings.SplitN(line, " ", 2)
		var (
			cmd  string
			args []string
		)
		cmd = strings.ToUpper(split[0])
		if len(split) > 1 {
			args = strings.Split(split[1], " ")
		}
		if !strings.HasPrefix(cmd, "M") &&
			!strings.HasPrefix(cmd, "G") {
			continue
		}
		stats.Commands[cmd]++
		stats.CommandCount++

		switch cmd {
		case "M190":
			if stats.BedTemperature == 0 && len(args) > 0 {
				var temp int64
				temp, err = strconv.ParseInt(args[0][1:], 10, 32)
				if err != nil {
					return
				}
				stats.BedTemperature = int(temp)
			}
		case "M109":
			if stats.ExtruderTemperature == 0 && len(args) > 0 {
				var temp int64
				temp, err = strconv.ParseInt(args[0][1:], 10, 32)
				if err != nil {
					return
				}
				stats.ExtruderTemperature = int(temp)
			}

		case "G90", "M82":
			extruderRelative = false

		case "G91", "M83":
			extruderRelative = true

		case "G0", "G1":
			if len(args) > 0 {
				var x, y, z float64              // coords
				extruder := map[string]float64{} // extruder movement
				var retract int

				for _, arg := range args {
					if len(arg) < 2 {
						continue
					}
					axis := strings.ToUpper(arg[0:1])
					switch axis {
					case "X":
						x, err = strconv.ParseFloat(arg[1:], 64)
						prevX = x
					case "Y":
						y, err = strconv.ParseFloat(arg[1:], 64)
						prevY = y
					case "Z":
						z, err = strconv.ParseFloat(arg[1:], 64)
						prevZ = z
					case "E", "A", "B", "C":
						extruder[axis], err = strconv.ParseFloat(arg[1:], 64)
						if err != nil {
							return
						}

						if extruder[axis] < 0 {
							prevRetract[axis] = -1
							retract = -1
						} else if extruder[axis] == 0 {
							retract = 0
						} else if extruder[axis] > 0 && prevRetract[axis] == -1 {
							prevRetract[axis] = 0
							retract = 1
						} else {
							retract = 0
						}

					}
					if err != nil {
						return
					}
					if retract == 0 && len(extruder) > 0 && !(prevX < 0 || prevY < 0 || prevZ < 0) {
						addExtruderMovement(extruderSum, extruder, extruderRelative)
					}
					if retract == 0 && len(extruder) > 0 {
						layers[prevZ] = true
					}
				}

				if stats.Min.X == 0 || stats.Min.X > x {
					stats.Min.X = x
				}
				if stats.Min.Y == 0 || stats.Min.Y > y {
					stats.Min.Y = y
				}
				if stats.Min.Z == 0 || stats.Min.Z > z {
					stats.Min.Z = z
				}

				if stats.Max.X == 0 || stats.Max.X < x {
					stats.Max.X = x
				}
				if stats.Max.Y == 0 || stats.Max.Y < y {
					stats.Max.Y = y
				}
				if stats.Max.Z == 0 || stats.Max.Z < z {
					stats.Max.Z = z
				}

			}
		}
	}

	if stats.CommandCount == 0 {
		return nil, nil
	}

	stats.FilamentExtrusion = extruderSum
	stats.LayerCount = len(layers)
	averageLayerHeight := stats.Max.Z / float64(len(layers))
	stats.AverageLayerHeight = float32(int(averageLayerHeight*100)) / 100

	return
}

func addExtruderMovement(sum, add map[string]float64, relative bool) {
	for k, v := range add {
		if _, exists := sum[k]; !exists || !relative {
			sum[k] = v
			continue
		}
		sum[k] = sum[k] + v
	}
}
