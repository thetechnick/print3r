package file

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

const testGcodeFile = "stack_box.gcode"

func TestAnalyse(t *testing.T) {
	// setup
	file, err := os.Open(testGcodeFile)
	if err != nil {
		t.Fatal(err)
		return
	}
	defer file.Close()

	assert := assert.New(t)
	result, err := Analyse(file)
	if assert.NoError(err, "Unexpected error in Analyse()") {
		assert.Equal(int(25890), result.CommandCount)
		assert.Equal(int(215), result.ExtruderTemperature)
		assert.Equal(int(55), result.BedTemperature)

		assert.Len(result.Commands, 16)
		assert.Equal(int(25042), result.Commands["G1"], "Number of G1 commands does not match")
		assert.Equal(float32(0.2), result.AverageLayerHeight)
		assert.Equal(241, result.LayerCount)
		_, eExists := result.FilamentExtrusion["E"]
		if assert.True(eExists, "E extruder should exist") {
			assert.True(result.FilamentExtrusion["E"] > 9535.6)
			assert.True(result.FilamentExtrusion["E"] < 9536.7)
		}
	}
}
