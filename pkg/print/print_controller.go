package print

import (
	"context"
	"reflect"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	db "github.com/thetechnick/print3r/pkg/storage"
)

// Controller runs print control loops
type Controller interface {
	Run() error
}

type printController struct {
	service      Service
	printStorage db.PrintStorage
	hub          db.EventHub

	log *log.Entry
}

// NewController creates a new print.Controller
func NewController(
	service Service,
	printStorage db.PrintStorage,
	hub db.EventHub,
) Controller {
	return &printController{
		service:      service,
		printStorage: printStorage,
		hub:          hub,

		log: log.WithField("module", "print.Controller"),
	}
}

func (c *printController) Run() (err error) {
	time.Sleep(5 * time.Second)

	client := c.hub.Register()

	ctx := context.TODO()
	var prints []*api.Print
	prints, err = c.printStorage.All(ctx)
	if err != nil {
		return
	}
	for _, p := range prints {
		if err := c.Sync(p); err != nil {
			c.log.WithError(err).WithField("name", p.Name).Warn("adding print")
		}
	}

	t := time.NewTicker(10 * time.Second)
	defer t.Stop()
	for {
		select {
		case <-t.C:
			var prints []*api.Print
			prints, err = c.printStorage.All(ctx)
			if err != nil {
				return
			}
			for _, p := range prints {
				if err := c.Sync(p); err != nil {
					c.log.WithError(err).WithField("name", p.Name).Warn("adding print")
				}
			}

		case event := <-client.Events():
			switch event.Operation {
			case api.ResourceEventOperationCreated:
				print, ok := event.New.(*api.Print)
				if !ok {
					continue
				}
				if err := c.Sync(print); err != nil {
					c.log.WithError(err).WithField("name", print.Name).Warn("adding print")
				}

			case api.ResourceEventOperationDeleted:
				print, ok := event.Old.(*api.Print)
				if !ok {
					continue
				}
				if err := c.Remove(print); err != nil {
					c.log.WithError(err).WithField("name", print.Name).Warn("removing print")
				}

			case api.ResourceEventOperationUpdated:
				oldPrint, ok := event.Old.(*api.Print)
				if !ok {
					continue
				}
				newPrint, ok := event.New.(*api.Print)
				if !ok {
					continue
				}

				if reflect.DeepEqual(oldPrint.Spec, newPrint.Spec) {
					continue
				}
				if err := c.Sync(newPrint); err != nil {
					c.log.WithError(err).WithField("name", newPrint.Name).Warn("adding print")
				}
			}
		}
	}
}

func (c *printController) Sync(p *api.Print) (err error) {
	c.log.WithField("name", p.Name).Debug("syncing print")

	ctx := context.TODO()
	switch p.Spec.State {
	case api.PrintSpecStatePrinted:
		if cond, ok := p.Status.Conditions.Get(api.PrintConditionTypeStarted); ok && cond.Status == api.ConditionStatusTrue {
			// has already started
			return
		}
		if err = c.service.Print(ctx, p); err != nil {
			return
		}
	case api.PrintSpecStateCanceled:
		if err = c.service.Cancel(ctx, p); err != nil {
			return
		}
	}

	return
}

func (c *printController) Remove(p *api.Print) (err error) {
	if err = c.service.Cancel(context.TODO(), p); err != nil {
		return
	}
	return
}
