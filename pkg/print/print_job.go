package print

import (
	"bufio"
	"context"
	"io"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/printer"
	"github.com/thetechnick/print3r/pkg/util"
)

// Job controls a print
type Job interface {
	util.Task
	StatusCh() <-chan api.PrintStatus
	Status() api.PrintStatus
}

type print struct {
	util.Task
	src          io.ReadCloser
	log          *log.Entry
	r            *bufio.Reader
	ctx          context.Context
	conn         printer.Connection
	fileAnalysis *api.FileAnalysis
	statusCh     chan api.PrintStatus

	stateLock             sync.RWMutex
	startedCondition      api.Condition
	pausedCondition       api.Condition
	canceledCondition     api.Condition
	finishedCondition     api.Condition
	commandsSend          uint64
	lastPublishedProgress float64
	initialHeatingDone    bool
	started               bool
}

var (
	notStartedReason               = "NotStarted"
	targetTemperatureReachedReason = "TargetTemperatureReached"
	belowTargetTemperature         = "BelowTargetTemperature"
	errorReason                    = "Error"
	canceledReason                 = "Canceled"
	resumedReason                  = "Resumed"
	pausedReason                   = "Paused"
	bedNotEnabledReason            = "BedNotEnabled"
)

// Available conditions types
var (
	ConditionStarted  = "Started"
	ConditionPaused   = "Paused"
	ConditionCanceled = "Canceled"
	ConditionFinished = "Finished"
)

// TODO fix missing types

// NewJob creates a new Job object
func NewJob(
	ctx context.Context,
	conn printer.Connection,
	fileAnalysis *api.FileAnalysis,
	src io.ReadCloser,
) Job {
	now := time.Now()
	p := &print{
		conn:         conn,
		src:          src,
		r:            bufio.NewReader(src),
		fileAnalysis: fileAnalysis,
		statusCh:     make(chan api.PrintStatus, 10),
		ctx:          ctx,
		log: log.
			WithField("module", "print"),
		startedCondition: api.Condition{
			Type:               ConditionStarted,
			Status:             api.ConditionStatusFalse,
			LastTransitionTime: now,
		},
		pausedCondition: api.Condition{
			Type:               ConditionPaused,
			Status:             api.ConditionStatusFalse,
			LastTransitionTime: now,
		},
		finishedCondition: api.Condition{
			Type:               ConditionFinished,
			Status:             api.ConditionStatusFalse,
			LastTransitionTime: now,
		},
		canceledCondition: api.Condition{
			Type:               ConditionCanceled,
			Status:             api.ConditionStatusFalse,
			LastTransitionTime: now,
		},
	}

	p.Task = util.NewTask(
		p.task,
		util.WithDoneHook(p.onDone),
		util.WithStartHook(p.onStart),
		util.WithCancelHook(p.onCancel),
		util.WithPauseHook(p.onPause),
		util.WithResumeHook(p.onResume),
	)
	return p
}

func (p *print) StatusCh() <-chan api.PrintStatus {
	return p.statusCh
}

func (p *print) Status() api.PrintStatus {
	p.stateLock.RLock()
	defer p.stateLock.RUnlock()
	return p.status()
}

func (p *print) status() api.PrintStatus {
	return api.PrintStatus{
		Progress: p.progress(),
		Conditions: api.NewConditionMapCollection([]api.Condition{
			p.startedCondition,
			p.canceledCondition,
			p.pausedCondition,
			p.finishedCondition,
		}),
	}
}

func (p *print) progress() float64 {
	return float64(p.commandsSend) / float64(p.fileAnalysis.CommandCount) * 100
}

func (p *print) publishStatus() {
	p.lastPublishedProgress = p.progress()
	p.statusCh <- api.PrintStatus{
		Progress: p.lastPublishedProgress,
		Conditions: api.NewConditionMapCollection([]api.Condition{
			p.startedCondition,
			p.canceledCondition,
			p.pausedCondition,
			p.finishedCondition,
		}),
	}
}

func (p *print) onStart() {
	p.stateLock.Lock()
	defer p.stateLock.Unlock()
	if p.started {
		return
	}
	p.started = true
	p.startedCondition = api.Condition{
		Type:               ConditionStarted,
		Status:             api.ConditionStatusTrue,
		LastTransitionTime: time.Now(),
	}
	p.publishStatus()

	if err := p.conn.EnableChecksum(p.ctx); err != nil {
		p.log.WithError(err).Error("enable checksum")
	}
}

func (p *print) onPause() {
	p.stateLock.Lock()
	defer p.stateLock.Unlock()
	p.pausedCondition = api.Condition{
		Type:               ConditionPaused,
		Status:             api.ConditionStatusTrue,
		Reason:             pausedReason,
		LastTransitionTime: time.Now(),
	}
	p.publishStatus()
}

func (p *print) onResume() {
	p.stateLock.Lock()
	defer p.stateLock.Unlock()
	p.pausedCondition = api.Condition{
		Type:               ConditionPaused,
		Status:             api.ConditionStatusFalse,
		Reason:             resumedReason,
		LastTransitionTime: time.Now(),
	}
	p.publishStatus()
}

func (p *print) onDone() {
	p.stateLock.Lock()
	defer p.stateLock.Unlock()

	if err := p.Error(); err == nil {
		// succeeded
		p.finishedCondition = api.Condition{
			Type:               ConditionFinished,
			Status:             api.ConditionStatusTrue,
			LastTransitionTime: time.Now(),
		}
	} else if err == util.ErrTaskCanceled {
		// canceled
		p.canceledCondition = api.Condition{
			Type:               ConditionCanceled,
			Status:             api.ConditionStatusTrue,
			Reason:             canceledReason,
			LastTransitionTime: time.Now(),
		}
	} else {
		// error
		p.canceledCondition = api.Condition{
			Type:               ConditionCanceled,
			Status:             api.ConditionStatusTrue,
			Reason:             errorReason,
			Message:            err.Error(),
			LastTransitionTime: time.Now(),
		}
	}
	p.publishStatus()
	if err := p.conn.DisableChecksum(p.ctx); err != nil {
		p.log.WithError(err).Error("disable checksum")
	}

	close(p.statusCh)
}

func (p *print) onCancel() {
	// turn off extruder heater
	if _, cerr := p.conn.Send(p.ctx, "M104 S0"); cerr != nil {
		p.log.WithError(cerr).Error("turn off extruder heater")
	}

	// turn off heatbed
	if _, cerr := p.conn.Send(p.ctx, "M140 S0"); cerr != nil {
		p.log.WithError(cerr).Error("turn off heatbed")
	}

	// turn off fan
	if _, cerr := p.conn.Send(p.ctx, "M107"); cerr != nil {
		p.log.WithError(cerr).Error("turn off fan")
	}

	return
}

func (p *print) task() (done bool, err error) {
	var line string
	line, err = p.r.ReadString('\n')
	if err != nil && err != io.EOF {
		return
	}

	line = util.RemoveComments(line)
	if line != "" {
		// count commands
		p.stateLock.Lock()
		p.commandsSend++
		p.stateLock.Unlock()

		if _, err = p.conn.Send(p.ctx, line); err != nil {
			return
		}
	}

	p.stateLock.RLock()
	progress := p.progress()
	if progress-p.lastPublishedProgress > 1 && progress != 100.0 {
		p.publishStatus()
	}
	p.stateLock.RUnlock()

	if err == io.EOF {
		// print finished
		p.log.Debug("finished")
		done = true
		err = nil
		return
	}
	return
}
