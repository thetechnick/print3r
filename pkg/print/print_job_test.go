package print

import (
	"context"
	"fmt"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/printer"
	. "github.com/thetechnick/print3r/pkg/printer/connection/test"
	"github.com/thetechnick/print3r/pkg/util"
)

func TestPrint(t *testing.T) {
	fileAnalysis := &api.FileAnalysis{
		CommandCount:        4,
		Commands:            map[string]int{},
		ExtruderTemperature: 0,
		BedTemperature:      0,
	}

	t.Run("Enable/Disable Checksum", func(t *testing.T) {
		assert := assert.New(t)

		cm := &ConnectionMock{}
		cm.On("EnableChecksum", mock.Anything).Return(nil)
		cm.On("DisableChecksum", mock.Anything).Return(nil)
		cm.On("LockForPrint", mock.Anything).Return(nil)

		ctx := context.Background()
		var (
			buf           util.ClosingBuffer
			statusUpdates []api.PrintStatus
			statusWG      sync.WaitGroup
		)
		p := NewJob(ctx, printer.Connection(cm), fileAnalysis, &buf)
		statusWG.Add(1)
		go func() {
			for s := range p.StatusCh() {
				statusUpdates = append(statusUpdates, s)
			}
			statusWG.Done()
		}()

		err := p.Start()
		assert.NoError(err)

		p.Wait()
		assert.NoError(p.Error())
		conditions := p.Status().Conditions
		if assert.Len(conditions, 4) {
			started, _ := conditions.Get(api.PrintConditionTypeStarted)
			assert.Equal(api.ConditionStatusTrue, started.Status)
			paused, _ := conditions.Get(api.PrintConditionTypePaused)
			assert.Equal(api.ConditionStatusFalse, paused.Status)
			finished, _ := conditions.Get(api.PrintConditionTypeFinished)
			assert.Equal(api.ConditionStatusTrue, finished.Status)
		}

		cm.AssertCalled(t, "EnableChecksum", mock.Anything)
		cm.AssertCalled(t, "DisableChecksum", mock.Anything)
		statusWG.Wait()
		assert.Len(statusUpdates, 2)
	})

	t.Run("Cancel", func(t *testing.T) {
		assert := assert.New(t)

		cm := &ConnectionMock{}
		cm.On("EnableChecksum", mock.Anything).Return(nil)
		cm.On("DisableChecksum", mock.Anything).Return(nil)
		cm.On("LockForPrint", mock.Anything).Return(nil)
		cm.On("Send", mock.Anything, mock.Anything).Return("", nil).Once()
		cm.On("Send", mock.Anything, mock.Anything).Return("", nil).Once()
		cm.On("Send", mock.Anything, mock.Anything).Return("", nil).Once()
		cm.On("Print", mock.Anything).Return(nil)

		ctx := context.Background()
		var (
			buf           util.ClosingBuffer
			statusUpdates []api.PrintStatus
			statusWG      sync.WaitGroup
		)
		p := NewJob(ctx, printer.Connection(cm), fileAnalysis, &buf)
		statusWG.Add(1)
		go func() {
			for s := range p.StatusCh() {
				statusUpdates = append(statusUpdates, s)
			}
			statusWG.Done()
		}()
		p.Start()
		p.Cancel()
		p.Wait()
		err := p.Error()
		assert.Equal(util.ErrTaskCanceled, err)
		assert.Equal(0.0, p.Status().Progress)
		conditions := p.Status().Conditions
		if assert.Len(conditions, 4) {
			started, _ := conditions.Get(api.PrintConditionTypeStarted)
			assert.Equal(api.ConditionStatusTrue, started.Status)

			paused, _ := conditions.Get(api.PrintConditionTypePaused)
			assert.Equal(api.ConditionStatusFalse, paused.Status)

			canceled, _ := conditions.Get(api.PrintConditionTypeCanceled)
			assert.Equal(api.ConditionStatusTrue, canceled.Status)

			finished, _ := conditions.Get(api.PrintConditionTypeFinished)
			assert.Equal(api.ConditionStatusFalse, finished.Status)
		}

		cm.AssertCalled(t, "EnableChecksum", mock.Anything)
		cm.AssertCalled(t, "DisableChecksum", mock.Anything)
		cm.AssertCalled(t, "Send", mock.Anything, "M104 S0")
		cm.AssertCalled(t, "Send", mock.Anything, "M140 S0")
		cm.AssertCalled(t, "Send", mock.Anything, "M107")

		statusWG.Wait()
		assert.Len(statusUpdates, 2)
	})

	t.Run("Print", func(t *testing.T) {
		assert := assert.New(t)

		cm := &ConnectionMock{}
		cm.On("EnableChecksum", mock.Anything).Return(nil)
		cm.On("DisableChecksum", mock.Anything).Return(nil)
		cm.On("LockForPrint", mock.Anything).Return(nil)
		cm.On("Send", mock.Anything, mock.Anything).Return("", nil).Once()
		cm.On("Send", mock.Anything, mock.Anything).Return("", nil).Once()
		cm.On("Send", mock.Anything, mock.Anything).Return("", nil).Once()
		cm.On("Send", mock.Anything, mock.Anything).Return("", nil).Once()
		cm.On("Print", mock.Anything).Return(nil)

		ctx := context.Background()
		var (
			buf           util.ClosingBuffer
			statusUpdates []api.PrintStatus
			statusWG      sync.WaitGroup
		)
		fmt.Fprint(&buf, "M104 S100\nM140 S100\nM105\nM105")
		p := NewJob(ctx, printer.Connection(cm), fileAnalysis, &buf)
		statusWG.Add(1)
		go func() {
			for s := range p.StatusCh() {
				statusUpdates = append(statusUpdates, s)
			}
			statusWG.Done()
		}()
		p.Start()
		p.Wait()

		err := p.Error()
		assert.NoError(err)
		assert.Equal(100.0, p.Status().Progress)
		conditions := p.Status().Conditions
		if assert.Len(conditions, 4) {
			started, _ := conditions.Get(api.PrintConditionTypeStarted)
			assert.Equal(api.ConditionStatusTrue, started.Status)

			paused, _ := conditions.Get(api.PrintConditionTypePaused)
			assert.Equal(api.ConditionStatusFalse, paused.Status)

			canceled, _ := conditions.Get(api.PrintConditionTypeCanceled)
			assert.Equal(api.ConditionStatusFalse, canceled.Status)

			finished, _ := conditions.Get(api.PrintConditionTypeFinished)
			assert.Equal(api.ConditionStatusTrue, finished.Status)
		}

		cm.AssertCalled(t, "EnableChecksum", mock.Anything)
		cm.AssertCalled(t, "DisableChecksum", mock.Anything)
		cm.AssertNumberOfCalls(t, "Send", 4)

		statusWG.Wait()
		assert.Len(statusUpdates, 5)
		for _, s := range statusUpdates {
			fmt.Printf("%#v\n", s)
		}
	})
}
