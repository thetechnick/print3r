package print

import (
	"context"
	"fmt"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/file"
	printerpkg "github.com/thetechnick/print3r/pkg/printer"
	db "github.com/thetechnick/print3r/pkg/storage"
	"github.com/thetechnick/print3r/pkg/volume"
)

// Service groups print functionality
type Service interface {
	Print(ctx context.Context, p *api.Print) error
	Cancel(ctx context.Context, p *api.Print) error
	UpdateStatus(ctx context.Context, p *api.Print) error
}

type service struct {
	printerStorage    db.PrinterStorage
	printStorage      db.PrintStorage
	volumeStorage     db.VolumeStorage
	connectionManager printerpkg.ConnectionManager
	volumeController  volume.Service

	printLock sync.Mutex
	prints    map[string]Job

	log *log.Entry
}

// NewService creates a new print.Service
func NewService(
	printerStorage db.PrinterStorage,
	printStorage db.PrintStorage,
	volumeController volume.Service,
	volumeStorage db.VolumeStorage,
	connectionManager printerpkg.ConnectionManager,
) Service {
	return &service{
		printerStorage:    printerStorage,
		printStorage:      printStorage,
		connectionManager: connectionManager,
		volumeController:  volumeController,
		volumeStorage:     volumeStorage,

		prints: map[string]Job{},
		log:    log.WithField("module", "print.Service"),
	}
}

func (s *service) Cancel(ctx context.Context, p *api.Print) (err error) {
	if print, ok := s.prints[p.UID]; ok {
		if err = print.Cancel(); err != nil {
			return
		}
		status := print.Status()
		if err = s.updatePrintStatus(p, status); err != nil {
			return
		}
		p.Status = status
	} else {
		p.Status.Conditions.Set(api.Condition{
			Type:               ConditionCanceled,
			Status:             api.ConditionStatusTrue,
			Reason:             canceledReason,
			LastTransitionTime: time.Now(),
		})
		if err = s.updatePrintStatus(p, p.Status); err != nil {
			return
		}
	}
	return
}

func (s *service) UpdateStatus(ctx context.Context, p *api.Print) (err error) {
	// TODO remove
	if print, ok := s.prints[p.UID]; ok {
		status := print.Status()
		if err = s.updatePrintStatus(p, status); err != nil {
			return
		}
		p.Status = status
	}
	return
}

func (s *service) Print(ctx context.Context, p *api.Print) (err error) {
	s.log.WithField("name", p.Name).Info("Starting print")
	defer func() {
		if err != nil {
			p.Status.Conditions.Set(api.Condition{
				Type:               api.PrintConditionTypeStarted,
				Status:             api.ConditionStatusFalse,
				Reason:             "StartError",
				Message:            err.Error(),
				LastTransitionTime: time.Now(),
			})
			if err = s.printStorage.UpdateStatus(ctx, p); err != nil {
				s.log.WithError(err).Error("error updating print status")
				return
			}
		}
	}()

	var v *api.Volume
	v, err = s.volumeStorage.FindOneByName(ctx, p.Spec.VolumeName())
	if err != nil {
		return
	}
	if v == nil {
		err = fmt.Errorf("volume not found")
		return
	}

	var printer *api.Printer
	printer, err = s.printerStorage.FindOneByName(ctx, p.Spec.Printer)
	if err != nil {
		return
	}
	if printer == nil {
		err = fmt.Errorf("printer not found")
		return
	}

	if c, ok := printer.Status.Conditions.Get(api.PrinterConditionTypeConnected); !ok || c.Status != api.ConditionStatusTrue {
		err = fmt.Errorf("printer not connected")
		return
	}
	if c, ok := printer.Status.Conditions.Get(api.PrinterConditionTypeBedClean); !ok || c.Status != api.ConditionStatusTrue {
		err = fmt.Errorf("printer bed not clean")
		return
	}

	vsc := s.volumeController.Source(ctx, v)
	if vsc == nil {
		err = fmt.Errorf("volume not found")
		return
	}
	var f volume.File
	f, err = vsc.Get(ctx, p.Spec.FileRef.File)
	if err != nil {
		return
	}

	var analysis *api.FileAnalysis
	analysis, err = file.Analyse(f)
	if err != nil {
		return
	}

	if _, err = f.Seek(0, 0); err != nil {
		return
	}

	s.log.Debug("aquiring connection")
	var conn printerpkg.Connection
	conn, err = s.connectionManager.AquireConnection(ctx, printer)
	if err != nil {
		return
	}
	s.log.Debug("aquired connection")

	job := NewJob(ctx, conn, analysis, f)
	s.printLock.Lock()
	s.prints[p.UID] = job
	s.printLock.Unlock()

	_, _, err = s.printerStorage.EnsureUpdate(ctx, printer, db.PrinterUpdateFn(func(printer *api.Printer) (err error) {
		printer.Status.Conditions.Set(api.Condition{
			Type:               api.PrinterConditionTypePrinting,
			Status:             api.ConditionStatusTrue,
			LastTransitionTime: time.Now(),
		})
		printer.Status.Conditions.Set(api.Condition{
			Type:               api.PrinterConditionTypeBedClean,
			Status:             api.ConditionStatusFalse,
			Reason:             "StartedPrinting",
			LastTransitionTime: time.Now(),
		})
		printer.Status.Print = &api.PrinterPrintStatus{
			Name: p.Name,
		}
		return
	}))
	if err != nil {
		s.log.WithError(err).Error("error updating printer status")
		err = nil
	}

	s.log.WithField("name", p.Name).Info("starting print")
	if err = job.Start(); err != nil {
		return
	}
	s.updatePrintStatus(p, job.Status())

	go func() {
		for status := range job.StatusCh() {
			s.updatePrintStatus(p, status)
		}
	}()

	go func() {
		job.Wait()
		s.printLock.Lock()
		if _, ok := s.prints[p.UID]; ok {
			delete(s.prints, p.UID)
		}
		s.printLock.Unlock()
		_, _, err := s.printerStorage.EnsureUpdate(ctx, printer, db.PrinterUpdateFn(func(printer *api.Printer) (err error) {
			printer.Status.Conditions.Set(api.Condition{
				Type:               api.PrinterConditionTypePrinting,
				Status:             api.ConditionStatusFalse,
				LastTransitionTime: time.Now(),
			})
			printer.Status.Print = nil
			return
		}))
		if err != nil {
			s.log.WithError(err).Error("error updating printer status")
		}
		s.connectionManager.ReleaseConnection(ctx, printer)
	}()

	return
}

func (s *service) updatePrintStatus(p *api.Print, status api.PrintStatus) (err error) {
	ctx := context.TODO()
	for {
		if p, err = s.printStorage.FindOneByName(ctx, p.Name); err != nil {
			return err
		}
		if p == nil {
			return
		}
		if status.Progress == 0 {
			status.Progress = p.Status.Progress
		}
		p.Status = status
		if err = s.printStorage.UpdateStatus(ctx, p); err != nil {
			if err == db.ErrNotFound {
				return nil
			}
			if err == db.ErrResourceVersionChanged {
				continue
			}
			return
		}
		return
	}
}
