package api

// Config holds the information needed connect to the server
type Config struct {
	Servers        map[string]*Server  `json:"servers"`
	Contexts       map[string]*Context `json:"contexts"`
	CurrentContext string              `json:"currentContext"`
}

// Server contains information about how to communicate with the server
type Server struct {
	// The Address of the server (https://hostname:port)
	Address string `json:"address"`
	// CertificateAuthority is the path to a cert file containing PEM-encoded certificate authority certificates
	CertificateAuthority string `json:"certificateAuthority,omitempty"`
	// CertificateAuthorityData contains PEM-encoded certificate authority certificates. Overrides CertificateAuthority
	CertificateAuthorityData []byte `json:"certificateAuthorityData,omitempty"`
}

// NewServer convinience function to create Servers
func NewServer() *Server {
	return &Server{}
}

// Context is a reference to other config options
type Context struct {
	Server         string `json:"server"`
	DefaultPrinter string `json:"defaultPrinter"`
}

// NewContext convinience function to create Servers
func NewContext() *Context {
	return &Context{}
}

func (c Config) DefaultPrinter() string {
	if context, cok := c.Contexts[c.CurrentContext]; cok {
		return context.DefaultPrinter
	}
	return ""
}
