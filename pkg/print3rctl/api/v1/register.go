package v1

import (
	"sort"

	"github.com/thetechnick/print3r/pkg/api/runtime"
	"github.com/thetechnick/print3r/pkg/api/runtime/conversion"
	"github.com/thetechnick/print3r/pkg/print3rctl/api"
)

// Scheme register variables
var (
	Scheme *runtime.Scheme
)

func init() {
	Scheme = runtime.NewScheme()
	if err := addConversionFuncs(Scheme); err != nil {
		panic(err)
	}
}

func addConversionFuncs(scheme *runtime.Scheme) error {
	return scheme.AddConversionFuncs(
		// Server
		func(in *Server, out *api.Server, s conversion.Scope) error {
			out.Address = in.Address
			out.CertificateAuthority = in.CertificateAuthority
			out.CertificateAuthorityData = in.CertificateAuthorityData
			return nil
		},
		func(in *api.Server, out *Server, s conversion.Scope) error {
			out.Address = in.Address
			out.CertificateAuthority = in.CertificateAuthority
			out.CertificateAuthorityData = in.CertificateAuthorityData
			return nil
		},

		// Context
		func(in *Context, out *api.Context, s conversion.Scope) error {
			out.DefaultPrinter = in.DefaultPrinter
			out.Server = in.Server
			return nil
		},
		func(in *api.Context, out *Context, s conversion.Scope) error {
			out.DefaultPrinter = in.DefaultPrinter
			out.Server = in.Server
			return nil
		},

		// Config
		func(in *Config, out *api.Config, s conversion.Scope) error {
			out.CurrentContext = in.CurrentContext

			out.Servers = make(map[string]*api.Server)
			if err := s.Convert(&in.Servers, &out.Servers); err != nil {
				return err
			}
			out.Contexts = make(map[string]*api.Context)
			if err := s.Convert(&in.Contexts, &out.Contexts); err != nil {
				return err
			}
			return nil
		},
		func(in *api.Config, out *Config, s conversion.Scope) error {
			out.CurrentContext = in.CurrentContext

			out.Servers = make([]NamedServer, 0, 0)
			if err := s.Convert(&in.Servers, &out.Servers); err != nil {
				return err
			}
			out.Contexts = make([]NamedContext, 0, 0)
			if err := s.Convert(&in.Contexts, &out.Contexts); err != nil {
				return err
			}
			return nil
		},

		// NamedServer
		func(in *[]NamedServer, out *map[string]*api.Server, s conversion.Scope) error {
			for _, curr := range *in {
				newServer := api.NewServer()
				if err := s.Convert(&curr.Server, newServer); err != nil {
					return err
				}
				(*out)[curr.Name] = newServer
			}

			return nil
		},
		func(in *map[string]*api.Server, out *[]NamedServer, s conversion.Scope) error {
			allKeys := make([]string, 0, len(*in))
			for key := range *in {
				allKeys = append(allKeys, key)
			}
			sort.Strings(allKeys)

			for _, key := range allKeys {
				newServer := (*in)[key]
				oldServer := &Server{}
				if err := s.Convert(newServer, oldServer); err != nil {
					return err
				}

				namedServer := NamedServer{key, *oldServer}
				*out = append(*out, namedServer)
			}

			return nil
		},

		// NamedContext
		func(in *[]NamedContext, out *map[string]*api.Context, s conversion.Scope) error {
			for _, curr := range *in {
				newContext := api.NewContext()
				if err := s.Convert(&curr.Context, newContext); err != nil {
					return err
				}
				(*out)[curr.Name] = newContext
			}

			return nil
		},
		func(in *map[string]*api.Context, out *[]NamedContext, s conversion.Scope) error {
			allKeys := make([]string, 0, len(*in))
			for key := range *in {
				allKeys = append(allKeys, key)
			}
			sort.Strings(allKeys)

			for _, key := range allKeys {
				newContext := (*in)[key]
				oldContext := &Context{}
				if err := s.Convert(newContext, oldContext); err != nil {
					return err
				}

				namedContext := NamedContext{key, *oldContext}
				*out = append(*out, namedContext)
			}

			return nil
		},
	)
}
