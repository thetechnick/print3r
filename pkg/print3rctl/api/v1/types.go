package v1

// Config holds the information needed to connect to the server
type Config struct {
	Servers        []NamedServer  `json:"servers"`
	Contexts       []NamedContext `json:"contexts"`
	CurrentContext string         `json:"currentContext"`
}

// Context is a reference to other config options
type Context struct {
	// Server is the name of the server for this context
	Server string `json:"server"`
	// DefaultPrinter contains the default printer to use with printer commands
	DefaultPrinter string `json:"defaultPrinter"`
}

// NamedContext relates a name to context information
type NamedContext struct {
	Name    string  `json:"name"`
	Context Context `json:"context"`
}

// Server contains information about how to communicate with the server
type Server struct {
	// The Address of the server (https://hostname:port)
	Address string `json:"address"`
	// CertificateAuthority is the path to a cert file containing PEM-encoded certificate authority certificates
	CertificateAuthority string `json:"certificateAuthority,omitempty"`
	// CertificateAuthorityData contains PEM-encoded certificate authority certificates. Overrides CertificateAuthority
	CertificateAuthorityData []byte `json:"certificateAuthorityData,omitempty"`
}

// NamedServer relates a name to server information
type NamedServer struct {
	Name   string `json:"name"`
	Server Server `json:"server"`
}
