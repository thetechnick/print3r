package v1

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/thetechnick/print3r/pkg/print3rctl/api"
	yaml "gopkg.in/yaml.v2"
)

func TestConfig(t *testing.T) {
	configYaml := `servers:
- name: dev
  server:
    address: localhost:8081
contexts:
- name: dev
  context:
    server: dev
current-context: dev`

	v1Config := &Config{}
	yaml.Unmarshal([]byte(configYaml), v1Config)

	internalConfig := &api.Config{}
	err := Scheme.Convert(v1Config, internalConfig)
	assert.NoError(t, err)
}
