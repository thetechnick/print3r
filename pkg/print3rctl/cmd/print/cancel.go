package print

import (
	"context"
	"fmt"
	"io"

	"github.com/spf13/cobra"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/print3rctl"
)

func newPrintCancelCommand(f print3rctl.Factory, out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:              "cancel",
		Short:            "Cancels a print",
		TraverseChildren: true,
		Run: func(cmd *cobra.Command, args []string) {
			printCancelOptions := &printCancelOptions{}
			print3rctl.CheckErr(printCancelOptions.Complete(cmd, args))
			print3rctl.CheckErr(printCancelOptions.Validate())
			print3rctl.CheckErr(printCancelOptions.Run(f, out))
		},
	}
	return cmd
}

type printCancelOptions struct {
	name string
}

func (o *printCancelOptions) Complete(cmd *cobra.Command, args []string) error {
	o.name = args[0]
	return nil
}

func (o *printCancelOptions) Validate() error {
	return nil
}

func (o *printCancelOptions) Run(f print3rctl.Factory, out io.Writer) (err error) {
	ctx := context.Background()
	var print *api.Print
	print, _, err = f.Client().Print().Cancel(ctx, &api.Print{
		ObjectMeta: api.ObjectMeta{
			Name: o.name,
		},
	})
	if err != nil {
		return
	}
	fmt.Fprintf(out, "Print %q canceled.\n", print.Name)
	return
}
