package print

import (
	"context"
	"errors"
	"fmt"
	"io"

	"github.com/spf13/cobra"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/print3rctl"
)

func newPrintCreateCommand(f print3rctl.Factory, out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:              "create",
		Short:            "Create a new print",
		TraverseChildren: true,
		Run: func(cmd *cobra.Command, args []string) {
			printCreateOptions := &printCreateOptions{}
			print3rctl.CheckErr(printCreateOptions.Complete(cmd))
			print3rctl.CheckErr(printCreateOptions.Validate())
			print3rctl.CheckErr(printCreateOptions.Run(f, out))
		},
	}
	cmd.Flags().String("name", "", "Name of the print")
	cmd.Flags().String("file", "", "Name of the file to print")
	cmd.Flags().String("volume", "", "Name of the volume of the file to print")
	cmd.Flags().String("printer", "", "Name of the printer to print on")
	return cmd
}

type printCreateOptions struct {
	name, printerName, fileName, volumeName string
}

func (o *printCreateOptions) Complete(cmd *cobra.Command) error {
	o.name = print3rctl.GetFlagString(cmd, "name")
	o.volumeName = print3rctl.GetFlagString(cmd, "volume")
	o.fileName = print3rctl.GetFlagString(cmd, "file")
	o.printerName = print3rctl.GetFlagString(cmd, "printer")
	return nil
}

func (o *printCreateOptions) Validate() error {
	if o.fileName == "" {
		return errors.New(`--file must be set`)
	}
	if o.printerName == "" {
		return errors.New(`--printer must be set`)
	}
	return nil
}

func (o *printCreateOptions) Run(f print3rctl.Factory, out io.Writer) (err error) {
	ctx := context.Background()
	var print *api.Print
	print, _, err = f.Client().Print().Create(ctx, &api.Print{
		ObjectMeta: api.ObjectMeta{
			Name: o.name,
		},
		Spec: api.PrintSpec{
			FileRef: api.FileRef{
				File:   o.fileName,
				Volume: o.volumeName,
			},
			Printer: o.printerName,
		},
	})
	if err != nil {
		return
	}
	fmt.Fprintf(out, "Print %q created.\n", print.Name)
	return
}
