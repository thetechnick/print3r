package print

import (
	"context"
	"errors"
	"fmt"
	"io"

	"github.com/spf13/cobra"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/print3rctl"
)

func newPrintDeleteCommand(f print3rctl.Factory, out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:              "delete",
		Short:            "Delete the print",
		TraverseChildren: true,
		Args:             cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			printDeleteOptions := &printDeleteOptions{}
			print3rctl.CheckErr(printDeleteOptions.Complete(cmd, args, f))
			print3rctl.CheckErr(printDeleteOptions.Validate())
			print3rctl.CheckErr(printDeleteOptions.Run(f, out))
		},
	}

	return cmd
}

type printDeleteOptions struct {
	name string
}

func (o *printDeleteOptions) Complete(cmd *cobra.Command, args []string, f print3rctl.Factory) error {
	if len(args) == 0 {
		return nil
	}
	o.name = args[0]
	return nil
}

func (o *printDeleteOptions) Validate() error {
	if o.name == "" {
		return errors.New("no print name")
	}
	return nil
}

func (o *printDeleteOptions) Run(f print3rctl.Factory, out io.Writer) (err error) {
	ctx := context.Background()
	var deleted *api.Print
	deleted, _, err = f.Client().Print().Delete(ctx, &api.Print{
		ObjectMeta: api.ObjectMeta{
			Name: o.name,
		},
	})
	if err != nil {
		return
	}

	fmt.Fprintf(out, "Print %q deleted\n", deleted.Name)
	return
}
