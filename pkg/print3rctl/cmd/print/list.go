package print

import (
	"context"
	"errors"
	"fmt"
	"io"

	"github.com/ghodss/yaml"
	"github.com/gogo/protobuf/jsonpb"
	"github.com/spf13/cobra"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/client"
	"github.com/thetechnick/print3r/pkg/api/v1"
	"github.com/thetechnick/print3r/pkg/print"
	"github.com/thetechnick/print3r/pkg/print3rctl"
	"github.com/thetechnick/print3r/pkg/print3rctl/formatter"
)

func newPrintListCommand(f print3rctl.Factory, out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:              "list",
		Short:            "List all prints",
		TraverseChildren: true,
		Run: func(cmd *cobra.Command, args []string) {
			printListOptions := &printListOptions{}
			print3rctl.CheckErr(printListOptions.Complete(cmd))
			print3rctl.CheckErr(printListOptions.Validate())
			print3rctl.CheckErr(printListOptions.Run(f, out))
		},
	}
	cmd.Flags().BoolP("watch", "w", false, "Watch for changes")
	cmd.Flags().Bool("no-header", false, "Do not print table header")
	cmd.Flags().StringP("output", "o", "", "One of 'yaml' or 'json'.")
	return cmd
}

type printListOptions struct {
	watch    bool
	noHeader bool
	output   string
}

func (o *printListOptions) Complete(cmd *cobra.Command) error {
	o.watch = print3rctl.GetFlagBool(cmd, "watch")
	o.noHeader = print3rctl.GetFlagBool(cmd, "no-header")
	o.output = print3rctl.GetFlagString(cmd, "output")
	return nil
}

func (o *printListOptions) Validate() error {
	if o.output != "" &&
		o.output != "yaml" &&
		o.output != "json" {
		return errors.New(`--output must be 'yaml' or 'json'`)
	}
	if o.output != "" && o.watch {
		return errors.New(`--watch and --output can't be combined`)
	}
	return nil
}

func (o *printListOptions) Run(f print3rctl.Factory, out io.Writer) (err error) {
	var (
		v1List    *v1.PrintList
		list      *api.PrintList
		marshaler = &jsonpb.Marshaler{OrigName: true}
	)
	ctx := context.Background()

	list, v1List, err = f.Client().Print().List(ctx, api.PrintListRequest{})
	if err != nil {
		return
	}

	switch o.output {
	case "":
		to := formatter.NewTableOutput().
			AddAllowedFields(api.Print{}).
			AddFieldOutputFn("progress", func(obj interface{}) string {
				p := obj.(*api.Print)
				return fmt.Sprintf("%.1f %%", p.Status.Progress)
			}).
			AddFieldOutputFn("volume", func(obj interface{}) string {
				p := obj.(*api.Print)
				return p.Spec.FileRef.Volume
			}).
			AddFieldOutputFn("file", func(obj interface{}) string {
				p := obj.(*api.Print)
				return p.Spec.FileRef.File
			}).
			AddFieldOutputFn("printer", func(obj interface{}) string {
				p := obj.(*api.Print)
				return p.Spec.Printer
			}).
			AddFieldOutputFn("status", func(obj interface{}) string {
				p := obj.(*api.Print)
				var started bool
				for _, c := range p.Status.Conditions.List() {
					switch c.Type {
					case print.ConditionPaused:
						if c.Status == api.ConditionStatusTrue {
							return "Paused"
						}
					case print.ConditionCanceled:
						if c.Status == api.ConditionStatusTrue {
							return "Canceled"
						}
					case print.ConditionFinished:
						if c.Status == api.ConditionStatusTrue {
							return "Finished"
						}
					case print.ConditionStarted:
						if c.Status == api.ConditionStatusTrue {
							started = true
						}
					}
				}
				if started {
					return "Running"
				}
				return "Pending"
			})

		cols := []string{"name", "progress", "volume", "file", "printer", "status"}
		if !o.noHeader {
			to.WriteHeader(cols)
		}
		for _, print := range list.Items {
			to.Write(cols, print)
		}
		to.Flush()

		if o.watch {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			var stream client.ResourceEventStreamClient
			stream, err = f.Client().ResourceEvent().Stream(ctx, client.ResourceEventStreamOpts{
				Type: "print3r.api.v1.Print",
			})
			if err != nil {
				return err
			}
			for {
				var event *api.ResourceEvent
				event, _, err = stream.Recv()
				if err == io.EOF {
					break
				}
				if err != nil {
					return err
				}
				if event.Operation == api.ResourceEventOperationDeleted {
					to.Write(cols, event.Old)
				} else {
					to.Write(cols, event.New)
				}
				to.Flush()
			}
		}

	case "yaml":
		var json string
		if json, err = marshaler.MarshalToString(v1List); err != nil {
			return
		}

		var b []byte
		b, err = yaml.JSONToYAML([]byte(json))
		if err != nil {
			return
		}
		fmt.Fprintln(out, string(b))

	case "json":
		var b string
		if b, err = marshaler.MarshalToString(v1List); err != nil {
			return
		}
		fmt.Fprint(out, b)
	}

	return nil
}
