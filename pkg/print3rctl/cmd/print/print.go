package print

import (
	"io"

	"github.com/spf13/cobra"
	"github.com/thetechnick/print3r/pkg/print3rctl"
)

func NewPrintCommand(f print3rctl.Factory, out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "print",
		Short: "Manage prints",
		Run: func(cmd *cobra.Command, args []string) {
			print3rctl.CheckErr(cmd.Usage())
		},
		TraverseChildren: true,
		SilenceUsage:     true,
		SilenceErrors:    true,
	}
	cmd.AddCommand(
		newPrintListCommand(f, out),
		newPrintCreateCommand(f, out),
		newPrintCancelCommand(f, out),
		newPrintDeleteCommand(f, out),
	)
	return cmd
}
