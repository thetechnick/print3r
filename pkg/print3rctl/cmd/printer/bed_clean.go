package printer

import (
	"context"
	"errors"
	"fmt"
	"io"
	"time"

	"github.com/spf13/cobra"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/print3rctl"
)

func newPrinterBedCleanCommand(f print3rctl.Factory, out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:              "bed-clean",
		Short:            "Signal a printer's bed is clean",
		TraverseChildren: true,
		Args:             cobra.MaximumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			printerBedCleanOptions := &printerBedCleanOptions{}
			print3rctl.CheckErr(printerBedCleanOptions.Complete(cmd, args, f))
			print3rctl.CheckErr(printerBedCleanOptions.Validate())
			print3rctl.CheckErr(printerBedCleanOptions.Run(f, out))
		},
	}

	return cmd
}

type printerBedCleanOptions struct {
	printerName string
}

func (o *printerBedCleanOptions) Complete(cmd *cobra.Command, args []string, f print3rctl.Factory) error {
	if len(args) > 0 {
		o.printerName = args[0]
	}
	o.printerName = fillPrinterID(o.printerName, cmd, f)
	return nil
}

func (o *printerBedCleanOptions) Validate() error {
	if o.printerName == "" {
		return errors.New("no printer id provided and no default printer is set in the configuration")
	}
	return nil
}

func (o *printerBedCleanOptions) Run(f print3rctl.Factory, out io.Writer) (err error) {
	var printer *api.Printer
	ctx := context.Background()

	printer, _, err = f.Client().Printer().Show(ctx, o.printerName)
	if err != nil {
		return
	}
	printer.Status.Conditions.Set(api.Condition{
		Type:               api.PrinterConditionTypeBedClean,
		Status:             api.ConditionStatusTrue,
		LastTransitionTime: time.Now(),
	})

	printer, _, err = f.Client().Printer().UpdateStatus(ctx, printer)
	if err != nil {
		return
	}

	fmt.Fprintf(out, "Printer %q bed is now clean\n", printer.Name)
	return
}
