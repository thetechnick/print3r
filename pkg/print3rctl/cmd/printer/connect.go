package printer

import (
	"context"
	"errors"
	"fmt"
	"io"

	"github.com/spf13/cobra"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/print3rctl"
)

func newPrinterConnectCommand(f print3rctl.Factory, out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:              "connect",
		Short:            "Open a connection to the printer",
		TraverseChildren: true,
		Args:             cobra.MaximumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			printerConnectOptions := &printerConnectOptions{}
			print3rctl.CheckErr(printerConnectOptions.Complete(cmd, args, f))
			print3rctl.CheckErr(printerConnectOptions.Validate())
			print3rctl.CheckErr(printerConnectOptions.Run(f, out))
		},
	}

	return cmd
}

type printerConnectOptions struct {
	printerName string
}

func (o *printerConnectOptions) Complete(cmd *cobra.Command, args []string, f print3rctl.Factory) error {
	if len(args) > 0 {
		o.printerName = args[0]
	}
	o.printerName = fillPrinterID(o.printerName, cmd, f)
	return nil
}

func (o *printerConnectOptions) Validate() error {
	if o.printerName == "" {
		return errors.New("no printer id provided and no default printer is set in the configuration")
	}
	return nil
}

func (o *printerConnectOptions) Run(f print3rctl.Factory, out io.Writer) (err error) {
	var printer *api.Printer
	ctx := context.Background()

	printer, _, err = f.Client().Printer().Show(ctx, o.printerName)
	if err != nil {
		return
	}
	printer, _, err = f.Client().Printer().Connect(ctx, printer)
	if err != nil {
		return
	}

	fmt.Fprintf(out, "Printer %q connected\n", printer.Name)
	return
}
