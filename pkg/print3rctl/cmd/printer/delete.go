package printer

import (
	"context"
	"errors"
	"fmt"
	"io"

	"github.com/spf13/cobra"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/print3rctl"
)

func newPrinterDeleteCommand(f print3rctl.Factory, out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:              "delete",
		Short:            "Delete the printer",
		TraverseChildren: true,
		Args:             cobra.MaximumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			printerDeleteOptions := &printerDeleteOptions{}
			print3rctl.CheckErr(printerDeleteOptions.Complete(cmd, args, f))
			print3rctl.CheckErr(printerDeleteOptions.Validate())
			print3rctl.CheckErr(printerDeleteOptions.Run(f, out))
		},
	}

	return cmd
}

type printerDeleteOptions struct {
	printerName string
}

func (o *printerDeleteOptions) Complete(cmd *cobra.Command, args []string, f print3rctl.Factory) error {
	if len(args) > 0 {
		o.printerName = args[0]
	}
	o.printerName = fillPrinterID(o.printerName, cmd, f)
	return nil
}

func (o *printerDeleteOptions) Validate() error {
	if o.printerName == "" {
		return errors.New("no printer name provided and no default printer is set in the configuration")
	}
	return nil
}

func (o *printerDeleteOptions) Run(f print3rctl.Factory, out io.Writer) (err error) {
	ctx := context.Background()
	var deleted *api.Printer
	deleted, _, err = f.Client().Printer().Delete(ctx, &api.Printer{
		ObjectMeta: api.ObjectMeta{
			Name: o.printerName,
		},
	})
	if err != nil {
		return
	}

	fmt.Fprintf(out, "Printer %q delete\n", deleted.Name)
	return
}
