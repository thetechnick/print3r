package printer

import (
	"context"
	"errors"
	"fmt"
	"io"

	"github.com/spf13/cobra"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/print3rctl"
)

func newPrinterDisconnectCommand(f print3rctl.Factory, out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:              "disconnect",
		Short:            "Closes a connection to the printer",
		TraverseChildren: true,
		Args:             cobra.MaximumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			printerDisconnectOptions := &printerDisconnectOptions{}
			print3rctl.CheckErr(printerDisconnectOptions.Complete(cmd, args, f))
			print3rctl.CheckErr(printerDisconnectOptions.Validate())
			print3rctl.CheckErr(printerDisconnectOptions.Run(f, out))
		},
	}

	return cmd
}

type printerDisconnectOptions struct {
	printerName string
}

func (o *printerDisconnectOptions) Complete(cmd *cobra.Command, args []string, f print3rctl.Factory) error {
	if len(args) > 0 {
		o.printerName = args[0]
	}
	o.printerName = fillPrinterID(o.printerName, cmd, f)
	return nil
}

func (o *printerDisconnectOptions) Validate() error {
	if o.printerName == "" {
		return errors.New("no printer id provided and no default printer is set in the configuration")
	}
	return nil
}

func (o *printerDisconnectOptions) Run(f print3rctl.Factory, out io.Writer) (err error) {
	var printer *api.Printer
	ctx := context.Background()

	printer, _, err = f.Client().Printer().Show(ctx, o.printerName)
	if err != nil {
		return
	}
	printer, _, err = f.Client().Printer().Disconnect(ctx, printer)
	if err != nil {
		return
	}

	fmt.Fprintf(out, "Printer %q disconnected\n", printer.Name)
	return
}
