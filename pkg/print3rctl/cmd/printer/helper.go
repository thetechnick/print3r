package printer

import (
	"github.com/spf13/cobra"
	"github.com/thetechnick/print3r/pkg/print3rctl"
)

func fillPrinterID(printerName string, cmd *cobra.Command, f print3rctl.Factory) string {
	if printerName == "" {
		printerName = print3rctl.GetFlagString(cmd, "printer")
	}
	if printerName == "" {
		printerName = f.Config().DefaultPrinter()
	}
	return printerName
}
