package printer

import (
	"io"

	"github.com/spf13/cobra"
	"github.com/thetechnick/print3r/pkg/print3rctl"
)

func NewPrinterCommand(f print3rctl.Factory, out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "printer",
		Short: "Manage printers",
		Run: func(cmd *cobra.Command, args []string) {
			print3rctl.CheckErr(cmd.Usage())
		},
		TraverseChildren: true,
		SilenceUsage:     true,
		SilenceErrors:    true,
	}
	cmd.AddCommand(
		newPrinterListCommand(f, out),
		newPrinterConnectCommand(f, out),
		newPrinterDisconnectCommand(f, out),
		newPrinterDeleteCommand(f, out),
		newPrinterBedCleanCommand(f, out),
	)

	cmd.PersistentFlags().StringP("printer", "p", "", "Specifies the printer to use.")
	cmd.Flag("printer").Annotations = map[string][]string{
		cobra.BashCompCustom: []string{"__print3rctl_printers"},
	}
	return cmd
}
