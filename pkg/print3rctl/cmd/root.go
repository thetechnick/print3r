package cmd

import (
	"fmt"
	"os"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/thetechnick/print3r/pkg/print3rctl"
	"github.com/thetechnick/print3r/pkg/print3rctl/cmd/print"
	"github.com/thetechnick/print3r/pkg/print3rctl/cmd/printer"
)

const (
	bashCompletionFunc = ``
)

var cfgFile string

type print3rCtlCommand struct {
}

// NewPrint3rCtlCommand creates the root command
func NewPrint3rCtlCommand(f print3rctl.Factory) *cobra.Command {
	c := print3rCtlCommand{}
	cmd := &cobra.Command{
		Use:                    "print3rctl",
		Short:                  "Print3r CLI",
		Long:                   "A command-line interface for Print3r",
		Run:                    c.run,
		TraverseChildren:       true,
		SilenceUsage:           true,
		SilenceErrors:          true,
		BashCompletionFunction: bashCompletionFunc,
	}
	cmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.print3rctl.yaml)")
	cmd.AddCommand(
		newVersionCommand(f, os.Stdout),
		print.NewPrintCommand(f, os.Stdout),
		printer.NewPrinterCommand(f, os.Stdout),
	)
	return cmd
}

func (c *print3rCtlCommand) run(cmd *cobra.Command, args []string) {
	cmd.Usage()
}

func init() {
	cobra.OnInitialize(initConfig)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	viper.SetConfigName(".print3rctl")
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		viper.AddConfigPath(home)
	}

	viper.AutomaticEnv()
	viper.ReadInConfig()
}
