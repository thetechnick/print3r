package cmd

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"

	"github.com/ghodss/yaml"
	"github.com/spf13/cobra"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/print3rctl"
	"github.com/thetechnick/print3r/pkg/version"
)

type clientServerVersions struct {
	Client *api.Version `json:"client"`
	Server *api.Version `json:"server,omitempty"`
}

func newVersionCommand(f print3rctl.Factory, out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "version",
		Short: "Prints the client and server version",
		Run: func(cmd *cobra.Command, args []string) {
			versionOptions := &versionOptions{}
			print3rctl.CheckErr(versionOptions.Complete(cmd))
			print3rctl.CheckErr(versionOptions.Validate())
			print3rctl.CheckErr(versionOptions.Run(f, out))
		},
	}
	cmd.Flags().BoolP("client", "c", false, "Client version only (no server required).")
	cmd.Flags().StringP("output", "o", "", "One of 'yaml' or 'json'.")
	return cmd
}

type versionOptions struct {
	clientOnly bool
	output     string
}

func (o *versionOptions) Complete(cmd *cobra.Command) error {
	o.clientOnly = print3rctl.GetFlagBool(cmd, "client")
	o.output = print3rctl.GetFlagString(cmd, "output")
	return nil
}

func (o *versionOptions) Validate() error {
	if o.output != "" &&
		o.output != "yaml" &&
		o.output != "json" {
		return errors.New(`--output must be 'yaml', 'json'`)
	}
	return nil
}

func (o *versionOptions) Run(f print3rctl.Factory, out io.Writer) (err error) {
	versions := &clientServerVersions{
		Client: version.Get(),
	}
	if !o.clientOnly {
		versions.Server, _, err = f.Client().Version().Version(context.Background())
		if err != nil {
			return err
		}
	}

	switch o.output {
	case "":
		fallthrough
	case "short":
		fmt.Fprintf(out, "Client: %s\n", versions.Client.Version)
		if versions.Server != nil {
			fmt.Fprintf(out, "Server: %s\n", versions.Server.Version)
		}
	case "json":
		b, err := json.MarshalIndent(versions, "", "  ")
		print3rctl.CheckErr(err)
		fmt.Fprintln(out, string(b))
	case "yaml":
		b, err := yaml.Marshal(versions)
		print3rctl.CheckErr(err)
		fmt.Fprintln(out, string(b))
	}
	return nil
}
