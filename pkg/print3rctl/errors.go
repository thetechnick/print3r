package print3rctl

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"google.golang.org/grpc/status"
)

// CheckErr checks if an error has occured
func CheckErr(err error) {
	if err != nil {
		var msg string
		if s, ok := status.FromError(err); ok {
			if len(s.Details()) > 0 {
				msg = fmt.Sprintf("%s: %v", s.Message(), s.Details())
			} else {
				msg = fmt.Sprintf("%s", s.Message())
			}
		} else {
			msg = err.Error()
		}

		if !strings.HasPrefix(msg, "error: ") {
			msg = fmt.Sprintf("error: %s", msg)
		}
		fmt.Fprintln(os.Stderr, msg)
		os.Exit(1)
	}
}

// UsageErrorf prints a usage error
func UsageErrorf(cmd *cobra.Command, format string, args ...interface{}) error {
	msg := fmt.Sprintf(format, args...)
	return fmt.Errorf("%s\nSee '%s -h' for help and examples", msg, cmd.CommandPath())
}
