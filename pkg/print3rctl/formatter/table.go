package formatter

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"reflect"
	"sort"
	"strings"
	"text/tabwriter"
	"unicode"
)

// NewTableOutput creates a new TableOutput.
func NewTableOutput() *TableOutput {
	return &TableOutput{
		w:             tabwriter.NewWriter(os.Stdout, 0, 0, 3, ' ', 0),
		columns:       map[string]bool{},
		fieldMapping:  map[string]FieldOutputFn{},
		fieldAlias:    map[string]string{},
		allowedFields: map[string]bool{},
	}
}

type FieldOutputFn func(obj interface{}) string

type writerFlusher interface {
	io.Writer
	Flush() error
}

// TableOutput is a generic way to format object as a table.
type TableOutput struct {
	w             writerFlusher
	columns       map[string]bool
	fieldMapping  map[string]FieldOutputFn
	fieldAlias    map[string]string
	allowedFields map[string]bool
}

// Columns returns a list of known output columns.
func (o *TableOutput) Columns() (cols []string) {
	for c := range o.columns {
		cols = append(cols, c)
	}
	sort.Strings(cols)
	return
}

// AddFieldAlias overrides the field name to allow custom column headers.
func (o *TableOutput) AddFieldAlias(field, alias string) *TableOutput {
	o.fieldAlias[field] = alias
	return o
}

// AddFieldOutputFn adds a function which handles the output of the specified field.
func (o *TableOutput) AddFieldOutputFn(field string, fn FieldOutputFn) *TableOutput {
	o.fieldMapping[field] = fn
	o.allowedFields[field] = true
	o.columns[field] = true
	return o
}

// AddAllowedFields reads all first level fieldnames of the struct and allowes them to be used.
func (o *TableOutput) AddAllowedFields(obj interface{}) *TableOutput {
	v := reflect.ValueOf(obj)
	if v.Kind() != reflect.Struct {
		panic("AddAllowedFields input must be a struct")
	}
	t := v.Type()
	for i := 0; i < v.NumField(); i++ {
		k := t.Field(i).Type.Kind()
		if k != reflect.Bool &&
			k != reflect.Float32 &&
			k != reflect.Float64 &&
			k != reflect.String &&
			k != reflect.Int {
			// only allow simple values
			// complex values need to be mapped via a FieldOutputFn
			continue
		}
		o.allowedFields[strings.ToLower(t.Field(i).Name)] = true
		o.allowedFields[fieldName(t.Field(i).Name)] = true
		o.columns[fieldName(t.Field(i).Name)] = true
	}
	return o
}

// RemoveAllowedField removes fields from the allowed list.
func (o *TableOutput) RemoveAllowedField(fields ...string) *TableOutput {
	for _, field := range fields {
		delete(o.allowedFields, field)
		delete(o.columns, field)
	}
	return o
}

// ValidateColumns returns an error if invalid columns are specified.
func (o *TableOutput) ValidateColumns(cols []string) error {
	var invalidCols []string
	for _, col := range cols {
		if _, ok := o.allowedFields[strings.ToLower(col)]; !ok {
			invalidCols = append(invalidCols, col)
		}
	}
	if len(invalidCols) > 0 {
		return fmt.Errorf("invalid table columns: %s", strings.Join(invalidCols, ","))
	}
	return nil
}

// WriteHeader writes the table header.
func (o *TableOutput) WriteHeader(cols []string) {
	var header []string
	for _, col := range cols {
		if alias, ok := o.fieldAlias[col]; ok {
			col = alias
		}
		header = append(header, strings.Replace(strings.ToUpper(col), "_", " ", -1))
	}
	fmt.Fprintln(o.w, strings.Join(header, "\t"))
}

func (o *TableOutput) Flush() error {
	return o.w.Flush()
}

// Write writes a table line.
func (o *TableOutput) Write(cols []string, obj interface{}) {
	data := map[string]interface{}{}
	objJSON, _ := json.Marshal(obj)
	json.Unmarshal(objJSON, &data)

	dataL := map[string]interface{}{}
	for key, value := range data {
		dataL[strings.ToLower(key)] = value
	}

	var out []string
	for _, col := range cols {
		colName := strings.ToLower(col)
		if alias, ok := o.fieldAlias[colName]; ok {
			if fn, ok := o.fieldMapping[alias]; ok {
				out = append(out, fn(obj))
				continue
			}
		}
		if fn, ok := o.fieldMapping[colName]; ok {
			out = append(out, fn(obj))
			continue
		}
		if value, ok := dataL[strings.Replace(colName, "_", "", -1)]; ok {
			if value == nil {
				out = append(out, na(""))
				continue
			}
			if b, ok := value.(bool); ok {
				out = append(out, yesno(b))
				continue
			}
			if s, ok := value.(string); ok {
				out = append(out, na(s))
				continue
			}
			out = append(out, fmt.Sprintf("%v", value))
		} else {
			out = append(out, na(""))
		}
	}
	fmt.Fprintln(o.w, strings.Join(out, "\t"))
}

func fieldName(name string) string {
	r := []rune(name)
	var out []rune
	for i := range r {
		if i > 0 && (unicode.IsUpper(r[i])) && (i+1 < len(r) && unicode.IsLower(r[i+1]) || unicode.IsLower(r[i-1])) {
			out = append(out, '_')
		}
		out = append(out, unicode.ToLower(r[i]))
	}
	return string(out)
}

func yesno(b bool) string {
	if b {
		return "yes"
	}
	return "no"
}

func na(s string) string {
	if s == "" {
		return "-"
	}
	return s
}
