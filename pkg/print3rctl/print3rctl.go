package print3rctl

import (
	"fmt"

	"github.com/spf13/viper"
	"github.com/thetechnick/print3r/pkg/api/client"
	"github.com/thetechnick/print3r/pkg/print3rctl/api"
	"github.com/thetechnick/print3r/pkg/print3rctl/api/v1"
	"google.golang.org/grpc"
)

type Factory interface {
	Config() api.Config
	ClientConn() *grpc.ClientConn
	Client() client.Client
}

type factory struct {
	config *api.Config
	conn   *grpc.ClientConn
	client client.Client
}

// NewFactory returns a new factory instance
func NewFactory() Factory {
	return &factory{}
}

func (f *factory) Config() api.Config {
	if f.config == nil {
		v1Config := &v1.Config{}
		if err := viper.Unmarshal(v1Config); err != nil {
			CheckErr(err)
		}

		f.config = &api.Config{}
		if err := v1.Scheme.Convert(v1Config, f.config); err != nil {
			CheckErr(err)
		}
	}
	return *f.config
}

func (f *factory) currentServer() *api.Server {
	config := f.Config()
	if context, cok := config.Contexts[config.CurrentContext]; cok {
		if server, sok := config.Servers[context.Server]; sok {
			return server
		}
	}
	return nil
}

func (f *factory) ClientConn() *grpc.ClientConn {
	if f.conn == nil {
		server := f.currentServer()
		if server == nil {
			CheckErr(fmt.Errorf("No server configured"))
			return nil
		}

		opts := []grpc.DialOption{}
		if server.CertificateAuthority == "" && len(server.CertificateAuthorityData) == 0 {
			opts = append(opts, grpc.WithInsecure())
		}

		if conn, err := grpc.Dial(server.Address, opts...); err == nil {
			f.conn = conn
		} else {
			CheckErr(err)
			return nil
		}
	}
	return f.conn
}

func (f *factory) Client() client.Client {
	if f.client == nil {
		f.client = client.NewClient(f.ClientConn())
	}
	return f.client
}
