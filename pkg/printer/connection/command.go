package connection

import (
	"sync"
	"time"
)

type command struct {
	line            string
	doneWG          sync.WaitGroup
	responseTimeout time.Duration

	resolved bool
	err      error
	result   string
}

func newCommand(line string, opts ...SendOpt) (c *command) {
	l := RemoveComments(line)
	if l == "" {
		return
	}
	c = &command{
		line: l,
	}
	c.doneWG.Add(1)
	for _, opt := range opts {
		opt(c)
	}
	return c
}

func (c *command) Timeout() time.Duration {
	return c.responseTimeout
}

func (c *command) Result() (string, error) {
	c.Wait()
	return c.result, c.err
}

func (c *command) Wait() {
	c.doneWG.Wait()
}

func (c *command) Resolve(result string, err error) {
	if c.resolved {
		return
	}
	c.resolved = true
	c.err = err
	c.result = result
	c.doneWG.Done()
}

type commandQueue struct {
	queue        []*command
	cond         *sync.Cond
	shuttingDown bool
}

func newCommandQueue() *commandQueue {
	return &commandQueue{
		cond: sync.NewCond(&sync.Mutex{}),
	}
}

func (q *commandQueue) Add(cmd *command) {
	q.cond.L.Lock()
	defer q.cond.L.Unlock()
	if q.shuttingDown {
		cmd.Resolve("", ErrConnectionClosed)
		return
	}
	q.queue = append(q.queue, cmd)
	q.cond.Signal()
}

func (q *commandQueue) Get() (cmd *command, shutdown bool) {
	q.cond.L.Lock()
	defer q.cond.L.Unlock()
	for len(q.queue) == 0 && !q.shuttingDown {
		q.cond.Wait()
	}
	if len(q.queue) == 0 {
		return nil, true
	}

	cmd, q.queue = q.queue[0], q.queue[1:]
	return cmd, false
}

func (q *commandQueue) ShutDown() {
	q.cond.L.Lock()
	defer q.cond.L.Unlock()
	q.shuttingDown = true
	for _, cmd := range q.queue {
		cmd.Resolve("", ErrConnectionClosed)
	}
	q.cond.Broadcast()
}
