package connection

import (
	"context"
	"errors"
	"io"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/tarm/serial"
)

// Monitor is responsible for monitoring a connection's state
type Monitor interface {
	Check(c *Connection) error
}

const (
	// DefaultHistoryLength default number of lines in the connections history buffer
	DefaultHistoryLength = 50

	// DefaultMonitorInterval default monitor ticker interval
	DefaultMonitorInterval = 3 * time.Second

	// DefaultMaxResendCount default number of times the same line can be resend before triggering ErrMaxResendsReached
	DefaultMaxResendCount = 5

	// DefaultResponseTimeout default time to wait for a response of the printer
	// if the printer sends something the timeout will be resetted
	DefaultResponseTimeout = 2 * time.Minute
)

var (
	// ErrConnectionClosed is returned if the connection is closed
	ErrConnectionClosed = errors.New("connection closed")

	// ErrMaxResendsReached is returned when the printer requested a resend of the same command too often
	ErrMaxResendsReached = errors.New("max resends reached")

	// ErrLineNotInHistory is returned when the printer requests a line that is not in our history
	ErrLineNotInHistory = errors.New("line not in history")

	// ErrPrinterSentStart is returned when the printer is reset
	ErrPrinterSentStart = errors.New("printer sent 'start', external reset?")

	// ErrResponseTimeout is returned when the printer did not send any response for the configured response timeout.
	ErrResponseTimeout = errors.New("printer failed to send response in time")
)

// Connection manages the g code commands via a serial connection
type Connection struct {
	logger          *logrus.Entry
	monitor         Monitor
	device          string
	baudRate        int
	writer          io.WriteCloser
	reader          io.ReadCloser
	historyLength   int
	maxResendCount  int
	monitorInterval time.Duration
	responseTimeout time.Duration

	lineHistory  *lineHistory
	commandQueue *commandQueue

	statusLock sync.RWMutex
	status     Status

	doneWG     sync.WaitGroup
	closeCh    chan struct{}
	closedLock sync.RWMutex
	closed     bool
	err        error

	checksumLock sync.RWMutex
	checksum     bool
}

// Opt adds configuration to a connection
type Opt func(conn *Connection)

// WithWriterReader tells the connection to use the provided writer and reader directly
func WithWriterReader(writer io.WriteCloser, reader io.ReadCloser) Opt {
	return func(conn *Connection) {
		conn.writer = writer
		conn.reader = reader
	}
}

// WithMonitorInterval sets the monitor ticker interval
func WithMonitorInterval(interval time.Duration) Opt {
	return func(conn *Connection) {
		conn.monitorInterval = interval
	}
}

// WithResponseTimeout sets the command response timeout
func WithResponseTimeout(interval time.Duration) Opt {
	return func(conn *Connection) {
		conn.responseTimeout = interval
	}
}

// WithHistoryLength sets the number of commands in the connections history buffer
// these commands can be resend when the printer requests them
func WithHistoryLength(length int) Opt {
	return func(conn *Connection) {
		conn.historyLength = length
	}
}

// WithMaxResendCount sets the max number of resends for the same line
func WithMaxResendCount(maxResendCount int) Opt {
	return func(conn *Connection) {
		conn.maxResendCount = maxResendCount
	}
}

// WithSerialDevice configures the connection to open a serial connection
func WithSerialDevice(device string, baudRate int) Opt {
	return func(conn *Connection) {
		conn.device = device
		conn.baudRate = baudRate
	}
}

// WithLogger adds a logger to the connection
func WithLogger(logger *logrus.Entry) Opt {
	return func(conn *Connection) {
		conn.logger = logger
	}
}

// WithMonitor adds a connection monitor to the connection
func WithMonitor(monitor Monitor) Opt {
	return func(conn *Connection) {
		conn.monitor = monitor
	}
}

// NewConnection creates a new connection instance
func NewConnection(opts ...Opt) *Connection {
	c := &Connection{
		closeCh: make(chan struct{}, 1),
	}
	for _, opt := range opts {
		opt(c)
	}
	if c.historyLength == 0 {
		c.historyLength = DefaultHistoryLength
	}
	if c.monitorInterval == 0 {
		c.monitorInterval = DefaultMonitorInterval
	}
	if c.maxResendCount == 0 {
		c.maxResendCount = DefaultMaxResendCount
	}
	if c.responseTimeout == 0 {
		c.responseTimeout = DefaultResponseTimeout
	}

	c.lineHistory = newLineHistory(c.historyLength)
	c.commandQueue = newCommandQueue()
	return c
}

// Status returns the connection status
func (c *Connection) Status() Status {
	c.statusLock.RLock()
	defer c.statusLock.RUnlock()
	return c.status
}

func (c *Connection) checkStartup() (err error) {
	if c.monitor == nil {
		return
	}
	if err = c.monitor.Check(c); err != nil {
		if err != ErrPrinterSentStart {
			return
		}
		if err = c.monitor.Check(c); err != nil {
			return
		}
	}
	return
}

func (c *Connection) monitorLoop(closeCh chan struct{}) (doneCh chan error) {
	t := time.NewTicker(c.monitorInterval)
	doneCh = make(chan error)
	if c.monitor != nil {
		go func() {
			defer c.logger.Debug("closed monitor loop")
			defer close(doneCh)
			var err error
			for {
				select {
				case <-t.C:
					if err = c.monitor.Check(c); err != nil {
						doneCh <- err
						return
					}
				case <-closeCh:
					t.Stop()
					return
				}
			}
		}()
	} else {
		// dummy monitor loop
		go func() {
			<-closeCh
			t.Stop()
			close(doneCh)
		}()
	}
	return
}

func (c *Connection) mainLoop(readDoneCh, sendDoneCh, monitorDoneCh chan error) {
	defer c.logger.Debug("connection closed")
	defer c.doneWG.Done()
	defer func() {
		c.writer.Close()
		c.reader.Close()
	}()
	var err error

	select {
	case err = <-readDoneCh:
		c.logger.Warn("read loop closed unexpectedly")
	case err = <-sendDoneCh:
		c.logger.Warn("send loop closed unexpectedly")
	case err = <-monitorDoneCh:
		c.logger.Warn("monitor loop closed unexpectedly")
	case <-c.closeCh:
		c.logger.Warn("closing connection")
	}
	c.close()

	if err != nil {
		c.err = err
	}

	c.commandQueue.ShutDown()

	// make sure all control loops are closed
	var readLoopClosed, sendLoopClosed, monitorLoopClosed bool
	for !readLoopClosed || !sendLoopClosed || !monitorLoopClosed {
		var ok bool
		select {
		case _, ok = <-readDoneCh:
			if !ok {
				readLoopClosed = true
			}
		case _, ok = <-sendDoneCh:
			if !ok {
				sendLoopClosed = true
			}
		case _, ok = <-monitorDoneCh:
			if !ok {
				monitorLoopClosed = true
			}
		}
	}
}

// Open starts the connection control loops
func (c *Connection) Open() (err error) {
	if c.writer == nil && c.reader == nil {
		// open serial connection
		c.logger.Debug("open serial connection")
		var p *serial.Port
		p, err = serial.OpenPort(&serial.Config{
			Name:        c.device,
			Baud:        int(c.baudRate),
			ReadTimeout: time.Millisecond * 10,
		})
		if err != nil {
			return
		}
		c.writer = p
		c.reader = p
	}

	c.doneWG.Add(1)
	readCh, readDoneCh := c.readLoop(c.closeCh)
	sendDoneCh := c.sendLoop(readCh)
	monitorDoneCh := c.monitorLoop(c.closeCh)
	go c.mainLoop(readDoneCh, sendDoneCh, monitorDoneCh)

	if c.monitor != nil {
		if err = c.monitor.Check(c); err != nil {
			c.close()
			return
		}
	}

	return
}

// Wait on the connections close
func (c *Connection) Wait() (err error) {
	c.doneWG.Wait()
	return c.err
}

func (c *Connection) close() {
	c.logger.
		Debug("close connection")
	c.closedLock.Lock()
	if c.closed {
		c.closedLock.Unlock()
		return
	}
	c.closed = true
	close(c.closeCh)
	c.closedLock.Unlock()
}

// Close disconnects the connection
func (c *Connection) Close() (err error) {
	c.close()
	c.doneWG.Wait()
	return
}

// EnableChecksum starts adding a checksum to each command
func (c *Connection) EnableChecksum(ctx context.Context) (err error) {
	c.checksumLock.Lock()
	if c.checksum {
		c.checksumLock.Unlock()
		return
	}
	c.checksum = true
	c.checksumLock.Unlock()
	_, err = c.Send(ctx, "M110 N0") // reset line history
	return
}

// DisableChecksum stops setting a checksum on each command
func (c *Connection) DisableChecksum(ctx context.Context) (err error) {
	c.checksumLock.Lock()
	defer c.checksumLock.Unlock()
	c.checksum = false
	return nil
}

// SendOpt adds configuration to a send command
type SendOpt func(cmd *command)

// SendWithResponseTimeout adds a response timeout to the send command
func SendWithResponseTimeout(timeout time.Duration) SendOpt {
	return func(cmd *command) {
		cmd.responseTimeout = timeout
	}
}

// Send sends a command to the printer and returns the result
func (c *Connection) Send(ctx context.Context, line string, opts ...SendOpt) (result string, err error) {
	cmd := newCommand(line, opts...)
	if cmd.Timeout() == 0 {
		cmd.responseTimeout = c.responseTimeout
	}
	c.commandQueue.Add(cmd)
	result, err = cmd.Result()
	return
}
