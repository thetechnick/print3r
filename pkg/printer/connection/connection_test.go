package connection

import (
	"context"
	"errors"
	"fmt"
	"io"
	"sync"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/thetechnick/print3r/pkg/util"
)

type failingMonitor struct{}

func (m *failingMonitor) Check(c *Connection) error {
	return errors.New("boom")
}

func TestConnection(t *testing.T) {
	t.Run("close with multiple commands in queue", func(t *testing.T) {
		var (
			w util.ClosingBuffer
			r util.ClosingBuffer
		)
		log.SetLevel(log.DebugLevel)
		l := log.WithField("test", t.Name())
		c := NewConnection(
			WithLogger(l),
			WithWriterReader(&w, &r))

		doneCh := make(chan error)
		go func() {
			defer close(doneCh)
			if err := c.Open(); err != nil {
				doneCh <- err
				return
			}
			doneCh <- c.Wait()
		}()

		ctx := context.Background()
		r.Write([]byte("line1\n"))

		var cmdWG sync.WaitGroup
		cmdWG.Add(3)

		go func() {
			_, err := c.Send(ctx, "M105")
			if assert.Error(t, err, "sending command") {
				assert.Equal(t, ErrConnectionClosed, err)
			}
			cmdWG.Done()
		}()
		go func() {
			_, err := c.Send(ctx, "M105")
			if assert.Error(t, err, "sending command") {
				assert.Equal(t, ErrConnectionClosed, err)
			}
			cmdWG.Done()
		}()
		go func() {
			_, err := c.Send(ctx, "M105")
			if assert.Error(t, err, "sending command") {
				assert.Equal(t, ErrConnectionClosed, err)
			}
			cmdWG.Done()
		}()

		assert.NoError(t, c.Close(), "closing connection")
		assert.NoError(t, <-doneCh, ".Wait() return")

		cmdWG.Wait()
	})

	t.Run("monitor startup failure", func(t *testing.T) {
		var (
			w util.ClosingBuffer
			r util.ClosingBuffer
		)
		log.SetLevel(log.DebugLevel)
		l := log.WithField("test", t.Name())
		c := NewConnection(
			WithLogger(l),
			WithWriterReader(&w, &r),
			WithMonitor(&failingMonitor{}))

		doneCh := make(chan error)
		go func() {
			defer close(doneCh)
			if err := c.Open(); err != nil {
				doneCh <- err
				return
			}
			doneCh <- c.Wait()
		}()

		ctx := context.Background()
		_, err := c.Send(ctx, "M105")
		if assert.Error(t, err, "sending command") {
			assert.Equal(t, ErrConnectionClosed, err)
		}
		err = <-doneCh
		if assert.Error(t, err, ".Wait() return") {
			assert.Equal(t, err.Error(), "boom")
		}
	})

	t.Run("success", func(t *testing.T) {
		var (
			w util.ClosingBuffer
			r util.ClosingBuffer
		)
		log.SetLevel(log.DebugLevel)
		l := log.WithField("test", t.Name())
		c := NewConnection(
			WithLogger(l),
			WithWriterReader(&w, &r))

		doneCh := make(chan error)
		go func() {
			defer close(doneCh)
			if err := c.Open(); err != nil {
				doneCh <- err
				return
			}
			doneCh <- c.Wait()
		}()

		ctx := context.Background()
		r.Write([]byte("line1\nOK: \n"))
		result, err := c.Send(ctx, "M105")
		if assert.NoError(t, err, "sending command") {
			assert.Equal(t, "line1\nOK: \n", result)
			assert.Equal(t, "M105\n", w.String())
		}
		assert.NoError(t, c.Close(), "closing connection")
		assert.NoError(t, <-doneCh, ".Wait() return")
	})

	t.Run("success with temperature", func(t *testing.T) {
		var (
			w util.ClosingBuffer
			r util.ClosingBuffer
		)
		log.SetLevel(log.DebugLevel)
		l := log.WithField("test", t.Name())
		c := NewConnection(
			WithLogger(l),
			WithWriterReader(&w, &r))

		doneCh := make(chan error)
		go func() {
			defer close(doneCh)
			if err := c.Open(); err != nil {
				doneCh <- err
				return
			}
			doneCh <- c.Wait()
		}()

		ctx := context.Background()
		r.Write([]byte("line1\nok T:27.0 /0.0 B:26.4 /0.0 T0:27.0 /0.0 @:0 B@:0\n"))
		result, err := c.Send(ctx, "M105")
		if assert.NoError(t, err, "sending command") {
			assert.Equal(t, "line1\nok T:27.0 /0.0 B:26.4 /0.0 T0:27.0 /0.0 @:0 B@:0\n", result)
			assert.Equal(t, "M105\n", w.String())
		}
		assert.NoError(t, c.Close(), "closing connection")
		assert.NoError(t, <-doneCh, ".Wait() return")

		status := c.Status()
		assert.Equal(t, 27.0, status.Extruder().Actual())
		assert.Equal(t, 0.0, status.Extruder().Target())
		assert.Equal(t, 26.4, status.Bed().Actual())
		assert.Equal(t, 0.0, status.Bed().Target())
	})

	t.Run("success with resend", func(t *testing.T) {
		var (
			w util.ClosingBuffer
			r util.ClosingBuffer
		)
		log.SetLevel(log.DebugLevel)
		l := log.WithField("test", t.Name())
		c := NewConnection(
			WithLogger(l),
			WithWriterReader(&w, &r))

		doneCh := make(chan error)
		go func() {
			defer close(doneCh)
			if err := c.Open(); err != nil {
				doneCh <- err
				return
			}
			doneCh <- c.Wait()
		}()

		ctx := context.Background()
		r.Write([]byte("OK: \nrs 1\nline1\nOK: \nOK: \nOK: \n"))
		assert.NoError(t, c.EnableChecksum(ctx), "EnableChecksum")

		result, err := c.Send(ctx, "M105")
		if assert.NoError(t, err, "sending command") {
			assert.Equal(t, "rs 1\nline1\nOK: \n", result)
			assert.Equal(t, "N1 M110 N0*124\nN1 M105*38\nN1 M105*38\n", w.String())
		}
		assert.NoError(t, c.Close(), "closing connection")
		assert.NoError(t, <-doneCh, ".Wait() return")
	})

	t.Run("timeout is resetted on response", func(t *testing.T) {
		var (
			w util.ClosingBuffer
		)
		r, readWriter := io.Pipe()

		log.SetLevel(log.DebugLevel)
		c := NewConnection(
			WithLogger(log.WithField("test", t.Name())),
			WithWriterReader(&w, r))

		doneCh := make(chan error)
		go func() {
			defer close(doneCh)
			if err := c.Open(); err != nil {
				doneCh <- err
				return
			}
			doneCh <- c.Wait()
		}()

		go func() {
			time.Sleep(30 * time.Millisecond)
			fmt.Fprintln(readWriter, "echo:busy: processing")
			time.Sleep(30 * time.Millisecond)
			fmt.Fprintln(readWriter, "echo:busy: processing")
			time.Sleep(30 * time.Millisecond)
			fmt.Fprintln(readWriter, "echo:busy: processing")
			time.Sleep(40 * time.Millisecond)
			fmt.Fprintln(readWriter, "ok")
			readWriter.Close() // close reader to produce EOF when reading
		}()

		ctx := context.Background()
		result, err := c.Send(ctx, "M105", SendWithResponseTimeout(50*time.Millisecond))

		if assert.NoError(t, err) {
			assert.Equal(t, "echo:busy: processing\necho:busy: processing\necho:busy: processing\nok\n", result)
			assert.Equal(t, "M105\n", w.String())
		}

		assert.NoError(t, c.Close(), "closing connection")
		err = <-doneCh
		assert.NoError(t, err, ".Wait() return")
	})

	t.Run("simple timeout", func(t *testing.T) {
		var (
			w util.ClosingBuffer
		)
		r, readWriter := io.Pipe()

		log.SetLevel(log.DebugLevel)
		c := NewConnection(
			WithLogger(log.WithField("test", t.Name())),
			WithWriterReader(&w, r))

		doneCh := make(chan error)
		go func() {
			defer close(doneCh)
			if err := c.Open(); err != nil {
				doneCh <- err
				return
			}
			doneCh <- c.Wait()
		}()

		go func() {
			time.Sleep(100 * time.Millisecond)
			fmt.Fprintln(readWriter, "ok")
			readWriter.Close() // close reader to produce EOF when reading
		}()

		ctx := context.Background()
		result, err := c.Send(ctx, "M105", SendWithResponseTimeout(50*time.Millisecond))

		if assert.Error(t, err) {
			assert.Equal(t, ErrResponseTimeout, err)
			assert.Equal(t, "", result)
			assert.Equal(t, "M105\n", w.String())
		}

		// connection state unknown -> reconnect
		err = <-doneCh
		assert.Error(t, err, ".Wait() return")
		assert.Equal(t, ErrResponseTimeout, err)
	})

	t.Run("error", func(t *testing.T) {
		var (
			w util.ClosingBuffer
			r util.ClosingBuffer
		)
		log.SetLevel(log.DebugLevel)
		c := NewConnection(
			WithLogger(log.WithField("test", t.Name())),
			WithWriterReader(&w, &r))

		doneCh := make(chan error)
		go func() {
			defer close(doneCh)
			if err := c.Open(); err != nil {
				doneCh <- err
				return
			}
			doneCh <- c.Wait()
		}()

		ctx := context.Background()
		r.Write([]byte("line1\nerror: not good\n"))
		result, err := c.Send(ctx, "M105")

		if assert.Error(t, err, "send error") {
			assert.Equal(t, "not good", err.Error())
			assert.Equal(t, "line1\nerror: not good\n", result)
			assert.Equal(t, "M105\n", w.String())
		}

		assert.NoError(t, c.Close(), "closing connection")
		assert.NoError(t, <-doneCh, ".Wait() return")
	})
}
