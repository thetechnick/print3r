package connection

import (
	"fmt"
	"sync"
)

type lineHistoryEntry struct {
	number int
	line   string
}

type lineHistory struct {
	maxEntries     int
	entries        []string
	lastLineNumber int
	rwLock         sync.RWMutex
}

func newLineHistory(maxEntries int) *lineHistory {
	return &lineHistory{
		maxEntries: maxEntries,
		entries:    []string{},
	}
}

func (h *lineHistory) LastLineNumber() int {
	return h.lastLineNumber
}

func (h *lineHistory) Reset(lineNumber int) {
	h.rwLock.Lock()
	defer h.rwLock.Unlock()
	h.lastLineNumber = lineNumber
	h.entries = []string{}
}

func (h *lineHistory) Add(line string, number int) (err error) {
	h.rwLock.Lock()
	defer h.rwLock.Unlock()

	l := len(h.entries)
	if l > 0 && number != h.lastLineNumber+1 {
		return fmt.Errorf("tried to append out of order, next line number in history should be %d, is %d", number, h.lastLineNumber+1)
	}

	if l >= h.maxEntries {
		h.entries = h.entries[1:]
	}
	h.entries = append(h.entries, line)
	h.lastLineNumber = number
	return
}

func (h *lineHistory) GetLine(number int) (line string, ok bool) {
	if number > h.lastLineNumber {
		return
	}
	index := len(h.entries) - (h.lastLineNumber - number) - 1
	if index >= 0 {
		return h.entries[index], true
	}
	return
}
