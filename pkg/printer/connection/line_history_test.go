package connection

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLineHistory(t *testing.T) {
	t.Run("Append/GetLine", func(t *testing.T) {
		t.Run("add", func(t *testing.T) {
			assert := assert.New(t)
			lh := newLineHistory(10)
			expectedLine := "line 0"
			err := lh.Add(expectedLine, 0)
			if assert.NoError(err) {
				line, ok := lh.GetLine(0)
				if assert.True(ok, "line 0 should exist") {
					assert.Equal(expectedLine, line)
				}
			}
		})

		t.Run("add overflow", func(t *testing.T) {
			assert := assert.New(t)
			lh := newLineHistory(2)
			if assert.NoError(lh.Add("line 0", 0)) &&
				assert.NoError(lh.Add("line 1", 1)) &&
				assert.NoError(lh.Add("line 2", 2)) {
				_, ok0 := lh.GetLine(0)
				assert.False(ok0, "line 0 should be removed from the history")

				line1, ok1 := lh.GetLine(1)
				if assert.True(ok1, "line 1 should exist") {
					assert.Equal("line 1", line1)
				}
				line2, ok2 := lh.GetLine(2)
				if assert.True(ok2, "line 2 should exist") {
					assert.Equal("line 2", line2)
				}
			}
		})
	})

	t.Run("Reset", func(t *testing.T) {
		assert := assert.New(t)
		lh := newLineHistory(10)
		expectedLine := "line 10"
		err := lh.Add(expectedLine, 0)
		if assert.NoError(err) {
			lh.Reset(0)
			_, ok0 := lh.GetLine(0)
			assert.False(ok0, "line 0 should be removed from the history")
		}
	})
}
