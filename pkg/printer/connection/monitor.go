package connection

import (
	"context"
	"time"
)

// PrinterMonitor is a monitor implementation
// It is calling M105 on the connection to check if the other end responds
type PrinterMonitor struct{}

// NewPrinterMonitor creates a new printer connection monitor
func NewPrinterMonitor() *PrinterMonitor {
	return &PrinterMonitor{}
}

// Check checks if the printer is still connected
func (m *PrinterMonitor) Check(c *Connection) (err error) {
	_, err = c.Send(context.Background(), "M105", SendWithResponseTimeout(5*time.Second))
	if err != ErrPrinterSentStart {
		return
	}
	_, err = c.Send(context.Background(), "M105", SendWithResponseTimeout(5*time.Second))
	return
}
