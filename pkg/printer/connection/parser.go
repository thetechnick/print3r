package connection

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var (
	regexFloatPattern         = `[-+]?[0-9]*\.?[0-9]+`
	regexPositiveFloatPattern = `[+]?[0-9]*\.?[0-9]+`
	regexIntPattern           = `\d+`
	regexTemp                 = regexp.MustCompile(
		fmt.Sprintf(`(?P<tool>B|T(?P<toolnum>\d*)):\s*(?P<actual>%s)(\s*\/?\s*(?P<target>%s))?`, regexFloatPattern, regexPositiveFloatPattern),
	)
	regexMinMaxError      = regexp.MustCompile(`Error:[0-9]`)
	regexResendLinenumber = regexp.MustCompile(fmt.Sprintf(`(?:N|N:)?(%s)`, regexIntPattern))
	regexParameterIntN    = regexp.MustCompile(fmt.Sprintf(`(?:^|[^A-Za-z])(?:[Nn])(%s)`, regexIntPattern))
)

// Temperature holds temperature information of a component
type Temperature struct {
	actual, target float64
}

// Actual returns the measured temperature
func (t Temperature) Actual() float64 {
	return t.actual
}

// Target the currently active target temperature
func (t Temperature) Target() float64 {
	return t.target
}

func containsTemperature(line string) bool {
	return (strings.Contains(line, " T:") ||
		strings.HasPrefix(line, "T:") ||
		strings.Contains(line, " T0:") ||
		strings.HasPrefix(line, "T0:")) &&
		regexTemp.MatchString(line)
}

func parseTemperature(line string) (extruderTemp, bedTemp *Temperature, err error) {
	if !containsTemperature(line) {
		return
	}
	m1 := regexTemp.FindAllString(line, -1)

	for _, m2 := range m1 {
		match := regexTemp.FindStringSubmatch(m2)
		groups := map[string]string{}
		for i, name := range regexTemp.SubexpNames() {
			if i != 0 {
				groups[name] = match[i]
			}
		}

		var actual float64
		actual, err = strconv.ParseFloat(groups["actual"], 64)
		if err != nil {
			return
		}

		var target float64
		if tS, ok := groups["target"]; ok && tS != "" {
			var t float64
			t, err = strconv.ParseFloat(tS, 64)
			if err != nil {
				return
			}
			target = t
		}
		if t := groups["tool"]; t == "T" {
			extruderTemp = &Temperature{actual, target}
		}
		if t := groups["tool"]; t == "B" {
			bedTemp = &Temperature{actual, target}
		}
	}
	return extruderTemp, bedTemp, nil
}

// Status contains printer status infos
type Status struct {
	extruder, bed Temperature
}

// Extruder returns the extruder temperature
func (t Status) Extruder() Temperature {
	return t.extruder
}

// Bed returns the bed temperature
func (t Status) Bed() Temperature {
	return t.bed
}

func (c *Connection) parseTemperature(line string) error {
	if containsTemperature(line) {
		c.statusLock.Lock()
		defer c.statusLock.Unlock()
		extruderTemp, bedTemp, err := parseTemperature(line)
		if err != nil {
			c.logger.WithError(err).Warn("error parsing temperature")
			return err
		}
		if extruderTemp != nil {
			c.status.extruder = *extruderTemp
		}
		if bedTemp != nil {
			c.status.bed = *bedTemp
		}
	}
	return nil
}
