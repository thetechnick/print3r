package connection

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseTemprature(t *testing.T) {
	t.Run("with target temp (M105)", func(t *testing.T) {
		assert := assert.New(t)
		e, b, err := parseTemperature("T:27.0 /0.0 B:27.8 /0.0 T0:27.0 /0.0 @:0 B@:0")
		if assert.NoError(err) &&
			assert.NotNil(e, "Missing extruder temp") &&
			assert.NotNil(b, "Missing bed temp") {

			// extruder
			assert.Equal(27.0, e.Actual(), "Unexpected extruder actual temperature")
			assert.Equal(0.0, e.Target(), "Unexpected extruder target temperature")

			// bed
			assert.Equal(27.8, b.Actual(), "Unexpected bed actual temperature")
			assert.Equal(0.0, b.Target(), "Unexpected bed target temperature")
		}
	})

	t.Run("without target temp (M109)", func(t *testing.T) {
		assert := assert.New(t)
		e, b, err := parseTemperature("T:41.83 E:0 B:26.8")
		if assert.NoError(err) &&
			assert.NotNil(e, "Missing extruder temp") &&
			assert.NotNil(b, "Missing bed temp") {

			// extruder
			assert.Equal(41.83, e.Actual(), "Unexpected extruder actual temperature")
			assert.Empty(e.Target(), "Unexpected extruder target temperature")

			// bed
			assert.Equal(26.8, b.Actual(), "Unexpected bed actual temperature")
			assert.Empty(b.Target(), "Unexpected bed target temperature")
		}
	})
}
