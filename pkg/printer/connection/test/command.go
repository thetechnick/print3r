package connection_test

import "github.com/stretchr/testify/mock"

// CommandMock mocks a connection
type CommandMock struct {
	mock.Mock
}

// Result mock
func (m *CommandMock) Result() (string, error) {
	args := m.Called()
	return args.String(0), args.Error(1)
}

// Done mock
func (m *CommandMock) Done() {
	m.Called()
}
