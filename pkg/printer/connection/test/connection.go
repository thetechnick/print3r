package connection_test

import (
	"context"

	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/printer/connection"
)

// ConnectionMock mocks a connection
type ConnectionMock struct {
	mock.Mock
}

// Close mock
func (m *ConnectionMock) Close() error {
	args := m.Called()
	return args.Error(0)
}

// Open mock
func (m *ConnectionMock) Open() error {
	args := m.Called()
	return args.Error(0)
}

// Wait mock
func (m *ConnectionMock) Wait() error {
	args := m.Called()
	return args.Error(0)
}

// Send mock
func (m *ConnectionMock) Send(ctx context.Context, line string, opts ...connection.SendOpt) (string, error) {
	args := m.Called(ctx, line)
	return args.String(0), args.Error(1)
}

// EnableChecksum mock
func (m *ConnectionMock) EnableChecksum(ctx context.Context) error {
	args := m.Called(ctx)
	return args.Error(0)
}

// DisableChecksum mock
func (m *ConnectionMock) DisableChecksum(ctx context.Context) error {
	args := m.Called(ctx)
	return args.Error(0)
}

// Status mock
func (m *ConnectionMock) Status() connection.Status {
	args := m.Called()
	return args.Get(0).(connection.Status)
}
