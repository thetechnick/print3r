package connection

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"
)

func (c *Connection) readLoop(closeCh chan struct{}) (readCh chan string, doneCh chan error) {
	readCh = make(chan string)
	doneCh = make(chan error, 2)
	r := bufio.NewReader(c.reader)
	go func() {
		var err error
		defer func() {
			close(readCh)
			close(doneCh)
			c.logger.Debug("closed read loop")
		}()

		for {
			select {
			case <-closeCh:
				return

			default:
				var l string
				if l, err = r.ReadString('\n'); err != nil {
					if err == io.EOF {
						continue
					}
					c.logger.
						WithError(err).
						WithField("line", l).
						Error("read error")
					doneCh <- err
					return
				}
				c.logger.WithField("rec", l).Debug("received")
				select {
				case <-closeCh:
					return
				case readCh <- l:
				}

			}
		}
	}()
	return
}

func (c *Connection) sendLoop(readCh chan string) (doneCh chan error) {
	doneCh = make(chan error)
	go func() {
		defer func() {
			close(doneCh)
			c.logger.Debug("closed send loop")
		}()
		for {
			cmd, shutdown := c.commandQueue.Get()
			if shutdown {
				return
			}

			if err := c.send(cmd, readCh); err != nil {
				c.logger.WithError(err).Error("error in send loop")
				doneCh <- err
				return
			}
		}
	}()
	return
}

func (c *Connection) send(cmd *command, readCh chan string) (err error) {
	if cmd == nil {
		return
	}
	// cannot cange checksum while sending
	c.checksumLock.RLock()
	defer c.checksumLock.RUnlock()
	var (
		lineToSend  string
		resendCount int
	)
	if c.checksum {
		lineNumber := c.lineHistory.LastLineNumber() + 1
		cmdPart := fmt.Sprintf("N%d %s", lineNumber, cmd.line)
		cs := byte(0)
		for _, c := range []byte(cmdPart) {
			cs = byte(cs) ^ c
		}

		lineToSend = fmt.Sprintf("%s*%d", cmdPart, int(cs))
		if err = c.lineHistory.Add(lineToSend, lineNumber); err != nil {
			return
		}
	} else {
		lineToSend = cmd.line
	}

	timeoutTimer := time.NewTimer(cmd.Timeout())

	c.logger.
		WithField("line", lineToSend).
		Debug("sending")
	if _, err = fmt.Fprintln(c.writer, lineToSend); err != nil {
		c.logger.
			WithError(err).
			Error("send error")
		return
	}

	if strings.HasPrefix(cmd.line, "M110") {
		ln := 0
		lnString := regexParameterIntN.FindStringSubmatch(cmd.line)
		if len(lnString) == 2 {
			if ln, err = strconv.Atoi(lnString[1]); err != nil {
				return
			}
		}

		c.logger.
			WithField("lineNumber", ln).
			Debug("detected M110, resetting history")
		c.lineHistory.Reset(ln)
	}

	var resultBuf bytes.Buffer
	for {
		select {
		case <-timeoutTimer.C:
			err = ErrResponseTimeout
			c.logger.WithError(err).Errorf("printer did not respond in time (%v): %q", cmd.Timeout(), lineToSend)
			cmd.Resolve("", err)
			return

		case r, ok := <-readCh:
			if !ok {
				cmd.Resolve(resultBuf.String(), ErrConnectionClosed)
				return
			}
			// reset response timeout
			if !timeoutTimer.Stop() {
				<-timeoutTimer.C
			}
			timeoutTimer.Reset(cmd.Timeout())

			line, lowerLine := getLine(r)
			if _, err = resultBuf.WriteString(r); err != nil {
				cmd.Resolve("", err)
				return
			}

			if strings.HasPrefix(lowerLine, "error:") {
				cmd.Resolve(resultBuf.String(), errors.New(strings.Trim(line[6:], " \n")))
				return
			}
			if lowerLine == "start" {
				c.logger.
					Warn("External reset detected, canceling current command, resetting line history")
				c.lineHistory.Reset(0)
				cmd.Resolve(resultBuf.String(), ErrPrinterSentStart)
				return
			}

			// Resend
			if strings.HasPrefix(line, "resend") ||
				strings.HasPrefix(line, "rs") {
				var ln int
				lnString := regexResendLinenumber.FindStringSubmatch(line)
				if len(lnString) == 2 {
					if ln, err = strconv.Atoi(lnString[1]); err != nil {
						return
					}
				}
				if cmd, ok := c.lineHistory.GetLine(ln); ok {
					if resendCount >= c.maxResendCount {
						err = ErrMaxResendsReached
						return
					}
					resendCount++
					c.logger.
						WithField("cmd", cmd).
						Debug("resending")
					if _, err = fmt.Fprintln(c.writer, cmd); err != nil {
						c.logger.
							WithError(err).
							Error("resend error")
						return
					}
					continue
				}
				err = ErrLineNotInHistory
				return
			}

			if err = c.parseTemperature(line); err != nil {
				return
			}

			if strings.HasPrefix(lowerLine, "ok") {
				cmd.Resolve(resultBuf.String(), nil)
				return
			}
		}
	}
}
