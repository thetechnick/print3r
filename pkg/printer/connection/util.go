package connection

import "strings"

func getLine(l string) (line, lowerLine string) {
	line = strings.Trim(string(l), " \n")
	return line, strings.ToLower(line)
}

// RemoveComments trims printer response or command comments
func RemoveComments(line string) string {
	index := strings.Index(line, ";")
	if index == -1 {
		return strings.Trim(line, " \n")
	}
	return strings.Trim(line[:index], " \n")
}
