package printer

import (
	"context"
	"errors"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/printer/connection"
	db "github.com/thetechnick/print3r/pkg/storage"
	"github.com/thetechnick/print3r/pkg/timeseries"
)

var (
	// ErrDeviceBusy is returned when the device is already in use by another printer
	ErrDeviceBusy = errors.New("error device is used by another printer")

	// ErrPrinterAlreadyConnected is returned when the printer is already connected
	ErrPrinterAlreadyConnected = errors.New("error printer already connected")

	// ErrPrinterNotConnected is returned when the printer is not connected
	ErrPrinterNotConnected = errors.New("error printer not connected")

	// ErrConnectionInUse is returned when a connection is still in use
	ErrConnectionInUse = errors.New("error connection is still in use")

	// save temperature history for 1 hour
	temperaturTimeSeriesMaxLength = 3600 / int(connection.DefaultMonitorInterval.Seconds())
)

// ConnectionManager handles connection livetime and access
type ConnectionManager interface {
	Connect(ctx context.Context, printer *api.Printer) error
	Disconnect(ctx context.Context, printer *api.Printer) error
	TemperatureHistory(ctx context.Context, printer *api.Printer) (*TemperatureHistory, error)
	AquireConnection(ctx context.Context, printer *api.Printer) (Connection, error)
	ReleaseConnection(ctx context.Context, printer *api.Printer)
	ConnectionInfo(ctx context.Context, printer *api.Printer) (connectedPrinter *api.Printer, isConnected bool)
}

// TemperatureHistory holds historic temperature data of a printer
type TemperatureHistory struct {
	sync.RWMutex
	ExtruderTarget,
	ExtruderActual,
	BedTarget,
	BedActual *timeseries.TimeSeries
}

// NewConnection creates a new connection using printer as config source
func NewConnection(printer *api.Printer) (c Connection) {
	l := log.
		WithField("module", "printer.Connection").
		WithField("name", printer.Name)
	c = connection.NewConnection(
		connection.WithLogger(l),
		connection.WithSerialDevice(printer.Spec.Device, int(printer.Spec.BaudRate)),
		connection.WithMonitor(connection.NewPrinterMonitor()),
	)
	return c
}

// Connection represents a open printer connection
type Connection interface {
	EnableChecksum(ctx context.Context) error
	DisableChecksum(ctx context.Context) error
	Close() error
	Open() error
	Wait() error
	Status() connection.Status
	Send(ctx context.Context, line string, opts ...connection.SendOpt) (string, error)
}

type connectionFactory func(p *api.Printer) (c Connection)

type connectionManager struct {
	printerStorage db.PrinterStorage
	connection     connectionFactory

	connections    map[string]*connectionEntry
	connectionLock sync.RWMutex
	log            *log.Entry
}

type connectionEntry struct {
	conn               Connection
	printer            *api.Printer
	temperatureHistory *TemperatureHistory
	aquired            bool
}

// NewConnectionManager creates a new ConnectionManager
func NewConnectionManager(printerStorage db.PrinterStorage) ConnectionManager {
	return &connectionManager{
		printerStorage: printerStorage,
		connection:     NewConnection,

		connections: map[string]*connectionEntry{},
		log:         log.WithField("module", "ConnectionManager"),
	}
}

func (m *connectionManager) getConnection(printer *api.Printer) Connection {
	m.connectionLock.RLock()
	defer m.connectionLock.RUnlock()
	if c, ok := m.connections[printer.UID]; ok {
		return c.conn
	}
	return nil
}

func (m *connectionManager) ConnectionInfo(ctx context.Context, printer *api.Printer) (connectedPrinter *api.Printer, isConnected bool) {
	m.connectionLock.RLock()
	defer m.connectionLock.RUnlock()
	var e *connectionEntry
	if e, isConnected = m.connections[printer.UID]; isConnected {
		connectedPrinter = e.printer
	}
	return
}

func (m *connectionManager) Connect(ctx context.Context, p *api.Printer) (err error) {
	m.connectionLock.Lock()
	defer m.connectionLock.Unlock()
	if _, ok := m.connections[p.UID]; ok {
		return ErrPrinterAlreadyConnected
	}

	for _, existingConn := range m.connections {
		if existingConn.printer.Spec.Device == p.Spec.Device {
			if c, ok := p.Status.Conditions.Get(api.PrinterConditionTypeConnected); ok &&
				c.Status == api.ConditionStatusFalse &&
				c.Reason == "DeviceBusy" {
				return ErrDeviceBusy
			}

			_, _, err := m.printerStorage.EnsureUpdate(ctx, p, db.PrinterUpdateFn(func(printer *api.Printer) (err error) {
				printer.Status.Conditions.Set(api.Condition{
					Type:               api.PrinterConditionTypeConnected,
					Reason:             "DeviceBusy",
					Message:            ErrDeviceBusy.Error(),
					Status:             api.ConditionStatusFalse,
					LastTransitionTime: time.Now(),
				})
				return
			}))
			if err != nil {
				m.log.
					WithField("name", p.Name).
					WithError(err).
					Error("error updating printer status")
			}
			return ErrDeviceBusy
		}
	}

	m.log.
		WithField("baud", p.Spec.BaudRate).
		WithField("device", p.Spec.Device).
		WithField("name", p.Name).
		Info("opening connection")

	connection := m.connection(p)
	if err = connection.Open(); err != nil {
		_, _, uErr := m.printerStorage.EnsureUpdate(ctx, p, db.PrinterUpdateFn(func(printer *api.Printer) error {
			printer.Status.Conditions.Set(api.Condition{
				Type:               api.PrinterConditionTypeConnected,
				Reason:             "ConnectionError",
				Message:            err.Error(),
				Status:             api.ConditionStatusFalse,
				LastTransitionTime: time.Now(),
			})
			printer.Status.Temperature = nil
			return nil
		}))
		if uErr != nil {
			m.log.
				WithField("name", p.Name).
				WithError(err).
				Error("error updating printer status")
		}
		return
	}

	_, _, err = m.printerStorage.EnsureUpdate(ctx, p, db.PrinterUpdateFn(func(printer *api.Printer) (err error) {
		printer.Status.Conditions.Set(api.Condition{
			Type:               api.PrinterConditionTypeConnected,
			Status:             api.ConditionStatusTrue,
			LastTransitionTime: time.Now(),
		})
		return
	}))
	if err != nil {
		return
	}

	entry := &connectionEntry{
		conn:    connection,
		printer: p,
		temperatureHistory: &TemperatureHistory{
			ExtruderActual: timeseries.NewTimeSeries(1*time.Second, timeseries.NewSeries(3600, timeseries.Avg)),
			BedActual:      timeseries.NewTimeSeries(1*time.Second, timeseries.NewSeries(3600, timeseries.Avg)),
			ExtruderTarget: timeseries.NewTimeSeries(1*time.Second, timeseries.NewSeries(3600, timeseries.Avg)),
			BedTarget:      timeseries.NewTimeSeries(1*time.Second, timeseries.NewSeries(3600, timeseries.Avg)),
		},
	}

	// monitor connection status
	go func(conn Connection, printerID string) {
		temperatureTicker := time.NewTicker(3 * time.Second)
		go func() {
			for t := range temperatureTicker.C {
				// temperature ticker
				_, _, err := m.printerStorage.EnsureUpdate(ctx, p, db.PrinterUpdateFn(func(printer *api.Printer) (err error) {
					status := conn.Status()
					printer.Status.Temperature = &api.PrinterTemperature{
						ExtruderActual: float32(status.Extruder().Actual()),
						ExtruderTarget: float32(status.Extruder().Target()),
						BedActual:      float32(status.Bed().Actual()),
						BedTarget:      float32(status.Bed().Target()),
					}

					entry.temperatureHistory.Lock()
					defer entry.temperatureHistory.Unlock()
					entry.temperatureHistory.ExtruderActual.AddWithTime(status.Extruder().Actual(), t)
					entry.temperatureHistory.ExtruderTarget.AddWithTime(status.Extruder().Target(), t)
					entry.temperatureHistory.BedActual.AddWithTime(status.Bed().Actual(), t)
					entry.temperatureHistory.BedTarget.AddWithTime(status.Bed().Target(), t)
					return
				}))
				if err != nil {
					m.log.WithError(err).Error("updating connection status in ticker")
				}
			}
		}()

		runErr := conn.Wait()
		defer temperatureTicker.Stop()
		var conditions []api.Condition
		if runErr != nil {
			m.log.
				WithError(runErr).
				WithField("id", printerID).
				Error("connection has been closed with error")
			conditions = []api.Condition{
				{
					Type:               "Connected",
					Reason:             "ConnectionError",
					Message:            runErr.Error(),
					Status:             api.ConditionStatusFalse,
					LastTransitionTime: time.Now(),
				},
			}
		} else {
			m.log.
				WithField("id", printerID).
				Info("connection has been closed")
			conditions = []api.Condition{
				{
					Type:               "Connected",
					Reason:             "ConnectionClosed",
					Status:             api.ConditionStatusFalse,
					LastTransitionTime: time.Now(),
				},
			}
		}
		m.connectionLock.Lock()
		defer m.connectionLock.Unlock()
		if _, exists := m.connections[printerID]; exists {
			delete(m.connections, printerID)
		}

		_, _, err := m.printerStorage.EnsureUpdate(ctx, p, db.PrinterUpdateFn(func(printer *api.Printer) (err error) {
			printer.Status.Conditions.SetMultiple(conditions...)
			printer.Status.Temperature = nil
			return
		}))
		if err != nil {
			m.log.
				WithError(err).
				WithField("id", printerID).
				Error("update printer status")
		}
	}(connection, p.UID)

	m.connections[p.UID] = entry
	return nil
}

func (m *connectionManager) TemperatureHistory(ctx context.Context, printer *api.Printer) (*TemperatureHistory, error) {
	m.connectionLock.Lock()
	defer m.connectionLock.Unlock()

	c, ok := m.connections[printer.UID]
	if !ok {
		return nil, ErrPrinterNotConnected
	}
	return c.temperatureHistory, nil
}

func (m *connectionManager) Disconnect(ctx context.Context, printer *api.Printer) (err error) {
	m.connectionLock.Lock()
	defer m.connectionLock.Unlock()

	c, ok := m.connections[printer.UID]
	if !ok {
		if condition, cok := printer.Status.Conditions.Get("Connected"); cok &&
			condition.Status == api.ConditionStatusFalse &&
			condition.Reason != "" {
			// reset reported errors when disconnect is requested
			_, _, err = m.printerStorage.EnsureUpdate(ctx, printer, db.PrinterUpdateFn(func(printer *api.Printer) (err error) {
				printer.Status.Conditions.Set(api.Condition{
					Type:               "Connected",
					Status:             api.ConditionStatusFalse,
					LastTransitionTime: time.Now(),
				})
				return
			}))
			if err != nil {
				m.log.WithError(err).Error("updating connection status in disconnect")
			}
		}
		return ErrPrinterNotConnected
	}
	if ok && c.aquired {
		_, _, err = m.printerStorage.EnsureUpdate(ctx, printer, db.PrinterUpdateFn(func(printer *api.Printer) (err error) {
			printer.Status.Conditions.Set(api.Condition{
				Type:               "Connected",
				Reason:             "ConnectionInUse",
				Message:            "Connection is still in use and cannot be closed",
				Status:             api.ConditionStatusTrue,
				LastTransitionTime: time.Now(),
			})
			return
		}))
		if err != nil {
			m.log.WithError(err).Error("updating connection status in disconnect")
		}
		return ErrConnectionInUse
	}

	m.log.Info("ConnectionManager.Disconnect")
	if err = c.conn.Close(); err != nil {
		return err
	}
	delete(m.connections, printer.UID)
	return nil
}

func (m *connectionManager) AquireConnection(ctx context.Context, printer *api.Printer) (Connection, error) {
	m.connectionLock.Lock()
	c, ok := m.connections[printer.UID]
	if ok && c.aquired {
		m.connectionLock.Unlock()
		return nil, ErrConnectionInUse
	}
	if !ok || c == nil {
		m.connectionLock.Unlock()
		return nil, ErrPrinterNotConnected
	}
	c.aquired = true
	m.connectionLock.Unlock()
	return m.getConnection(printer), nil
}

func (m *connectionManager) ReleaseConnection(ctx context.Context, printer *api.Printer) {
	m.connectionLock.Lock()
	defer m.connectionLock.Unlock()
	c, ok := m.connections[printer.UID]
	if ok && c.aquired {
		c.aquired = false
	}
	return
}
