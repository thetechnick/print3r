package printer

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/printer/connection"
	conntest "github.com/thetechnick/print3r/pkg/printer/connection/test"
	"github.com/thetechnick/print3r/pkg/storage/test"
)

func TestConnectionManager(t *testing.T) {
	ctx := context.Background()
	c := &conntest.ConnectionMock{}
	printerStorage := &test.PrinterStorageMock{}
	printer := &api.Printer{
		ObjectMeta: api.ObjectMeta{
			UID: "123",
		},
	}

	c.
		On("Status").
		Return(connection.Status{})
	c.
		On("Open").
		Return(nil)
	c.
		On("Wait").
		Return(nil)
	c.
		On("Close").
		Return(nil)

	printerStorage.
		On("FindOneByName", mock.Anything, mock.Anything).
		Return(printer, nil)
	printerStorage.
		On("UpdateStatus", mock.Anything, mock.Anything).
		Return(nil)
	var p1 *api.Printer
	var p2 *api.Printer
	printerStorage.
		On("EnsureUpdate", mock.Anything, mock.Anything, mock.Anything).
		Return(p1, p2, nil)

	cm := NewConnectionManager(printerStorage).(*connectionManager)
	cm.connection = func(p *api.Printer) Connection {
		return c
	}

	assert.NoError(t, cm.Connect(ctx, printer))

	ac, err := cm.AquireConnection(ctx, printer)
	assert.NoError(t, err)
	assert.Exactly(t, c, ac)

	_, err = cm.AquireConnection(ctx, printer)
	assert.Equal(t, ErrConnectionInUse, err)

	assert.Equal(t, ErrConnectionInUse, cm.Disconnect(ctx, printer))

	cm.ReleaseConnection(ctx, printer)

	assert.NoError(t, cm.Disconnect(ctx, printer))
}
