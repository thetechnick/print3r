package printer

import (
	"context"
	"reflect"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	db "github.com/thetechnick/print3r/pkg/storage"
)

// Controller manages printer control loops
type Controller interface {
	Run() error
}

type printerController struct {
	printStorage      db.PrintStorage
	printerStorage    db.PrinterStorage
	hub               db.EventHub
	connectionManager ConnectionManager

	log *log.Entry
}

// NewController returns a new Controller
func NewController(
	printerStorage db.PrinterStorage,
	printStorage db.PrintStorage,
	connectionManager ConnectionManager,
	hub db.EventHub,
) Controller {
	return &printerController{
		printerStorage:    printerStorage,
		printStorage:      printStorage,
		connectionManager: connectionManager,
		hub:               hub,

		log: log.WithField("module", "printer.Controller"),
	}
}

func (c *printerController) Run() (err error) {
	client := c.hub.Register()

	var printers []*api.Printer
	ctx := context.Background()
	printers, err = c.printerStorage.All(ctx)
	if err != nil {
		return
	}
	for _, p := range printers {
		if err := c.Sync(ctx, p); err != nil {
			c.log.WithError(err).WithField("name", p.Name).Warn("syncing printer")
		}
	}

	t := time.NewTicker(10 * time.Second)
	defer t.Stop()
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()
	for {
		select {
		case <-t.C:
			printers, err = c.printerStorage.All(ctx)
			if err != nil {
				return
			}
			for _, p := range printers {
				if err := c.Sync(ctx, p); err != nil {
					c.log.WithError(err).WithField("name", p.Name).Warn("syncing printer")
				}
			}

		case event := <-client.Events():
			switch event.Operation {
			case api.ResourceEventOperationCreated:
				printer, ok := event.New.(*api.Printer)
				if !ok {
					continue
				}
				if err := c.Sync(ctx, printer); err != nil {
					c.log.WithError(err).WithField("name", printer.Name).Warn("adding printer")
				}

			case api.ResourceEventOperationDeleted:
				printer, ok := event.Old.(*api.Printer)
				if !ok {
					continue
				}
				if err := c.Remove(ctx, printer); err != nil {
					c.log.WithError(err).WithField("name", printer.Name).Warn("removing printer")
				}

			case api.ResourceEventOperationUpdated:
				oldPrinter, ok := event.Old.(*api.Printer)
				if !ok {
					continue
				}
				newPrinter, ok := event.New.(*api.Printer)
				if !ok {
					continue
				}

				if checkNeedsReconnect(newPrinter, oldPrinter) {
					if err := c.Remove(ctx, oldPrinter); err != nil {
						c.log.WithError(err).WithField("name", newPrinter.Name).Warn("reconnecting printer")
					}
				}

				if reflect.DeepEqual(oldPrinter.Spec, newPrinter.Spec) {
					continue
				}
				if err := c.Sync(ctx, newPrinter); err != nil {
					c.log.WithError(err).WithField("name", newPrinter.Name).Warn("updating printer")
				}
			}
		}
	}
}

func (c *printerController) Sync(ctx context.Context, p *api.Printer) error {
	c.log.WithField("name", p.Name).WithField("statue", p.Spec.State).Debug("syncing printer")
	switch p.Spec.State {
	case api.PrinterSpecStateConnected:
		if connectedPrinter, ok := c.connectionManager.ConnectionInfo(ctx, p); ok {
			if checkNeedsReconnect(p, connectedPrinter) {
				c.log.WithField("name", p.Name).Info("needs reconnect")
				if err := c.connectionManager.Disconnect(ctx, p); err != nil {
					if err != ErrPrinterNotConnected {
						return err
					}
				}
			}
		}

		if err := c.connectionManager.Connect(ctx, p); err != nil {
			if err != ErrPrinterAlreadyConnected {
				return err
			}
		}

	case api.PrinterSpecStateDisconnected:
		if err := c.connectionManager.Disconnect(ctx, p); err != nil {
			if err != ErrPrinterNotConnected {
				return err
			}
		}
	}
	return nil
}

func (c *printerController) Remove(ctx context.Context, p *api.Printer) error {
	if err := c.connectionManager.Disconnect(ctx, p); err != nil {
		if err == ErrPrinterNotConnected {
			return nil
		}
		return err
	}
	return nil
}

func checkNeedsReconnect(newPrinter, oldPrinter *api.Printer) bool {
	if newPrinter == nil || oldPrinter == nil {
		return false
	}
	if oldPrinter.Spec.BaudRate != newPrinter.Spec.BaudRate ||
		oldPrinter.Spec.Device != newPrinter.Spec.Device {
		return true
	}
	return false
}
