package test

import (
	"context"

	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/printer"
)

// ConnectionManagerMock is a mock of printer.ConnectionManager
type ConnectionManagerMock struct {
	mock.Mock
}

func (m *ConnectionManagerMock) Connect(ctx context.Context, printer *api.Printer) error {
	args := m.Called(ctx, printer)
	return args.Error(0)
}

func (m *ConnectionManagerMock) Disconnect(ctx context.Context, printer *api.Printer) error {
	args := m.Called(ctx, printer)
	return args.Error(0)
}

func (m *ConnectionManagerMock) TemperatureHistory(ctx context.Context, p *api.Printer) (*printer.TemperatureHistory, error) {
	args := m.Called(ctx, p)
	return args.Get(0).(*printer.TemperatureHistory), args.Error(1)
}

func (m *ConnectionManagerMock) AquireConnection(ctx context.Context, p *api.Printer) (printer.Connection, error) {
	args := m.Called(ctx, p)
	return args.Get(0).(printer.Connection), args.Error(1)
}

func (m *ConnectionManagerMock) ReleaseConnection(ctx context.Context, p *api.Printer) {
	m.Called(ctx, p)
}

func (m *ConnectionManagerMock) ConnectionInfo(ctx context.Context, p *api.Printer) (connectedPrinter *api.Printer, isConnected bool) {
	args := m.Called(ctx, p)
	return args.Get(0).(*api.Printer), args.Bool(1)
}
