package server

import (
	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"strings"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api/v1"
	"github.com/thetechnick/print3r/pkg/api/v1/services"
	"github.com/thetechnick/print3r/pkg/volume"
	"github.com/thetechnick/print3r/ui"

	"github.com/tmc/grpc-websocket-proxy/wsproxy"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

// Config contains the server config
type Config struct {
	Address             string
	InsecureHTTPAddress string
	InsecureGRPCAddress string
	CertPEM             []byte
	KeyPEM              []byte
}

// Server represents the main application server
type Server interface {
	Run() error
}

// NewServer creates a new Server instance
func NewServer(
	c *Config,
	// fs webdav.FileSystem,
	vc volume.Service,
	versionService v1.VersionServiceServer,
	volumeService v1.VolumeServiceServer,
	eventService v1.ResourceEventServiceServer,
	printerService v1.PrinterServiceServer,
	printService v1.PrintServiceServer,
	videoStreamService v1.VideoStreamServiceServer,
	videoStreamEndpoint *services.VideoStreamStreamEndpoint,
) (Server, error) {
	// Certificates
	pair, err := tls.X509KeyPair(c.CertPEM, c.KeyPEM)
	if err != nil {
		return nil, err
	}

	// GRPC
	grpcServer := grpc.NewServer()
	mux := http.NewServeMux()

	mux.Handle("/webdav/", vc)

	// register services
	v1.RegisterVersionServiceServer(grpcServer, versionService)
	v1.RegisterVolumeServiceServer(grpcServer, volumeService)
	v1.RegisterResourceEventServiceServer(grpcServer, eventService)
	v1.RegisterPrinterServiceServer(grpcServer, printerService)
	v1.RegisterPrintServiceServer(grpcServer, printService)
	v1.RegisterVideoStreamServiceServer(grpcServer, videoStreamService)

	s := &server{
		config:              c,
		cert:                &pair,
		grpcServer:          grpcServer,
		mux:                 mux,
		videoStreamEndpoint: videoStreamEndpoint,
	}
	return s, nil
}

type server struct {
	config              *Config
	cert                *tls.Certificate
	grpcServer          *grpc.Server
	mux                 *http.ServeMux
	videoStreamEndpoint *services.VideoStreamStreamEndpoint
}

func (s *server) Run() error {
	// Open listen connection
	conn, err := net.Listen("tcp", s.config.Address)
	if err != nil {
		return err
	}

	// GRPC GW
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	gwmux := runtime.NewServeMux(
		runtime.WithProtoErrorHandler(
			runtime.ProtoErrorHandlerFunc(DefaultHTTPError)))

	clientConn, err := grpc.Dial(s.config.InsecureGRPCAddress, grpc.WithInsecure())
	if err != nil {
		return err
	}

	// Register servers and handlers
	if err = v1.RegisterVersionServiceHandler(ctx, gwmux, clientConn); err != nil {
		return err
	}
	if err = v1.RegisterVolumeServiceHandler(ctx, gwmux, clientConn); err != nil {
		return err
	}
	if err = v1.RegisterResourceEventServiceHandler(ctx, gwmux, clientConn); err != nil {
		return err
	}
	if err = v1.RegisterPrinterServiceHandler(ctx, gwmux, clientConn); err != nil {
		return err
	}
	if err = v1.RegisterPrintServiceHandler(ctx, gwmux, clientConn); err != nil {
		return err
	}
	if err = v1.RegisterVideoStreamServiceHandler(ctx, gwmux, clientConn); err != nil {
		return err
	}
	gwmux.Handle("GET", v1.VideoStreamServiceStreamPattern, s.videoStreamEndpoint.Handle)

	// Server
	s.mux.HandleFunc("/health", func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprint(w, "ok")
	})
	s.mux.Handle("/api/v1/", http.StripPrefix("/api/v1", wsproxy.WebsocketProxy(gwmux)))

	uiFiles := http.FileServer(ui.Assets)
	s.mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if strings.HasSuffix(r.RequestURI, "js") ||
			strings.HasSuffix(r.RequestURI, "css") ||
			strings.HasSuffix(r.RequestURI, "json") ||
			strings.HasSuffix(r.RequestURI, "png") {

			uiFiles.ServeHTTP(w, r)
			return
		}

		r.URL.Path = "/"
		uiFiles.ServeHTTP(w, r)
	})

	secureServer := &http.Server{
		Addr:    s.config.Address,
		Handler: grpcHandlerFunc(s.grpcServer, allowCORS(s.mux)),
		TLSConfig: &tls.Config{
			Certificates: []tls.Certificate{*s.cert},
			NextProtos:   []string{"h2"},
		},
	}

	log.Info("server secure api: ", s.config.Address)
	log.Info("server insecure http api: ", s.config.InsecureHTTPAddress)
	log.Info("server insecure grpc api: ", s.config.InsecureGRPCAddress)

	insecureGRPCL, err := net.Listen("tcp", s.config.InsecureGRPCAddress)
	if err != nil {
		return err
	}

	insecureGRPCErrCh := make(chan error)
	go func(errCh chan error) {
		errCh <- s.grpcServer.Serve(insecureGRPCL)
	}(insecureGRPCErrCh)

	insecureHTTPErrCh := make(chan error)
	go func(errCh chan error) {
		errCh <- http.ListenAndServe(s.config.InsecureHTTPAddress, allowCORS(s.mux))
	}(insecureHTTPErrCh)

	secureErrCh := make(chan error)
	go func(errCh chan error) {
		errCh <- secureServer.Serve(tls.NewListener(conn, secureServer.TLSConfig))
	}(secureErrCh)

	select {
	case err := <-insecureGRPCErrCh:
		log.WithError(err).Fatal("error serving insecure grpc api")
	case err := <-insecureHTTPErrCh:
		log.WithError(err).Fatal("error serving insecure http api")
	case err := <-secureErrCh:
		log.WithError(err).Fatal("error serving secure api")
	}

	return nil
}
