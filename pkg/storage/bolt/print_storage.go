package bolt

import (
	"context"

	"github.com/boltdb/bolt"
	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/v1"
	db "github.com/thetechnick/print3r/pkg/storage"
)

// NewPrintStorage creates a new PrintStorage
func NewPrintStorage(db *bolt.DB, hub db.EventHub) (db.PrintStorage, error) {
	s := &printStorage{
		hub: hub,
	}
	s.base = &baseStorage{
		log: log.WithField("module", "bolt.printStorage"),

		db:          db,
		bucket:      "print3r:prints",
		typeName:    "print",
		toStorage:   toStorageFn(s.toStorage),
		fromStorage: fromStorageFn(s.fromStorage),
		key:         keyFn(s.key),
	}
	return s, s.base.init()
}

type printStorage struct {
	base *baseStorage
	hub  db.EventHub
}

func (s *printStorage) Delete(ctx context.Context, p *api.Print) (deleted *api.Print, err error) {
	var obj api.Object
	if obj, err = s.base.Delete(p); err != nil {
		return
	}
	deleted = obj.(*api.Print)
	if deleted != nil {
		s.hub.Broadcast(deleted, nil)
	}
	return
}

func (s *printStorage) Create(ctx context.Context, p *api.Print) (err error) {
	err = s.base.Create(p)
	if err != nil {
		return
	}
	s.hub.Broadcast(nil, p)
	return
}

func (s *printStorage) Update(ctx context.Context, p *api.Print) (err error) {
	var o, n api.Object
	o, n, err = s.base.Update(p, func(old, new api.Object) {
		oldObj := old.(*api.Print)
		newObj := new.(*api.Print)

		// do not update the status
		newObj.Status = oldObj.Status
	})
	s.hub.Broadcast(o, n)
	return
}

func (s *printStorage) UpdateStatus(ctx context.Context, p *api.Print) (err error) {
	var o, n api.Object
	o, n, err = s.base.Update(p, func(old, new api.Object) {
		oldObj := old.(*api.Print)
		newObj := new.(*api.Print)

		// do not update the spec or metadata
		newObj.Spec = oldObj.Spec
		newObj.ObjectMeta = oldObj.ObjectMeta
	})
	s.hub.Broadcast(o, n)
	return
}

func (s *printStorage) FindOneByName(ctx context.Context, name string) (print *api.Print, err error) {
	var obj api.Object
	obj, err = s.base.FindOneByName(name)
	if err != nil {
		return
	}
	if obj != nil {
		print = obj.(*api.Print)
	}
	return
}

func (s *printStorage) All(ctx context.Context) (list []*api.Print, err error) {
	var objs []api.Object
	if objs, err = s.base.All(); err != nil {
		return
	}
	for _, obj := range objs {
		list = append(list, obj.(*api.Print))
	}
	return
}

func (s *printStorage) key(obj api.Object) []byte {
	return []byte(obj.GetName())
}

func (s *printStorage) fromStorage(b []byte) (obj api.Object, err error) {
	if b == nil {
		return
	}

	so := &v1.Print{}
	if err = proto.Unmarshal(b, so); err != nil {
		return
	}

	o := &api.Print{}
	err = v1.Scheme.Convert(so, o)
	obj = o
	return
}

func (s *printStorage) toStorage(o api.Object) (b []byte, err error) {
	p := o.(*api.Print)
	so := &v1.Print{}
	if err = v1.Scheme.Convert(p, so); err != nil {
		return
	}

	if b, err = proto.Marshal(so); err != nil {
		return
	}
	return
}
