package bolt

import (
	"context"
	"os"
	"testing"

	"github.com/boltdb/bolt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/storage/test"
)

const printTestDBFile = "print-test.db"

func TestPrintStorage(t *testing.T) {
	os.Remove(printTestDBFile)
	defer os.Remove(printTestDBFile)

	ctx := context.Background()
	boltDB, err := bolt.Open(printTestDBFile, 0600, nil)
	if err != nil {
		t.Fatal(err)
	}

	var eventHub test.EventHubMock
	eventHub.On("Broadcast", mock.Anything, mock.Anything)
	storage, err := NewPrintStorage(boltDB, &eventHub)
	if err != nil {
		t.Fatal(err)
	}

	print := &api.Print{
		ObjectMeta: api.ObjectMeta{
			Name: "test",
		},
		Spec: api.PrintSpec{
			Printer: "test",
		},
	}
	if err = storage.Create(ctx, print); err != nil {
		t.Fatal(err)
	}

	vl, err := storage.All(ctx)
	if err != nil {
		t.Fatal(err)
	}
	if assert.Len(t, vl, 1) {
		assert.Equal(t, vl[0].Spec.Printer, "test")
	}

	v, err := storage.FindOneByName(ctx, "test")
	if err != nil {
		t.Fatal(err)
	}
	if assert.NotNil(t, v) {
		assert.Equal(t, v.Spec.Printer, "test")
	}

	updatedPrint := &api.Print{
		ObjectMeta: api.ObjectMeta{
			Name: "test",
		},
		Spec: api.PrintSpec{
			Printer: "test-updated",
		},
	}
	err = storage.Update(ctx, updatedPrint)
	if err != nil {
		t.Fatal(err)
	}

	v, err = storage.FindOneByName(ctx, "test")
	if err != nil {
		t.Fatal(err)
	}
	if assert.NotNil(t, v) {
		assert.Equal(t, v.Spec.Printer, "test-updated")
	}

	deleted, err := storage.Delete(ctx, v)
	if err != nil {
		t.Fatal(err)
	}
	assert.NotNil(t, deleted, `entry with name "test" should be found`)

	v, err = storage.FindOneByName(ctx, "test")
	if err != nil {
		t.Fatal(err)
	}
	assert.Nil(t, v)
}
