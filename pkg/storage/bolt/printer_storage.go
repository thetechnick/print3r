package bolt

import (
	"context"

	"github.com/boltdb/bolt"
	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/v1"
	db "github.com/thetechnick/print3r/pkg/storage"
)

const printerBucket = "print3r:printers"

// NewPrinterStorage creates a new PrinterStorage
func NewPrinterStorage(db *bolt.DB, hub db.EventHub) (db.PrinterStorage, error) {
	s := &printerStorage{
		hub: hub,
	}
	s.base = &baseStorage{
		log: log.WithField("module", "bolt.printerStorage"),

		db:          db,
		bucket:      "print3r:printers",
		typeName:    "printer",
		toStorage:   toStorageFn(s.toStorage),
		fromStorage: fromStorageFn(s.fromStorage),
		key:         keyFn(s.key),
	}
	return s, s.base.init()
}

type printerStorage struct {
	base *baseStorage
	hub  db.EventHub
}

func (s *printerStorage) Delete(ctx context.Context, p *api.Printer) (deleted *api.Printer, err error) {
	var obj api.Object
	if obj, err = s.base.Delete(p); err != nil {
		return
	}
	deleted = obj.(*api.Printer)
	if deleted != nil {
		s.hub.Broadcast(deleted, nil)
	}
	return
}

func (s *printerStorage) Create(ctx context.Context, p *api.Printer) (err error) {
	err = s.base.Create(p)
	if err != nil {
		return
	}
	s.hub.Broadcast(nil, p)
	return
}

func (s *printerStorage) Update(ctx context.Context, p *api.Printer) (err error) {
	var o, n api.Object
	o, n, err = s.base.Update(p, func(old, new api.Object) {
		oldObj := old.(*api.Printer)
		newObj := new.(*api.Printer)
		// do not update the status
		newObj.Status = oldObj.Status
	})
	s.hub.Broadcast(o, n)
	return
}

func (s *printerStorage) EnsureUpdate(ctx context.Context, p *api.Printer, fn db.PrinterUpdateFn) (old, new *api.Printer, err error) {
	for {
		var toUpdate *api.Printer
		if toUpdate, err = s.FindOneByName(ctx, p.Name); err != nil {
			return
		}
		if toUpdate == nil || p.UID != toUpdate.UID {
			// was deleted
			return
		}

		if err = fn(toUpdate); err != nil {
			return
		}
		var oldObj, newObj api.Object
		oldObj, newObj, err = s.base.Update(toUpdate, noopUpdateHook)
		if err != nil {
			if err == db.ErrResourceVersionChanged {
				continue
			}
			return
		}
		s.hub.Broadcast(oldObj, newObj)
		return
	}
}

func (s *printerStorage) UpdateStatus(ctx context.Context, v *api.Printer) (err error) {
	var o, n api.Object
	o, n, err = s.base.Update(v, func(old, new api.Object) {
		oldObj := old.(*api.Printer)
		newObj := new.(*api.Printer)

		// do not update the spec or metadata
		newObj.Spec = oldObj.Spec
		newObj.ObjectMeta = oldObj.ObjectMeta
	})
	s.hub.Broadcast(o, n)
	return
}

func (s *printerStorage) FindOneByName(ctx context.Context, name string) (volume *api.Printer, err error) {
	var obj api.Object
	obj, err = s.base.FindOneByName(name)
	if err != nil {
		return
	}
	if obj != nil {
		volume = obj.(*api.Printer)
	}
	return
}

func (s *printerStorage) All(ctx context.Context) (list []*api.Printer, err error) {
	var objs []api.Object
	if objs, err = s.base.All(); err != nil {
		return
	}
	for _, obj := range objs {
		list = append(list, obj.(*api.Printer))
	}
	return
}

func (s *printerStorage) key(obj api.Object) []byte {
	return []byte(obj.GetName())
}

func (s *printerStorage) fromStorage(b []byte) (o api.Object, err error) {
	if b == nil {
		return
	}

	so := &v1.Printer{}
	if err = proto.Unmarshal(b, so); err != nil {
		return
	}

	printer := &api.Printer{}
	err = v1.Scheme.Convert(so, printer)
	o = printer
	return
}

func (s *printerStorage) toStorage(obj api.Object) (b []byte, err error) {
	printer := obj.(*api.Printer)
	so := &v1.Printer{}
	if err = v1.Scheme.Convert(printer, so); err != nil {
		return
	}

	if b, err = proto.Marshal(so); err != nil {
		return
	}
	return
}
