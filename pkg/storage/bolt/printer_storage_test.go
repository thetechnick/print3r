package bolt

import (
	"context"
	"os"
	"testing"

	"github.com/boltdb/bolt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/storage/test"
)

const printerTestDBFile = "printer-test.db"

func TestPrinterStorage(t *testing.T) {
	os.Remove(printerTestDBFile)
	defer os.Remove(printerTestDBFile)

	ctx := context.Background()
	boltDB, err := bolt.Open(printerTestDBFile, 0600, nil)
	if err != nil {
		t.Fatal(err)
	}
	var eventHub test.EventHubMock
	eventHub.On("Broadcast", mock.Anything, mock.Anything)

	storage, err := NewPrinterStorage(boltDB, &eventHub)
	if err != nil {
		t.Fatal(err)
	}

	printer := &api.Printer{
		ObjectMeta: api.ObjectMeta{
			Name: "test",
		},
		Spec: api.PrinterSpec{
			Device: "/dev/abc",
		},
	}
	if err = storage.Create(ctx, printer); err != nil {
		t.Fatal(err)
	}

	vl, err := storage.All(ctx)
	if err != nil {
		t.Fatal(err)
	}
	if assert.Len(t, vl, 1) {
		assert.Equal(t, vl[0].Spec.Device, "/dev/abc")
	}

	v, err := storage.FindOneByName(ctx, "test")
	if err != nil {
		t.Fatal(err)
	}
	if assert.NotNil(t, v) {
		assert.Equal(t, v.Spec.Device, "/dev/abc")
	}

	updatedVolume := &api.Printer{
		ObjectMeta: api.ObjectMeta{
			Name: "test",
		},
		Spec: api.PrinterSpec{
			Device: "/dev/abcd",
		},
	}
	err = storage.Update(ctx, updatedVolume)
	if err != nil {
		t.Fatal(err)
	}

	v, err = storage.FindOneByName(ctx, "test")
	if err != nil {
		t.Fatal(err)
	}
	if assert.NotNil(t, v) {
		assert.Equal(t, v.Spec.Device, "/dev/abcd")
	}

	deleted, err := storage.Delete(ctx, v)
	if err != nil {
		t.Fatal(err)
	}
	assert.NotNil(t, deleted, `entry with name "test" should be found`)

	v, err = storage.FindOneByName(ctx, "test")
	if err != nil {
		t.Fatal(err)
	}
	assert.Nil(t, v)
}
