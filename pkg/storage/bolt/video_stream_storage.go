package bolt

import (
	"context"

	"github.com/boltdb/bolt"
	"github.com/gogo/protobuf/proto"
	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/v1"
	"github.com/thetechnick/print3r/pkg/storage"
)

// NewVideoStreamStorage creates a new VideoStreamStorage
func NewVideoStreamStorage(db *bolt.DB, hub storage.EventHub) (storage.VideoStreamStorage, error) {
	s := &videoStreamStorage{
		hub: hub,
	}
	s.base = &baseStorage{
		log: log.WithField("module", "bolt.videoStreamStorage"),

		db:          db,
		bucket:      "print3r:video_streams",
		typeName:    "video_stream",
		toStorage:   toStorageFn(s.toStorage),
		fromStorage: fromStorageFn(s.fromStorage),
		key:         keyFn(s.key),
	}
	return s, s.base.init()
}

type videoStreamStorage struct {
	base *baseStorage
	hub  storage.EventHub
}

func (s *videoStreamStorage) Delete(ctx context.Context, v *api.VideoStream) (deleted *api.VideoStream, err error) {
	var obj api.Object
	if obj, err = s.base.Delete(v); err != nil {
		return
	}
	deleted = obj.(*api.VideoStream)
	if deleted != nil {
		s.hub.Broadcast(deleted, nil)
	}
	return
}

func (s *videoStreamStorage) Create(ctx context.Context, v *api.VideoStream) (err error) {
	err = s.base.Create(v)
	if err != nil {
		return
	}
	s.hub.Broadcast(nil, v)
	return
}

func (s *videoStreamStorage) Update(ctx context.Context, v *api.VideoStream) (err error) {
	var o, n api.Object
	o, n, err = s.base.Update(v, func(old, new api.Object) {
		oldObj := old.(*api.VideoStream)
		newObj := new.(*api.VideoStream)

		// do not update the status
		newObj.Status = oldObj.Status
	})
	s.hub.Broadcast(o, n)
	return
}

func (s *videoStreamStorage) UpdateStatus(ctx context.Context, v *api.VideoStream) (err error) {
	var o, n api.Object
	o, n, err = s.base.Update(v, func(old, new api.Object) {
		oldObj := old.(*api.VideoStream)
		newObj := new.(*api.VideoStream)

		// do not update the spec or metadata
		newObj.Spec = oldObj.Spec
		newObj.ObjectMeta = oldObj.ObjectMeta
	})
	s.hub.Broadcast(o, n)
	return
}

func (s *videoStreamStorage) EnsureUpdate(ctx context.Context, p *api.VideoStream, fn storage.VideoStreamUpdateFn) (old, new *api.VideoStream, err error) {
	for {
		var toUpdate *api.VideoStream
		if toUpdate, err = s.FindOneByName(ctx, p.Name); err != nil {
			return
		}
		if toUpdate == nil || p.UID != toUpdate.UID {
			// was deleted
			return
		}

		if err = fn(toUpdate); err != nil {
			return
		}
		var oldObj, newObj api.Object
		oldObj, newObj, err = s.base.Update(toUpdate, noopUpdateHook)
		if err != nil {
			if err == storage.ErrResourceVersionChanged {
				continue
			}
			return
		}
		s.hub.Broadcast(oldObj, newObj)
		return
	}
}

func (s *videoStreamStorage) FindOneByName(ctx context.Context, name string) (videoStream *api.VideoStream, err error) {
	var obj api.Object
	obj, err = s.base.FindOneByName(name)
	if err != nil {
		return
	}
	if obj != nil {
		videoStream = obj.(*api.VideoStream)
	}
	return
}

func (s *videoStreamStorage) All(ctx context.Context) (list []*api.VideoStream, err error) {
	var objs []api.Object
	if objs, err = s.base.All(); err != nil {
		return
	}
	for _, obj := range objs {
		list = append(list, obj.(*api.VideoStream))
	}
	return
}

func (s *videoStreamStorage) key(obj api.Object) []byte {
	return []byte(obj.GetName())
}

func (s *videoStreamStorage) fromStorage(b []byte) (obj api.Object, err error) {
	if b == nil {
		return
	}

	so := &v1.VideoStream{}
	if err = proto.Unmarshal(b, so); err != nil {
		return
	}

	o := &api.VideoStream{}
	err = v1.Scheme.Convert(so, o)
	obj = o
	return
}

func (s *videoStreamStorage) toStorage(o api.Object) (b []byte, err error) {
	p := o.(*api.VideoStream)
	so := &v1.VideoStream{}
	if err = v1.Scheme.Convert(p, so); err != nil {
		return
	}

	if b, err = proto.Marshal(so); err != nil {
		return
	}
	return
}
