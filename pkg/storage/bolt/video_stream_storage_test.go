package bolt

import (
	"context"
	"os"
	"testing"

	"github.com/boltdb/bolt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/storage/test"
)

const videoStreamTestDBFile = "video-stream-test.db"

func TestVideoStreamStorage(t *testing.T) {
	os.Remove(videoStreamTestDBFile)
	defer os.Remove(videoStreamTestDBFile)

	ctx := context.Background()
	boltDB, err := bolt.Open(videoStreamTestDBFile, 0600, nil)
	if err != nil {
		t.Fatal(err)
	}
	var eventHub test.EventHubMock
	eventHub.On("Broadcast", mock.Anything, mock.Anything)

	storage, err := NewVideoStreamStorage(boltDB, &eventHub)
	if err != nil {
		t.Fatal(err)
	}

	printer := &api.VideoStream{
		ObjectMeta: api.ObjectMeta{
			Name: "test",
		},
		Spec: api.VideoStreamSpec{
			Motion: &api.MotionConfig{
				Device: "/dev/abc",
			},
		},
	}
	if err = storage.Create(ctx, printer); err != nil {
		t.Fatal(err)
	}

	vl, err := storage.All(ctx)
	if err != nil {
		t.Fatal(err)
	}
	if assert.Len(t, vl, 1) {
		assert.Equal(t, vl[0].Spec.Motion.Device, "/dev/abc")
	}

	v, err := storage.FindOneByName(ctx, "test")
	if err != nil {
		t.Fatal(err)
	}
	if assert.NotNil(t, v) {
		assert.Equal(t, v.Spec.Motion.Device, "/dev/abc")
	}

	updatedVolume := &api.VideoStream{
		ObjectMeta: api.ObjectMeta{
			Name: "test",
		},
		Spec: api.VideoStreamSpec{
			Motion: &api.MotionConfig{
				Device: "/dev/abcd",
			},
		},
	}
	err = storage.Update(ctx, updatedVolume)
	if err != nil {
		t.Fatal(err)
	}

	v, err = storage.FindOneByName(ctx, "test")
	if err != nil {
		t.Fatal(err)
	}
	if assert.NotNil(t, v) {
		assert.Equal(t, v.Spec.Motion.Device, "/dev/abcd")
	}

	deleted, err := storage.Delete(ctx, v)
	if err != nil {
		t.Fatal(err)
	}
	assert.NotNil(t, deleted, `entry with name "test" should be found`)

	v, err = storage.FindOneByName(ctx, "test")
	if err != nil {
		t.Fatal(err)
	}
	assert.Nil(t, v)
}
