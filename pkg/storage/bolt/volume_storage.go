package bolt

import (
	"context"
	"errors"

	"github.com/boltdb/bolt"
	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/api/v1"
	db "github.com/thetechnick/print3r/pkg/storage"
)

var errBucketNotFound = errors.New("bucket not found")

// NewVolumeStorage creates a new VolumeStorage
func NewVolumeStorage(db *bolt.DB, hub db.EventHub) (db.VolumeStorage, error) {
	s := &volumeStorage{
		hub: hub,
	}
	s.base = &baseStorage{
		log: log.WithField("module", "bolt.volumeStorage"),

		db:          db,
		bucket:      "print3r:volumes",
		typeName:    "volume",
		toStorage:   toStorageFn(s.toStorage),
		fromStorage: fromStorageFn(s.fromStorage),
		key:         keyFn(s.key),
	}
	return s, s.base.init()
}

type volumeStorage struct {
	base *baseStorage
	hub  db.EventHub
}

func (s *volumeStorage) Delete(ctx context.Context, v *api.Volume) (deleted *api.Volume, err error) {
	var obj api.Object
	if obj, err = s.base.Delete(v); err != nil {
		return
	}
	deleted = obj.(*api.Volume)
	if deleted != nil {
		s.hub.Broadcast(deleted, nil)
	}
	return
}

func (s *volumeStorage) Create(ctx context.Context, v *api.Volume) (err error) {
	err = s.base.Create(v)
	if err != nil {
		return
	}
	s.hub.Broadcast(nil, v)
	return
}

func (s *volumeStorage) Update(ctx context.Context, v *api.Volume) (err error) {
	var o, n api.Object
	o, n, err = s.base.Update(v, func(old, new api.Object) {
		oldObj := old.(*api.Volume)
		newObj := new.(*api.Volume)
		// do not update the status
		newObj.Status = oldObj.Status
	})
	s.hub.Broadcast(o, n)
	return
}

func (s *volumeStorage) UpdateStatus(ctx context.Context, v *api.Volume) (err error) {
	var o, n api.Object
	o, n, err = s.base.Update(v, func(old, new api.Object) {
		oldObj := old.(*api.Volume)
		newObj := new.(*api.Volume)
		// do not update the spec or metadata
		newObj.Spec = oldObj.Spec
		newObj.ObjectMeta = oldObj.ObjectMeta
	})
	s.hub.Broadcast(o, n)
	return
}

func (s *volumeStorage) FindOneByName(ctx context.Context, name string) (volume *api.Volume, err error) {
	var obj api.Object
	obj, err = s.base.FindOneByName(name)
	if err != nil {
		return
	}
	if obj != nil {
		volume = obj.(*api.Volume)
	}
	return
}

func (s *volumeStorage) All(ctx context.Context) (list []*api.Volume, err error) {
	var objs []api.Object
	if objs, err = s.base.All(); err != nil {
		return
	}
	for _, obj := range objs {
		list = append(list, obj.(*api.Volume))
	}
	return
}

func (s *volumeStorage) key(obj api.Object) []byte {
	return []byte(obj.GetName())
}

func (s *volumeStorage) fromStorage(b []byte) (obj api.Object, err error) {
	if b == nil {
		return
	}

	sVolume := &v1.Volume{}
	if err = proto.Unmarshal(b, sVolume); err != nil {
		return
	}

	volume := &api.Volume{}
	err = v1.Scheme.Convert(sVolume, volume)
	obj = volume
	return
}

func (s *volumeStorage) toStorage(obj api.Object) (b []byte, err error) {
	v := obj.(*api.Volume)
	sv := &v1.Volume{}
	if err = v1.Scheme.Convert(v, sv); err != nil {
		return
	}

	if b, err = proto.Marshal(sv); err != nil {
		return
	}
	return
}
