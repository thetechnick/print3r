package bolt

import (
	"context"
	"os"
	"testing"

	"github.com/boltdb/bolt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/storage/test"
)

const volumeTestDBFile = "volume-test.db"

func TestVolumeStorage(t *testing.T) {
	os.Remove(volumeTestDBFile)
	defer os.Remove(volumeTestDBFile)

	ctx := context.Background()
	boltDB, err := bolt.Open(volumeTestDBFile, 0600, nil)
	if err != nil {
		t.Fatal(err)
	}
	var eventHub test.EventHubMock
	eventHub.On("Broadcast", mock.Anything, mock.Anything)

	storage, err := NewVolumeStorage(boltDB, &eventHub)
	if err != nil {
		t.Fatal(err)
	}

	volume := &api.Volume{
		ObjectMeta: api.ObjectMeta{
			Name: "test",
		},
		Spec: api.VolumeSpec{
			Local: &api.LocalVolumeSource{
				Path: "/test/path",
			},
		},
	}
	if err = storage.Create(ctx, volume); err != nil {
		t.Fatal(err)
	}

	vl, err := storage.All(ctx)
	if err != nil {
		t.Fatal(err)
	}
	if assert.Len(t, vl, 1) {
		assert.Equal(t, vl[0].Spec.Local.Path, "/test/path")
	}

	v, err := storage.FindOneByName(ctx, "test")
	if err != nil {
		t.Fatal(err)
	}
	if assert.NotNil(t, v) {
		assert.Equal(t, v.Spec.Local.Path, "/test/path")
	}

	updatedVolume := &api.Volume{
		ObjectMeta: api.ObjectMeta{
			Name: "test",
		},
		Spec: api.VolumeSpec{
			Local: &api.LocalVolumeSource{
				Path: "/test/path/updated",
			},
		},
	}
	err = storage.Update(ctx, updatedVolume)
	if err != nil {
		t.Fatal(err)
	}

	v, err = storage.FindOneByName(ctx, "test")
	if err != nil {
		t.Fatal(err)
	}
	if assert.NotNil(t, v) {
		assert.Equal(t, v.Spec.Local.Path, "/test/path/updated")
	}

	deleted, err := storage.Delete(ctx, v)
	if err != nil {
		t.Fatal(err)
	}
	assert.NotNil(t, deleted, `entry with name "test" should be found`)

	v, err = storage.FindOneByName(ctx, "test")
	if err != nil {
		t.Fatal(err)
	}
	assert.Nil(t, v)
}
