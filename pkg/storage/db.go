package storage

import (
	"context"
	"errors"

	"github.com/thetechnick/print3r/pkg/api"
)

var (
	// ErrIdentifierMissing is returned if the given object is missing its name or uid
	ErrIdentifierMissing = errors.New("missing name or uid")

	// ErrResourceVersionChanged is returned if the resource version of the object has changed
	ErrResourceVersionChanged = errors.New("resource version changed")

	// ErrNameConflict is returned if a item with the name already exists
	ErrNameConflict = errors.New("a resource with this name already exists")

	// ErrNotFound is returned when the requested item was not found
	ErrNotFound = errors.New("not found")
)

// PrinterLimiter limits the amount of returned items
type PrinterLimiter interface {
	Limit(ctx context.Context, items []*api.Printer, limit int) (out []*api.Printer, cursor string)
	Continue(ctx context.Context, cursor string, limit int) (out []*api.Printer, err error)
}

// PrintLimiter limits the amount of returned items
type PrintLimiter interface {
	Limit(ctx context.Context, items []*api.Print, limit int) (out []*api.Print, cursor string)
	Continue(ctx context.Context, cursor string, limit int) (out []*api.Print, err error)
}

// VolumeLimiter limits the amount of returned items
type VolumeLimiter interface {
	Limit(ctx context.Context, items []*api.Volume, limit int) (out []*api.Volume, cursor string)
	Continue(ctx context.Context, cursor string, limit int) (out []*api.Volume, err error)
}

// VolumeFileLimiter limits the amount of returned items
type VolumeFileLimiter interface {
	Limit(ctx context.Context, items []*api.VolumeFile, limit int) (out []*api.VolumeFile, cursor string)
	Continue(ctx context.Context, cursor string, limit int) (out []*api.VolumeFile, err error)
}

// VideoStreamLimiter limits the amount of returned items
type VideoStreamLimiter interface {
	Limit(ctx context.Context, items []*api.VideoStream, limit int) (out []*api.VideoStream, cursor string)
	Continue(ctx context.Context, cursor string, limit int) (out []*api.VideoStream, err error)
}

// VolumeStorage persists volumes
type VolumeStorage interface {
	Create(ctx context.Context, volume *api.Volume) error
	Update(ctx context.Context, v *api.Volume) (err error)
	UpdateStatus(ctx context.Context, v *api.Volume) (err error)
	All(ctx context.Context) (list []*api.Volume, err error)
	Delete(ctx context.Context, volume *api.Volume) (deleted *api.Volume, err error)
	FindOneByName(ctx context.Context, name string) (volume *api.Volume, err error)
}

// VideoStreamUpdateFn is called to update a printer
type VideoStreamUpdateFn func(*api.VideoStream) error

// VideoStreamStorage persists volumes
type VideoStreamStorage interface {
	Create(ctx context.Context, volume *api.VideoStream) error
	Update(ctx context.Context, v *api.VideoStream) (err error)
	UpdateStatus(ctx context.Context, v *api.VideoStream) (err error)

	EnsureUpdate(ctx context.Context, v *api.VideoStream, fn VideoStreamUpdateFn) (old, new *api.VideoStream, err error)

	All(ctx context.Context) (list []*api.VideoStream, err error)
	Delete(ctx context.Context, volume *api.VideoStream) (deleted *api.VideoStream, err error)
	FindOneByName(ctx context.Context, name string) (volume *api.VideoStream, err error)
}

// PrinterUpdateFn is called to update a printer
type PrinterUpdateFn func(*api.Printer) error

// PrinterStorage persists printers
type PrinterStorage interface {
	Create(ctx context.Context, p *api.Printer) error
	Update(ctx context.Context, p *api.Printer) (err error)
	UpdateStatus(ctx context.Context, p *api.Printer) (err error)

	EnsureUpdate(ctx context.Context, p *api.Printer, fn PrinterUpdateFn) (old, new *api.Printer, err error)

	All(ctx context.Context) (list []*api.Printer, err error)
	Delete(ctx context.Context, p *api.Printer) (deleted *api.Printer, err error)
	FindOneByName(ctx context.Context, name string) (f *api.Printer, err error)
}

// PrintStorage persists prints
type PrintStorage interface {
	Create(ctx context.Context, p *api.Print) error
	Update(ctx context.Context, p *api.Print) (err error)
	UpdateStatus(ctx context.Context, p *api.Print) (err error)
	All(ctx context.Context) (list []*api.Print, err error)
	Delete(ctx context.Context, p *api.Print) (deleted *api.Print, err error)
	FindOneByName(ctx context.Context, name string) (p *api.Print, err error)
}
