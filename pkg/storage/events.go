package storage

import (
	"time"

	"github.com/google/uuid"
	"github.com/thetechnick/print3r/pkg/api"
)

// EventHub dispatches resource events to registered clients
type EventHub interface {
	Broadcast(old, new api.Object)
	Register() EventClient
	Unregister(c EventClient)
	Close() error
	Run() error
}

// EventClient represents a client of the event hub
type EventClient interface {
	events() chan<- *api.ResourceEvent
	Events() <-chan *api.ResourceEvent
	Close()
}

type hub struct {
	clients    map[EventClient]bool
	broadcast  chan *api.ResourceEvent
	register   chan EventClient
	unregister chan EventClient
	cancelCh   chan struct{}
	doneCh     chan error
}

// NewEventHub creates a new EventHub
func NewEventHub() EventHub {
	return &hub{
		broadcast:  make(chan *api.ResourceEvent),
		register:   make(chan EventClient),
		unregister: make(chan EventClient),
		clients:    make(map[EventClient]bool),
		cancelCh:   make(chan struct{}),
		doneCh:     make(chan error),
	}
}

func (h *hub) Broadcast(old, new api.Object) {
	var op api.ResourceEventOperation
	if new == nil {
		op = api.ResourceEventOperationDeleted
	} else if old == nil {
		op = api.ResourceEventOperationCreated
	} else {
		op = api.ResourceEventOperationUpdated
	}
	event := &api.ResourceEvent{
		ID:          uuid.New().String(),
		Operation:   op,
		EmittedTime: time.Now(),
		New:         new,
		Old:         old,
	}
	h.broadcast <- event
}

func (h *hub) Register() EventClient {
	c := newEventClient()
	h.register <- c
	return c
}

func (h *hub) Unregister(c EventClient) {
	h.unregister <- c
}

func (h *hub) Close() error {
	close(h.cancelCh)
	return <-h.doneCh
}

func (h *hub) Run() (err error) {
	defer func() { close(h.doneCh) }()
	for {
		select {
		case <-h.cancelCh:
			for c := range h.clients {
				c.Close()
				delete(h.clients, c)
			}
			return
		case c := <-h.register:
			h.clients[c] = true
		case c := <-h.unregister:
			if _, ok := h.clients[c]; ok {
				c.Close()
				delete(h.clients, c)
			}
		case message := <-h.broadcast:
			for c := range h.clients {
				select {
				case c.events() <- message:
				default:
					c.Close()
					delete(h.clients, c)
				}
			}
		}
	}
}

type client struct {
	send chan *api.ResourceEvent
}

func newEventClient() EventClient {
	return &client{
		send: make(chan *api.ResourceEvent, 100),
	}
}

func (c *client) Close() {
	close(c.send)
}

func (c *client) events() chan<- *api.ResourceEvent {
	return c.send
}

func (c *client) Events() <-chan *api.ResourceEvent {
	return c.send
}
