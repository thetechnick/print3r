package inmem

import (
	"context"
	"sync"
	"time"

	"github.com/google/uuid"
	db "github.com/thetechnick/print3r/pkg/storage"
)

// newLimiter creates a new inmemory limiter
func newLimiter() *limiter {
	return &limiter{
		data:   map[string][]interface{}{},
		timers: map[string]*time.Timer{},
		t:      time.Minute * 2,
	}
}

type limiter struct {
	data   map[string][]interface{}
	timers map[string]*time.Timer
	m      sync.Mutex
	t      time.Duration
}

func (l *limiter) Limit(ctx context.Context, items []interface{}, limit int) (out []interface{}, c string) {
	if limit == 0 {
		return items, ""
	}

	l.m.Lock()
	defer l.m.Unlock()
	c = uuid.New().String()
	out, l.data[c] = limitItems(items, limit)

	l.timers[c] = time.AfterFunc(l.t, func() {
		l.m.Lock()
		defer l.m.Unlock()
		if t, ok := l.timers[c]; ok {
			t.Stop()
			delete(l.timers, c)
		}
		delete(l.data, c)
	})
	return
}

func (l *limiter) Continue(ctx context.Context, c string, limit int) (out []interface{}, err error) {
	l.m.Lock()
	defer l.m.Unlock()
	items, ok := l.data[c]
	if !ok {
		err = db.ErrNotFound
		return
	}
	out, l.data[c] = limitItems(items, limit)
	if len(l.data[c]) == 0 {
		if t, ok := l.timers[c]; ok {
			t.Stop()
			delete(l.timers, c)
		}
		delete(l.data, c)
	}
	return
}

func limitItems(items []interface{}, limit int) (limited []interface{}, rest []interface{}) {
	i := 0
	for _, item := range items {
		if limit == 0 || i < limit {
			limited = append(limited, item)
		} else {
			rest = append(rest, item)
		}
		i++
	}
	return
}
