package inmem

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/thetechnick/print3r/pkg/api"
)

func TestLimiter(t *testing.T) {
	items := []interface{}{
		&api.Print{},
		&api.Print{},
		&api.Print{},
		&api.Print{},
		&api.Print{},
	}
	ctx := context.Background()

	l := newLimiter()
	t.Run("Continue error on invalid token", func(t *testing.T) {
		_, err := l.Continue(ctx, "hans", 10)
		assert.Error(t, err)
	})

	var ctoken string
	t.Run("Limit", func(t *testing.T) {
		var limited []interface{}
		limited, ctoken = l.Limit(ctx, items, 2)
		assert.NotEmpty(t, ctoken)
		assert.Len(t, limited, 2)
	})

	t.Run("Continue get next page", func(t *testing.T) {
		limited, err := l.Continue(ctx, ctoken, 2)
		assert.NoError(t, err)
		assert.Len(t, limited, 2)
	})

	t.Run("Continue request all", func(t *testing.T) {
		limited, err := l.Continue(ctx, ctoken, 10000)
		assert.NoError(t, err)
		assert.Len(t, limited, 1)
	})

	t.Run("Continue error on finished token", func(t *testing.T) {
		_, err := l.Continue(ctx, ctoken, 10)
		assert.Error(t, err)
	})
}
