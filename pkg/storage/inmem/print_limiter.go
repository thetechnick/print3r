package inmem

import (
	"context"

	"github.com/thetechnick/print3r/pkg/api"
	db "github.com/thetechnick/print3r/pkg/storage"
)

// PrintLimiter implements db.PrintLimiter interface
type PrintLimiter struct {
	l *limiter
}

// NewPrintLimiter creates a new limiter with an in memory cursor cache
func NewPrintLimiter() db.PrintLimiter {
	return &PrintLimiter{
		l: newLimiter(),
	}
}

func (l *PrintLimiter) Limit(ctx context.Context, items []*api.Print, limit int) (out []*api.Print, cursor string) {
	objects := make([]interface{}, len(items))
	for i, item := range items {
		objects[i] = interface{}(item)
	}

	var outObjects []interface{}
	outObjects, cursor = l.l.Limit(ctx, objects, limit)
	for _, o := range outObjects {
		out = append(out, o.(*api.Print))
	}
	return
}

func (l *PrintLimiter) Continue(ctx context.Context, cursor string, limit int) (out []*api.Print, err error) {
	var outObjects []interface{}
	outObjects, err = l.l.Continue(ctx, cursor, limit)
	for _, o := range outObjects {
		out = append(out, o.(*api.Print))
	}
	return
}
