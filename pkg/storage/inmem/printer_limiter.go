package inmem

import (
	"context"

	"github.com/thetechnick/print3r/pkg/api"
	db "github.com/thetechnick/print3r/pkg/storage"
)

// PrinterLimiter implements db.PrinterLimiter interface
type PrinterLimiter struct {
	l *limiter
}

// NewPrinterLimiter creates a new limiter with an in memory cursor cache
func NewPrinterLimiter() db.PrinterLimiter {
	return &PrinterLimiter{
		l: newLimiter(),
	}
}

func (l *PrinterLimiter) Limit(ctx context.Context, items []*api.Printer, limit int) (out []*api.Printer, cursor string) {
	objects := make([]interface{}, len(items))
	for i, item := range items {
		objects[i] = interface{}(item)
	}

	var outObjects []interface{}
	outObjects, cursor = l.l.Limit(ctx, objects, limit)
	for _, o := range outObjects {
		out = append(out, o.(*api.Printer))
	}
	return
}

func (l *PrinterLimiter) Continue(ctx context.Context, cursor string, limit int) (out []*api.Printer, err error) {
	var outObjects []interface{}
	outObjects, err = l.l.Continue(ctx, cursor, limit)
	for _, o := range outObjects {
		out = append(out, o.(*api.Printer))
	}
	return
}
