package inmem

import (
	"context"

	"github.com/thetechnick/print3r/pkg/api"
	db "github.com/thetechnick/print3r/pkg/storage"
)

// VideoStreamLimiter implements db.VideoStreamLimiter interface
type VideoStreamLimiter struct {
	l *limiter
}

// NewVideoStreamLimiter creates a new limiter with an in memory cursor cache
func NewVideoStreamLimiter() db.VideoStreamLimiter {
	return &VideoStreamLimiter{
		l: newLimiter(),
	}
}

func (l *VideoStreamLimiter) Limit(ctx context.Context, items []*api.VideoStream, limit int) (out []*api.VideoStream, cursor string) {
	objects := make([]interface{}, len(items))
	for i, item := range items {
		objects[i] = interface{}(item)
	}

	var outObjects []interface{}
	outObjects, cursor = l.l.Limit(ctx, objects, limit)
	for _, o := range outObjects {
		out = append(out, o.(*api.VideoStream))
	}
	return
}

func (l *VideoStreamLimiter) Continue(ctx context.Context, cursor string, limit int) (out []*api.VideoStream, err error) {
	var outObjects []interface{}
	outObjects, err = l.l.Continue(ctx, cursor, limit)
	for _, o := range outObjects {
		out = append(out, o.(*api.VideoStream))
	}
	return
}
