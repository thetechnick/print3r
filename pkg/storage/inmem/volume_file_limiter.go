package inmem

import (
	"context"

	"github.com/thetechnick/print3r/pkg/api"
	db "github.com/thetechnick/print3r/pkg/storage"
)

// VolumeFileLimiter implements db.VolumeFileLimiter interface
type VolumeFileLimiter struct {
	l *limiter
}

// NewVolumeFileLimiter creates a new limiter with an in memory cursor cache
func NewVolumeFileLimiter() db.VolumeFileLimiter {
	return &VolumeFileLimiter{
		l: newLimiter(),
	}
}

func (l *VolumeFileLimiter) Limit(ctx context.Context, items []*api.VolumeFile, limit int) (out []*api.VolumeFile, cursor string) {
	objects := make([]interface{}, len(items))
	for i, item := range items {
		objects[i] = interface{}(item)
	}

	var outObjects []interface{}
	outObjects, cursor = l.l.Limit(ctx, objects, limit)
	for _, o := range outObjects {
		out = append(out, o.(*api.VolumeFile))
	}
	return
}

func (l *VolumeFileLimiter) Continue(ctx context.Context, cursor string, limit int) (out []*api.VolumeFile, err error) {
	var outObjects []interface{}
	outObjects, err = l.l.Continue(ctx, cursor, limit)
	for _, o := range outObjects {
		out = append(out, o.(*api.VolumeFile))
	}
	return
}
