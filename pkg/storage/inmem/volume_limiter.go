package inmem

import (
	"context"

	"github.com/thetechnick/print3r/pkg/api"
	db "github.com/thetechnick/print3r/pkg/storage"
)

// VolumeLimiter implements db.VolumeLimiter interface
type VolumeLimiter struct {
	l *limiter
}

// NewVolumeLimiter creates a new limiter with an in memory cursor cache
func NewVolumeLimiter() db.VolumeLimiter {
	return &VolumeLimiter{
		l: newLimiter(),
	}
}

func (l *VolumeLimiter) Limit(ctx context.Context, items []*api.Volume, limit int) (out []*api.Volume, cursor string) {
	objects := make([]interface{}, len(items))
	for i, item := range items {
		objects[i] = interface{}(item)
	}

	var outObjects []interface{}
	outObjects, cursor = l.l.Limit(ctx, objects, limit)
	for _, o := range outObjects {
		out = append(out, o.(*api.Volume))
	}
	return
}

func (l *VolumeLimiter) Continue(ctx context.Context, cursor string, limit int) (out []*api.Volume, err error) {
	var outObjects []interface{}
	outObjects, err = l.l.Continue(ctx, cursor, limit)
	for _, o := range outObjects {
		out = append(out, o.(*api.Volume))
	}
	return
}
