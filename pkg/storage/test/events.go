package test

import (
	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/storage"
)

// EventHubMock mock of storage.EventHub
type EventHubMock struct {
	mock.Mock
}

// Broadcast bock of storage.EventHub.Broadcast
func (m *EventHubMock) Broadcast(old, new api.Object) {
	m.Called(old, new)
}

// Register bock of storage.EventHub.Register
func (m *EventHubMock) Register() storage.EventClient {
	args := m.Called()
	return args.Get(0).(storage.EventClient)
}

// Unregister bock of storage.EventHub.Unregister
func (m *EventHubMock) Unregister(c storage.EventClient) {
	m.Called(c)
}

// Close bock of storage.EventHub.Close
func (m *EventHubMock) Close() error {
	args := m.Called()
	return args.Error(0)
}

// Run bock of storage.EventHub.Run
func (m *EventHubMock) Run() error {
	args := m.Called()
	return args.Error(0)
}

// EventClientMock mock of storage.EventClient
type EventClientMock struct {
	mock.Mock
}

// Events bock of storage.EventClient.Events
func (m *EventClientMock) Events() <-chan *api.ResourceEvent {
	args := m.Called()
	return args.Get(0).(<-chan *api.ResourceEvent)
}

// Close bock of storage.EventClient.Close
func (m *EventClientMock) Close() {
	m.Called()
	return
}
