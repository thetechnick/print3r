package test

import (
	"context"

	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
)

// PrintLimiterMock mock of storage.PrintLimiter
type PrintLimiterMock struct {
	mock.Mock
}

// Limit mock of storage.PrintLimiter.Limit
func (m *PrintLimiterMock) Limit(ctx context.Context, items []*api.Print, limit int) (out []*api.Print, cursor string) {
	args := m.Called(ctx, items, limit)
	return args.Get(0).([]*api.Print), args.String(1)
}

// Continue mock of storage.PrintLimiter.Continue
func (m *PrintLimiterMock) Continue(ctx context.Context, cursor string, limit int) (out []*api.Print, err error) {
	args := m.Called(ctx, cursor, limit)
	return args.Get(0).([]*api.Print), args.Error(1)
}
