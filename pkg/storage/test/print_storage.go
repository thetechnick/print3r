package test

import (
	"context"

	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
)

// PrintStorageMock is a mock of db.PrintStorage
type PrintStorageMock struct {
	mock.Mock
}

// Create mock of db.PrintStorage.Create
func (m *PrintStorageMock) Create(ctx context.Context, v *api.Print) error {
	args := m.Called(ctx, v)
	return args.Error(0)
}

// Update mock of db.PrintStorage.Update
func (m *PrintStorageMock) Update(ctx context.Context, v *api.Print) error {
	args := m.Called(ctx, v)
	return args.Error(0)
}

// UpdateStatus mock of db.PrintStorage.UpdateStatus
func (m *PrintStorageMock) UpdateStatus(ctx context.Context, v *api.Print) error {
	args := m.Called(ctx, v)
	return args.Error(0)
}

// All mock of db.PrintStorage.All
func (m *PrintStorageMock) All(ctx context.Context) ([]*api.Print, error) {
	args := m.Called(ctx)
	return args.Get(0).([]*api.Print), args.Error(1)
}

// Delete mock of db.PrintStorage.Delete
func (m *PrintStorageMock) Delete(ctx context.Context, p *api.Print) (*api.Print, error) {
	args := m.Called(ctx, p)
	return args.Get(0).(*api.Print), args.Error(1)
}

// FindOneByName mock of db.PrintStorage.FindOneByName
func (m *PrintStorageMock) FindOneByName(ctx context.Context, name string) (*api.Print, error) {
	args := m.Called(ctx, name)
	return args.Get(0).(*api.Print), args.Error(1)
}
