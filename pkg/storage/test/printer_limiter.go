package test

import (
	"context"

	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
)

// PrinterLimiterMock mock of storage.PrinterLimiter
type PrinterLimiterMock struct {
	mock.Mock
}

// Limit mock of storage.PrinterLimiter.Limit
func (m *PrinterLimiterMock) Limit(ctx context.Context, items []*api.Printer, limit int) (out []*api.Printer, cursor string) {
	args := m.Called(ctx, items, limit)
	return args.Get(0).([]*api.Printer), args.String(1)
}

// Continue mock of storage.PrinterLimiter.Continue
func (m *PrinterLimiterMock) Continue(ctx context.Context, cursor string, limit int) (out []*api.Printer, err error) {
	args := m.Called(ctx, cursor, limit)
	return args.Get(0).([]*api.Printer), args.Error(1)
}
