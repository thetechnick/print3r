package test

import (
	"context"

	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/storage"
)

// PrinterStorageMock is a mock of storage.PrinterStorage
type PrinterStorageMock struct {
	mock.Mock
}

// Create mock of storage.PrinterStorage.Create
func (m *PrinterStorageMock) Create(ctx context.Context, v *api.Printer) error {
	args := m.Called(ctx, v)
	return args.Error(0)
}

// Update mock of storage.PrinterStorage.Update
func (m *PrinterStorageMock) Update(ctx context.Context, v *api.Printer) error {
	args := m.Called(ctx, v)
	return args.Error(0)
}

// EnsureUpdate mock of storage.PrinterStorage.EnsureUpdate
func (m *PrinterStorageMock) EnsureUpdate(ctx context.Context, v *api.Printer, fn storage.PrinterUpdateFn) (old, new *api.Printer, err error) {
	args := m.Called(ctx, v, fn)
	return args.Get(0).(*api.Printer), args.Get(1).(*api.Printer), args.Error(2)
}

// UpdateStatus mock of storage.PrinterStorage.UpdateStatus
func (m *PrinterStorageMock) UpdateStatus(ctx context.Context, v *api.Printer) error {
	args := m.Called(ctx, v)
	return args.Error(0)
}

// All mock of storage.PrinterStorage.All
func (m *PrinterStorageMock) All(ctx context.Context) ([]*api.Printer, error) {
	args := m.Called(ctx)
	return args.Get(0).([]*api.Printer), args.Error(1)
}

// Delete mock of storage.PrinterStorage.Delete
func (m *PrinterStorageMock) Delete(ctx context.Context, p *api.Printer) (*api.Printer, error) {
	args := m.Called(ctx, p)
	return args.Get(0).(*api.Printer), args.Error(1)
}

// FindOneByName mock of storage.PrinterStorage.FindOneByName
func (m *PrinterStorageMock) FindOneByName(ctx context.Context, name string) (*api.Printer, error) {
	args := m.Called(ctx, name)
	return args.Get(0).(*api.Printer), args.Error(1)
}
