package test

import (
	"context"

	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
)

// VideoStreamLimiterMock mock of storage.VideoStreamLimiter
type VideoStreamLimiterMock struct {
	mock.Mock
}

// Limit mock of storage.VideoStreamLimiter.Limit
func (m *VideoStreamLimiterMock) Limit(ctx context.Context, items []*api.VideoStream, limit int) (out []*api.VideoStream, cursor string) {
	args := m.Called(ctx, items, limit)
	return args.Get(0).([]*api.VideoStream), args.String(1)
}

// Continue mock of storage.VideoStreamLimiter.Continue
func (m *VideoStreamLimiterMock) Continue(ctx context.Context, cursor string, limit int) (out []*api.VideoStream, err error) {
	args := m.Called(ctx, cursor, limit)
	return args.Get(0).([]*api.VideoStream), args.Error(1)
}
