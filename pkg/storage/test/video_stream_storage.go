package test

import (
	"context"

	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/storage"
)

// VideoStreamStorageMock is a mock of db.VideoStreamStorage
type VideoStreamStorageMock struct {
	mock.Mock
}

// Create mock of db.VideoStreamStorage.Create
func (m *VideoStreamStorageMock) Create(ctx context.Context, v *api.VideoStream) error {
	args := m.Called(ctx, v)
	return args.Error(0)
}

// Update mock of db.VideoStreamStorage.Update
func (m *VideoStreamStorageMock) Update(ctx context.Context, v *api.VideoStream) error {
	args := m.Called(ctx, v)
	return args.Error(0)
}

// UpdateStatus mock of db.VideoStreamStorage.UpdateStatus
func (m *VideoStreamStorageMock) UpdateStatus(ctx context.Context, v *api.VideoStream) error {
	args := m.Called(ctx, v)
	return args.Error(0)
}

// EnsureUpdate mock of storage.VideoStreamStorage.EnsureUpdate
func (m *VideoStreamStorageMock) EnsureUpdate(ctx context.Context, v *api.VideoStream, fn storage.VideoStreamUpdateFn) (old, new *api.VideoStream, err error) {
	args := m.Called(ctx, v, fn)
	return args.Get(0).(*api.VideoStream), args.Get(1).(*api.VideoStream), args.Error(2)
}

// All mock of db.VideoStreamStorage.All
func (m *VideoStreamStorageMock) All(ctx context.Context) ([]*api.VideoStream, error) {
	args := m.Called(ctx)
	return args.Get(0).([]*api.VideoStream), args.Error(1)
}

// Delete mock of db.VideoStreamStorage.Delete
func (m *VideoStreamStorageMock) Delete(ctx context.Context, p *api.VideoStream) (*api.VideoStream, error) {
	args := m.Called(ctx, p)
	return args.Get(0).(*api.VideoStream), args.Error(1)
}

// FindOneByName mock of db.VideoStreamStorage.FindOneByName
func (m *VideoStreamStorageMock) FindOneByName(ctx context.Context, name string) (*api.VideoStream, error) {
	args := m.Called(ctx, name)
	return args.Get(0).(*api.VideoStream), args.Error(1)
}
