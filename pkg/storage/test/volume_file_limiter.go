package test

import (
	"context"

	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
)

// VolumeFileLimiterMock mock of storage.VolumeFileLimiter
type VolumeFileLimiterMock struct {
	mock.Mock
}

// Limit mock of storage.VolumeFileLimiter.Limit
func (m *VolumeFileLimiterMock) Limit(ctx context.Context, items []*api.VolumeFile, limit int) (out []*api.VolumeFile, cursor string) {
	args := m.Called(ctx, items, limit)
	return args.Get(0).([]*api.VolumeFile), args.String(1)
}

// Continue mock of storage.VolumeFileLimiter.Continue
func (m *VolumeFileLimiterMock) Continue(ctx context.Context, cursor string, limit int) (out []*api.VolumeFile, err error) {
	args := m.Called(ctx, cursor, limit)
	return args.Get(0).([]*api.VolumeFile), args.Error(1)
}
