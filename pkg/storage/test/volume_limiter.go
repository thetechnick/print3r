package test

import (
	"context"

	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
)

// VolumeLimiterMock mock of storage.VolumeLimiter
type VolumeLimiterMock struct {
	mock.Mock
}

// Limit mock of storage.VolumeLimiter.Limit
func (m *VolumeLimiterMock) Limit(ctx context.Context, items []*api.Volume, limit int) (out []*api.Volume, cursor string) {
	args := m.Called(ctx, items, limit)
	return args.Get(0).([]*api.Volume), args.String(1)
}

// Continue mock of storage.VolumeLimiter.Continue
func (m *VolumeLimiterMock) Continue(ctx context.Context, cursor string, limit int) (out []*api.Volume, err error) {
	args := m.Called(ctx, cursor, limit)
	return args.Get(0).([]*api.Volume), args.Error(1)
}
