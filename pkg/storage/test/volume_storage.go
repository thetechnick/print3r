package test

import (
	"context"

	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
)

// VolumeStorageMock is a mock of db.VolumeStorage
type VolumeStorageMock struct {
	mock.Mock
}

// Create mock of db.VolumeStorage.Create
func (m *VolumeStorageMock) Create(ctx context.Context, v *api.Volume) error {
	args := m.Called(ctx, v)
	return args.Error(0)
}

// Update mock of db.VolumeStorage.Update
func (m *VolumeStorageMock) Update(ctx context.Context, v *api.Volume) error {
	args := m.Called(ctx, v)
	return args.Error(0)
}

// UpdateStatus mock of db.VolumeStorage.UpdateStatus
func (m *VolumeStorageMock) UpdateStatus(ctx context.Context, v *api.Volume) error {
	args := m.Called(ctx, v)
	return args.Error(0)
}

// All mock of db.VolumeStorage.All
func (m *VolumeStorageMock) All(ctx context.Context) ([]*api.Volume, error) {
	args := m.Called(ctx)
	return args.Get(0).([]*api.Volume), args.Error(1)
}

// Delete mock of db.VolumeStorage.Delete
func (m *VolumeStorageMock) Delete(ctx context.Context, vol *api.Volume) (*api.Volume, error) {
	args := m.Called(ctx, vol)
	return args.Get(0).(*api.Volume), args.Error(1)
}

// FindOneByName mock of db.VolumeStorage.FindOneByName
func (m *VolumeStorageMock) FindOneByName(ctx context.Context, name string) (*api.Volume, error) {
	args := m.Called(ctx, name)
	return args.Get(0).(*api.Volume), args.Error(1)
}
