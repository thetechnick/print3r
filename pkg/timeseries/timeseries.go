package timeseries

import (
	"time"
)

type aggregateFn func(current *float64, new float64) *float64

var (
	// Override aggregates values by just using the newer value
	Override = aggregateFn(func(current *float64, new float64) *float64 {
		return &new
	})
	// Sum aggregates values by adding them
	Sum = aggregateFn(func(current *float64, new float64) *float64 {
		if current == nil {
			return &new
		}
		v := *current + new
		return &v
	})
	// Avg calculates an average over aggregated values
	Avg = aggregateFn(func(current *float64, new float64) *float64 {
		if current == nil {
			return &new
		}
		v := (*current + new) / 2
		return &v
	})
)

// Series is a ordered list of values with a fixed size
// which will override itself when a values are out of range
type Series struct {
	calc   aggregateFn
	values []*float64
	c      int
}

// NewSeries creates a new series of values
func NewSeries(size int, fn aggregateFn) *Series {
	return &Series{values: make([]*float64, size), calc: fn}
}

// Size returns the size of the series
func (s *Series) Size() int {
	return len(s.values)
}

// Range returns all values in the series starting at offset from the cursor
func (s *Series) Range(offset, size int) []*float64 {
	out := make([]*float64, size)

	if offset > 0 {
		return out
	}
	if offset == 0 {
		out[0] = s.values[s.c]
		return out
	}

	i := 0
	for o := offset; o < 0 && i < size; o++ {
		if o < -len(s.values) {
			out[i] = nil
			i++
			continue
		}
		si := s.index(s.c + o + 1)
		out[i] = s.values[si]
		i++
	}
	return out
}

func (s *Series) index(i int) int {
	if i >= len(s.values) {
		return i - len(s.values)
	}
	if i < 0 {
		return i + len(s.values)
	}
	return i
}

// Get returns all values
func (s *Series) Get() []*float64 {
	i := s.index(s.c + 1)
	return append(s.values[i:], s.values[:i]...)
}

// Set sets a value at the given offset
func (s *Series) Set(offset int, v float64) (ok bool) {
	if offset > len(s.values) {
		// shortcut to reset everything and start new
		for i := range s.values {
			s.values[i] = nil
		}
		s.c = 0
		s.values[0] = &v
		ok = true
		return
	}

	if offset > 0 {
		end := offset + s.c
		for i := s.c + 1; i <= end; i++ {
			index := i
			if index+1 > len(s.values) {
				index = index - len(s.values)
			}
			s.values[index] = nil
			s.c = index
		}
	}

	if offset >= 0 {
		ok = true
		s.values[s.c] = s.calc(s.values[s.c], v)
	} else if offset*-1 < len(s.values) {
		ok = true
		s.values[s.c+offset] = s.calc(s.values[s.c], v)
	}
	return
}

// Point represents a value at a point in time
type Point struct {
	value float64
	time  time.Time
}

// Value returns the value of the point
func (p *Point) Value() float64 {
	return p.value
}

// Time returns the time for the point
func (p *Point) Time() time.Time {
	return p.time
}

// TimeSeries represents a series ordered by time
type TimeSeries struct {
	resolution time.Duration
	end        time.Time
	s          *Series
}

// NewTimeSeries creates a new TimeSeries instance
func NewTimeSeries(resolution time.Duration, s *Series) *TimeSeries {
	return &TimeSeries{
		resolution: resolution,
		s:          s,
	}
}

// Points returns a slice of all points in this timeseries
func (l *TimeSeries) Points() (r []*Point) {
	t := l.end.Add(l.resolution * -time.Duration(l.s.Size()))
	for _, v := range l.s.Get() {
		if v != nil {
			r = append(r, &Point{value: *v, time: t})
		}
		t = t.Add(l.resolution)
	}
	return
}

// Range returns a new TimeSeries adjusted to the given timeframe
func (l *TimeSeries) Range(start, end time.Time, size int, aggregateFn aggregateFn) *TimeSeries {
	resolution := end.Sub(start) / time.Duration(size)
	nl := NewTimeSeries(resolution, NewSeries(size, aggregateFn))
	for _, p := range l.Points() {
		nl.AddWithTime(p.value, p.time)
	}
	return nl
}

// AddWithTime adds a new point to the timeseries at the given time
func (l *TimeSeries) AddWithTime(v float64, t time.Time) {
	var offset int
	if l.end.IsZero() {
		l.end = t
	} else {
		offset = int(t.Sub(l.end) / l.resolution)
	}

	if l.s.Set(offset, v) {
		if offset > 0 {
			l.end = l.end.Add(l.resolution * time.Duration(offset))
		}
	}
}

// Add adds a new point now
func (l *TimeSeries) Add(v float64) {
	l.AddWithTime(v, time.Now())
}
