package timeseries

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// func TestT(t *testing.T) {
// 	end := time.Now().Add(10 * time.Second)
// 	ti := end
// 	end = end.Add(4 * time.Second)
// 	resolution := 2 * time.Second
// 	offset := int(ti.Sub(end) / resolution)
// 	assert.Equal(t, 0, offset)
// }

func p(vals []*float64) {
	for _, v := range vals {
		if v == nil {
			fmt.Print("-")
		} else {
			fmt.Print(*v)
		}
	}
	fmt.Print("\n")
}

func TestTimeSeries(t *testing.T) {
	t.Run("Points", func(t *testing.T) {
		l := NewTimeSeries(1*time.Second, NewSeries(10, Sum))
		assert.Len(t, l.Points(), 0) // no data points in series

		start := time.Now()
		l.AddWithTime(1, start)
		l.AddWithTime(10, start.Add(1*time.Second))
		l.AddWithTime(100, start.Add(2*time.Second))

		p := l.Points()
		assert.Len(t, p, 3)
		assert.Equal(t, 1.0, p[0].value)
		assert.Equal(t, 10.0, p[1].value)
		assert.Equal(t, 100.0, p[2].value)
	})

	t.Run("Range", func(t *testing.T) {
		l := NewTimeSeries(1*time.Second, NewSeries(10, Sum))
		assert.Len(t, l.Points(), 0) // no data points in series

		start := time.Now()
		l.AddWithTime(1, start)
		l.AddWithTime(10, start.Add(1*time.Second))
		l.AddWithTime(100, start.Add(2*time.Second))
		l.AddWithTime(1000, start.Add(3*time.Second))
		assert.Equal(t, l.end, start.Add(3*time.Second))

		points := l.Points()
		assert.Len(t, points, 4)

		nl := l.Range(start, start.Add(4*time.Second), 2, Sum)
		np := nl.Points()
		assert.Len(t, np, 2)
		assert.Equal(t, 11.0, np[0].value)
		assert.Equal(t, 1100.0, np[1].value)
	})
}

func TestSeries(t *testing.T) {
	t.Run("Range", func(t *testing.T) {
		s := NewSeries(10, Override)

		s.Set(0, 1)
		s.Set(1, 2)
		s.Set(1, 3)
		s.Set(1, 4)
		s.Set(1, 5)
		s.Set(1, 6)
		s.Set(1, 7)
		s.Set(1, 8)
		s.Set(1, 9)
		s.Set(1, 10)

		r := s.Range(-5, 3)
		assert.Len(t, r, 3)
		assert.Equal(t, 6.0, *r[0])
		assert.Equal(t, 7.0, *r[1])
		assert.Equal(t, 8.0, *r[2])

		r = s.Range(0, 3)
		assert.Len(t, r, 3)
		assert.Equal(t, 10.0, *r[0])
		assert.Nil(t, r[1])
		assert.Nil(t, r[2])

	})

	t.Run("Get", func(t *testing.T) {
		s := NewSeries(10, Override)
		assert.Len(t, s.Get(), 10)
	})

	t.Run("Set", func(t *testing.T) {
		s := NewSeries(10, Override)
		assert.True(t, s.Set(0, 10))
		assert.Equal(t, 0, s.c)
		assert.Equal(t, 10.0, *s.values[0])

		assert.True(t, s.Set(5, 15))
		assert.Equal(t, 5, s.c)
		assert.Equal(t, 10.0, *s.values[0])
		assert.Equal(t, 15.0, *s.values[5])

		assert.True(t, s.Set(-5, 11))
		assert.Equal(t, 5, s.c)
		assert.Equal(t, 11.0, *s.values[0])

		assert.True(t, s.Set(5, 25))
		assert.Equal(t, 0, s.c)
		assert.Equal(t, 25.0, *s.values[0])
		assert.Equal(t, 15.0, *s.values[5])

		assert.False(t, s.Set(-11, 20))

		s.Set(20, 10)
		assert.Equal(t, 0, s.c)
		assert.Equal(t, 10.0, *s.values[0])

		t.Run("sum", func(t *testing.T) {
			s := NewSeries(10, Sum)
			s.Set(0, 10)
			assert.Equal(t, 0, s.c)
			assert.Equal(t, 10.0, *s.values[0])

			s.Set(0, 10)
			assert.Equal(t, 0, s.c)
			assert.Equal(t, 20.0, *s.values[0])
		})

		t.Run("avg", func(t *testing.T) {
			s := NewSeries(10, Avg)
			s.Set(0, 10)
			assert.Equal(t, 0, s.c)
			assert.Equal(t, 10.0, *s.values[0])

			s.Set(0, 20)
			assert.Equal(t, 0, s.c)
			assert.Equal(t, 15.0, *s.values[0])
		})
	})
}

func BenchmarkSeries(b *testing.B) {
	s := NewSeries(300, Override)
	b.Run("Set", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			s.Set(1, 10)
		}
	})
}
