package util

import (
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"
	"github.com/golang/protobuf/ptypes/duration"
	"github.com/golang/protobuf/ptypes/timestamp"
)

const TypeURL = "type.print3r.xyz/"

// MarshalAny takes the protocol buffer and encodes it into google.protobuf.Any.
func MarshalAny(pb proto.Message) (*any.Any, error) {
	value, err := proto.Marshal(pb)
	if err != nil {
		return nil, err
	}
	return &any.Any{TypeUrl: TypeURL + proto.MessageName(pb), Value: value}, nil
}

// UnmarshalAny parses the protocol buffer representation in a google.protobuf.Any
func UnmarshalAny(any *any.Any, pb proto.Message) error {
	return ptypes.UnmarshalAny(any, pb)
}

// AnyMessageName returns the name of the message contained in a google.protobuf.Any message.
func AnyMessageName(any *any.Any) (string, error) {
	return ptypes.AnyMessageName(any)
}

// TimestampProto converts a time.Time into google.protobuf.Timestamp
func TimestampProto(t time.Time) (*timestamp.Timestamp, error) {
	if t.IsZero() {
		return nil, nil
	}
	return ptypes.TimestampProto(t)
}

// Timestamp converts google.protobuf.Timestamp into time.Time
func Timestamp(ts *timestamp.Timestamp) (time.Time, error) {
	if ts == nil {
		return time.Time{}, nil
	}
	return ptypes.Timestamp(ts)
}

// DurationProto converts a time.Duration into google.protobuf.Duration
func DurationProto(d time.Duration) (*duration.Duration, error) {
	if d == 0 {
		return nil, nil
	}
	return ptypes.DurationProto(d), nil
}

// Duration converts google.protobuf.Duration into time.Duration
func Duration(d *duration.Duration) (time.Duration, error) {
	if d == nil {
		return 0, nil
	}
	return ptypes.Duration(d)
}
