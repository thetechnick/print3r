package util

import (
	"testing"
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/stretchr/testify/assert"
)

func TestMarshalAny(t *testing.T) {
	msg, err := TimestampProto(time.Now())
	if err != nil {
		t.Fatal(err)
	}

	any, err := MarshalAny(msg)
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, "google.protobuf.Timestamp", proto.MessageName(msg))

	assert.Equal(t, "type.print3r.xyz/google.protobuf.Timestamp", any.TypeUrl)
}
