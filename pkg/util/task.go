package util

import (
	"errors"
	"sync"
)

var (
	// ErrTaskCanceled is returned when the task was canceled
	ErrTaskCanceled = errors.New("task canceled")
	// ErrTaskAlreadyPaused is returned when trying to pause a paused task
	ErrTaskAlreadyPaused = errors.New("task already paused")
	// ErrTaskAlreadyCanceled is returned when trying to cancel a canceled task
	ErrTaskAlreadyCanceled = errors.New("task already canceled")
	// ErrTaskAlreadyStarted is returned trying to start a task that is already running
	ErrTaskAlreadyStarted = errors.New("task already started")
	// ErrTaskNotPaused is returned when trying to resume a print that is not paused
	ErrTaskNotPaused = errors.New("task not paused")
)

// Task executes the given function until it is canceled or paused
type Task interface {
	Start() error
	Cancel() error
	Pause() error
	Resume() error
	Wait()
	Error() error
	IsPaused() bool
}

// TaskFn is the function task executes
type TaskFn func() (done bool, err error)

// TaskOption configures a task
type TaskOption func(*task)

// TaskHook can be used to do cleanup tasks
type TaskHook func()

// WithCancelHook adds a hook that is run after the task is canceled
func WithCancelHook(hook TaskHook) TaskOption {
	return func(task *task) {
		task.cancelHook = hook
	}
}

// WithDoneHook adds a hook that is run after the task finishes or is canceled
func WithDoneHook(hook TaskHook) TaskOption {
	return func(task *task) {
		task.doneHook = hook
	}
}

// WithStartHook adds a hook that is run when the task is started
func WithStartHook(hook TaskHook) TaskOption {
	return func(task *task) {
		task.startHook = hook
	}
}

// WithPauseHook adds a hook that is run when the task is paused
func WithPauseHook(hook TaskHook) TaskOption {
	return func(task *task) {
		task.pauseHook = hook
	}
}

// WithResumeHook adds a hook that is run before the task is resumed
func WithResumeHook(hook TaskHook) TaskOption {
	return func(task *task) {
		task.resumeHook = hook
	}
}

// NewTask creates a new Taks with the given function
func NewTask(fn TaskFn, opts ...TaskOption) Task {
	t := &task{
		fn:     fn,
		stopCh: make(chan bool),
	}
	t.doneWG.Add(1)
	for _, option := range opts {
		option(t)
	}
	return t
}

type task struct {
	fn TaskFn

	// hooks
	cancelHook TaskHook
	doneHook   TaskHook
	startHook  TaskHook
	pauseHook  TaskHook
	resumeHook TaskHook

	doneWG     sync.WaitGroup
	loopDoneWG sync.WaitGroup
	stateLock  sync.RWMutex
	started    bool
	paused     bool
	canceled   bool
	err        error
	stopCh     chan bool
}

func (t *task) IsPaused() bool {
	t.stateLock.RLock()
	defer t.stateLock.RUnlock()
	return t.paused
}

func (t *task) Error() error {
	t.stateLock.RLock()
	defer t.stateLock.RUnlock()
	return t.err
}

func (t *task) Wait() {
	t.doneWG.Wait()
}

func (t *task) Start() error {
	t.stateLock.Lock()
	defer t.stateLock.Unlock()
	if t.started || t.paused {
		return ErrTaskAlreadyStarted
	}
	t.started = true
	if t.startHook != nil {
		t.startHook()
	}
	go t.run()
	return nil
}

func (t *task) Cancel() error {
	t.stateLock.Lock()
	if t.canceled {
		t.stateLock.Unlock()
		return ErrTaskAlreadyCanceled
	}
	t.canceled = true
	if !t.paused {
		t.stopCh <- true
	}
	t.err = ErrTaskCanceled
	t.stateLock.Unlock()
	t.loopDoneWG.Wait()

	if t.cancelHook != nil {
		t.cancelHook()
	}

	if t.doneHook != nil {
		t.doneHook()
	}

	t.doneWG.Done()
	close(t.stopCh)
	return nil
}

func (t *task) Pause() error {
	t.stateLock.Lock()
	if t.paused {
		t.stateLock.Unlock()
		return ErrTaskAlreadyPaused
	}
	t.paused = true
	if t.pauseHook != nil {
		t.pauseHook()
	}
	t.stateLock.Unlock()

	t.stopCh <- true
	t.loopDoneWG.Wait()
	return nil
}

func (t *task) Resume() error {
	t.stateLock.Lock()
	if !t.paused {
		t.stateLock.Unlock()
		return ErrTaskNotPaused
	}
	t.paused = false
	if t.resumeHook != nil {
		t.resumeHook()
	}
	t.stateLock.Unlock()

	t.Start()
	return nil
}

func (t *task) run() {
	t.loopDoneWG.Add(1)

	var err error
	var done bool
	defer func() {
		t.stateLock.Lock()
		t.started = false
		if err != nil {
			t.err = err
		}
		t.stateLock.Unlock()
		t.loopDoneWG.Done()

		if done || err != nil {
			if t.doneHook != nil {
				t.doneHook()
			}

			t.doneWG.Done()
			close(t.stopCh)
		}
	}()

	for {
		select {
		case <-t.stopCh:
			return
		default:
			done, err = t.fn()
			if done || err != nil {
				return
			}

		}
	}
}
