package util

import (
	"errors"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestTask(t *testing.T) {
	t.Run("done", func(t *testing.T) {
		assert := assert.New(t)

		doneHookCalled := false
		called := 0
		task := NewTask(func() (bool, error) {
			called++
			return true, nil
		}, WithDoneHook(func() {
			doneHookCalled = true
			return
		}))
		assert.NoError(task.Start())
		assert.EqualError(task.Start(), ErrTaskAlreadyStarted.Error())
		task.Wait()

		assert.NoError(task.Error())
		assert.Equal(1, called)
		assert.True(doneHookCalled)
	})

	t.Run("pause", func(t *testing.T) {
		assert := assert.New(t)

		var lock sync.RWMutex
		canBeCalled := true
		stop := false
		task := NewTask(func() (bool, error) {
			lock.RLock()
			defer lock.RUnlock()
			if !canBeCalled {
				t.Error("taskFn should not be called")
				return true, nil
			}
			if stop {
				return true, nil
			}
			return false, nil
		})

		task.Start()
		assert.NoError(task.Pause())
		assert.EqualError(task.Pause(), ErrTaskAlreadyPaused.Error())
		lock.Lock()
		canBeCalled = false
		lock.Unlock()
		assert.True(task.IsPaused())

		time.Sleep(50 * time.Millisecond)

		lock.Lock()
		canBeCalled = true
		stop = true
		lock.Unlock()
		assert.NoError(task.Resume())
		assert.EqualError(task.Resume(), ErrTaskNotPaused.Error())
		task.Wait()
		assert.NoError(task.Error())
	})

	t.Run("cancel", func(t *testing.T) {
		assert := assert.New(t)
		var lock sync.RWMutex
		canBeCalled := true
		cancelHookCalled := false
		doneHookCalled := false
		task := NewTask(
			func() (bool, error) {
				lock.RLock()
				defer lock.RUnlock()
				if !canBeCalled {
					t.Error("taskFn should not be called")
					return true, nil
				}
				return false, nil
			},
			WithCancelHook(func() {
				cancelHookCalled = true
				return
			}),
			WithDoneHook(func() {
				doneHookCalled = true
				return
			}))

		task.Start()
		assert.NoError(task.Cancel())
		assert.EqualError(task.Cancel(), ErrTaskAlreadyCanceled.Error())

		lock.Lock()
		canBeCalled = false
		lock.Unlock()

		time.Sleep(50 * time.Millisecond)

		task.Wait()
		assert.EqualError(task.Error(), ErrTaskCanceled.Error())
		assert.True(cancelHookCalled)
		assert.True(doneHookCalled)
	})

	t.Run("error", func(t *testing.T) {
		assert := assert.New(t)
		expectedErr := errors.New("error")
		task := NewTask(func() (bool, error) {
			return false, expectedErr
		})

		task.Start()
		task.Wait()
		assert.EqualError(expectedErr, task.Error().Error())
	})

}
