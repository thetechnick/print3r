package util

import (
	"bytes"
	"crypto/sha256"
	"io"
	"math/rand"
	"strings"
	"sync"
)

// RemoveComments trims gcode comments from the input
func RemoveComments(line string) string {
	index := strings.Index(line, ";")
	if index == -1 {
		return strings.Trim(line, " \n")
	}
	return strings.Trim(line[:index], " \n")
}

// Checksum returns a sha256 checksum of the file
func Checksum(reader io.Reader) (checksum []byte, err error) {
	h := sha256.New()
	if _, err = io.Copy(h, reader); err != nil {
		return
	}

	checksum = h.Sum(nil)
	return
}

// ClosingBuffer is a bytes.Buffer that can be closed
type ClosingBuffer struct {
	Buffer
}

// Close does nothing
func (cb *ClosingBuffer) Close() (err error) {
	return
}

// Buffer combines a bytes.Buffer with an sync.RWMutex to be safe for concurrent use.
type Buffer struct {
	b bytes.Buffer
	m sync.RWMutex
}

func (b *Buffer) Read(p []byte) (n int, err error) {
	b.m.RLock()
	defer b.m.RUnlock()
	return b.b.Read(p)
}

func (b *Buffer) Write(p []byte) (n int, err error) {
	b.m.Lock()
	defer b.m.Unlock()
	return b.b.Write(p)
}

func (b *Buffer) String() string {
	b.m.RLock()
	defer b.m.RUnlock()
	return b.b.String()
}

const letterBytes = "abcdefghijklmnopqrstuvwxyz0123456789"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

// RandNameSuffix creates a random string of lowercase characters
func RandNameSuffix(n int) string {
	b := make([]byte, n)
	// A rand.Int63() generates 63 random bits, enough for letterIdxMax letters!
	for i, cache, remain := n-1, rand.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = rand.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}
