package util

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRemoveComments(t *testing.T) {
	t.Run("strip comments", func(t *testing.T) {
		assert := assert.New(t)
		result := RemoveComments("M105 ;just a comment \n  \n")
		assert.Equal("M105", result)
	})

	t.Run("default", func(t *testing.T) {
		assert := assert.New(t)
		result := RemoveComments("M105  \n  \n")
		assert.Equal("M105", result)
	})
}
