package version

import (
	"fmt"
	"runtime"
	"strconv"
	"time"

	"github.com/thetechnick/print3r/pkg/api"
)

// Version provided by compile time -ldflags.
var (
	defaultContent = "was not build properly"
	Version        = defaultContent
	Branch         = defaultContent
	BuildDate      = "10"
)

// Get returns the version information
func Get() *api.Version {
	v := &api.Version{
		Version:   Version,
		Branch:    Branch,
		GoVersion: runtime.Version(),
		Platform:  fmt.Sprintf("%s/%s", runtime.GOOS, runtime.GOARCH),
	}

	i, err := strconv.ParseInt(BuildDate, 10, 64)
	if err != nil {
		panic(fmt.Errorf("Error parsing build time: %v", err))
	}
	v.BuildDate = time.Unix(i, 0).UTC()
	return v
}
