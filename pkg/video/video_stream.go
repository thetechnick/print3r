package video

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"

	"github.com/thetechnick/print3r/pkg/api"
)

type VideoStream interface {
	Open() error
	Wait() error
	Close() error
	Port() int
}

type motionVideoStream struct {
	config api.MotionConfig
	port   int
	cmd    *exec.Cmd
	file   *os.File
}

func NewMotionVideoStream(port int, config api.MotionConfig) VideoStream {
	return &motionVideoStream{
		config: config,
		port:   port,
	}
}

func (s *motionVideoStream) Port() int {
	return s.port
}

func (s *motionVideoStream) Open() (err error) {
	if s.file, err = ioutil.TempFile("", "print3r-video"); err != nil {
		return
	}

	fmt.Fprintf(s.file, "stream_port %d\nstream_maxrate 100\noutput_pictures off\n", s.port)
	fmt.Fprintf(s.file, "videodevice %s\n", s.config.Device)
	fmt.Fprintf(s.file, "width %d\n", s.config.Width)
	fmt.Fprintf(s.file, "height %d\n", s.config.Height)

	s.cmd = exec.Command("motion", "-c", s.file.Name())
	if err = s.cmd.Start(); err != nil {
		return
	}

	return
}

func (s *motionVideoStream) Wait() (err error) {
	if s.cmd != nil {
		err = s.cmd.Wait()
	}
	if s.file != nil {
		s.file.Close()
		os.Remove(s.file.Name())
	}
	if err != nil && err.Error() == "signal: killed" {
		return nil
	}
	return
}

func (s *motionVideoStream) Close() (err error) {
	if s.cmd != nil && s.cmd.Process != nil {
		return s.cmd.Process.Kill()
	}
	return
}
