package video

import (
	"context"
	"reflect"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/storage"
)

// Controller represents video controller functions
type Controller interface {
	Run() error
	Close() error
	// TODO remove
	Port(ctx context.Context, v *api.VideoStream) int
}

type videoStreamController struct {
	storage         storage.VideoStreamStorage
	hub             storage.EventHub
	videoStreams    map[string]VideoStream
	videoStreamLock sync.RWMutex
	log             *log.Entry
	stopCh          chan struct{}
}

// NewController creates a new video Controller
func NewController(
	storage storage.VideoStreamStorage,
	hub storage.EventHub,
) Controller {
	return &videoStreamController{
		storage:      storage,
		hub:          hub,
		videoStreams: map[string]VideoStream{},
		log:          log.WithField("module", "video.Controller"),
		stopCh:       make(chan struct{}),
	}
}

func (c *videoStreamController) Port(ctx context.Context, v *api.VideoStream) int {
	c.videoStreamLock.RLock()
	defer c.videoStreamLock.RUnlock()
	if stream, ok := c.videoStreams[v.Name]; ok {
		return stream.Port()
	}
	return 0
}

func (c *videoStreamController) Close() (err error) {
	close(c.stopCh)
	c.videoStreamLock.Lock()
	defer c.videoStreamLock.Unlock()
	for _, v := range c.videoStreams {
		v.Close()
	}
	return
}

func (c *videoStreamController) Run() (err error) {
	client := c.hub.Register()
	var videoStreams []*api.VideoStream
	ctx := context.Background()
	if videoStreams, err = c.storage.All(ctx); err != nil {
		return
	}
	for _, v := range videoStreams {
		if err = c.Sync(ctx, v); err != nil {
			c.log.WithError(err).WithField("name", v.Name).Warn("syncing video stream")
		}
	}

	t := time.NewTicker(10 * time.Second)
	defer t.Stop()
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()
	for {
		select {
		case <-c.stopCh:
			return

		case <-t.C:
			videoStreams, err = c.storage.All(ctx)
			if err != nil {
				return
			}
			for _, p := range videoStreams {
				if err := c.Sync(ctx, p); err != nil {
					c.log.WithError(err).WithField("name", p.Name).Warn("syncing video stream")
				}
			}

		case event := <-client.Events():
			switch event.Operation {
			case api.ResourceEventOperationCreated:
				videoStream, ok := event.New.(*api.VideoStream)
				if !ok {
					continue
				}
				if err := c.Sync(ctx, videoStream); err != nil {
					c.log.WithError(err).WithField("name", videoStream.Name).Warn("adding video stream")
				}

			case api.ResourceEventOperationDeleted:
				videoStream, ok := event.Old.(*api.VideoStream)
				if !ok {
					continue
				}
				if err := c.Remove(ctx, videoStream); err != nil {
					c.log.WithError(err).WithField("name", videoStream.Name).Warn("removing videoStream")
				}

			case api.ResourceEventOperationUpdated:
				oldVideoStream, ok := event.Old.(*api.VideoStream)
				if !ok {
					continue
				}
				newVideoStream, ok := event.New.(*api.VideoStream)
				if !ok {
					continue
				}

				if hasConfigChanged(newVideoStream, oldVideoStream) {
					if err := c.Remove(ctx, oldVideoStream); err != nil {
						c.log.WithError(err).WithField("name", newVideoStream.Name).Warn("reconnecting video stream")
					}
				}

				if reflect.DeepEqual(oldVideoStream.Spec, newVideoStream.Spec) {
					continue
				}
				if err := c.Sync(ctx, newVideoStream); err != nil {
					c.log.WithError(err).WithField("name", newVideoStream.Name).Warn("updating video stream")
				}
			}
		}
	}
}

func (c *videoStreamController) Remove(ctx context.Context, v *api.VideoStream) (err error) {
	c.videoStreamLock.Lock()
	defer c.videoStreamLock.Unlock()

	if stream, ok := c.videoStreams[v.Name]; ok {
		delete(c.videoStreams, v.Name)
		return stream.Close()
	}
	return
}

func (c *videoStreamController) Sync(ctx context.Context, v *api.VideoStream) (err error) {
	c.videoStreamLock.Lock()
	defer c.videoStreamLock.Unlock()
	c.log.WithField("name", v.Name).Debug("sync")

	if _, ok := c.videoStreams[v.Name]; ok {
		return
	}

	if v.Spec.Motion != nil {
		stream := NewMotionVideoStream(30123, *v.Spec.Motion)
		c.videoStreams[v.Name] = stream
		if err = stream.Open(); err != nil {
			return
		}
		_, _, err = c.storage.EnsureUpdate(ctx, v, storage.VideoStreamUpdateFn(func(videoStream *api.VideoStream) (err error) {
			videoStream.Status.Conditions.Set(api.Condition{
				Type:               api.VideoStreamConditionTypeReady,
				Status:             api.ConditionStatusTrue,
				LastTransitionTime: time.Now(),
			})
			return
		}))
		if err != nil {
			c.log.
				WithField("name", v.Name).
				WithError(err).
				Error("error updating video stream status")
		}
		go func() {
			err := stream.Wait()
			if err != nil {
				c.log.WithError(err).Error("video stream stopped")
			}
			c.videoStreamLock.Lock()
			defer c.videoStreamLock.Unlock()
			delete(c.videoStreams, v.Name)
			_, _, err = c.storage.EnsureUpdate(ctx, v, storage.VideoStreamUpdateFn(func(videoStream *api.VideoStream) (err error) {
				videoStream.Status.Conditions.Set(api.Condition{
					Type:               api.VideoStreamConditionTypeReady,
					Status:             api.ConditionStatusFalse,
					LastTransitionTime: time.Now(),
				})
				return
			}))
			if err != nil {
				c.log.
					WithField("name", v.Name).
					WithError(err).
					Error("error updating video stream status")
			}
		}()
	}

	return
}

func hasConfigChanged(newVideoStream, oldVideoStream *api.VideoStream) bool {
	if newVideoStream == nil || oldVideoStream == nil {
		return false
	}
	oldMotion := oldVideoStream.Spec.Motion
	newMotion := newVideoStream.Spec.Motion
	if oldMotion == nil || newMotion == nil {
		return false
	}

	if oldMotion.Device != newMotion.Device ||
		oldMotion.Height != newMotion.Height ||
		oldMotion.Width != newMotion.Width {
		return true
	}
	return false
}
