package video

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/thetechnick/print3r/pkg/api"
)

func TestMotionVideoStream(t *testing.T) {
	stream := NewMotionVideoStream(30345, api.MotionConfig{
		Device: "/tmp/this-device-does-not-exist",
		Width:  1920,
		Height: 1080,
	})
	assert.NoError(t, stream.Open())
	assert.NoError(t, stream.Close())
	assert.NoError(t, stream.Wait())
}
