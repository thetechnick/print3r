package volume

import (
	"context"
	"reflect"

	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	db "github.com/thetechnick/print3r/pkg/storage"
)

// Controller runs volume control loops
type Controller interface {
	Run() error
}

type controller struct {
	log           *log.Entry
	volumeStorage db.VolumeStorage
	service       Service
	hub           db.EventHub
}

// NewController creates a new Controller instance
func NewController(
	volumeStorage db.VolumeStorage,
	service Service,
	hub db.EventHub,
) Controller {
	return &controller{
		volumeStorage: volumeStorage,
		service:       service,
		hub:           hub,
		log:           log.WithField("module", "volume.Controller"),
	}
}

func (c *controller) Run() (err error) {
	client := c.hub.Register()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var volumes []*api.Volume
	if volumes, err = c.volumeStorage.All(ctx); err != nil {
		return
	}
	for _, volume := range volumes {
		if err = c.service.Add(ctx, volume); err != nil {
			return
		}
	}

	for {
		select {
		case event := <-client.Events():
			switch event.Operation {
			case api.ResourceEventOperationCreated:
				volume, ok := event.New.(*api.Volume)
				if !ok {
					continue
				}
				if err := c.service.Add(ctx, volume); err != nil {
					c.log.
						WithError(err).
						WithField("name", volume.Name).
						Error("adding newVolume")
				}

			case api.ResourceEventOperationDeleted:
				volume, ok := event.Old.(*api.Volume)
				if !ok {
					continue
				}
				if err := c.service.Remove(ctx, volume); err != nil {
					c.log.
						WithError(err).
						WithField("name", volume.Name).
						Error("removing newVolume")
				}
			case api.ResourceEventOperationUpdated:
				oldVolume, ok := event.Old.(*api.Volume)
				if !ok {
					continue
				}
				newVolume, ok := event.New.(*api.Volume)
				if !ok {
					continue
				}
				if reflect.DeepEqual(oldVolume.Spec, newVolume.Spec) {
					continue
				}

				c.log.
					WithField("name", newVolume.Name).
					Debug("volumeSpec changed")
				if err := c.service.Remove(ctx, oldVolume); err != nil {
					c.log.
						WithError(err).
						WithField("name", newVolume.Name).
						Error("removing old volume")
				} else {
					if err := c.service.Add(ctx, newVolume); err != nil {
						c.log.
							WithError(err).
							WithField("name", newVolume.Name).
							Error("adding new volume")
					}
				}

			}
		}
	}
}
