package volume

import (
	"context"
	"os"
	"path"

	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/file"
	"golang.org/x/net/webdav"
)

type localVolumeSource struct {
	path     string
	fs       webdav.FileSystem
	volume   *api.Volume
	analyser file.Analyser
	log      *log.Entry
}

// newLocalVolumeSource creates a new localVolumeSource
func newLocalVolumeSource(
	volume *api.Volume,
	path string,
	fs webdav.FileSystem,
) *localVolumeSource {
	return &localVolumeSource{
		volume:   volume,
		path:     path,
		fs:       fs,
		analyser: file.Analyse,

		log: log.WithField("module", "localVolumeSource"),
	}
}

func (c *localVolumeSource) Delete(ctx context.Context, file string) (found bool, err error) {
	c.log.WithField("path", file).Debug("delete file")
	if _, err = c.fs.Stat(ctx, file); err != nil {
		if err == os.ErrNotExist {
			err = nil
			return
		}
	}
	if err = c.fs.RemoveAll(ctx, file); err != nil {
		return
	}
	found = true
	return
}

func (c *localVolumeSource) List(ctx context.Context) ([]*api.VolumeFile, error) {
	return c.iteratePath(ctx, "/")
}

func (c *localVolumeSource) iteratePath(ctx context.Context, p string) (volumeFiles []*api.VolumeFile, err error) {
	var (
		file         webdav.File
		stat         os.FileInfo
		contentFiles []os.FileInfo
	)
	if file, err = c.fs.OpenFile(ctx, p, os.O_RDONLY, os.ModePerm); err != nil {
		return
	}
	if stat, err = file.Stat(); err != nil {
		return
	}

	if !stat.IsDir() {
		volumeFiles = []*api.VolumeFile{
			{Path: path.Join(p), Size: stat.Size()},
		}
		return
	}

	if contentFiles, err = file.Readdir(-1); err != nil {
		return
	}
	for _, cFile := range contentFiles {
		var subVolumeFiles []*api.VolumeFile
		if subVolumeFiles, err = c.iteratePath(ctx, path.Join(p, cFile.Name())); err != nil {
			return
		}
		volumeFiles = append(volumeFiles, subVolumeFiles...)
	}
	return
}

func (c *localVolumeSource) Get(ctx context.Context, file string) (File, error) {
	f, err := c.fs.OpenFile(ctx, file, os.O_RDONLY, os.ModePerm)
	if err != nil {
		return nil, err
	}

	return f, nil
}
