package volume

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"strings"
	"sync"

	log "github.com/sirupsen/logrus"
	"github.com/thetechnick/print3r/pkg/api"
	db "github.com/thetechnick/print3r/pkg/storage"
	"golang.org/x/net/webdav"
)

type Service interface {
	ServeHTTP(w http.ResponseWriter, req *http.Request)
	Source(ctx context.Context, volume *api.Volume) Source
	Add(ctx context.Context, volume *api.Volume) error
	Remove(ctx context.Context, volume *api.Volume) error
}

type File interface {
	io.Reader
	io.Seeker
	io.Closer
}

// Source implements volume source specific functions
type Source interface {
	Delete(ctx context.Context, file string) (bool, error)
	Get(ctx context.Context, file string) (content File, err error)
	List(ctx context.Context) ([]*api.VolumeFile, error)
}

type service struct {
	sources        map[string]Source
	webdavHandlers map[string]http.Handler
	sourcesLock    sync.RWMutex

	volumeStorage db.VolumeStorage

	log *log.Entry
}

func NewService(
	volumeStorage db.VolumeStorage,
) Service {
	return &service{
		volumeStorage:  volumeStorage,
		sources:        map[string]Source{},
		webdavHandlers: map[string]http.Handler{},
		log:            log.WithField("module", "printer.Service"),
	}
}

func (s *service) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	s.sourcesLock.RLock()
	defer s.sourcesLock.RUnlock()
	parts := strings.Split(strings.Replace(req.URL.EscapedPath(), "/webdav/", "", 1), "/")
	volumeName := parts[0]
	if handler, ok := s.webdavHandlers[volumeName]; ok {
		handler.ServeHTTP(w, req)
		return
	}
	w.WriteHeader(http.StatusNotFound)
}

func (s *service) Source(ctx context.Context, volume *api.Volume) Source {
	s.sourcesLock.RLock()
	defer s.sourcesLock.RUnlock()
	if source, ok := s.sources[volume.UID]; ok {
		return source
	}
	return nil
}

func (s *service) Add(ctx context.Context, volume *api.Volume) error {
	s.sourcesLock.Lock()
	defer s.sourcesLock.Unlock()
	name := volume.Name
	source := newLocalVolumeSource(
		volume,
		volume.Spec.Local.Path,
		webdav.Dir(volume.Spec.Local.Path),
	)
	s.sources[volume.UID] = source
	s.webdavHandlers[name] = &webdav.Handler{
		Prefix:     fmt.Sprintf("/webdav/%s", name),
		FileSystem: source.fs,
		LockSystem: webdav.NewMemLS(),
	}
	return nil
}

func (s *service) Remove(ctx context.Context, volume *api.Volume) error {
	s.log.
		WithField("name", volume.Name).
		Debug("volume was deleted")
	s.sourcesLock.Lock()
	defer s.sourcesLock.Unlock()
	delete(s.sources, volume.UID)
	delete(s.webdavHandlers, volume.Name)
	return nil
}
