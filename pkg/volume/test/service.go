package test

import (
	"context"
	"net/http"

	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/volume"
)

// ServiceMock mock of volume.Service
type ServiceMock struct {
	mock.Mock
}

func (s *ServiceMock) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	s.Called(w, req)
	return
}

// Source mock of volume.Service.Source
func (s *ServiceMock) Source(ctx context.Context, v *api.Volume) volume.Source {
	args := s.Called(ctx, v)
	return args.Get(0).(volume.Source)
}

// Add mock of volume.Service.Add
func (s *ServiceMock) Add(ctx context.Context, volume *api.Volume) error {
	args := s.Called(ctx, volume)
	return args.Error(0)
}

// Remove mock of volume.Service.Remove
func (s *ServiceMock) Remove(ctx context.Context, volume *api.Volume) error {
	args := s.Called(ctx, volume)
	return args.Error(0)
}
