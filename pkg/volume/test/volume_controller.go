package test

import (
	"context"
	"io"
	"net/http"

	"github.com/stretchr/testify/mock"
	"github.com/thetechnick/print3r/pkg/api"
	"github.com/thetechnick/print3r/pkg/volume"
)

// Service is a mock of volume.Service
type Service struct {
	mock.Mock
}

// ServeHTTP mock of volume.Service.ServeHTTP
func (m *Service) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	m.Called(w, req)
	return
}

// Source mock of Service.Source
func (m *Service) Source(v *api.Volume) volume.Source {
	args := m.Called(v)
	return args.Get(0).(volume.Source)
}

// Add mock of Service.Add
func (m *Service) Add(volume *api.Volume) error {
	args := m.Called(volume)
	return args.Error(0)
}

// Remove mock of Service.Remove
func (m *Service) Remove(volume *api.Volume) error {
	args := m.Called(volume)
	return args.Error(0)
}

// Source is a mock of volume.Source
type Source struct {
	mock.Mock
}

// Sync mock of volume.Source.Sync
func (m *Source) Sync() error {
	args := m.Called()
	return args.Error(0)
}

// Delete mock of volume.Source.Delete
func (m *Source) Delete(ctx context.Context, file string) (bool, error) {
	args := m.Called(ctx, file)
	return args.Bool(0), args.Error(1)
}

// Get mock of volume.Source.Get
func (m *Source) Get(ctx context.Context, file string) (content io.ReadCloser, err error) {
	args := m.Called(ctx, file)
	return args.Get(0).(io.ReadCloser), args.Error(1)
}
