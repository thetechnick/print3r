import 'dart:async';
import 'dart:convert';
import 'dart:html' as html;
import 'package:http/http.dart';
import 'package:angular/angular.dart';
import 'printer_model.dart';
import 'print_model.dart';
import 'resource_event_model.dart';
import 'status_model.dart';
import 'volume_model.dart';
import 'video_stream_model.dart';

String get host {
  var host = html.window.location.host;
  if (host.startsWith('localhost')) {
    return 'localhost:8081';
  }
  return host;
}

@Injectable()
class ResourceEventClient {
  final Uri _endpointURL;
  final StreamController<ResourceEvent> _events;
  html.WebSocket _ws;

  ResourceEventClient()
      : _endpointURL = Uri.parse('ws://${host}/api/v1/resource_events'),
        _events = new StreamController<ResourceEvent>.broadcast() {
    _ws = new html.WebSocket(_endpointURL.toString());
    _ws.onMessage.map((html.MessageEvent e) {
      Map<String, dynamic> data = json.decode(e.data);
      if (data != null) {
        return new ResourceEvent.fromJson(data['result']);
      }
      return null;
    }).listen((e) {
      if (e != null) {
        _events.add(e);
      }
    });
  }

  Stream<ResourceEvent> stream() {
    return _events.stream;
  }
}

@Injectable()
class VideoStreamClient {
  final Uri _endpointURL;
  final Client _client;

  VideoStreamClient(this._client)
      : _endpointURL = Uri.parse('http://${host}/api/v1/video_streams');

  Future<VideoStreamList> all() async {
    var resp = await _client.get(_endpointURL);
    if (resp.statusCode != 200) {
      return null;
    }
    return new VideoStreamList.fromJson(json.decode(resp.body));
  }

  String streamUrl(String name) {
    Uri url = _endpointURL.resolve('video_streams/${name}/stream');
    return url.toString();
  }

  Future<VideoStream> show(String name) async {
    Uri url = _endpointURL.resolve('video_streams/${name}');
    var resp = await _client.get(url);
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }

    return new VideoStream.fromJson(json.decode(resp.body));
  }

  Future<VideoStream> create(VideoStream videoStream) async {
    var resp =
        await _client.post(_endpointURL, body: json.encode(videoStream.toJson()));
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }
    return new VideoStream.fromJson(json.decode(resp.body));
  }

  Future<VideoStream> delete(VideoStream videoStream) async {
    Uri url = _endpointURL.resolve('video_streams/${videoStream.metadata.name}');
    var resp = await _client.delete(url);
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }
    return new VideoStream.fromJson(json.decode(resp.body));
  }

  Future<VideoStream> update(VideoStream print) async {
    Uri url = _endpointURL.resolve('video_streams/${print.metadata.name}');
    var resp = await _client.put(url, body: json.encode(print.toJson()));
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }
    return new VideoStream.fromJson(json.decode(resp.body));
  }
}

@Injectable()
class PrintClient {
  final Uri _endpointURL;
  final Client _client;

  PrintClient(this._client)
      : _endpointURL = Uri.parse('http://${host}/api/v1/prints');

  Future<PrintList> all() async {
    var resp = await _client.get(_endpointURL);
    if (resp.statusCode != 200) {
      return null;
    }
    return new PrintList.fromJson(json.decode(resp.body));
  }

  Future<Print> show(String name) async {
    Uri url = _endpointURL.resolve('prints/${name}');
    var resp = await _client.get(url);
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }

    return new Print.fromJson(json.decode(resp.body));
  }

  Future<Print> create(Print print) async {
    var resp =
        await _client.post(_endpointURL, body: json.encode(print.toJson()));
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }
    return new Print.fromJson(json.decode(resp.body));
  }

  Future<Print> delete(Print print) async {
    Uri url = _endpointURL.resolve('prints/${print.metadata.name}');
    var resp = await _client.delete(url);
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }
    return new Print.fromJson(json.decode(resp.body));
  }

  Future<Print> cancel(Print print) {
    print.spec.state = 'Canceled';
    return update(print);
  }

  Future<Print> update(Print print) async {
    Uri url = _endpointURL.resolve('prints/${print.metadata.name}');
    var resp = await _client.put(url, body: json.encode(print.toJson()));
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }
    return new Print.fromJson(json.decode(resp.body));
  }
}

@Injectable()
class VolumeClient {
  final Uri _endpointURL;
  final Client _client;

  VolumeClient(this._client)
      : _endpointURL = Uri.parse('http://${host}/api/v1/volumes');

  Future<VolumeList> all() async {
    var resp = await _client.get(_endpointURL);
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }

    return new VolumeList.fromJson(json.decode(resp.body));
  }

  Future<VolumeFileList> listFiles(Volume volume) async {
    Uri url = _endpointURL.resolve('volumes/${volume.metadata.name}/files');
    var resp = await _client.get(url);
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }

    return new VolumeFileList.fromJson(json.decode(resp.body));
  }

  Future<Volume> show(String name) async {
    Uri url = _endpointURL.resolve('volumes/${name}');
    var resp = await _client.get(url);
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }

    return new Volume.fromJson(json.decode(resp.body));
  }

  Future<Volume> update(Volume volume) async {
    Uri url = _endpointURL.resolve('volumes/${volume.metadata.name}');
    var resp = await _client.put(url, body: json.encode(volume.toJson()));
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }
    return new Volume.fromJson(json.decode(resp.body));
  }

  Future<Volume> delete(Volume volume) async {
    Uri url = _endpointURL.resolve('volumes/${volume.metadata.name}');
    var resp = await _client.delete(url);
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }
    return new Volume.fromJson(json.decode(resp.body));
  }
}

@Injectable()
class PrinterClient {
  final Uri _endpointURL;
  final Client _client;
  final ResourceEventClient _resourceEventClient;

  PrinterClient(this._client, this._resourceEventClient)
      : _endpointURL = Uri.parse('http://${host}/api/v1/printers');

  Future<PrinterList> all() async {
    var resp = await _client.get(_endpointURL);
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }

    return new PrinterList.fromJson(json.decode(resp.body));
  }

  Future<Printer> show(String name) async {
    Uri url = _endpointURL.resolve('printers/${name}');
    var resp = await _client.get(url);
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }

    return new Printer.fromJson(json.decode(resp.body));
  }

  Future<Printer> create(Printer printer) async {
    var resp =
        await _client.post(_endpointURL, body: json.encode(printer));
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }
    return new Printer.fromJson(json.decode(resp.body));
  }

  Future<PrinterTemperatureHistory> temperatureHistory(Printer printer, DateTime start, DateTime end, int size) async {
    Uri url = _endpointURL
        .resolve('printers/${printer.metadata.name}/temperature_history')
        .replace(queryParameters: {
      'start': [start.toUtc().toIso8601String()],
      'end': [end.toUtc().toIso8601String()],
      'size': [size.toString()],
    });
    var resp = await _client.get(url);
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }

    return new PrinterTemperatureHistory.fromJson(json.decode(resp.body));
  }

  Future<Printer> connect(Printer printer) {
    var lastTransition = printer.connectedCondition.lastTransitionTime;

    var updated = _resourceEventClient
        .stream()
        .where((event) =>
            event.operation == 'Updated' && event.newObject.data is Printer)
        .map((event) => event.newObject.data as Printer)
        .where((printer) {
      return printer.connectedCondition.lastTransitionTime
          .isAfter(lastTransition);
    }).first;

    printer.spec.state = 'Connected';
    return update(printer).then((p) => updated);
  }

  Future<Printer> update(Printer printer) async {
    Uri url = _endpointURL.resolve('printers/${printer.metadata.name}');
    var resp = await _client.put(url, body: json.encode(printer.toJson()));
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }
    return new Printer.fromJson(json.decode(resp.body));
  }

  Future<Printer> updateStatus(Printer printer) async {
    Uri url = _endpointURL.resolve('printers/${printer.metadata.name}/status');
    var resp = await _client.put(url, body: json.encode(printer.toJson()));
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }
    return new Printer.fromJson(json.decode(resp.body));
  }

  Future<Printer> disconnect(Printer printer) {
    printer.spec.state = 'Disconnected';
    return update(printer);
  }

  Future<Printer> delete(Printer printer) async {
    Uri url = _endpointURL.resolve('printers/${printer.metadata.name}');
    var resp = await _client.delete(url);
    if (resp.statusCode != 200) {
      throw new Status.fromJson(json.decode(resp.body));
    }
    return new Printer.fromJson(json.decode(resp.body));
  }
}
