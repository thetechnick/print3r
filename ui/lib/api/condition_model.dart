library print3r.api.condition;

import 'package:json_annotation/json_annotation.dart';
import 'util.dart';

part 'condition_model.g.dart';

@JsonSerializable()
class Condition extends Object with _$ConditionSerializerMixin {
  String type, status, reason, message;
  DateTime lastTransitionTime;

  Condition();

  factory Condition.fromJson(json) {
    json['lastTransitionTime'] = shortenDate(json['lastTransitionTime']);
    return _$ConditionFromJson(json);
  }

  bool get isTrue {
    if (status == 'True') {
      return true;
    }
    return false;
  }
}
