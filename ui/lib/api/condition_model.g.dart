// GENERATED CODE - DO NOT MODIFY BY HAND

part of print3r.api.condition;

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Condition _$ConditionFromJson(Map<String, dynamic> json) {
  return new Condition()
    ..type = json['type'] as String
    ..status = json['status'] as String
    ..reason = json['reason'] as String
    ..message = json['message'] as String
    ..lastTransitionTime = json['lastTransitionTime'] == null
        ? null
        : DateTime.parse(json['lastTransitionTime'] as String);
}

abstract class _$ConditionSerializerMixin {
  String get type;
  String get status;
  String get reason;
  String get message;
  DateTime get lastTransitionTime;
  Map<String, dynamic> toJson() => <String, dynamic>{
        'type': type,
        'status': status,
        'reason': reason,
        'message': message,
        'lastTransitionTime': lastTransitionTime?.toIso8601String()
      };
}
