library print3r.api.meta;

import 'package:json_annotation/json_annotation.dart';
import 'util.dart';

part 'meta_model.g.dart';

abstract class ObjectMetadata {
  ObjectMeta metadata;
}

@JsonSerializable()
class ObjectMeta extends Object with _$ObjectMetaSerializerMixin {
  String uid, resourceVersion, name;
  DateTime created, updated;

  ObjectMeta();

  factory ObjectMeta.fromJson(json) {
    json['created'] = shortenDate(json['created']);
    json['updated'] = shortenDate(json['updated']);
    return _$ObjectMetaFromJson(json);
  }
}
