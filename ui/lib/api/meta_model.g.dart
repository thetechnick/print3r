// GENERATED CODE - DO NOT MODIFY BY HAND

part of print3r.api.meta;

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ObjectMeta _$ObjectMetaFromJson(Map<String, dynamic> json) {
  return new ObjectMeta()
    ..uid = json['uid'] as String
    ..resourceVersion = json['resourceVersion'] as String
    ..name = json['name'] as String
    ..created = json['created'] == null
        ? null
        : DateTime.parse(json['created'] as String)
    ..updated = json['updated'] == null
        ? null
        : DateTime.parse(json['updated'] as String);
}

abstract class _$ObjectMetaSerializerMixin {
  String get uid;
  String get resourceVersion;
  String get name;
  DateTime get created;
  DateTime get updated;
  Map<String, dynamic> toJson() => <String, dynamic>{
        'uid': uid,
        'resourceVersion': resourceVersion,
        'name': name,
        'created': created?.toIso8601String(),
        'updated': updated?.toIso8601String()
      };
}
