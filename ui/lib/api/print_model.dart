library print3r.api.print;

import 'package:json_annotation/json_annotation.dart';
import 'meta_model.dart';
import 'condition_model.dart';

part 'print_model.g.dart';

@JsonSerializable()
class PrintList extends Object with _$PrintListSerializerMixin {
  List<Print> items = [];

  PrintList();

  factory PrintList.fromJson(json) => _$PrintListFromJson(json);
}

@JsonSerializable()
class Print extends Object with _$PrintSerializerMixin {
  ObjectMeta metadata;
  PrintSpec spec;
  PrintStatus status;

  Print();

  factory Print.fromJson(json) => _$PrintFromJson(json);
}

@JsonSerializable()
class PrintSpec extends Object with _$PrintSpecSerializerMixin {
  String printer, state;
  FileRef fileRef;

  PrintSpec();

  factory PrintSpec.fromJson(json) => _$PrintSpecFromJson(json);
}

@JsonSerializable()
class FileRef extends Object with _$FileRefSerializerMixin {
  String volume, file;

  FileRef();

  factory FileRef.fromJson(json) => _$FileRefFromJson(json);
}

@JsonSerializable()
class PrintStatus extends Object with _$PrintStatusSerializerMixin {
  List<Condition> conditions;
  double progress;

  PrintStatus();

  factory PrintStatus.fromJson(json) => _$PrintStatusFromJson(json);

  String get status {
    if (cancel.isTrue) {
      return "Canceled";
    }
    if (finished.isTrue) {
      return "Finished";
    }
    if (started.isTrue) {
      return "Started";
    }
    return "Pending";
  }

  Condition get started {
    for (var c in conditions) {
      if (c.type == "Started") {
        return c;
      }
    }
    return null;
  }

  Condition get cancel {
    for (var c in conditions) {
      if (c.type == "Canceled") {
        return c;
      }
    }
    return null;
  }

  Condition get finished {
    for (var c in conditions) {
      if (c.type == "Finished") {
        return c;
      }
    }
    return null;
  }
}
