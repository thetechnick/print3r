// GENERATED CODE - DO NOT MODIFY BY HAND

part of print3r.api.print;

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PrintList _$PrintListFromJson(Map<String, dynamic> json) {
  return new PrintList()
    ..items = (json['items'] as List)
        ?.map((e) => e == null ? null : new Print.fromJson(e))
        ?.toList();
}

abstract class _$PrintListSerializerMixin {
  List<Print> get items;
  Map<String, dynamic> toJson() => <String, dynamic>{'items': items};
}

Print _$PrintFromJson(Map<String, dynamic> json) {
  return new Print()
    ..metadata = json['metadata'] == null
        ? null
        : new ObjectMeta.fromJson(json['metadata'])
    ..spec = json['spec'] == null ? null : new PrintSpec.fromJson(json['spec'])
    ..status = json['status'] == null
        ? null
        : new PrintStatus.fromJson(json['status']);
}

abstract class _$PrintSerializerMixin {
  ObjectMeta get metadata;
  PrintSpec get spec;
  PrintStatus get status;
  Map<String, dynamic> toJson() =>
      <String, dynamic>{'metadata': metadata, 'spec': spec, 'status': status};
}

PrintSpec _$PrintSpecFromJson(Map<String, dynamic> json) {
  return new PrintSpec()
    ..printer = json['printer'] as String
    ..state = json['state'] as String
    ..fileRef =
        json['fileRef'] == null ? null : new FileRef.fromJson(json['fileRef']);
}

abstract class _$PrintSpecSerializerMixin {
  String get printer;
  String get state;
  FileRef get fileRef;
  Map<String, dynamic> toJson() =>
      <String, dynamic>{'printer': printer, 'state': state, 'fileRef': fileRef};
}

FileRef _$FileRefFromJson(Map<String, dynamic> json) {
  return new FileRef()
    ..volume = json['volume'] as String
    ..file = json['file'] as String;
}

abstract class _$FileRefSerializerMixin {
  String get volume;
  String get file;
  Map<String, dynamic> toJson() =>
      <String, dynamic>{'volume': volume, 'file': file};
}

PrintStatus _$PrintStatusFromJson(Map<String, dynamic> json) {
  return new PrintStatus()
    ..conditions = (json['conditions'] as List)
        ?.map((e) => e == null ? null : new Condition.fromJson(e))
        ?.toList()
    ..progress = (json['progress'] as num)?.toDouble();
}

abstract class _$PrintStatusSerializerMixin {
  List<Condition> get conditions;
  double get progress;
  Map<String, dynamic> toJson() =>
      <String, dynamic>{'conditions': conditions, 'progress': progress};
}
