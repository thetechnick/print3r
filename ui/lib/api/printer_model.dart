library print3r.api.printer;

import 'package:json_annotation/json_annotation.dart';
import 'meta_model.dart';
import 'condition_model.dart';
import 'util.dart';

part 'printer_model.g.dart';

@JsonSerializable()
class PrinterList extends Object with _$PrinterListSerializerMixin {
  List<Printer> items = [];

  PrinterList();

  factory PrinterList.fromJson(json) => _$PrinterListFromJson(json);
}

@JsonSerializable(
  includeIfNull: false,
)
class Printer extends Object with _$PrinterSerializerMixin {
  ObjectMeta metadata;
  PrinterSpec spec;
  PrinterStatus status;

  Printer();

  factory Printer.fromJson(json) => _$PrinterFromJson(json);

  String get temperature {
    if (status.temperature != null) {
      return '${status.temperature.bedActual}/${status.temperature.extruderActual} °C';
    }
    return '-/- °C';
  }

  Condition get bedCleanCondition {
    for (var c in status.conditions) {
      if (c.type == 'BedClean') {
        return c;
      }
    }
    return null;
  }

  Condition get printingCondition {
    for (var c in status.conditions) {
      if (c.type == 'Printing') {
        return c;
      }
    }
    return null;
  }

  Condition get connectedCondition {
    for (var c in status.conditions) {
      if (c.type == 'Connected') {
        return c;
      }
    }
    return null;
  }

  String get connectionStatus {
    if (status.conditions == null) {
      return 'Disconnected';
    }
    if (printingCondition.isTrue) {
      return 'Printing';
    }
    for (var c in status.conditions) {
      if (c.type == 'Connected' && c.isTrue) {
        return 'Connected';
      }
      if (c.type == 'Connected' &&
          c.status == 'False' &&
          c.reason == "ConnectionError") {
        return 'Error';
      }
    }
    return 'Disconnected';
  }
}

@JsonSerializable(
  includeIfNull: false,
)
class PrinterSpec extends Object with _$PrinterSpecSerializerMixin {
  String model, device, state, baudRate, videoStream;

  PrinterSpec();

  factory PrinterSpec.fromJson(json) => _$PrinterSpecFromJson(json);
}

@JsonSerializable(
  includeIfNull: false,
)
class PrinterStatus extends Object with _$PrinterStatusSerializerMixin {
  PrinterTemperature temperature;
  List<Condition> conditions;
  PrinterPrintStatus print;

  PrinterStatus();

  factory PrinterStatus.fromJson(json) => _$PrinterStatusFromJson(json);
}

@JsonSerializable(
  includeIfNull: false,
)
class PrinterPrintStatus extends Object with _$PrinterPrintStatusSerializerMixin {
  String name;

  PrinterPrintStatus();

  factory PrinterPrintStatus.fromJson(json) => _$PrinterPrintStatusFromJson(json);
}

@JsonSerializable(
  includeIfNull: false,
)
class PrinterTemperature extends Object
    with _$PrinterTemperatureSerializerMixin {
  double extruderActual, extruderTarget, bedActual, bedTarget;

  PrinterTemperature();

  factory PrinterTemperature.fromJson(json) =>
      _$PrinterTemperatureFromJson(json);
}

@JsonSerializable()
class PrinterTemperatureHistory extends Object
    with _$PrinterTemperatureHistorySerializerMixin {
  List<PrinterTemperatureHistoryPoint> extruderTarget,
      extruderActual,
      bedTarget,
      bedActual;

  PrinterTemperatureHistory();

  factory PrinterTemperatureHistory.fromJson(json) =>
      _$PrinterTemperatureHistoryFromJson(json);
}

@JsonSerializable()
class PrinterTemperatureHistoryPoint extends Object
    with _$PrinterTemperatureHistoryPointSerializerMixin {
  DateTime time;
  double value;

  PrinterTemperatureHistoryPoint();

  factory PrinterTemperatureHistoryPoint.fromJson(json) {
    json['time'] = shortenDate(json['time']);
    return _$PrinterTemperatureHistoryPointFromJson(json);
  }
}
