// GENERATED CODE - DO NOT MODIFY BY HAND

part of print3r.api.printer;

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PrinterList _$PrinterListFromJson(Map<String, dynamic> json) {
  return new PrinterList()
    ..items = (json['items'] as List)
        ?.map((e) => e == null ? null : new Printer.fromJson(e))
        ?.toList();
}

abstract class _$PrinterListSerializerMixin {
  List<Printer> get items;
  Map<String, dynamic> toJson() => <String, dynamic>{'items': items};
}

Printer _$PrinterFromJson(Map<String, dynamic> json) {
  return new Printer()
    ..metadata = json['metadata'] == null
        ? null
        : new ObjectMeta.fromJson(json['metadata'])
    ..spec =
        json['spec'] == null ? null : new PrinterSpec.fromJson(json['spec'])
    ..status = json['status'] == null
        ? null
        : new PrinterStatus.fromJson(json['status']);
}

abstract class _$PrinterSerializerMixin {
  ObjectMeta get metadata;
  PrinterSpec get spec;
  PrinterStatus get status;
  Map<String, dynamic> toJson() {
    var val = <String, dynamic>{};

    void writeNotNull(String key, dynamic value) {
      if (value != null) {
        val[key] = value;
      }
    }

    writeNotNull('metadata', metadata);
    writeNotNull('spec', spec);
    writeNotNull('status', status);
    return val;
  }
}

PrinterSpec _$PrinterSpecFromJson(Map<String, dynamic> json) {
  return new PrinterSpec()
    ..model = json['model'] as String
    ..device = json['device'] as String
    ..state = json['state'] as String
    ..baudRate = json['baudRate'] as String
    ..videoStream = json['videoStream'] as String;
}

abstract class _$PrinterSpecSerializerMixin {
  String get model;
  String get device;
  String get state;
  String get baudRate;
  String get videoStream;
  Map<String, dynamic> toJson() {
    var val = <String, dynamic>{};

    void writeNotNull(String key, dynamic value) {
      if (value != null) {
        val[key] = value;
      }
    }

    writeNotNull('model', model);
    writeNotNull('device', device);
    writeNotNull('state', state);
    writeNotNull('baudRate', baudRate);
    writeNotNull('videoStream', videoStream);
    return val;
  }
}

PrinterStatus _$PrinterStatusFromJson(Map<String, dynamic> json) {
  return new PrinterStatus()
    ..temperature = json['temperature'] == null
        ? null
        : new PrinterTemperature.fromJson(json['temperature'])
    ..conditions = (json['conditions'] as List)
        ?.map((e) => e == null ? null : new Condition.fromJson(e))
        ?.toList()
    ..print = json['print'] == null
        ? null
        : new PrinterPrintStatus.fromJson(json['print']);
}

abstract class _$PrinterStatusSerializerMixin {
  PrinterTemperature get temperature;
  List<Condition> get conditions;
  PrinterPrintStatus get print;
  Map<String, dynamic> toJson() {
    var val = <String, dynamic>{};

    void writeNotNull(String key, dynamic value) {
      if (value != null) {
        val[key] = value;
      }
    }

    writeNotNull('temperature', temperature);
    writeNotNull('conditions', conditions);
    writeNotNull('print', print);
    return val;
  }
}

PrinterPrintStatus _$PrinterPrintStatusFromJson(Map<String, dynamic> json) {
  return new PrinterPrintStatus()..name = json['name'] as String;
}

abstract class _$PrinterPrintStatusSerializerMixin {
  String get name;
  Map<String, dynamic> toJson() {
    var val = <String, dynamic>{};

    void writeNotNull(String key, dynamic value) {
      if (value != null) {
        val[key] = value;
      }
    }

    writeNotNull('name', name);
    return val;
  }
}

PrinterTemperature _$PrinterTemperatureFromJson(Map<String, dynamic> json) {
  return new PrinterTemperature()
    ..extruderActual = (json['extruderActual'] as num)?.toDouble()
    ..extruderTarget = (json['extruderTarget'] as num)?.toDouble()
    ..bedActual = (json['bedActual'] as num)?.toDouble()
    ..bedTarget = (json['bedTarget'] as num)?.toDouble();
}

abstract class _$PrinterTemperatureSerializerMixin {
  double get extruderActual;
  double get extruderTarget;
  double get bedActual;
  double get bedTarget;
  Map<String, dynamic> toJson() {
    var val = <String, dynamic>{};

    void writeNotNull(String key, dynamic value) {
      if (value != null) {
        val[key] = value;
      }
    }

    writeNotNull('extruderActual', extruderActual);
    writeNotNull('extruderTarget', extruderTarget);
    writeNotNull('bedActual', bedActual);
    writeNotNull('bedTarget', bedTarget);
    return val;
  }
}

PrinterTemperatureHistory _$PrinterTemperatureHistoryFromJson(
    Map<String, dynamic> json) {
  return new PrinterTemperatureHistory()
    ..extruderTarget = (json['extruderTarget'] as List)
        ?.map((e) =>
            e == null ? null : new PrinterTemperatureHistoryPoint.fromJson(e))
        ?.toList()
    ..extruderActual = (json['extruderActual'] as List)
        ?.map((e) =>
            e == null ? null : new PrinterTemperatureHistoryPoint.fromJson(e))
        ?.toList()
    ..bedTarget = (json['bedTarget'] as List)
        ?.map((e) =>
            e == null ? null : new PrinterTemperatureHistoryPoint.fromJson(e))
        ?.toList()
    ..bedActual = (json['bedActual'] as List)
        ?.map((e) =>
            e == null ? null : new PrinterTemperatureHistoryPoint.fromJson(e))
        ?.toList();
}

abstract class _$PrinterTemperatureHistorySerializerMixin {
  List<PrinterTemperatureHistoryPoint> get extruderTarget;
  List<PrinterTemperatureHistoryPoint> get extruderActual;
  List<PrinterTemperatureHistoryPoint> get bedTarget;
  List<PrinterTemperatureHistoryPoint> get bedActual;
  Map<String, dynamic> toJson() => <String, dynamic>{
        'extruderTarget': extruderTarget,
        'extruderActual': extruderActual,
        'bedTarget': bedTarget,
        'bedActual': bedActual
      };
}

PrinterTemperatureHistoryPoint _$PrinterTemperatureHistoryPointFromJson(
    Map<String, dynamic> json) {
  return new PrinterTemperatureHistoryPoint()
    ..time =
        json['time'] == null ? null : DateTime.parse(json['time'] as String)
    ..value = (json['value'] as num)?.toDouble();
}

abstract class _$PrinterTemperatureHistoryPointSerializerMixin {
  DateTime get time;
  double get value;
  Map<String, dynamic> toJson() =>
      <String, dynamic>{'time': time?.toIso8601String(), 'value': value};
}
