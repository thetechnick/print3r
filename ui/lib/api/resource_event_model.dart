library print3r.api.resource_event;

import 'package:json_annotation/json_annotation.dart';
import 'printer_model.dart';
import 'util.dart';
import 'status_model.dart';
import 'print_model.dart';
import 'volume_model.dart';

part 'resource_event_model.g.dart';



@JsonSerializable()
class ResourceEvent extends Object with _$ResourceEventSerializerMixin {
  String id, operation;
  DateTime emittedTime;
  @JsonKey(name: 'old')
  Any oldObject;
  @JsonKey(name: 'new')
  Any newObject;

  ResourceEvent();

  factory ResourceEvent.fromJson(json) {
    json['emittedTime'] = shortenDate(json['emittedTime']);
    return _$ResourceEventFromJson(json);
  }
}

class Any {
  final String type;
  final dynamic data;

  Any(this.type, this.data);

  factory Any.fromJson(json) {
    String type = json['@type'];
    dynamic object;
    switch (type) {
      case 'type.print3r.xyz/print3r.api.v1.Print':
        object = new Print.fromJson(json);
        break;

      case 'type.print3r.xyz/print3r.api.v1.Printer':
        object = new Printer.fromJson(json);
        break;

      case 'type.print3r.xyz/print3r.api.v1.Volume':
        object = new Volume.fromJson(json);
        break;

      case 'type.print3r.xyz/print3r.api.v1.ErrorFieldViolation':
        object = new ErrorFieldViolation.fromJson(json);
        break;
    }
    return new Any(json['@type'], object);
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    '@type': type,
  };
}
