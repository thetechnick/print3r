// GENERATED CODE - DO NOT MODIFY BY HAND

part of print3r.api.resource_event;

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResourceEvent _$ResourceEventFromJson(Map<String, dynamic> json) {
  return new ResourceEvent()
    ..id = json['id'] as String
    ..operation = json['operation'] as String
    ..emittedTime = json['emittedTime'] == null
        ? null
        : DateTime.parse(json['emittedTime'] as String)
    ..oldObject = json['old'] == null ? null : new Any.fromJson(json['old'])
    ..newObject = json['new'] == null ? null : new Any.fromJson(json['new']);
}

abstract class _$ResourceEventSerializerMixin {
  String get id;
  String get operation;
  DateTime get emittedTime;
  Any get oldObject;
  Any get newObject;
  Map<String, dynamic> toJson() => <String, dynamic>{
        'id': id,
        'operation': operation,
        'emittedTime': emittedTime?.toIso8601String(),
        'old': oldObject,
        'new': newObject
      };
}
