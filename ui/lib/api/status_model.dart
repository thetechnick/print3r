library print3r.api.status;

import 'package:json_annotation/json_annotation.dart';
import 'resource_event_model.dart';

part 'status_model.g.dart';

@JsonSerializable(
  includeIfNull: false,
)
class Status extends Object with _$StatusSerializerMixin implements Exception {
  int code;
  String message;
  List<Any> details;

  Status(this.code, this.message, this.details);

  factory Status.fromJson(json) => _$StatusFromJson(json);

  String toString() {
    var msg = [message];
    if (details != null && details.length > 0) {
      for (var detail in details) {
        if (detail.data is ErrorFieldViolation) {
          var fv = detail.data as ErrorFieldViolation;
          msg.add('${fv.field}: ${fv.description}');
        }
      }
    }
    return msg.join(', ');
  }
}

@JsonSerializable()
class ErrorFieldViolation extends Object with _$ErrorFieldViolationSerializerMixin {
  String description, field;

  ErrorFieldViolation(this.description, this.field);

  factory ErrorFieldViolation.fromJson(json) => _$ErrorFieldViolationFromJson(json);
}
