// GENERATED CODE - DO NOT MODIFY BY HAND

part of print3r.api.status;

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Status _$StatusFromJson(Map<String, dynamic> json) {
  return new Status(
      json['code'] as int,
      json['message'] as String,
      (json['details'] as List)
          ?.map((e) => e == null ? null : new Any.fromJson(e))
          ?.toList());
}

abstract class _$StatusSerializerMixin {
  int get code;
  String get message;
  List<Any> get details;
  Map<String, dynamic> toJson() {
    var val = <String, dynamic>{};

    void writeNotNull(String key, dynamic value) {
      if (value != null) {
        val[key] = value;
      }
    }

    writeNotNull('code', code);
    writeNotNull('message', message);
    writeNotNull('details', details);
    return val;
  }
}

ErrorFieldViolation _$ErrorFieldViolationFromJson(Map<String, dynamic> json) {
  return new ErrorFieldViolation(
      json['description'] as String, json['field'] as String);
}

abstract class _$ErrorFieldViolationSerializerMixin {
  String get description;
  String get field;
  Map<String, dynamic> toJson() =>
      <String, dynamic>{'description': description, 'field': field};
}
