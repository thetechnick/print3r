final RegExp _timeShortener = new RegExp(r'(.*\..{6})');

/// DateTime.parse can not handle the timestamp precision from the api
String shortenDate(String input) {
  if (input == null) {
    return null;
  }
  if (!_timeShortener.hasMatch(input)) {
    return input;
  }
  String short = _timeShortener.firstMatch(input).group(0);
  if (short != "") {
    return short + "Z";
  }
  return null;
}
