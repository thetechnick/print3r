library print3r.api.video_stream;

import 'package:json_annotation/json_annotation.dart';
import 'meta_model.dart';

part 'video_stream_model.g.dart';

@JsonSerializable()
class VideoStreamList extends Object with _$VideoStreamListSerializerMixin {
  List<VideoStream> items = [];

  VideoStreamList();

  factory VideoStreamList.fromJson(json) => _$VideoStreamListFromJson(json);
}


@JsonSerializable(
  includeIfNull: false,
)
class VideoStream extends Object with _$VideoStreamSerializerMixin {
  ObjectMeta metadata;
  VideoStreamSpec spec;

  String get type {
    if (spec.motion != null) {
      return 'motion';
    }
    return 'unknown';
  }

  VideoStream();

  factory VideoStream.fromJson(json) => _$VideoStreamFromJson(json);
}

@JsonSerializable(
  includeIfNull: false,
)
class VideoStreamSpec extends Object with _$VideoStreamSpecSerializerMixin {
  MotionConfig motion;

  VideoStreamSpec();

  factory VideoStreamSpec.fromJson(json) => _$VideoStreamSpecFromJson(json);
}

@JsonSerializable(
  includeIfNull: false,
)
class MotionConfig extends Object with _$MotionConfigSerializerMixin {
  String device;
  int height, width;

  MotionConfig();

  factory MotionConfig.fromJson(json) => _$MotionConfigFromJson(json);
}
