// GENERATED CODE - DO NOT MODIFY BY HAND

part of print3r.api.video_stream;

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VideoStreamList _$VideoStreamListFromJson(Map<String, dynamic> json) {
  return new VideoStreamList()
    ..items = (json['items'] as List)
        ?.map((e) => e == null ? null : new VideoStream.fromJson(e))
        ?.toList();
}

abstract class _$VideoStreamListSerializerMixin {
  List<VideoStream> get items;
  Map<String, dynamic> toJson() => <String, dynamic>{'items': items};
}

VideoStream _$VideoStreamFromJson(Map<String, dynamic> json) {
  return new VideoStream()
    ..metadata = json['metadata'] == null
        ? null
        : new ObjectMeta.fromJson(json['metadata'])
    ..spec = json['spec'] == null
        ? null
        : new VideoStreamSpec.fromJson(json['spec']);
}

abstract class _$VideoStreamSerializerMixin {
  ObjectMeta get metadata;
  VideoStreamSpec get spec;
  Map<String, dynamic> toJson() {
    var val = <String, dynamic>{};

    void writeNotNull(String key, dynamic value) {
      if (value != null) {
        val[key] = value;
      }
    }

    writeNotNull('metadata', metadata);
    writeNotNull('spec', spec);
    return val;
  }
}

VideoStreamSpec _$VideoStreamSpecFromJson(Map<String, dynamic> json) {
  return new VideoStreamSpec()
    ..motion = json['motion'] == null
        ? null
        : new MotionConfig.fromJson(json['motion']);
}

abstract class _$VideoStreamSpecSerializerMixin {
  MotionConfig get motion;
  Map<String, dynamic> toJson() {
    var val = <String, dynamic>{};

    void writeNotNull(String key, dynamic value) {
      if (value != null) {
        val[key] = value;
      }
    }

    writeNotNull('motion', motion);
    return val;
  }
}

MotionConfig _$MotionConfigFromJson(Map<String, dynamic> json) {
  return new MotionConfig()
    ..device = json['device'] as String
    ..height = json['height'] as int
    ..width = json['width'] as int;
}

abstract class _$MotionConfigSerializerMixin {
  String get device;
  int get height;
  int get width;
  Map<String, dynamic> toJson() {
    var val = <String, dynamic>{};

    void writeNotNull(String key, dynamic value) {
      if (value != null) {
        val[key] = value;
      }
    }

    writeNotNull('device', device);
    writeNotNull('height', height);
    writeNotNull('width', width);
    return val;
  }
}
