library print3r.api.volume;

import 'package:json_annotation/json_annotation.dart';
import 'meta_model.dart';

part 'volume_model.g.dart';

@JsonSerializable()
class Volume extends Object with _$VolumeSerializerMixin {
  ObjectMeta metadata;
  VolumeSpec spec;

  String get type {
    if (spec.local != null) {
      return 'local';
    }
    return 'unknown';
  }

  Volume();

  factory Volume.fromJson(json) => _$VolumeFromJson(json);
}

@JsonSerializable()
class VolumeList extends Object with _$VolumeListSerializerMixin {
  List<Volume> items = [];

  VolumeList();

  factory VolumeList.fromJson(json) => _$VolumeListFromJson(json);
}

@JsonSerializable()
class VolumeSpec extends Object with _$VolumeSpecSerializerMixin {
  LocalVolumeSource local;

  VolumeSpec();

  factory VolumeSpec.fromJson(json) => _$VolumeSpecFromJson(json);
}

@JsonSerializable()
class LocalVolumeSource extends Object with _$LocalVolumeSourceSerializerMixin {
  String path;

  LocalVolumeSource();

  factory LocalVolumeSource.fromJson(json) => _$LocalVolumeSourceFromJson(json);
}

@JsonSerializable()
class VolumeFileList extends Object with _$VolumeFileListSerializerMixin {
  List<VolumeFile> items = [];

  VolumeFileList();

  factory VolumeFileList.fromJson(json) => _$VolumeFileListFromJson(json);
}

@JsonSerializable()
class VolumeFile extends Object with _$VolumeFileSerializerMixin {
  String path;
  int size;

  VolumeFile();

  factory VolumeFile.fromJson(json) => _$VolumeFileFromJson(json);
}
