// GENERATED CODE - DO NOT MODIFY BY HAND

part of print3r.api.volume;

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Volume _$VolumeFromJson(Map<String, dynamic> json) {
  return new Volume()
    ..metadata = json['metadata'] == null
        ? null
        : new ObjectMeta.fromJson(json['metadata'])
    ..spec =
        json['spec'] == null ? null : new VolumeSpec.fromJson(json['spec']);
}

abstract class _$VolumeSerializerMixin {
  ObjectMeta get metadata;
  VolumeSpec get spec;
  Map<String, dynamic> toJson() =>
      <String, dynamic>{'metadata': metadata, 'spec': spec};
}

VolumeList _$VolumeListFromJson(Map<String, dynamic> json) {
  return new VolumeList()
    ..items = (json['items'] as List)
        ?.map((e) => e == null ? null : new Volume.fromJson(e))
        ?.toList();
}

abstract class _$VolumeListSerializerMixin {
  List<Volume> get items;
  Map<String, dynamic> toJson() => <String, dynamic>{'items': items};
}

VolumeSpec _$VolumeSpecFromJson(Map<String, dynamic> json) {
  return new VolumeSpec()
    ..local = json['local'] == null
        ? null
        : new LocalVolumeSource.fromJson(json['local']);
}

abstract class _$VolumeSpecSerializerMixin {
  LocalVolumeSource get local;
  Map<String, dynamic> toJson() => <String, dynamic>{'local': local};
}

LocalVolumeSource _$LocalVolumeSourceFromJson(Map<String, dynamic> json) {
  return new LocalVolumeSource()..path = json['path'] as String;
}

abstract class _$LocalVolumeSourceSerializerMixin {
  String get path;
  Map<String, dynamic> toJson() => <String, dynamic>{'path': path};
}

VolumeFileList _$VolumeFileListFromJson(Map<String, dynamic> json) {
  return new VolumeFileList()
    ..items = (json['items'] as List)
        ?.map((e) => e == null ? null : new VolumeFile.fromJson(e))
        ?.toList();
}

abstract class _$VolumeFileListSerializerMixin {
  List<VolumeFile> get items;
  Map<String, dynamic> toJson() => <String, dynamic>{'items': items};
}

VolumeFile _$VolumeFileFromJson(Map<String, dynamic> json) {
  return new VolumeFile()
    ..path = json['path'] as String
    ..size = json['size'] as int;
}

abstract class _$VolumeFileSerializerMixin {
  String get path;
  int get size;
  Map<String, dynamic> toJson() =>
      <String, dynamic>{'path': path, 'size': size};
}
