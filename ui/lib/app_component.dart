import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:ui/api/api.dart';

import 'src/main_header/main_header_component.dart';
import 'src/printer_overview/printer_overview_component.dart';
import 'src/print_overview/print_overview_component.dart';
import 'src/volume_overview/volume_overview_component.dart';
import 'src/video_stream_overview/video_stream_overview_component.dart';
import 'src/dashboard/dashboard_component.dart';
import 'src/routes.dart';

// AngularDart info: https://webdev.dartlang.org/angular
// Components info: https://webdev.dartlang.org/components

@Component(
  selector: 'my-app',
  styleUrls: ['app_component.css'],
  templateUrl: 'app_component.html',
  directives: [
    MainHeaderComponent,
    PrinterOverviewComponent,
    PrintOverviewComponent,
    DashboardComponent,
    VolumeOverviewComponent,
    VideoStreamOverviewComponent,
    routerDirectives,
  ],
  providers: [
    ClassProvider(Routes),
    ClassProvider(PrinterClient),
    ClassProvider(PrintClient),
    ClassProvider(VideoStreamClient),
    ClassProvider(VolumeClient),
    ClassProvider(ResourceEventClient),
  ],
)
class AppComponent {
  final Routes routes;

  AppComponent(this.routes);
}
