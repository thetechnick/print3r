@JS('Chartist')
library chartist;

import 'package:js/js.dart';
import 'helper.dart';

DateTime jsDate(DateTime date) {
  return toJSDate(date.millisecondsSinceEpoch);
}

external get FixedScaleAxis;

@JS('Line')
class LineChart {
  LineChart.fakeConstructor$();
  external factory LineChart(dynamic selector, ChartData data, LineChartOptions opts);
  external Function get update;
  external set update(Function v);
}

@anonymous
@JS()
abstract class ChartData {
  external List get labels;
  external set labels(List v);
  external List<Series> get series;
  external set series(List<Series> v);
  external factory ChartData({List labels, List<Series> series});
}

@anonymous
@JS()
class Series {
  external String get name;
  external set name(String v);
  external List<SeriesPoint> get data;
  external set data(List<SeriesPoint> v);
  external factory Series({
    String name,
    List<SeriesPoint> data,
  });
}

@anonymous
@JS()
abstract class ILineChartAxis {
  external num get offset;
  external set offset(num v);
  external String get position;
  external set position(String v);
  external dynamic
      /*{
            x?: number;
            y?: number;
        }*/
      get labelOffset;
  external set labelOffset(
      dynamic /*{
            x?: number;
            y?: number;
        }*/
      v);
  external bool get showLabel;
  external set showLabel(bool v);
  external bool get showGrid;
  external set showGrid(bool v);
  external Function get labelInterpolationFnc;
  external set labelInterpolationFnc(Function v);
  external factory ILineChartAxis(
      {num offset,
      String position,
      dynamic /*{
            x?: number;
            y?: number;
        }*/
      labelOffset,
      bool showLabel,
      bool showGrid,
      Function labelInterpolationFnc});
}

@anonymous
@JS()
class ChartistStepAxis extends ILineChartAxis {
  external dynamic get type;
  external set type(dynamic v);
  external List /*List<String>|List<num>*/ get ticks;
  external set ticks(List /*List<String>|List<num>*/ v);
  external bool get stretch;
  external set stretch(bool v);
  external int get divisor;
  external set divisor(int v);
  external factory ChartistStepAxis(
      {dynamic type,
      List /*List<String>|List<num>*/ ticks,
      bool stretch,
      num offset,
      int divisor,
      String position,
      dynamic /*{
            x?: number;
            y?: number;
        }*/
      labelOffset,
      bool showLabel,
      bool showGrid,
      Function labelInterpolationFnc});
}

@anonymous
@JS()
abstract class SeriesPoint {
  external DateTime get x;
  external set x(DateTime v);
  external num get y;
  external set y(num v);
  external factory SeriesPoint({
    DateTime x,
    num y,
  });
}

@anonymous
@JS()
abstract class IChartOptions {
  /// If true the whole data is reversed including labels, the series order as well as the whole series data arrays.
  external bool get reverseData;
  external set reverseData(bool v);
  external List<dynamic> get plugins;
  external set plugins(List<dynamic> v);
  external factory IChartOptions({bool reverseData, List<dynamic> plugins});
}

@anonymous
@JS()
class LineChartOptions extends IChartOptions {
  external dynamic /*ChartistStepAxis|IChartistFixedScaleAxis|IChartistAutoScaleAxis*/ get axisX;
  external set axisX(
      dynamic /*ChartistStepAxis|IChartistFixedScaleAxis|IChartistAutoScaleAxis*/ v);
  external dynamic /*ChartistStepAxis|IChartistFixedScaleAxis|IChartistAutoScaleAxis*/ get axisY;
  external set axisY(
      dynamic /*ChartistStepAxis|IChartistFixedScaleAxis|IChartistAutoScaleAxis*/ v);
  external dynamic /*num|String*/ get width;
  external set width(dynamic /*num|String*/ v);
  external dynamic /*num|String*/ get height;
  external set height(dynamic /*num|String*/ v);
  external bool get showLine;
  external set showLine(bool v);
  external bool get showPoint;
  external set showPoint(bool v);
  external bool get showArea;
  external set showArea(bool v);
  external num get areaBase;
  external set areaBase(num v);
  external dynamic /*Function|bool*/ get lineSmooth;
  external set lineSmooth(dynamic /*Function|bool*/ v);
  external num get low;
  external set low(num v);
  external num get high;
  external set high(num v);
  external List<dynamic /*String|num*/ > get ticks;
  external set ticks(List<dynamic /*String|num*/ > v);
  external ChartPadding get chartPadding;
  external set chartPadding(ChartPadding v);
  external bool get fullWidth;
  external set fullWidth(bool v);
  // external ILineChartClasses get classNames;
  // external set classNames(ILineChartClasses v);
  external dynamic
      /*JSMap of <String,{
                lineSmooth?: Function | boolean;
                showLine?: boolean;
                showPoint?: boolean;
                showArea?: boolean;
                areaBase?: number;
            }>*/
      get series;
  external set series(
      dynamic /*JSMap of <String,{
                lineSmooth?: Function | boolean;
                showLine?: boolean;
                showPoint?: boolean;
                showArea?: boolean;
                areaBase?: number;
            }>*/
      v);
  external factory LineChartOptions(
      {dynamic /*ChartistStepAxis|IChartistFixedScaleAxis|IChartistAutoScaleAxis*/ axisX,
      dynamic /*ChartistStepAxis|IChartistFixedScaleAxis|IChartistAutoScaleAxis*/ axisY,
      dynamic /*num|String*/ width,
      dynamic /*num|String*/ height,
      bool showLine,
      bool showPoint,
      bool showArea,
      num areaBase,
      dynamic /*Function|bool*/ lineSmooth,
      num low,
      num high,
      List<dynamic /*String|num*/ > ticks,
      ChartPadding chartPadding,
      bool fullWidth,
      // ILineChartClasses classNames,
      dynamic /*JSMap of <String,{
                lineSmooth?: Function | boolean;
                showLine?: boolean;
                showPoint?: boolean;
                showArea?: boolean;
                areaBase?: number;
            }>*/
      series,
      bool reverseData,
      List<dynamic> plugins});
}

@anonymous
@JS()
class ChartPadding {
  external num get top;
  external set top(num v);
  external num get right;
  external set right(num v);
  external num get bottom;
  external set bottom(num v);
  external num get left;
  external set left(num v);
  external factory ChartPadding({num top, num right, num bottom, num left});
}
