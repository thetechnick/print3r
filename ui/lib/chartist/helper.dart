@JS()
library chartist.helpers;

import 'package:js/js.dart';

@JS()
external DateTime toJSDate(int millies);
