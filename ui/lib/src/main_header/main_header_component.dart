import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';

import '../routes.dart';

@Component(
  selector: 'main-header',
  // styleUrls: const ['main_header_component.css'],
  templateUrl: 'main_header_component.html',
  // pipes: const[],
  directives: const [
    coreDirectives,
    routerDirectives,
  ],
)
class MainHeaderComponent {
  Routes routes;
  bool open = false;

  MainHeaderComponent(this.routes);

  toggle() {
    open = !open;
  }
}
