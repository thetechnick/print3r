import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:ui/api/api.dart';

@Component(
  selector: 'print-create',
  templateUrl: 'print_create_component.html',
  styleUrls: const ['print_create_component.css'],
  pipes: const[],
  directives: const [
    coreDirectives,
    formDirectives,
  ],
)
class PrintCreateComponent {
  final PrintClient _client;

  bool isModalOpen = false;
  String name, volume, file, printer, error;

  PrintCreateComponent(this._client) {
    _reset();
  }

  cancel() {
    isModalOpen = false;
  }

  open() {
    _reset();
    isModalOpen = true;
  }

  submit() async {
    var newPrint = new Print()..metadata = new ObjectMeta()..spec = new PrintSpec();
    newPrint.metadata.name = name;
    newPrint.spec..printer = printer..state = 'Printed'..fileRef = new FileRef();
    newPrint.spec.fileRef.volume = volume;
    newPrint.spec.fileRef.file = file;

    try {
      await _client.create(newPrint);
    } on Status catch (status) {
      error = status.toString();
      return;
    }

    isModalOpen = false;
  }

  _reset() {
    name = null;
    volume = null;
    file = null;
    printer = null;
    error = '';
  }
}
