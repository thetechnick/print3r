import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:ui/api/api.dart';

import './print_create/print_create_component.dart';

@Component(
  selector: 'print-overview',
  styleUrls: ['print_overview_component.css'],
  templateUrl: 'print_overview_component.html',
  pipes: const [
    DecimalPipe,
  ],
  directives: const [
    coreDirectives,
    routerDirectives,
    PrintCreateComponent,
  ],
)
class PrintOverviewComponent implements OnInit, OnDestroy {
  final PrintClient _client;
  final ResourceEventClient _events;
  final Map<String, Print> _prints;
  final Map<String, bool> _dropdowns;
  StreamSubscription<ResourceEvent> _sub;

  final List<Print> prints;

  PrintOverviewComponent(this._client, this._events) : _prints = {}, prints = [], _dropdowns = {};

  ngOnInit() async {
    _sub = _events.stream().listen((ResourceEvent event) {
      switch (event.operation) {
        case 'Created':
          if (event.newObject.data is Print) {
            _update(event.newObject.data);
          }
          break;
        case 'Updated':
          if (event.newObject.data is Print) {
            _update(event.newObject.data);
          }
          break;
        case 'Deleted':
          if (event.oldObject.data is Print) {
            _delete(event.oldObject.data);
          }
          break;
      }
      return;
    });

    var printList = await _client.all();
    if (printList.items == null || printList.items.length == 0) {
      return;
    }
    for (var print in printList.items) {
      _update(print);
    }
  }

  ngOnDestroy() async {
    await _sub.cancel();
  }

  int trackByFn(int index, item) {
    return index;
  }

  openDropdown(Print print) {
    _dropdowns[print.metadata.name] = true;
  }

  closeDropdown(Print print) {
    _dropdowns.remove(print.metadata.name);
  }

  bool isDropdownOpen(Print print) {
    if (_dropdowns.containsKey(print.metadata.name)) {
      return true;
    }
    return false;
  }

  cancel(Print print) async {
    closeDropdown(print);
    print.spec.state = 'Canceled';
    await _client.update(print);
  }

  delete(Print print) async {
    closeDropdown(print);
    await _client.delete(print);
  }

  _update(Print print) {
    _prints[print.metadata.name] = print;
    prints.clear();
    prints.addAll(_prints.values);
  }

  _delete(Print print) {
    _prints.remove(print.metadata.name);
    prints.clear();
    prints.addAll(_prints.values);
  }
}
