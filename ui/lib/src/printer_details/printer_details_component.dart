import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:ui/api/api.dart';

import '../route_paths.dart' as paths;
import 'printer_video/printer_video_component.dart';

@Component(
  selector: 'printer-details',
  styleUrls: ['printer_details_component.css'],
  templateUrl: 'printer_details_component.html',
  pipes: const [],
  directives: const [
    coreDirectives,
    routerDirectives,
    PrinterVideoComponent,
  ],
)
class PrinterDetailsComponent implements OnActivate {
  final PrinterClient _client;

  Printer printer;

  PrinterDetailsComponent(this._client);

  @override
  Future<void> onActivate(_, RouterState current) async {
    final name = current.parameters[paths.nameParam];
    if (name != null) printer = await _client.show(name);
    // TODO: EVENTS
  }
}
