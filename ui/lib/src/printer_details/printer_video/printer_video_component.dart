import 'package:angular/angular.dart';
import 'package:ui/api/api.dart';

@Component(
  selector: 'printer-video',
  styleUrls: ['printer_video_component.css'],
  templateUrl: 'printer_video_component.html',
  pipes: const [],
  directives: const [
    coreDirectives,
  ],
)
class PrinterVideoComponent {
  final VideoStreamClient _client;

  @Input()
  Printer printer;

  PrinterVideoComponent(this._client);

  bool get videoEnabled {
    if (printer == null) {
      return false;
    }
    return printer.spec.videoStream != null && printer.spec.videoStream != '';
  }

  String get videoUrl {
    if (printer == null) {
      return '';
    }
    return _client.streamUrl(printer.spec.videoStream);
  }
}
