import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:ui/api/api.dart';

@Component(
  selector: 'printer-edit',
  templateUrl: 'printer_edit_component.html',
  styleUrls: const ['printer_edit_component.css'],
  pipes: const[],
  directives: const [
    coreDirectives,
    formDirectives,
  ],
)
class PrinterEditComponent implements OnChanges {
  @Input()
  bool open;

  @Input()
  String error;

  bool isModalOpen = false;

  @Input()
  Printer printer;

  @Output('canceled')
  Stream<Null> get onCanceled => _onCanceled.stream;
  final StreamController<Null> _onCanceled =
      new StreamController<Null>.broadcast(sync: true);

  @Output('submitted')
  Stream<Printer> get onSubmitted => _onSubmitted.stream;
  final StreamController<Printer> _onSubmitted =
      new StreamController<Printer>.broadcast(sync: true);

  bool get isCreate => printer == null;
  String get title => isCreate ? 'Create a new printer' : 'Edit printer';

  String model, device, name, videoStream, baudRate;

  PrinterEditComponent() {
    error = '';
  }

  ngOnChanges(Map<String, SimpleChange> changes) {
    if (changes.containsKey('open') && !isModalOpen && open) {
      // reset modal before opening
      _reset();
    }
    isModalOpen = open;
  }

  submit() async {
    Printer newPrinter;
    newPrinter = new Printer()..metadata = new ObjectMeta()..spec = new PrinterSpec();
    if (printer == null) {
      newPrinter.metadata.name = name;
    } else {
      newPrinter.metadata = printer.metadata;
    }

    newPrinter.spec
    ..model = model
    ..device = device
    ..baudRate = baudRate
    ..videoStream = videoStream;

    _onSubmitted.add(newPrinter);
  }

  cancel() {
    _onCanceled.add(null);
  }

  _reset() {
    name = null;
    baudRate = null;
    model = null;
    device = null;
    videoStream = null;

    if (printer != null) {
      name = printer.metadata.name;
      baudRate = printer.spec.baudRate;
      model = printer.spec.model;
      device = printer.spec.device;
      videoStream = printer.spec.videoStream;
    }
  }
}
