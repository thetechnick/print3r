import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:ui/api/api.dart';

import 'printer_overview_item/printer_overview_item_component.dart';
import 'printer_overview_create/printer_overview_create_component.dart';

@Component(
  selector: 'printer-overview',
  templateUrl: 'printer_overview_component.html',
  directives: const [
    PrinterOverviewItemComponent,
    PrinterOverviewCreateComponent,
    coreDirectives,
    routerDirectives,
  ],
)
class PrinterOverviewComponent implements OnInit, OnDestroy {
  final PrinterClient _client;
  final ResourceEventClient _events;
  final Map<String, Printer> _printers;
  StreamSubscription<ResourceEvent> _sub;

  final List<Printer> printers;

  PrinterOverviewComponent(this._client, this._events) : _printers = {}, printers = [];

  ngOnInit() async {
    _sub = _events.stream().listen((ResourceEvent event) {
      switch (event.operation) {
        case 'Created':
          if (event.newObject.data is Printer) {
            _update(event.newObject.data);
          }
          break;
        case 'Updated':
          if (event.newObject.data is Printer) {
            _update(event.newObject.data);
          }
          break;
        case 'Deleted':
          if (event.oldObject.data is Printer) {
            _delete(event.oldObject.data);
          }
          break;
      }
      return;
    });

    var printerList = await _client.all();
    if (printerList.items == null || printerList.items.length == 0) {
      return;
    }
    for (var printer in printerList.items) {
      _update(printer);
    }
  }

  ngOnDestroy() async {
    await _sub.cancel();
  }

  int trackByFn(int index, item) {
    return index;
  }

  _update(Printer printer) {
    _printers[printer.metadata.name] = printer;
    printers.clear();
    printers.addAll(_printers.values);
  }

  _delete(Printer printer) {
    _printers.remove(printer.metadata.name);
    printers.clear();
    printers.addAll(_printers.values);
  }
}
