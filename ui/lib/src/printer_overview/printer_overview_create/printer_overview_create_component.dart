import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:ui/api/api.dart';

import '../printer_edit/printer_edit_component.dart';

@Component(
  selector: 'printer-overview-create',
  styleUrls: const ['printer_overview_create_component.css'],
  templateUrl: 'printer_overview_create_component.html',
  pipes: const[],
  directives: const [
    coreDirectives,
    formDirectives,
    PrinterEditComponent,
  ],
)
class PrinterOverviewCreateComponent {
  bool isModalOpen;
  String error;
  PrinterClient _client;

  PrinterOverviewCreateComponent(this._client) {
    isModalOpen = false;
    error = '';
  }

  open() {
    isModalOpen = true;
  }

  Future<Null> submit(Printer newPrinter) async {
    try {
      await _client.create(newPrinter);
    } on Status catch (status) {
      error = status.toString();
      return;
    }

    isModalOpen = false;
  }

  cancel() {
    isModalOpen = false;
  }
}
