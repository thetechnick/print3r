import 'dart:async';
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:ui/chartist/chartist.dart';
import 'package:ui/api/api.dart';

import '../printer_edit/printer_edit_component.dart';
import '../../route_paths.dart' as paths;

@Component(
  selector: 'printer-overview-item',
  styleUrls: ['printer_overview_item_component.css'],
  templateUrl: 'printer_overview_item_component.html',
  pipes: const [
    DecimalPipe,
  ],
  directives: const [
    coreDirectives,
    routerDirectives,
    PrinterEditComponent,
  ],
)
class PrinterOverviewItemComponent implements OnInit, OnDestroy, OnChanges {
  final PrinterClient _client;
  final PrintClient _printClient;

  Series _extruderActual, _bedActual;
  LineChart _chart;
  Timer _timer;

  @Input()
  Printer printer;

  @ViewChild('chart')
  HtmlElement chartElement;

  bool isEditModalOpen = false;
  String error;
  double printProgress = null;

  static const Duration updateInterval = const Duration(seconds: 10);
  static const int size = 30;

  PrinterOverviewItemComponent(this._client, this._printClient);

  ngOnInit() async {
    _start();
  }

  ngOnDestroy() async {
    _stop();
  }

  ngOnChanges(Map<String, SimpleChange> changes) {
    if (changes.containsKey('printer')) {
      _checkPrinting();
      if (printer == null || !printer.connectedCondition.isTrue) {
        _stop();
      } else {
        _start();
      }
    }
  }

  String printerUrl(String name) =>
      paths.printerDetails.toUrl(parameters: {paths.nameParam: name});

  _start() async {
    // setup
    if (_chart == null) {
      _extruderActual = new Series(name: 'extruder actual', data: []);
      _bedActual = new Series(name: 'bed actual', data: []);

      var data = new ChartData(series: [
        _extruderActual,
        _bedActual,
      ]);
      var opts = new LineChartOptions(
          showPoint: false,
          showArea: true,
          fullWidth: true,
          low: 0,
          chartPadding: new ChartPadding(top: 2, bottom: 0, right: 0, left: 0),
          axisX: new ChartistStepAxis(
            type: FixedScaleAxis,
            divisor: 5,
            offset: 0,
          ),
          axisY: new ChartistStepAxis(
            offset: 0,
          ));
      _chart = new LineChart(chartElement, data, opts);
    }

    if (printer == null || !printer.connectedCondition.isTrue) {
      return;
    }

    if (_timer == null) {
      _update();
      _timer = new Timer.periodic(updateInterval, (_) async {
        _update();
      });
    }
  }

  _update() async {
    PrinterTemperatureHistory history;
    var now = new DateTime.now();
    try {
      history = await _client.temperatureHistory(
          printer, now.subtract(new Duration(minutes: 5)), now, size);
    } on Status catch (status) {
      if (status.code == 9 || status.code == 5) {
        // printer not connected or not found
        _stop();
        return;
      }
      rethrow;
    }

    _extruderActual.data = _mapDataPoints(history.extruderActual);
    _bedActual.data = _mapDataPoints(history.bedActual);
    _chart.update();
  }

  _stop() {
    if (_timer != null) {
      _timer.cancel();
      _timer = null;
    }
    if (_chart != null) {
      _extruderActual.data = [];
      _bedActual.data = [];
      _chart.update();
    }
  }

  _checkPrinting() async {
    if (printer.printingCondition.isTrue && printer.status.print != null) {
      var print = await _printClient.show(printer.status.print.name);
      if (print.status.progress != null) {
        printProgress = print.status.progress;
      } else {
        printProgress = 0.0;
      }
    } else {
      printProgress = null;
    }
  }

  List<SeriesPoint> _mapDataPoints(List<PrinterTemperatureHistoryPoint> p) {
    if (p == null || p.length == 0) {
      return [];
    }
    return p
        .map((point) => new SeriesPoint(x: jsDate(point.time), y: point.value))
        .toList();
  }

  bool get isClean {
    return printer.bedCleanCondition.isTrue;
  }

  clean(Event event) async {
    event.stopPropagation();
    for (var c in printer.status.conditions) {
      if (c.type == "BedClean") {
        c.status = "True";
      }
    }
    await _client.updateStatus(printer);
  }

  delete(Event event) async {
    event.stopPropagation();
    await _client.delete(printer);
  }

  switchConnection(Event event) async {
    event.stopPropagation();
    if (printer.spec.state == "Connected") {
      await _client.disconnect(printer);
      return;
    }
    await _client.connect(printer);
  }

  edit(Event event) async {
    event.stopPropagation();
    isEditModalOpen = true;
  }

  cancel() {
    isEditModalOpen = false;
  }

  submit(Printer printer) async {
    try {
      await _client.update(printer);
    } on Status catch (status) {
      error = status.toString();
      return;
    }
    isEditModalOpen = false;
  }
}
