import 'package:angular_router/angular_router.dart';

final printerOverview = RoutePath(path: 'printers');
final volumeOverview = RoutePath(path: 'volumes');
final videoStreamOverview = RoutePath(path: 'video-streams');
final printOverview = RoutePath(path: 'prints');
final dashboard = RoutePath(path: 'dashboard');

const nameParam = 'name';
final printerDetails = RoutePath(path: '${printerOverview.path}/:$nameParam');
