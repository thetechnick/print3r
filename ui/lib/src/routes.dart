import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';

import 'route_paths.dart' as paths;
import 'printer_overview/printer_overview_component.template.dart' as poct;
import 'printer_details/printer_details_component.template.dart' as pdct;
import 'print_overview/print_overview_component.template.dart' as printoct;
import 'volume_overview/volume_overview_component.template.dart' as voct;
import 'video_stream_overview/video_stream_overview_component.template.dart' as vsoct;
import 'dashboard/dashboard_component.template.dart' as dct;

@Injectable()
class Routes {
  RoutePath get printerOverview => paths.printerOverview;
  RoutePath get printOverview => paths.printOverview;
  RoutePath get dashboard => paths.dashboard;
  RoutePath get volumeOverview => paths.volumeOverview;
  RoutePath get videoStreamOverview => paths.videoStreamOverview;

  final List<RouteDefinition> all = [
    RouteDefinition.redirect(path: '', redirectTo: paths.dashboard.toUrl()),
    RouteDefinition(path: paths.dashboard.path, component: dct.DashboardComponentNgFactory),
    RouteDefinition(path: paths.printerOverview.path, component: poct.PrinterOverviewComponentNgFactory),
    RouteDefinition(path: paths.printerDetails.path, component: pdct.PrinterDetailsComponentNgFactory),
    RouteDefinition(path: paths.printOverview.path, component: printoct.PrintOverviewComponentNgFactory),
    RouteDefinition(path: paths.volumeOverview.path, component: voct.VolumeOverviewComponentNgFactory),
    RouteDefinition(path: paths.videoStreamOverview.path, component: vsoct.VideoStreamOverviewComponentNgFactory),
  ];
}
