import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:ui/api/api.dart';

@Component(
  selector: 'video-stream-overview',
  templateUrl: 'video_stream_overview_component.html',
  directives: const [
    coreDirectives,
    routerDirectives,
  ],
)
class VideoStreamOverviewComponent implements OnInit, OnDestroy {
  final VideoStreamClient _client;
  final ResourceEventClient _events;
  final Map<String, VideoStream> _videoStreams;
  StreamSubscription<ResourceEvent> _sub;

  final List<VideoStream> videoStreams;

  VideoStreamOverviewComponent(this._client, this._events)  : _videoStreams = {}, videoStreams = [];

  ngOnInit() async {
    _sub = _events.stream().listen((ResourceEvent event) {
      switch (event.operation) {
        case 'Created':
          if (event.newObject.data is VideoStream) {
            _update(event.newObject.data);
          }
          break;
        case 'Updated':
          if (event.newObject.data is VideoStream) {
            _update(event.newObject.data);
          }
          break;
        case 'Deleted':
          if (event.oldObject.data is VideoStream) {
            _delete(event.oldObject.data);
          }
          break;
      }
      return;
    });

    var videoStreamList = await _client.all();
    if (videoStreamList.items == null || videoStreamList.items.length == 0) {
      return;
    }
    for (var videoStream in videoStreamList.items) {
      _update(videoStream);
    }
  }

  ngOnDestroy() async {
    await _sub.cancel();
  }

  int trackByFn(int index, item) {
    return index;
  }

  _update(VideoStream videoStream) {
    _videoStreams[videoStream.metadata.name] = videoStream;
    videoStreams.clear();
    videoStreams.addAll(_videoStreams.values);
  }

  _delete(VideoStream videoStream) {
    _videoStreams.remove(videoStream.metadata.name);
    videoStreams.clear();
    videoStreams.addAll(_videoStreams.values);
  }
}
