import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:ui/api/api.dart';

@Component(
  selector: 'volume-overview',
  templateUrl: 'volume_overview_component.html',
  directives: const [
    coreDirectives,
    routerDirectives,
  ],
)
class VolumeOverviewComponent implements OnInit, OnDestroy {
  final VolumeClient _client;
  final ResourceEventClient _events;
  final Map<String, Volume> _volumes;
  StreamSubscription<ResourceEvent> _sub;

  final List<Volume> volumes;

  VolumeOverviewComponent(this._client, this._events) : _volumes = {}, volumes = [];

  ngOnInit() async {
    _sub = _events.stream().listen((ResourceEvent event) {
      switch (event.operation) {
        case 'Created':
          if (event.newObject.data is Volume) {
            _update(event.newObject.data);
          }
          break;
        case 'Updated':
          if (event.newObject.data is Volume) {
            _update(event.newObject.data);
          }
          break;
        case 'Deleted':
          if (event.oldObject.data is Volume) {
            _delete(event.oldObject.data);
          }
          break;
      }
      return;
    });

    var volumeList = await _client.all();
    if (volumeList.items == null || volumeList.items.length == 0) {
      return;
    }
    for (var volume in volumeList.items) {
      _update(volume);
    }
  }

  ngOnDestroy() async {
    await _sub.cancel();
  }

  int trackByFn(int index, item) {
    return index;
  }

  _update(Volume volume) {
    _volumes[volume.metadata.name] = volume;
    volumes.clear();
    volumes.addAll(_volumes.values);
  }

  _delete(Volume volume) {
    _volumes.remove(volume.metadata.name);
    volumes.clear();
    volumes.addAll(_volumes.values);
  }
}
